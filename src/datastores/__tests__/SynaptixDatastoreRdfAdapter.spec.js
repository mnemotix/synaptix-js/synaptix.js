/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import SynaptixDatastoreRdfAdapter from "../SynaptixDatastoreRdfAdapter";

let modelDefinitionsRegister = new ModelDefinitionsRegister([]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");

jest.mock('../SynaptixDatastoreRdfSession', () => jest.fn(() => ({})));
import SynaptixDatastoreRdfSessionMock from '../SynaptixDatastoreRdfSession';


const getAdapter = () => {
  return new SynaptixDatastoreRdfAdapter({
    modelDefinitionsRegister,
    networkLayer
  });
};

describe('SynaptixDatastoreRdfAdapter', () => {
  describe('.getSession()', () => {
    let processEnvSave = {};
    let manipulatedEnvVars = ['SCHEMA_NAMESPACE_MAPPING', 'NODES_NAMESPACE_URI', 'NODES_PREFIX'];

    beforeEach(() => {
      /* save initial state env vars */
      manipulatedEnvVars.forEach(varName => processEnvSave[varName] = process.env[varName]);
      /* setup default env vars for test suite */
      process.env.SCHEMA_NAMESPACE_MAPPING = '{}';
      process.env.NODES_NAMESPACE_URI = 'http://ns.mnemotix.com/';
      process.env.NODES_PREFIX = 'mnx';
    });

    afterEach(() => {
      /* restore env vars */
      manipulatedEnvVars.forEach(varName => process.env[varName] = processEnvSave[varName]);
    });

    describe('env var handling', () => {
      it('should throw an error when getting session if env variables missing or malformed', async () => {
        delete process.env.SCHEMA_NAMESPACE_MAPPING;
        delete process.env.NODES_NAMESPACE_URI;
        delete process.env.NODES_PREFIX;
        expect(() => getAdapter()).toThrow("env-var: \"SCHEMA_NAMESPACE_MAPPING\" is a required variable, but it was not set");
        process.env.SCHEMA_NAMESPACE_MAPPING = "bad string";

        expect(() => getAdapter()).toThrow("env-var: \"SCHEMA_NAMESPACE_MAPPING\" should be valid (parseable) JSON");
        process.env.SCHEMA_NAMESPACE_MAPPING = JSON.stringify({});

        expect(() => getAdapter()).toThrow("env-var: \"NODES_NAMESPACE_URI\" is a required variable, but it was not set");

        process.env.NODES_NAMESPACE_URI = "http://ns.mnemotix.com/";
        expect(() => getAdapter()).toThrow("env-var: \"NODES_PREFIX\" is a required variable, but it was not set");
      });

      it('parses env.SCHEMA_NAMESPACE_MAPPING as JSON and uses it for the session constructor', () => {
        process.env.SCHEMA_NAMESPACE_MAPPING = '{"myProperty": "myValue"}';

        let adapater = getAdapter();
        adapater.getSession();

        /* Check if SynaptixDatastoreRdfSession constructor has been invoked with the correct option value for schemaNamespaceMapping */
        expect(SynaptixDatastoreRdfSessionMock.mock.calls[0][0].schemaNamespaceMapping).toEqual({myProperty: 'myValue'});
      });
    });
  });
});




