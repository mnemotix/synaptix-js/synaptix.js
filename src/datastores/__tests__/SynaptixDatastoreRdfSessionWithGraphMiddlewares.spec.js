/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import SynaptixDatastoreRdfSession from '../SynaptixDatastoreRdfSession';
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";
import {PubSub} from "graphql-subscriptions";
import generateId from "nanoid/generate";
import {MnxActionGraphMiddleware} from "../../datamodules/middlewares/graph/MnxActionGraphMiddleware";
import SSOUser from "../../datamodules/drivers/sso/models/SSOUser";
import {
  BarDefinitionMock,
  BazDefinitionMock,
  FooDefinitionMock,
  NotInstantiableDefinitionMock
} from "../../datamodel/__tests__/mocks/definitions";

jest.mock("nanoid/generate");
jest.mock("dayjs", () => jest.fn((...args) => ({
  format: () => "now"
})));

jest.setTimeout(10000)

let userAccountUri = "test:userAccount/1234";
let modelDefinitionsRegister = new ModelDefinitionsRegister([NotInstantiableDefinitionMock, BarDefinitionMock, FooDefinitionMock, BazDefinitionMock]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();
let context = new GraphQLContext({
  user: new SSOUser({
    user: {
      id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
      username: 'test@domain.com',
      barLiteral1: 'John',
      barLiteral2: 'Doe'
    }
  }), lang: 'fr'
});

let session = new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine,
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  graphMiddlewares: [new MnxActionGraphMiddleware()],
  indexDisabled: true
});

let spyOnIndexServicefulltextSearch = jest.spyOn(session.getIndexService(), 'getNodes');
let spyOnGraphControllerInsertTriples = jest.spyOn(session.getGraphControllerService().getGraphControllerPublisher(), 'insertTriples');
let spyOnGraphControllerUpdateTriples = jest.spyOn(session.getGraphControllerService().getGraphControllerPublisher(), 'updateTriples');
spyOnIndexServicefulltextSearch.mockImplementation(async () => {
  return {};
});
jest.spyOn(session, 'isIndexDisabled').mockImplementation(() => true);
jest.spyOn(session, 'getLoggedUserAccount').mockImplementation(() => ({id: userAccountUri}));

describe('SynaptixDatastoreRdfSessionWithGraphMiddlewares', () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it("should create a graph with actions attributed to a userAccount", async () => {
    spyOnGraphControllerInsertTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.createObject({
      graphQLType: "Bar",
      objectInput: {
        barLiteral1: "Derek",
        barLiteral2: "Devel",
        bazInputs: [{
          bazLabel1: "Pro"
        }, {
          bazLabel1: "Mobile"
        }, {
          id: "test:baz/1233456"
        }]
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerInsertTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/baz/1233456> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/5>.
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 \"Derek\";
  mnx:barLiteral2 \"Devel\";
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/2>, <http://ns.mnemotix.com/instances/baz/3>, <http://ns.mnemotix.com/instances/baz/1233456>;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/4>.
 <http://ns.mnemotix.com/instances/baz/2> rdf:type mnx:Baz;
  mnx:bazLabel1 \"Pro\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/4>.
 <http://ns.mnemotix.com/instances/baz/3> rdf:type mnx:Baz;
  mnx:bazLabel1 \"Mobile\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/4>.
 <http://ns.mnemotix.com/instances/creation/4> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/bar/1\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Bar\\\",\\\"http://ns.mnemotix.com/onto/barLiteral1\\\":[{\\\"@value\\\":\\\"Derek\\\"}],\\\"http://ns.mnemotix.com/onto/barLiteral2\\\":[{\\\"@value\\\":\\\"Devel\\\"}],\\\"http://ns.mnemotix.com/onto/hasBaz\\\":[{\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Pro\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"45d53cff8da35473902c02184311686e72865ff1\\\"},{\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Mobile\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"a9195bd94ce672e725568c72b9737ce1d3ac8a30\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/1233456\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"@ref\\\":\\\"3d565ba6af71ec48c23d74d94756e9a8f51deeec\\\"}],\\\"@ref\\\":\\\"12010bb007e091f45db18bd72473bf39c513b48f\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/2\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Pro\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"45fa22221b25e1f0cb5add828f7765ca625a9704\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/3\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Mobile\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"efed1815592c5c22604550e140d214adb95177af\\\"}]\".
 <http://ns.mnemotix.com/instances/update/5> rdf:type mnx:Update;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/1233456\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"@ref\\\":\\\"3d565ba6af71ec48c23d74d94756e9a8f51deeec\\\"}]\".
}`
    });
  });

  it("should update an object with actions attributed to a userAccount ", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      updatingProps: {
        barLiteral1: "Derek",
        barLiteral2: "Devel",
        bazInputs: [{
          bazLabel1: "Pro",
        }, {
          bazLabel1: "Mobile",
        }, {
          id: "test:baz/1233456"
        }]
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
DELETE {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2.
}
INSERT {
 <http://ns.mnemotix.com/instances/baz/1233456> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/3>.
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 \"Derek\";
  mnx:barLiteral2 \"Devel\";
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>, <http://ns.mnemotix.com/instances/baz/2>, <http://ns.mnemotix.com/instances/baz/1233456>;
  mnx:hasUpdate <http://ns.mnemotix.com/instances/update/3>.
 <http://ns.mnemotix.com/instances/baz/1> rdf:type mnx:Baz;
  mnx:bazLabel1 \"Pro\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/4>.
 <http://ns.mnemotix.com/instances/baz/2> rdf:type mnx:Baz;
  mnx:bazLabel1 \"Mobile\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/4>.
 <http://ns.mnemotix.com/instances/creation/4> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/1\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Pro\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"e2e6e640c698cc2cbcdca668cde2e950daacc5eb\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/2\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Mobile\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"1c617cd13245b8ce63ec3d980632fc34db5a02c4\\\"}]\".
 <http://ns.mnemotix.com/instances/update/3> rdf:type mnx:Update;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/bar/1\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Bar\\\",\\\"http://ns.mnemotix.com/onto/barLiteral1\\\":[{\\\"@value\\\":\\\"Derek\\\"}],\\\"http://ns.mnemotix.com/onto/barLiteral2\\\":[{\\\"@value\\\":\\\"Devel\\\"}],\\\"http://ns.mnemotix.com/onto/hasBaz\\\":[{\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Pro\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"45d53cff8da35473902c02184311686e72865ff1\\\"},{\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"http://ns.mnemotix.com/onto/bazLabel1\\\":[{\\\"@value\\\":\\\"Mobile\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@ref\\\":\\\"a9195bd94ce672e725568c72b9737ce1d3ac8a30\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/1233456\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"@ref\\\":\\\"3d565ba6af71ec48c23d74d94756e9a8f51deeec\\\"}],\\\"@ref\\\":\\\"12010bb007e091f45db18bd72473bf39c513b48f\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/baz/1233456\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Baz\\\",\\\"@ref\\\":\\\"3d565ba6af71ec48c23d74d94756e9a8f51deeec\\\"}]\".
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2. }
}`
    });
  });

  it("should disable an object ", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.removeObject({
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT {
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasDeletion <http://ns.mnemotix.com/instances/deletion/1>.
 <http://ns.mnemotix.com/instances/deletion/1> rdf:type mnx:Deletion;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/bar/1\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Bar\\\",\\\"@ref\\\":\\\"3f3852a5f5dbca18ac1d9508b24568b258594fb1\\\"}]\".
}
WHERE {  }`
    });
  });

  it("should disable a list of objects ", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));


    jest.spyOn(session, 'getObjectsCount').mockImplementation(() => 3);
    jest.spyOn(session, 'getObjects').mockImplementation(() => [{id: "test:bar/1"}, {id:"test:bar/2"}, {id:"test:bar/3"}]);

    await session.removeObjects({
      modelDefinition: BarDefinitionMock,
      objectIds: ["test:bar/1", "test:bar/2", "test:bar/3"]
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT {
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasDeletion <http://ns.mnemotix.com/instances/deletion/1>.
 <http://ns.mnemotix.com/instances/bar/2> mnx:hasDeletion <http://ns.mnemotix.com/instances/deletion/1>.
 <http://ns.mnemotix.com/instances/bar/3> mnx:hasDeletion <http://ns.mnemotix.com/instances/deletion/1>.
 <http://ns.mnemotix.com/instances/deletion/1> rdf:type mnx:Deletion;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/bar/1\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Bar\\\",\\\"@ref\\\":\\\"3f3852a5f5dbca18ac1d9508b24568b258594fb1\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/bar/2\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Bar\\\",\\\"@ref\\\":\\\"4c3f12e1977109d876c0697f0ea1b9e1cc35233a\\\"},{\\\"@id\\\":\\\"http://ns.mnemotix.com/instances/bar/3\\\",\\\"@type\\\":\\\"http://ns.mnemotix.com/onto/Bar\\\",\\\"@ref\\\":\\\"bb1955db7a2da64f5496bd3841dd022d37cb0517\\\"}]\".
}
WHERE {  }`
    });
  });

  it("should disable a big list of objects ", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    const totalCount = 3000;
    const pageSize   = 1000;

    jest.spyOn(session, 'getObjectsCount').mockImplementation(() => totalCount);
    jest.spyOn(session, 'getObjects').mockImplementation(() => [...Array(totalCount)].map((_, index) => ({id: `test:bar/${index}`})));

    await session.removeObjects({
      modelDefinition: BarDefinitionMock,
      objectIds: [...Array(totalCount)].map((_, index) => `test:bar/${index}`)
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledTimes(3);
  });
});




