/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache. foo/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import SynaptixDatastoreRdfSession from "../SynaptixDatastoreRdfSession";
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";
import { PubSub } from "graphql-subscriptions";
import generateId from "nanoid/generate";
import { Model } from "../../datamodel/toolkit/models/Model";

import {
  BarDefinitionMock,
  NotInstantiableDefinitionMock,
  BazDefinitionMock,
  FooDefinitionMock
} from "../../datamodel/__tests__/mocks/definitions";

jest.mock("nanoid/generate");

let modelDefinitionsRegister = new ModelDefinitionsRegister([
  NotInstantiableDefinitionMock,
  BarDefinitionMock,
  BazDefinitionMock,
  FooDefinitionMock
]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();
let context = new GraphQLContext({
  anonymous: true,
  lang: "fr"
});

let session = new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine,
  schemaNamespaceMapping: {
    mnx: "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  nodesTypeFormatter: type => `prefix-for-testing-${type}`,
  indexDisabled: true
});

let spyOnIndexGetNodes = jest.spyOn(session.getIndexService(), "getNodes");
let spyOnGraphControllerConstruct = jest.spyOn(
  session.getGraphControllerService().getGraphControllerPublisher(),
  "construct"
);
let spyOnGraphControllerInsertTriples = jest.spyOn(
  session.getGraphControllerService().getGraphControllerPublisher(),
  "insertTriples"
);
let spyOnGraphControllerUpdateTriples = jest.spyOn(
  session.getGraphControllerService().getGraphControllerPublisher(),
  "updateTriples"
);

jest.spyOn(session, "isIndexDisabled").mockImplementation(() => true);

spyOnIndexGetNodes.mockImplementation(async () => {
  return {};
});

describe("SynaptixDatastoreRdfSession", () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it("should play well with global id reprensentation extraction and stringification", async () => {
    expect(
      session.parseGlobalId("test:prefix-for-testing-Bar/43433432")
    ).toEqual({
      id: "http://ns.mnemotix.com/instances/prefix-for-testing-Bar/43433432",
      type: "Bar"
    });
  });

  it("Should get non reversed linked nodes", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:bar/1",
      "@context": {}
    }));

    // This is a rdfObjectProperty
    let ac = await session.getLinkedObjectFor({
      object: new Model(
        "test:prefix-for-testing-Bar/1",
        "test:prefix-for-testing-Bar/1",
        {}
      ),
      linkDefinition: BarDefinitionMock.getLink("hasBaz"),
      bypassIndex: true
    });

    expect(spyOnGraphControllerConstruct).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Baz.
 ?uri mnx:bazLabel1 ?bazLabel1.
}
WHERE {
 ?uri rdf:type mnx:Baz.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:hasBaz ?uri.
 OPTIONAL { ?uri mnx:bazLabel1 ?bazLabel1. }
}`
    });
  });

  it("Should get reversed linked nodes", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:bar/1",
      "@context": {}
    }));

    // This is a rdfReversedObjectProperty
    let ac = await session.getLinkedObjectFor({
      object: new Model(
        "test:prefix-for-testing-Foo/1",
        "test:prefix-for-testing-Foo/1",
        {},
        "Foo"
      ),
      linkDefinition: FooDefinitionMock.getLink("hasBaz"),
      bypassIndex: true
    });

    expect(spyOnGraphControllerConstruct).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Baz.
 ?uri mnx:bazLabel1 ?bazLabel1.
}
WHERE {
 ?uri rdf:type mnx:Baz.
 ?uri mnx:hasBaz <http://ns.mnemotix.com/instances/prefix-for-testing-Foo/1>.
 OPTIONAL { ?uri mnx:bazLabel1 ?bazLabel1. }
}`
    });
  });

  it("Should get linked nodes with filters", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:bar/1",
      "@context": {}
    }));

    let ac = await session.getObjects({
      modelDefinition: BazDefinitionMock,
      args: {
        linkFilters: [
          {
            rdfFilterFunction: {
              generateRdfFilterDefinition: ({ literalVariable }) => ({
                rdfFunction: "http://ns.mnemotix.com/onto/isEqual",
                args: [
                  literalVariable,
                  BarDefinitionMock.getLiteral("barLiteral1").toSparqlValue(
                    "john.doe@mnemotix.com"
                  )
                ]
              }),
              literalDefinition: BarDefinitionMock.getLiteral("barLiteral1")
            },
            linkDefinition: BazDefinitionMock.getLink("hasBar")
          }
        ]
      }
    });

    expect(spyOnGraphControllerConstruct).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Baz.
 ?uri mnx:bazLabel1 ?bazLabel1.
}
WHERE {
 ?uri rdf:type mnx:Baz.
 ?uri mnx:hasBar ?hasBar.
 ?hasBar mnx:barLiteral1 ?barLiteral1.
 OPTIONAL { ?uri mnx:bazLabel1 ?bazLabel1. }
 FILTER(mnx:isEqual(?barLiteral1, "john.doe@mnemotix.com"))
}`
    });
  });

  it("should get model definition given a prefixed URI", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:bar/1",
      "@type": "mnx:Bar",
      "@context": {}
    }));

    let bar = new Model(
      "mnx:prefix-for-testing-Bar/1",
      "mnx:prefix-for-testing-Bar/1",
      {},
      "Bar"
    );

    let barDefinition = await session.getModelDefinitionForModel({
      model: bar
    });

    expect(barDefinition).toBe(BarDefinitionMock);
  });

  it("should get model definition given an absolute URI", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:bar/1",
      "@type": "http://ns.mnemotix.com/onto/Bar",
      "@context": {}
    }));

    let bar = new Model(
      "http://ns.mnemotix.com/prefix-for-testing-Bar/1",
      "http://ns.mnemotix.com/prefix-for-testing-Bar/1",
      {},
      "Bar"
    );

    let barDefinition = await session.getModelDefinitionForModel({
      model: bar
    });

    expect(barDefinition).toBe(BarDefinitionMock);
  });

  it("should get model definition given a list of heterogeneous URI", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:bar/1",
      "@type": "http://ns.mnemotix.com/onto/Bar",
      "@context": {}
    }));

    let bar = new Model(
      "http://ns.mnemotix.com/prefix-for-testing-Bar/1",
      "http://ns.mnemotix.com/prefix-for-testing-Bar/1",
      {
        types: [
          "http://ns.mnemotix.com/onto/Bar",
          "http://www.w3.org/2002/07/owl#Thing",
          "mnx:Bar"
        ]
      },
      "Bar"
    );

    let barDefinition = await session.getModelDefinitionForModel({
      model: bar
    });

    expect(barDefinition).toBe(BarDefinitionMock);
  });

  it("should get the more specific model definition given an URI", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:bar/1",
      "@type": ["mnx:Bar", "mnx:Agent", "owl:Thing"],
      "@context": {}
    }));

    let bar = new Model(
      "mnx:prefix-for-testing-Bar/1",
      "mnx:prefix-for-testing-Bar/1",
      {},
      "Bar"
    );

    let barDefinition = await session.getModelDefinitionForModel({
      model: bar
    });

    expect(barDefinition).toBe(BarDefinitionMock);
  });

  it("should extract links from object input", async () => {
    expect(
      await session._extractLinksFromObjectInput({
        modelDefinition: BarDefinitionMock,
        objectInput: {
          barLiteral1: "Derek",
          barLiteral2: "Devel",
          bazInputs: [
            {
              bazLabel1: "Pro",
            },
            {
              bazLabel1: "Mobile",
            },
            {
              id: "mnxd:prefix-for-testing-Baz/1233456"
            },
            {
              id: "mnxd:prefix-for-testing-Baz/updateIt",
              bazLabel1: "0600000000"
            }
          ],
          fooInput: {
            fooLabel1: "derek@mnemotix.com"
          }
        },
        links: [
          BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetId(
            "mnxd:baz/1234"
          )
        ]
      })
    ).toEqual({
      links: [
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetId(
          "mnxd:baz/1234"
        ),
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            bazLabel1: "Pro"
          }
        }),
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            bazLabel1: "Mobile",
          }
        }),
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetId(
          "http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1233456"
        ),
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetId:
            "http://ns.mnemotix.com/instances/prefix-for-testing-Baz/updateIt",
          targetObjectInput: {
            bazLabel1: "0600000000"
          }
        }),
        BarDefinitionMock.getLink("hasFoo").generateLinkFromTargetProps({
          targetObjectInput: {
            fooLabel1: "derek@mnemotix.com"
          }
        })
      ],
      objectInput: {
        barLiteral1: "Derek",
        barLiteral2: "Devel"
      }
    });
  });

  it("should extract nested links from object input", async () => {
    expect(
      await session._extractLinksFromObjectInput({
        modelDefinition: BarDefinitionMock,
        objectInput: {
          barLiteral1: "Derek",
          bazInputs: [
            {
              bazLabel1: "The boss",
               fooInput: {
                fooLabel1: "Mnx"
              }
            }
          ],
          bazInputsToDelete: ["baz/123", "baz/456"]
        }
      })
    ).toEqual({
      links: [
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps(
          {
            targetObjectInput: {
              bazLabel1: "The boss",
            },
            targetNestedLinks: [
              BazDefinitionMock.getLink("hasFoo").generateLinkFromTargetProps({
                targetObjectInput: {
                  fooLabel1: "Mnx"
                }
              })
            ]
          }
        ),
        BarDefinitionMock.getLink("hasBaz").generateDeletionLink({
          targetId: "http://ns.mnemotix.com/instances/baz/123"
        }),
        BarDefinitionMock.getLink("hasBaz").generateDeletionLink({
          targetId: "http://ns.mnemotix.com/instances/baz/456"
        })
      ],
      objectInput: {
        barLiteral1: "Derek"
      }
    });
  });

  it("should create a graph", async () => {
    spyOnGraphControllerInsertTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.createObject({
      graphQLType: "Bar",
      objectInput: {
        barLiteral1: "Derek",
        barLiteral2: "Devel",
        bazInputs: [
          {
            bazLabel1: "Pro",
          },
          {
            bazLabel1: "Mobile",
          },
          {
            id: "mnxd:prefix-for-testing-Baz/1233456"
          }
        ],
        fooInput: {
          fooLabel1: "derek@mnemotix.com"
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerInsertTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "Derek";
  mnx:barLiteral2 "Devel";
  mnx:hasBaz <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/2>, <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/3>, <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1233456>;
  mnx:hasFoo <http://ns.mnemotix.com/instances/prefix-for-testing-Foo/4>.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/2> rdf:type mnx:Baz;
  mnx:bazLabel1 "Pro"@fr.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/3> rdf:type mnx:Baz;
  mnx:bazLabel1 "Mobile"@fr.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Foo/4> rdf:type mnx:Foo;
  mnx:fooLabel1 "derek@mnemotix.com"@fr.
}`
    });
  });

  it("should update an object", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: BarDefinitionMock,
      objectId: "test:prefix-for-testing-Bar/1",
      updatingProps: {
        barLiteral1: "Derek",
        barLiteral2: "Devel",
        barLabel1: "Hello, I'm Derek !",
        bazInputs: [
          {
            bazLabel1: "Pro",
          },
          {
            bazLabel1: "Mobile",
          },
          {
            id: "mnxd:prefix-for-testing-Baz/1233456"
          }
        ],
        fooInput: {
          fooLabel1: "derek@mnemotix.com"
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE {
 <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1;
  mnx:hasFoo ?hasFoo.
}
INSERT {
 <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:barLiteral1 "Derek";
  mnx:barLiteral2 "Devel";
  mnx:barLabel1 "Hello, I'm Derek !"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1>, <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/2>, <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1233456>;
  mnx:hasFoo <http://ns.mnemotix.com/instances/prefix-for-testing-Foo/3>.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1> rdf:type mnx:Baz;
  mnx:bazLabel1 "Pro"@fr.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/2> rdf:type mnx:Baz;
  mnx:bazLabel1 "Mobile"@fr.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Foo/3> rdf:type mnx:Foo;
  mnx:fooLabel1 "derek@mnemotix.com"@fr.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:hasFoo ?hasFoo. }
}`
    });
  });

  it("shoud remove a node link in case of 1:1 relationship for null value", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: BarDefinitionMock,
      objectId: "test:prefix-for-testing-Bar/1",
      updatingProps: {
        fooInput: null
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE { <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:hasFoo ?hasFoo. }
WHERE { OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:hasFoo ?hasFoo. } }`
    });
  });

  it("shoud remove a node link in case of 1:1 relationship for id === null value", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: BarDefinitionMock,
      objectId: "test:prefix-for-testing-Bar/1",
      updatingProps: {
        fooInput: {
          id: null
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE { <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:hasFoo ?hasFoo. }
WHERE { OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Bar/1> mnx:hasFoo ?hasFoo. } }`
    });
  });

  it("shoud not remove a node link in case of 1:N relationship", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: BarDefinitionMock,
      objectId: "test:prefix-for-testing-Bar/1",
      updatingProps: {
        bazInputs: [null]
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).not.toBeCalled();
  });

  it("should btach update an object", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    jest.spyOn(session, 'getObjectsCount').mockImplementation(() => 3);
    jest.spyOn(session, 'getObjects').mockImplementation(() => [{id: "test:bar/1"}, {id:"test:bar/2"}, {id:"test:bar/3"}]);

    await session.updateObjects({
      modelDefinition: BarDefinitionMock,
      objectIds: ["test:bar/1", "test:bar/2", "test:bar/3"],
      updatingProps: {
        barLiteral1: "Derek",
        barLiteral2: "Devel",
        barLabel1: "Hello, I'm Derek !",
        bazInputs: [
          {
            bazLabel1: "Pro",
          },
          {
            bazLabel1: "Mobile",
          },
          {
            id: "mnxd:prefix-for-testing-Baz/1233456"
          }
        ],
        fooInput: {
          fooLabel1: "derek@mnemotix.com"
        }
      },
      lang: "fr"
    });


    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: []
      },
      query: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
DELETE {
 ?node mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1;
  mnx:hasFoo ?hasFoo.
}
INSERT {
 ?node mnx:barLiteral1 "Derek";
  mnx:barLiteral2 "Devel";
  mnx:barLabel1 "Hello, I'm Derek !"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1>, <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/2>, <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1233456>;
  mnx:hasFoo <http://ns.mnemotix.com/instances/prefix-for-testing-Foo/3>.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/1> rdf:type mnx:Baz;
  mnx:bazLabel1 "Pro"@fr.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Baz/2> rdf:type mnx:Baz;
  mnx:bazLabel1 "Mobile"@fr.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Foo/3> rdf:type mnx:Foo;
  mnx:fooLabel1 "derek@mnemotix.com"@fr.
}
WHERE {
 ?node rdf:type mnx:Bar.
 OPTIONAL { ?node mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { ?node mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  ?node mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
 OPTIONAL { ?node mnx:hasFoo ?hasFoo. }
 FILTER(?node IN(<http://ns.mnemotix.com/instances/bar/1>, <http://ns.mnemotix.com/instances/bar/2>, <http://ns.mnemotix.com/instances/bar/3>))
}`
    });
  });
});
