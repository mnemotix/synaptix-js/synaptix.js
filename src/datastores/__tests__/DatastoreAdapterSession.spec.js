/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import DatastoreSessionAbstract from '../DatastoreSessionAbstract';
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";

class DatastoreSessionOkTest extends DatastoreSessionAbstract {
}

describe('DatastoreSessionAbstract', () => {

  it('throws an exception if instantied as abstract', () => {
    expect(() => {
      let datastore = new DatastoreSessionAbstract();
    }).toThrow(TypeError);

  });

  it('throws an exception is not instantiated with a GraphQLContext', () => {
    expect(() => {
      new DatastoreSessionOkTest();
    }).toThrow("The `context` parameter must be defined and instance of GraphQLContext");

    expect(() => {
      new DatastoreSessionOkTest({});
    }).toThrow("The `context` parameter must be defined and instance of GraphQLContext");
  });

  it('should return a context when instantiated', () => {
    let context = new GraphQLContext({
      anonymous: true,
      lang: "fr"
    });

    let session = new DatastoreSessionOkTest(context);
    expect(session.getContext()).toBe(context);
  })
});




