/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import GraphControllerService from "../datamodules/services/graph/GraphControllerService";
import {gatherModelDefinitionHierarchy} from "../datamodel/toolkit/definitions/helpers";
import NetworkLayerAMQP from "../network/amqp/NetworkLayerAMQP";
import SynaptixDatastoreSession from "./SynaptixDatastoreSession";
import {I18nError} from "../utilities/error/I18nError";
import env from 'env-var';
import omit from "lodash/omit";
import {LinkFilter} from "../datamodel/toolkit/utils/filter";
import { ModelDefinitionAbstract, LinkDefinition } from "../datamodel/toolkit/definitions";
import {logDebug} from "../utilities/logger";


/**
 * This is the synaptix session designed to use RDF triple store.
 */
export default class SynaptixDatastoreRdfSession extends SynaptixDatastoreSession {
  /** @type {GraphControllerService} */
  _graphControllerService;

  /** @type {string} */
  _nodesPrefix;

  /**
   * Constructor
   *
   * @param {GraphQLContext} context GraphQL context object
   * @param {NetworkLayer} networkLayer
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {PubSubEngine} pubSubEngine
   * @param {object} schemaNamespaceMapping - Project namespaces mapping.
   * @param {object} namedGraphsMapping- Named graphs mapping.
   * @param {string} nodesNamespaceURI  - The URI namespace used to create new node (aka: to generate new URIs)
   * @param {string} [nodesPrefix=ex]   - The prefix used for nodesNamespaceURI
   * @param {function} [nodesTypeFormatter] - This function tweaks a node type into a URI prefix-styled format
   * @param {GraphMiddleware[]} graphMiddlewares - A list of middlewares for GraphControllerService
   * @param {e.Response} [res] - Express response object
   * @param {SSOApiClient} [ssoApiClient]
   * @param {SSOMiddleware[]} [ssoMiddlewares] - A list of middlewares for SSO actions
   * @param {string} [adminUserGroupId] - Define UserGroup instance related to administrators.
   * @param {string} [defaultNamedGraphURI] - The URI of the global named graph where data are inserted
   * @param {boolean} [indexDisabled=false] - Disable index.
   * @param {NodeCache} cache
   */
  constructor({context, networkLayer, modelDefinitionsRegister, pubSubEngine, schemaNamespaceMapping, nodesNamespaceURI, nodesPrefix, nodesTypeFormatter, graphMiddlewares, ssoApiClient, ssoMiddlewares, res, adminUserGroupId, defaultNamedGraphURI, indexDisabled, cache}) {
    super({
      context,
      networkLayer,
      modelDefinitionsRegister,
      pubSubEngine,
      ssoApiClient,
      ssoMiddlewares,
      res,
      adminUserGroupId,
      indexDisabled,
      cache
    });

    this._nodesTypeFormatter = nodesTypeFormatter;
    this._nodesPrefix = nodesPrefix;
    this._nodesNamespaceURI = nodesNamespaceURI;
    this._schemaNamespaceMapping = schemaNamespaceMapping || {};
    this._namedGraphsMapping = env.get("NAMED_GRAPHS_MAPPING").asJson() || {};
    this._defaultNamedGraphId = defaultNamedGraphURI || env.get("NODES_NAMED_GRAPH").asString();

    this._graphControllerService = new GraphControllerService({
      networkLayer,
      graphQLcontext: context,
      schemaNamespaceMapping,
      nodesNamespaceURI,
      nodesPrefix,
      middlewares: graphMiddlewares,
      nodesTypeFormatter: this.getNodesTypeFormatter(),
      defaultNamedGraphURI: this._defaultNamedGraphId,
      namedGraphsMapping: this._namedGraphsMapping,
      indexDisabled: this.isIndexDisabled()
    });

    (graphMiddlewares || []).map(graphMiddleware => graphMiddleware.attachSession(this));
  }

  /**
   * @return {string}
   */
  getNodesNamespaceURI() {
    return this._nodesNamespaceURI;
  }

  /**
   * @return {string}
   */
  getNodesPrefix() {
    return this._nodesPrefix;
  }

  getSchemaNamespaceMapping() {
    return {
      ...this._schemaNamespaceMapping,
      [this._nodesPrefix]: this._nodesNamespaceURI
    };
  }

  /**
   * This method returns that is called to take a type and get it's related form in an instance URI prefix.
   *
   * By default, and for example :
   *
   * A type `mnx:UserAccount` is tranformed into `useraccount`.
   * And a  `mnx:UserAccount` node URI could be `https://data.mnemotix.com/useraccount/36545`
   *
   * @return {function}
   */
  getNodesTypeFormatter() {
    return this._nodesTypeFormatter || ((type) => type.toLowerCase());
  }

  /**
   * @return {GraphControllerService}
   */
  getGraphControllerService() {
    return this._graphControllerService;
  }

  /**
   * @return {GraphControllerPublisher}
   */
  getGraphClient(){
    return this._graphControllerService.getGraphControllerPublisher();
  }

  /**
   * Extract an id from a globalId object representation
   * @param {string} globalId
   * @return {string}
   */
  extractIdFromGlobalId(globalId) {
    //
    // If Relay is activated, use default method.
    //
    if (env.get("USE_GRAPHQL_RELAY").default("0").asBool()) {
      return super.extractIdFromGlobalId(globalId);
      //
      // Otherwise, we assume that the datastore internal id IS the globalId
      //
    } else {
      let id;

      // If globalId is an absolute URI. Nothing to do.
      if (!!globalId.match(/https?:\/\//)) {
        id = globalId;
        // If it's a prefixed URI, we should reconstruct the absolute URI by finding the good namespace.
      } else if (globalId.includes(':')) {
        let delimiterPos = globalId.indexOf(':');
        let prefix = globalId.slice(0, delimiterPos);
        let localId = globalId.slice(delimiterPos + 1);
        let mapping = Object.entries(this._schemaNamespaceMapping).find(([namespacePrefix, namespaceURI]) => namespacePrefix === prefix);

        if (mapping) {
          id = `${mapping[1]}${localId}`;
        } else {
          id = `${this._nodesNamespaceURI}${localId}`;
        }
        // If there is no prefix, we should restruct the prefixed AND absolute URI
      } else {
        id = `${this._nodesNamespaceURI}${globalId}`;
      }

      return id;
    }
  }

  /**
   * Parse a graphQL global id string and return it's object representation.
   *
   * @param globalId
   * @return {{id: *, type: string}}
   */
  parseGlobalId(globalId) {
    //
    // If Relay is activated, use default method.
    //
    if (env.get("USE_GRAPHQL_RELAY").default("0").asBool()) {
      return super.parseGlobalId(globalId);
    } else {
      let id = this.extractIdFromGlobalId(globalId);
      let type = this.getModelDefinitionsRegister().getGraphQLTypeForURI({
        uri: id,
        nodesNamespaceURI: this._nodesNamespaceURI,
        nodesTypeFormatter: this.getNodesTypeFormatter()
      });

      // For retrieved type, URI must be reconstructed is gived as prefixed.
      return {
        type,
        id
      };
    }
  }

  /**
   * Stringify a globalId object representation.
   *
   * @param {string} type - Object GraphQL type
   * @param {string} id   - Object id
   * @return {*}
   */
  stringifyGlobalId({type, id}) {
    if (!!parseInt(process.env.USE_GRAPHQL_RELAY)) {
      return super.stringifyGlobalId({type, id});
    } else {
      // If id matches the default namespace, remove prefix or absolute namespace.
      if (id.includes(`${this._nodesPrefix}:`)) {
        id = id.replace(`${this._nodesPrefix}:`, '');
      } else if (id.includes(this._nodesNamespaceURI)) {
        id = id.replace(this._nodesNamespaceURI, '');
      } else {
        let mapping = Object.entries(this._schemaNamespaceMapping).find(([namespacePrefix, namespaceURI]) => id.includes(namespaceURI));

        if (mapping) {
          id = id.replace(mapping[1], `${mapping[0]}:`)
        }
      }

      return id;
    }
  }

  /**
   * @param id
   */
  async getGraphQLTypeForId(id){
    // Fast way. Determine type given a namespace/prefix.
    let type = this.getModelDefinitionsRegister().getGraphQLTypeForURI({
      uri: id,
      nodesNamespaceURI: this._nodesNamespaceURI,
      nodesTypeFormatter: this.getNodesTypeFormatter()
    });

    // Slow (but sure way). Ask to the graph.
    if(!type){
      type = await this._graphControllerService.getRdfTypeForURI({uri: this.extractIdFromGlobalId(id)});
    }

    if(!type){
      throw new I18nError(`GraphQL type can't be inferred for ${id}.`);
    }

    const modelDefinition = this.getModelDefinitionForRdfType({type});

    if (!modelDefinition) {
      throw new I18nError(`Can't find ModelDefinitio for Rdf type ${type}.`)
    }

    return modelDefinition.getGraphQLType();
  }

  /**
   * Normalize an URI into its absolute formalism.
   * @param uri
   * @return {string}
   */
  normalizeAbsoluteUri({uri}) {
    if (!uri.match(/https?:\/\//)) {
      let mapping = Object.entries(this.getSchemaNamespaceMapping()).find(([namespacePrefix, namespaceURI]) => !!uri.match(new RegExp(`^${namespacePrefix}:`)));

      if (mapping) {
        return uri.replace(`${mapping[0]}:`, mapping[1]);
      }
    }

    return uri;
  }

  /**
   * Normalize an URI into its prefixed formalism.
   *
   * @param uri
   * @return {string}
   */
  normalizePrefixedUri({uri}) {
    if (!!uri.match(/https?:\/\//)) {
      let mapping = Object.entries(this.getSchemaNamespaceMapping()).find(([namespacePrefix, namespaceURI]) => uri.includes(namespaceURI));

      if (mapping) {
        return uri.replace(mapping[1], `${mapping[0]}:`);
      }
    }

    return uri;
  }


  /**
   * In RDF id are URIs. To avoid mixing between prefixed and absolute URIS,
   * we choose to normalize id into URI absolute form.
   * @param id
   * @return {string}
   */
  normalizeId(id){
    return this.normalizeAbsoluteUri({uri: id});
  }

  /**
   * Generate a URI given a modelDefinition
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [idPrefix]  - Prefix before random id generation.
   * @param {string} [idValue]   - Set an manual id in place of random id generation.
   */
  generateUriForModelDefinition({modelDefinition, idPrefix, idValue}) {
    return modelDefinition.generateURI({
      nodesNamespaceURI: this.getNodesNamespaceURI(),
      nodeTypeFormatter: this.getNodesTypeFormatter(),
      idPrefix,
      idValue
    })
  }

  /**
   * @param {string|string[]} type
   * @param {boolean} [returnList]
   * @return {typeof ModelDefinitionAbstract|typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForRdfType({type, returnList}) {
    let candidatesModelDefinitions = [];

    if (Array.isArray(type)) {
      candidatesModelDefinitions = type.reduce((acc, candidateType) => {
        let absoluteCandidateType = this.normalizeAbsoluteUri({uri: candidateType});
        let prefixedCandidateType = this.normalizePrefixedUri({uri: candidateType});

        let modelDefinition = this._modelDefinitionRegister.getModelDefinitionForRdfType([prefixedCandidateType, absoluteCandidateType], false);

        if (modelDefinition) {
          acc.push(modelDefinition);
        }

        return acc;
      }, []);

      candidatesModelDefinitions.sort((modelDefinitionA, modelDefinitionB) => {
        let hierarchyDefinitions = gatherModelDefinitionHierarchy(modelDefinitionB);

        // Check is modelDefinitionB herits from modelDefinitionA
        if (hierarchyDefinitions.includes(modelDefinitionA)) {
          return 1;
        } else {
          return -1
        }
      });
    } else {
      let absoluteType = this.normalizeAbsoluteUri({uri: type});
      let prefixedType = this.normalizePrefixedUri({uri: type});

      candidatesModelDefinitions.push(this._modelDefinitionRegister.getModelDefinitionForRdfType([prefixedType, absoluteType]));
    }

    return returnList ? candidatesModelDefinitions : candidatesModelDefinitions[0];
  }

  /**
   * Returns a definition given an URI
   * @param {Model} model
   * @return {typeof ModelDefinitionAbstract}
   */
  async getModelDefinitionForModel({model}) {
    let modelDefinition = await super.getModelDefinitionForModel({model});

    if (modelDefinition == null && model.types) {
      // This is the case when model is roughly parsed form indexed object.
      // The type is an array of RDF types.
      modelDefinition = this.getModelDefinitionForRdfType({type: model.types});
    }

    if (modelDefinition == null) {
      let uri = model.uri;
      let type = await this._graphControllerService.getRdfTypeForURI({uri});

      if(type){
        return this.getModelDefinitionForRdfType({type});
      }
    }

    if (modelDefinition) {
      model.setType(modelDefinition.getNodeType());
    }

    return modelDefinition;
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param {string} [lang]
   * @param {boolean} [returnFirstOneIfNotExistForLang=true]
   * @param {boolean} [langFlagEnabled=false] Add the language flag (ex @fr) to the label
   * @return {string|string[]}
   */
  async getLocalizedLabelFor({object, labelDefinition, lang, returnFirstOneIfNotExistForLang, langFlagEnabled}) {
    if (!lang) {
      lang = this.getLang();
    }

    let label = await super.getLocalizedLabelFor({object, labelDefinition, lang, returnFirstOneIfNotExistForLang, langFlagEnabled});

    if (label == null) {
      label = await this._graphControllerService.getLocalizedLabelForNode({
        labelDefinition,
        sourceNode: object,
        lang,
        returnFirstOneIfNotExistForLang,
        langFlagEnabled
      });
    }

    return label;
  }


  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {ResolverArgs} args
   * @param {boolean} [discardTypeAssertion=false] - Discard RDF type assertion. This this usefull when URI reference are stored in triple store but belongs to another triple store (Ex: Geonames)
   * @param {LinkFilter[]} [linkFilters] - A list of link filters to add.
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
   * @param {bool} [fetchIdsOnly=false]
   * @param {bool} [forceFallbackIfEmpty=false]
   * @return {Model|Model[]}
   */
  async getLinkedObjectFor({object, linkDefinition, args, discardTypeAssertion, linkFilters, fragmentDefinitions, asCollection, fetchIdsOnly, forceFallbackIfEmpty}) {
    let result = await super.getLinkedObjectFor({object, linkDefinition, fragmentDefinitions, args, asCollection, fetchIdsOnly, forceFallbackIfEmpty});

    // Sometimes "null" value simply means that there is not "single" linked node. So don't refetch the graph.
    if ((result == null && (this.isIndexDisabled() || !linkDefinition.getRelatedModelDefinition().getIndexType())) || (forceFallbackIfEmpty && linkDefinition.isPlural() && result?.length === 0)) {
      let modelDefinition = linkDefinition.getRelatedModelDefinition();
      let linkPaths = [];

      if(linkDefinition.getLinkPath()){
       linkPaths = [linkDefinition.getLinkPath()];
      } else {
        linkFilters = [new LinkFilter({
          id: object.id,
          isReversed: true,
          linkDefinition
        })].concat(linkFilters || [])
      }

      let nodes = await this._graphControllerService.getNodes({
        modelDefinition,
        linkFilters,
        linkPaths,
        discardTypeAssertion,
        fragmentDefinitions,
        limit: this.getLimitFromArgs(args),
        offset: this.getOffsetFromArgs(args),
        sortings: this.getSortingsFromArgs({args, modelDefinition}),
        queries: this.getQueryStringsFromArgs({args, modelDefinition}),
        asCollection
      });

      if (linkDefinition.isPlural()) {
        result = nodes;
      } else {
        result = nodes[0];
      }
    }

    if(!result){
      return linkDefinition.isPlural() ? [] : null;
    }

    return result;
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {ResolverArgs} args
   * @param {boolean} [discardTypeAssertion=false] - Discard RDF type assertion. This this usefull when URI reference are stored in triple store but belongs to another triple store (Ex: Geonames)
   * @param {LinkFilter[]} [linkFilters] - A list of link filters to add.
   *
   * @return {number}
   */
  async getLinkedObjectsCountFor({object, linkDefinition, args, discardTypeAssertion, linkFilters}) {
    let count = await super.getLinkedObjectsCountFor({object, linkDefinition, args});

    if (count == null) {
      count = await this._graphControllerService.getNodes({
        modelDefinition: linkDefinition.getRelatedModelDefinition(),
        linkFilters: [{
          id: object.id,
          isReversed: true,
          linkDefinition
        }].concat(linkFilters || []),
        discardTypeAssertion,
        justCount: true
      })
    }

    return count || 0;
  }

  /**
   * @param {Model} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} [linkPath]
   * @return {boolean}
   */
  async isObjectExistsForLinkPath({object, modelDefinition, linkPath}) {
    let exists = await super.isObjectExistsForLinkPath({object, modelDefinition, linkPath});

    if (exists == null) {
      exists = await this._graphControllerService.isNodeExistsForLinkPath({
        id: object.id,
        modelDefinition,
        linkPath
      });
    }

    return exists;
  }

  /**
   * Is object exists
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @return {boolean}
   */
  async isObjectExists({modelDefinition, objectId}) {
    let exists = await super.isObjectExists({modelDefinition, objectId});

    if (exists == null) {
      exists = await this._graphControllerService.isNodeExists({id: objectId, modelDefinition});
    }

    return exists;
  }

  /**
   * Get object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {string} lang
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @param {ResolverArgs} [args={}]
   * @return {Model}
   */
  async getObject({modelDefinition, objectId, lang, args, fragmentDefinitions}) {
    let object = await super.getObject({modelDefinition, objectId, lang, args, fragmentDefinitions});

    if (object === null) {
      object = await this._graphControllerService.getNode({
        id: objectId,
        modelDefinition,
        lang,
        fragmentDefinitions,
        ...this.parseResolverArgsIntoFilters({modelDefinition, args})
      });
    }

    return object;
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {ResolverArgs} args
   * @param {FragmentDefinition[]} fragmentDefinitions
   * @param {boolean} [firstOne=false]
   * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
   * @param {boolean} [forceFallbackIfEmpty]
   * @param {boolean} [fetchIdsOnly] - Optimise ES request by excluding all properties execpt id
   * @return {Model[]}
   */
  async getObjects({modelDefinition, args, fragmentDefinitions, firstOne, asCollection, forceFallbackIfEmpty = false, fetchIdsOnly = false}) {
    let objects = await super.getObjects({modelDefinition, args, fragmentDefinitions, firstOne, asCollection, forceFallbackIfEmpty, fetchIdsOnly});

    // Sometimes "null" value simply means that there is no matching node while "firstOne" is set. So don't refetch the graph.
    if (objects == null && (forceFallbackIfEmpty || !firstOne || this.isIndexDisabled() || !modelDefinition.getIndexType())) {
      objects = await this._graphControllerService.getNodes({
        modelDefinition,
        qs: args?.qs,
        limit: this.getLimitFromArgs(args),
        offset: this.getOffsetFromArgs(args),
        sortings: this.getSortingsFromArgs({args, modelDefinition}),
        queries: this.getQueryStringsFromArgs({args, modelDefinition}),
        fragmentDefinitions,
        ...this.parseResolverArgsIntoFilters({modelDefinition, args}),
        asCollection
      });

      return firstOne ? objects?.[0] : objects;
    } else {
      if(!objects){
        return firstOne ? null : [];
      }

      return objects;
    }
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {ResolverArgs} args
   * @return {Model[]}
   */
  async getObjectsCount({modelDefinition, args}) {
    let count = await super.getObjectsCount({modelDefinition, args});

    if (count == null) {
      count = await this._graphControllerService.getNodes({
        modelDefinition,
        qs: args?.qs,
        justCount: true,
        ...this.parseResolverArgsIntoFilters({modelDefinition, args}),
      });
    }

    return count || 0;
  }

  /**
   * Creates an object
   *
   * @param {string} [graphQLType] - Type of object
   * @param {ModelDefinitionAbstract} [modelDefinition]
   * @param {Link[]} [links] -  List of links
   * @param {object} [objectInput] - Object properties
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {boolean} [waitForIndexSyncing] - Should wait indexation task.
   *
   * @return {Model}
   */
  async createObject({graphQLType, modelDefinition, links, objectInput, lang, uri, waitForIndexSyncing}) {
    if (!modelDefinition) {
      modelDefinition = this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(graphQLType);
    }

    ({links, objectInput} = await this._extractLinksFromObjectInput({modelDefinition, objectInput, links}));

    this.setIndexDisabled();

    return this._graphControllerService.createNode({
      modelDefinition,
      links,
      objectInput,
      lang,
      uri,
      waitForIndexSyncing
    });
  }

  /**
   * Update object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {object} [updatingProps]
   * @param {Link[]} [links] -  List of links
   * @param {string|null} [lang]
   */
  async updateObject({modelDefinition, objectId, updatingProps, links, lang}) {
    ({links, objectInput: updatingProps} = await this._extractLinksFromObjectInput({
      modelDefinition,
      objectInput: updatingProps,
      links
    }));

    this.setIndexDisabled();

    return this._graphControllerService.updateNode({
      modelDefinition,
      objectInput: {
        id: objectId,
        ...(updatingProps || {}),
      },
      lang,
      links
    });
  }

  /**
   * Update objects with same props (called batch update).
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string[]} [objectIds]
   * @param {ResolverArgs} [args={}]
   * @param {string[]} [filters]
   * @param {object} updatingProps
   * @param {Link[]} links -  List of links
   * @param {string|null} lang
   *
   * @return {Model[]}
   */
  async updateObjects({modelDefinition, objectIds = [], args = {}, updatingProps, links, lang} = {}) {
    const pageSize = 1000;
    let updatedNodes = [];
    let eligibleIds = [];

    if(objectIds?.length === 0 && Object.keys(args).length === 0){
      throw new I18nError("objectIds or args must be passed to batch update items");
    }

    if(objectIds?.length !== 0){
      args.ids = objectIds;
    }

    const totalCount = await this.getObjectsCount({
      modelDefinition, args
    })

    // First get eligible ids using standard search function.
    for(let index = 0; index < Math.ceil(totalCount/pageSize); index++){
      args.first = pageSize;
      args.offset = pageSize * index;

      const objects = await this.getObjects({
        modelDefinition,
        args,
        fetchIdsOnly: true
      })

      eligibleIds = eligibleIds.concat(objects.map(({id}) => id));
    }

    ({links, objectInput: updatingProps} = await this._extractLinksFromObjectInput({
      modelDefinition,
      objectInput: updatingProps,
      links
    }));

    // Then remove them step by step.
    for(let index = 0; index < Math.ceil(totalCount/pageSize); index++){
      const nodes = await this._graphControllerService.updateNodes({
        modelDefinition,
        ids:  eligibleIds.slice(index * pageSize, pageSize * (index + 1)),
        objectsInput: updatingProps || {},
        links,
        lang,
      });

      updatedNodes = updatedNodes.concat(nodes);
    }

    await new Promise((done) => setTimeout(done, 2000));

    this.setIndexDisabled();

    return updatedNodes;
  }

  /**
   * Remove object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {boolean} [permanentRemoval] - Is the object just disabled (must use a graph middleware) or removed in graph.
   */
  async removeObject({modelDefinition, objectId, permanentRemoval = false} = {}) {
    this.setIndexDisabled();

    return this._graphControllerService.removeNode({
      modelDefinition,
      id: objectId,
      permanentRemoval
    });
  }

  /**
   * Remove objects
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {array} [objectIds]
   * @param {ResolverArgs} [args={}]
   * @param {boolean} [permanentRemoval] - Are objects just disabled (must use a graph middleware) or removed in graph.
   *
   * @return {array} List of deleted ids.
   */
  async removeObjects({modelDefinition, objectIds, args = {}, permanentRemoval = false} = {}) {
    const pageSize = 1000;
    let eligibleIds = [];

    if(!objectIds && Object.keys(args).length === 0){
      throw new I18nError("objectIds or args must be passed to batch remove items");
    }

    if(objectIds){
      args.ids = objectIds;
    }

    const totalCount = await this.getObjectsCount({
      modelDefinition, args
    })

    // First get eligible ids using standard search function.
    for(let index = 0; index < Math.ceil(totalCount/pageSize); index++){
      args.first = pageSize;
      args.offset = pageSize * index;

      const objects = await this.getObjects({
        modelDefinition,
        args,
        fetchIdsOnly: true
      })
      eligibleIds = eligibleIds.concat(objects.map(({id}) => id));
    }

    // Then remove them step by step.
    for(let index = 0; index < Math.ceil(totalCount/pageSize); index++){
      await this._graphControllerService.removeNodes({
        modelDefinition,
        ids: eligibleIds.slice(index * pageSize, pageSize * (index + 1)),
        permanentRemoval
      });
    }

    await new Promise((done) => setTimeout(done, 2000));

    return eligibleIds;
  }

  /**
   * Create edge between two nodes and re-index them
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId) {
    this.setIndexDisabled();

    await this._graphControllerService.createEdge(modelDefinition, objectId, linkDefinition, targetId);

    let object = await this.updateObject({
      modelDefinition,
      objectId
    });

    let target = await this.updateObject({
      modelDefinition: linkDefinition.getRelatedModelDefinition(),
      objectId: targetId
    });

    return {
      object,
      target
    };
  }

  /**
   * Create edges between a node and target nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string[]} targetIds
   *
   * @return {Model}
   */
  async createEdges(modelDefinition, objectId, linkDefinition, targetIds) {
    this.setIndexDisabled();

    for (let targetId of targetIds) {
      await this._graphControllerService.createEdge(modelDefinition, objectId, linkDefinition, targetId);
      await this.updateObject({
        modelDefinition: linkDefinition.getRelatedModelDefinition(),
        targetId: objectId
      });
    }
    await this.updateObject({modelDefinition, objectId})
  }

  /**
   * Remove edge between nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async removeEdge(modelDefinition, objectId, linkDefinition, targetId) {
    this.setIndexDisabled();

    await this._graphControllerService.deleteEdges([{modelDefinition, objectId, linkDefinition, targetId}]);

    let object = await this.updateObject({modelDefinition, objectId});
    let target = await this.updateObject({
      modelDefinition: linkDefinition.getRelatedModelDefinition(),
      objectId: targetId
    });

    return {
      object,
      target
    };
  }

  /**
   * Remove a bunch of edges between nodes and re-index them
   * @param {Array<{modelDefinition: ModelDefinitionAbstract, objectId: string, linkDefinition: LinkDefinition, targetId: string}>} edges
   * 
   * @return {Array<{object: Model, target: Model}>}
   */
   async removeEdges(edges) {
    this.setIndexDisabled();

    await this._graphControllerService.deleteEdges(edges);

    let updatedPairs = [];
    for (let {modelDefinition, objectId, linkDefinition, targetId} of edges) {
      let object = await this.updateObject({modelDefinition, objectId});
      let target = await this.updateObject({
        modelDefinition: linkDefinition.getRelatedModelDefinition(),
        objectId: targetId
      });
      updatedPairs.push({object, target})
    }

    return updatedPairs

  }

  //
  // Utilities
  //

  /**
   * Get an GraphQL objectInput and extract links from them.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]}  links
   */
  async _extractLinksFromObjectInput({modelDefinition, objectInput, links}) {
    objectInput = objectInput || {};
    links = links || [];

    return Object.entries(objectInput).reduce(async (acc, [property, value]) => {
      let {objectInput, links} = await acc;

      let linkDefinition = modelDefinition.getLinks().find(link => link.getGraphQLInputName() === property);

      if (linkDefinition) {
        let targetInputs = Array.isArray(value) ? value : [value];

        for (let targetInput of targetInputs) {
          // If target input is filled as null or id is specifically set as null, this is a deletion link.
          if (targetInput === null || (targetInput?.id === null)) {
            links.push(linkDefinition.generateDeletionLink());
            // If target input has an id, this is a link creation to an existing object with optional update.
          } else if (targetInput?.id) {
            let targetId = this.extractIdFromGlobalId(targetInput.id);
            let targetObjectInput = omit(targetInput, ["id", "inheritedTypename"]);
            let targetNestedLinks;
            let targetModelDefinition = this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(
              await this.extractTypeFromGlobalId(targetId)
            );

            // Even if targetId exists, so a link creation is requested to an existing object,
            // it could be useful to be able to update "in place" this existing object hierarchy.
            // For that, just extract hypothetical nested links.
            ({links: targetNestedLinks, objectInput: targetObjectInput} = await this._extractLinksFromObjectInput({
              modelDefinition: targetModelDefinition,
              objectInput: targetObjectInput,
            }));

            links.push(linkDefinition.generateLinkFromTargetProps({
              targetId,
              targetObjectInput,
              targetModelDefinition,
              targetNestedLinks
            }));
            // Else this is a link creation to a object to create
          } else if (Object.keys(targetInput).length > 0) {
            /**
             * The following test ensures that input object related model definition is instantiable.
             * If not instantiable:
             *  - It's possible to pass an "inheritedTypename" parameter that refers to an instiable inherited model definition.
             *  - Otherwise throw an Error.
             */
            if (linkDefinition.getRelatedModelDefinition().isInstantiable() || targetInput.inheritedTypename) {
              let targetNestedLinks;
              let targetModelDefinition = targetInput.inheritedTypename ? this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(targetInput.inheritedTypename) : linkDefinition.getRelatedModelDefinition();
              ({links: targetNestedLinks, objectInput: targetInput} = await this._extractLinksFromObjectInput({
                modelDefinition: targetModelDefinition,
                objectInput: targetInput,
              }));

              links.push(linkDefinition.generateLinkFromTargetProps({
                targetObjectInput: targetInput,
                targetNestedLinks,
                targetModelDefinition
              }));

            } else {
              let inheritedModelDefinitions = this.getModelDefinitionsRegister().getInheritedModelDefinitionsFor(linkDefinition.getRelatedModelDefinition());

              throw new I18nError(`Input object described in "${linkDefinition.getGraphQLInputName()}" property refers to the model definition "${linkDefinition.getRelatedModelDefinition().name}"  that is defined as not instantiable. Please specify "inheritedTypename" property with a inherited type that refered to a model definition defined as instantiable. That is to say one of these : [${inheritedModelDefinitions.map(inheritedModelDefinition => inheritedModelDefinition.getGraphQLType()).join(", ")}]. 
  {
    ${linkDefinition.getGraphQLInputName()}{
       inheritedTypename: "${inheritedModelDefinitions.map(inheritedModelDefinition => inheritedModelDefinition.getGraphQLType()).join(" | ")}"
       ...
    }
  }            
`, "NOT_INSTANTIABLE_OBJECT_INPUT")
            }
          }
        }
      } else {
        let linkDefinition = modelDefinition.getLinks().find(link => link.getGraphQLInputName() === property.replace("ToDelete", ""));

        if (linkDefinition) {
          if (value && Array.isArray(value)) {
            for (let targetId of value) {
              links.push(linkDefinition.generateDeletionLink({
                targetId: this.extractIdFromGlobalId(targetId)
              }))
            }
          }
        } else {
          objectInput[property] = value;
        }
      }

      return Promise.resolve({objectInput, links});

    }, Promise.resolve({objectInput: {}, links: links || []}));
  }
}