/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This is an error extended with a i18n key
 */
export class I18nError extends Error {
  /**
   * @param {string} message
   * @param {string} [i18nKey=SERVER_ERROR]
   * @param {number} [statusCode=500]
   */
  constructor(message, i18nKey = "SERVER_ERROR", statusCode = 500, captureInSentry = false) {
    super();
    this.message = message;
    this.i18nKey = i18nKey || "SERVER_ERROR";
    this.statusCode = statusCode;
    this.name = this.constructor.name;
    this.captureInSentry = captureInSentry || false;
  }
}