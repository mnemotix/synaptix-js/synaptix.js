/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {I18nError} from "./I18nError";


export class ApiValidationError extends I18nError {
    /**
     * @param {string} message
     * @param {string} [i18nKey]
     * @param {object} [customInfos]
     * @param {string} [statusCode]
     * @param {boolean} [captureInSentry]
     */
    constructor(message, i18nKey = "API_VALIDATION_ERROR", customInfos = null, statusCode = 400, captureInSentry = false) {
      super(message, i18nKey, statusCode, captureInSentry);
      this.customInfos = customInfos;
    }
}