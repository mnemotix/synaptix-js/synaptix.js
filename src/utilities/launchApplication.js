/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import chalk from 'chalk';
import got from "got";
import cookieParser from 'cookie-parser';
import { doubleCsrf } from "csrf-csrf";
import bodyParser from 'body-parser';
import cors from 'cors';
import env from 'env-var';
import expressWinston from "express-winston";
import {initEnvironment} from "./environment/initEnvironment";
import {
  generateSSOEndpoint as generateDefaultSSOEndpoint
} from "./express/generators/generateSSOEndpoint";
import {logError, logInfo} from "./logger";
import {generateGraphQLEndpoint} from "./express/generators/generateGraphQLEndpoint";
import {attachDatastoreSession} from "./express/middlewares/attachDatastoreSession";
import ExpressApp from './express/ExpressApp';
import SSOApiClient from "../datamodules/drivers/sso/SSOApiClient";
import SynaptixPackage from "../../package.json";
import {generateDefaultDatastoreAdapter} from "./express/generators/generateDefaultDatastoreAdapter";
import {DataModel} from "../datamodel/DataModel";
import {logger} from "./logger/logger";

/**
 * @typedef {object} GraphQLEndpointDefinition
 * @property {string} endpointURI - URI of endpoint
 * @property {string} [datastoreKey=default]
 * @property {string} [endpointSubscriptionsURI] - Enable subscriptions providing an URI of the endpoint
 * @property {DataModel} [dataModel] - Executable GraphQL Schema
 * @property {Schema} [graphQLSchema] - Executable GraphQL Schema
 * @property {DatastoreAdapterAbstract} [datastoreAdapter] - Datastore adapater to resolve data
 * @property {boolean} [acceptAnonymousRequest] - Disable authentication
 * @property {boolean} [disableAuthRedirection] - Disable auth redirection and send 401 response.
 */

/**
 * @callback GraphQLEndpointsCallback
 * @param {Object} options - options
 * @param {ExpressApp} options.app - Express app
 * @param {SSOApiClient} [options.ssoApiClient] ssoApiClient
 * @param {SSOMiddleware[]} options.ssoMiddlewares
 * @returns {[GraphQLEndpointDefinition]}
 */


/**
 * Launch an application.
 *
 * @param {object} Package - the npm package.json object
 * @param {object} environment - List of environment variables definition
 * @param {[GraphQLEndpointDefinition]} graphQLEndpoints
 * @param {GraphQLEndpointsCallback} generateGraphQLEndpoints - A function returning a list of GraphQL endpoints definitions
 * @param {Array.<function>} launchMiddlewares - Apply middleware before launching express.
 * @param {function} addUserAttributesOnRegister
 * @param {SSOEndpointCallback} [generateSSOEndpoint]
 * @param {SSOMiddleware[]} [ssoMiddlewares]
 * @param {function[]} [initMiddlewares] - Apply middleware after creating express application
 * @param {SSOApiClient} [ssoApiClient]
 * @return {Promise<{app: ExpressApp}>}
 */
export async function launchApplication({Package, environment, graphQLEndpoints, generateGraphQLEndpoints, launchMiddlewares, generateSSOEndpoint, ssoApiClient, ssoMiddlewares, initMiddlewares}){
  if (!generateGraphQLEndpoints && !graphQLEndpoints) {
    throw new Error("You must provide a 'generateGraphQLEndpoints' callback to provide GraphQL endpoint definitions or provide them directy in 'graphQLEndpoints'");
  }

  /* istanbul ignore next */
  logInfo(`Running ${chalk.yellow.bold(Package.name.toUpperCase())}...`);
  logInfo(`Version ${chalk.yellow(Package.version)}`);
  logInfo(`Built on top of @synaptix/server (${chalk.yellow(SynaptixPackage.version)})`);
  logInfo(`Coded with ${chalk.red('♥')} by ${chalk.underline.yellow('Mnemotix')}\n`);

  initEnvironment(environment);

  /** @type {ExpressApp} */
  let app = new ExpressApp();
  const entrypoint = app.getServer();

  if (initMiddlewares) {
    for (let initMiddleware of initMiddlewares) {
      await initMiddleware({app});
    }
  }

  if(env.get("HTTP_REQUESTS_LOGGING_ENABLED").asBool()){
    app.use(expressWinston.logger({
      winstonInstance: logger,
      requestWhitelist: ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query', 'body'],
      headerBlacklist: ['cookie'],
      ignoreRoute: function (req, res) {
        return !!req.originalUrl.match(/(^\/a\/|\.js|\.ico$)/)
      },
    }))
  }

  if (!env.get("CORS_DISABLED").asBool()) {
    app.use(cors({
      origin: env.get("CORS_ORIGIN").default("*").asString(),
      methods:  env.get("CORS_METHODS").default("GET,HEAD,PUT,PATCH,POST,DELETE").asString(),
      allowedHeaders: env.get("CORS_METHODS").asString(),
      credentials:  env.get("CORS_CREDENTIALS").default("1").asBool(),
      preflightContinue: false,
      optionsSuccessStatus: 204
    }));
  }

  app.use(cookieParser());
  app.use(bodyParser.json({ limit: env.get("BODY_PARSER_LIMIT").default("100kb") }));
  app.use(bodyParser.urlencoded({extended: true}));

  if (!ssoApiClient && !env.get("OAUTH_DISABLED").asBool()){
    ssoApiClient = new SSOApiClient({
      apiTokenEndpointUrl:  env.get("OAUTH_ADMIN_TOKEN_URL").required().asString(),
      apiEndpointUrl: env.get("OAUTH_ADMIN_API_URL").required().asString(),
      apiLogin: env.get("OAUTH_ADMIN_USERNAME").required().asString(),
      apiPassword: env.get("OAUTH_ADMIN_PASSWORD").required().asString(),
    });
  }

  if(generateGraphQLEndpoints) {
    graphQLEndpoints = await generateGraphQLEndpoints({app, ssoApiClient, ssoMiddlewares});
  }

  //
  // Add datastores to ExpressApp
  //
  for (let [index, {datastoreAdapter, datastoreKey, dataModel}] of Object.entries(graphQLEndpoints)) {
    //
    // That lines enable to setup a default datastore adapter.
    //
    if(!datastoreAdapter && dataModel instanceof DataModel){
      datastoreAdapter = generateDefaultDatastoreAdapter({
        dataModel,
        ssoApiClient
      });

      graphQLEndpoints[index].graphQLSchema = dataModel.generateExecutableSchema();
      graphQLEndpoints[index].datastoreAdapter = datastoreAdapter;
    }

    if(datastoreAdapter){
      app.addDatastoreAdapter({
        datastoreAdapter,
        key: datastoreKey || "default"
      });
    }
  }

  let authenticate = () => (req, res, next) => next();

  if(ssoApiClient){
    const certsUrl = env.get("OAUTH_CERTS_URL").asString();
    let certificates = {};
    //
    // Init authentication middleware
    //
    logInfo(`Generating SSO endpoint on 
  - Authentication   : ${env.get("OAUTH_AUTH_URL").asString()}
  - Token validation : ${env.get("OAUTH_TOKEN_URL").asString()}
  - Logout           : ${env.get("OAUTH_LOGOUT_URL").asString()}
  - Admin token      : ${env.get("OAUTH_ADMIN_TOKEN_URL").asString()}
  - Admin API        : ${env.get("OAUTH_ADMIN_API_URL").asString()}
  - Certificates     : ${certsUrl}
  `);

    if(certsUrl){
      logInfo("Fetching SSO certificates....")
      const {body: certs} = await got(certsUrl, {responseType: 'json'});
      certificates = (certs.keys || []).reduce((certificates, {kid, x5c}) => {
        certificates[kid] = Array.isArray(x5c) ? x5c[0] : x5c;
        return certificates;
      }, {})

      logInfo(`Found ${Object.values(certificates).length}.`)
    }

    /** @type {SSOEndpointDefinition} */
    ({authenticate} = generateSSOEndpoint
      ? generateSSOEndpoint({app})
      : generateDefaultSSOEndpoint(app, {
        authorizationURL: env.get("OAUTH_AUTH_URL").asString(),
        tokenURL: env.get("OAUTH_TOKEN_URL").asString(),
        logoutURL: env.get("OAUTH_LOGOUT_URL").asString(),
        clientID: env.get("OAUTH_REALM_CLIENT_ID").asString(),
        clientSecret: env.get("OAUTH_REALM_CLIENT_SECRET").asString(),
        baseURL: env.get("APP_URL").asString(),
        registrationEnabled: !env.get("OAUTH_REGISTRATION_DISABLED").asBool(),
        certificates,
        ssoApiClient
      }));
  } else {
    logInfo(`SSO authentication is ${chalk.bold("disabled")}`);
  }



  // Securing "/heartbeat" URL is usefull to regenerate token if outdated.
  if (env.get("NODE_ENV").asString() !== 'test') {
    app.use('/heartbeat', authenticate({acceptAnonymousRequest: true}));
  }

  app.get('/heartbeat', (req, res) => {
    return res.status(200).send("︎💔🎈🏓🎉🕶");
  });

  const csfrProtectionEnabled = env.get("CSRF_DSC_PROTECTION_ENABLED").asBool();
  const {
    generateToken: generateCsrfToken,
    validateRequest: validateCsrfToken
  } = doubleCsrf({
    getSecret: () => env.get("UUID").asString(), // A function that optionally takes the request and returns a secret
    cookieName: "x-csrf-hash", // The name of the cookie to be used, recommend using Host prefix.
    cookieOptions: {
      httpOnly: true,
      sameSite: "strict",  // Recommend you make this strict if posible
      path: "/",
      secure: true
    },
    size: 64, // The size of the generated tokens in bits
    ignoredMethods: ["GET", "HEAD", "OPTIONS"], // A list of request methods that will not be protected.
    getTokenFromRequest: (req) => req.headers["x-csrf-token"], // A function that returns the token from the request
  });

  app.get("/csrf-token", (req, res) => {
    return res.json({
      token: generateCsrfToken(res, req),
    });
  });

  logInfo(`CSRF double submission cookie Token/Token_hash available at ${env.get("APP_URL").asString()}/csrf-token`)

  for (let {endpointURI, graphQLSchema, datastoreAdapter, acceptAnonymousRequest, disableAuthRedirection} of  graphQLEndpoints) {
    let endpointAbsoluteURI = `${env.get("APP_URL").asString()}${endpointURI}`.replace('//', '/');
    let playgroundURI = (endpointURI + '/playground').replace('//', '/');

    if (env.get("NODE_ENV").asString() !== 'test') {
      app.use(endpointURI, authenticate({acceptAnonymousRequest, disableAuthRedirection}));
      app.get(playgroundURI, authenticate({acceptAnonymousRequest, disableAuthRedirection}), (req, res, next) => {
        if(csfrProtectionEnabled){
          res.setHeader('x-csrf-token', generateCsrfToken(res, req));
        }
        next();
      });
    }

    app.get(endpointURI, (req, res) => {
      res.redirect(playgroundURI);
    });

    await generateGraphQLEndpoint(app, {
      schema: graphQLSchema,
      endpointURI,
      datastoreAdapter,
      acceptAnonymousRequest,
      validateCsrfToken: csfrProtectionEnabled && validateCsrfToken
    });

    logInfo(`GraphQL endpoint listening on ${chalk.blue.underline(endpointAbsoluteURI)}`);
    logInfo(`  - SSO Authentication ${chalk.bold(acceptAnonymousRequest ? chalk.red("disabled") : chalk.green('enabled'))} `);
    logInfo(`  - CSRF double submission cookie ${chalk.bold(csfrProtectionEnabled ? chalk.green('enabled') : chalk.red("disabled") )}`);

    logInfo(`GraphQL playground started at ${env.get("APP_URL").asString()}${playgroundURI}`);
  }

  //
  // Call extra middlewares
  //
  if (launchMiddlewares) {
    for (let launchMiddleware of launchMiddlewares) {
      await launchMiddleware({
        app,
        authenticate,
        ssoApiClient,
        ssoMiddlewares,
        attachDatastoreSession({acceptAnonymousRequest, datastoreAdapter} = {}){
          return attachDatastoreSession({ app, acceptAnonymousRequest, datastoreAdapter })
        }
      });
    }
  }

  try {
    for (let {datastoreAdapter} of graphQLEndpoints) {
      await datastoreAdapter?.init({});
    }

    entrypoint.listen(env.get("APP_PORT").asInt(), '0.0.0.0', (err) => {
      if (err) {
        logError(err);
      }

      logInfo(`ExpressJS server started and listening on port ${chalk.red.bold(env.get("APP_PORT").asInt())}...`);
    });
  } catch (err) {
    logError(err);
    process.exit(1);
  }

  return {app, authenticate};
}

