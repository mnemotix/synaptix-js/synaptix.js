/**
 * JSON Schemas (spec http://json-schema.org/) describing the expected JSON format for messages used
 * by the SSO endpoint "login" (request, success and error responses)
 */


const requestJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/login/request",
  "type": "object",
  "properties": {
    "username": {"type": "string"},
    "password": {"type": "string"}
  },
  "additionalProperties": false,
  "required": ["username", "password"]
};

/* TODO */
const successResponseJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/login/successResponse",
  "type": "object"
};

const errorResponseJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/login/errorResponse",
  "oneOf": [{
    /* Bad request error */
    "type": "object",
    "properties": {
      "errors": {
        "type": "object",
        "properties": {
          "username": {"type": "string"},
          "password": {"type": "string"},
          "additionalProperties": false
        }
      },
    },
    "additionalProperties": false,
    "required": ["errors"]
  }, {
    /* Unexpected error */
    "type": "object",
    "properties": {
      "error": {"type": "string"}
    },
    "additionalProperties": false,
    "required": ["error"]
  }]
};

export const SSOEndpointLoginJSONSchemas = {
  request: requestJSONSchema,
  successResponse: successResponseJSONSchema,
  errorResponse: errorResponseJSONSchema
};

