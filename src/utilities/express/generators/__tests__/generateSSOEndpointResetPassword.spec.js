import request from 'supertest';
import bodyParser from 'body-parser';
import {generateSSOEndpoint} from '../generateSSOEndpoint';
import SSOApiClient from '../../../../datamodules/drivers/sso/SSOApiClient';
import {SSOEndpointMessageExamples} from '../../../../testing';
import {SSOEndpointResetPasswordJSONSchemas} from '../__jsonSchemas__/SSOEndpointResetPasswordJSONSchemas';
import ExpressApp from "../../ExpressApp";
import SSOUser from "../../../../datamodules/drivers/sso/models/SSOUser";
import SSOControllerService from "../../../../datamodules/services/sso/SSOControllerService";

jest.mock('../../../../datamodules/drivers/sso/SSOApiClient');
jest.mock('../../middlewares/attachDatastoreSession');
jest.mock('../../../../datamodules/services/sso/SSOControllerService');

let app;
let ssoApiClient = new SSOApiClient({
  tokenEndpointUrl: '/api/token',
  apiEndpointUrl: '/api',
  login: "123",
  password: "123"
});

beforeEach(() => {
  app = new ExpressApp();
  app.use(bodyParser.json());

  generateSSOEndpoint(app, {
    authorizationURL: '/auth',
    tokenURL: '/token',
    logoutURL: '/logout',
    clientID: '123',
    clientSecret: '123',
    baseURL: '/',
    adminUsername: '123',
    adminPassword: '123',
    adminTokenUrl: '/admin/token',
    adminApiUrl: '/api',
    registrationEnabled: true,
    ssoApiClient
  });
  SSOApiClient.mockClear();
});

// noinspection JSUnusedLocalSymbols,JSUnusedLocalSymbols,JSUnusedLocalSymbols
let myUser = new SSOUser({
  user: {
    id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
    createdTimestamp: 1553185843355,
    username: 'test@domain.com',
    enabled: true,
    totp: false,
    emailVerified: false,
    firstName: 'John',
    lastName: 'Doe',
  }
});

describe('/reset-password endpoint', () => {
  describe('successfull request', () => {
    it('returns OK and called resetUserPasswordByMail', async () => {
      let requestJSON = JSON.parse(SSOEndpointMessageExamples['POST /auth/reset-password'].request);

      let response = await request(app.getOriginalApp())
        .post('/auth/reset-password')
        .send(requestJSON);

      expect(response.status).toEqual(200);

      SSOControllerService.prototype.resetPassword.mockImplementation(() => {
      });

      /* Expect resetUserPasswordByMail to have been called with correct user id */
      expect(SSOControllerService.prototype.resetPassword.mock.calls.length).toEqual(1);
    });
  });

  describe('unexpected error handling', () => {
    test('when calling resetUserPasswordByMail', async () => {
      SSOControllerService.prototype.resetPassword.mockImplementation(() => {
        throw new Error('Another unexpected error');
      });

      let response = await request(app.getOriginalApp())
        .post('/auth/reset-password')
        .send({username: 'test@domain.com'});

      expect(response.status).toEqual(500);
      expect(response.body).toEqual({
        error: "Error: Another unexpected error"
      });
      expect(response.body).toMatchSchema(SSOEndpointResetPasswordJSONSchemas.errorResponse);
    });
  });
});

