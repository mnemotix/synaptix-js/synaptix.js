/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {logDebug, logError, logWarning} from '../../logger';
import {Authenticator} from 'passport';
import OAuth2Strategy from 'passport-oauth2';
import LocalStrategy from 'passport-local';
import env from "env-var";
import {OAuth2RefreshTokenStrategy} from "../../passport/OAuth2RefreshTokenStrategy";
import {check} from 'express-validator/check';
import SSOApiClient from "../../../datamodules/drivers/sso/SSOApiClient";
import SSOUser from "../../../datamodules/drivers/sso/models/SSOUser";
import {sendValidationErrorsJSON} from "../utils/sendValidationErrorsJSON";
import {attachDatastoreSession} from "../middlewares/attachDatastoreSession";

/**
 * Setup a default SSO login process.
 * @param {ExpressApp} app Express application
 * @param {SSOApiClient} ssoApiClient
 * @param {string} authorizationURL
 * @param {string} tokenURL
 * @param {string} logoutURL
 * @param {string} clientID
 * @param {string} clientSecret
 * @param {object} certificates
 * @param {string} baseURL
 * @param {boolean} [registrationEnabled=false]
 *
 * @return {SSOEndpointDefinition}}
 */
export let generateSSOEndpoint = (app, {ssoApiClient, authorizationURL, tokenURL, logoutURL, clientID, clientSecret, baseURL, registrationEnabled, certificates = {}}) => {
  if (!authorizationURL) {
    throw new Error(`SSO enpoint authorizationURL must be provided`);
  }

  if (!tokenURL) {
    throw new Error(`SSO enpoint tokenURL must be provided`);
  }

  if (!logoutURL) {
    throw new Error(`SSO enpoint logoutURL must be provided`);
  }

  if (!clientID || !clientSecret) {
    throw new Error(`SSO enpoint clientID and clientSecret must be provided`);
  }

  if (!baseURL) {
    throw new Error(`Application baseUrl must be provided`);
  }

  if(!certificates || Object.values(certificates).length === 0){
    logWarning("Caution, OAuth JWT signing certificate is not provided. This might lead to JWT authentication bypass. Please provide it in OAUTH_CERT variable.")
  }

  let passport = new Authenticator();

  let refreshStrategy = new OAuth2RefreshTokenStrategy({
    userProperty: 'ticket', // Active user property name to store OAuth tokens
  });

  passport.use('default', refreshStrategy);

  let oauth2Strategy = new OAuth2Strategy({
      authorizationURL,
      tokenURL,
      clientID,
      clientSecret,
      callbackURL: `${baseURL}/auth/login/callback`,
      passReqToCallback: false
    },
    refreshStrategy.getOAuth2StrategyCallback()
  );

  passport.use('oauth2', oauth2Strategy);
  refreshStrategy.useOAuth2Strategy(oauth2Strategy);

  let localStrategy = new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password'
    },
    refreshStrategy.getLocalStrategyCallback() //Create a callback for LocalStrategy
  );

  passport.use('local', localStrategy);
  refreshStrategy.useLocalStrategy(localStrategy);


  /**
   * @param req
   * @param res
   * @param next
   * @param {SSOUser} user
   * @return {Promise<*>}
   */
  let afterLogin = async ({req, res, next, user}) => {
    if (!req.refererUser || user.getAccessToken() !== req.refererUser.getAccessToken()) {
      //logDebug("ACCESS_TOKEN CHANGED, refresh cookie !");
      user.toCookie(res);
      
      if (env.get("USE_AUTH_HEADER").default("0").asBool()) {
        user.setAuthHeader(res);
      }
    }

    // for(let middleware of middlewares || []) {
    //   await middleware.afterLogin({user, ssoApiClient, datastoreSession})
    // }

    return next ? next() : res.json(user.toJSON());
  };

  /** @type {AuthenticationMiddleware} */
  let authenticate = ({acceptAnonymousRequest, disableAuthRedirection} = {}) => {
    return (req, res, next) => {
      let jwt = req.query?.jwt;

      if (!jwt && env.get("USE_AUTH_HEADER").default("0").asBool()) {
        const authHeader = req.headers['authorization'];
        if (authHeader && authHeader.indexOf(" ") > -1) {
          const [authType, authToken] = authHeader.split(" ");
          if (authType === "Bearer") {
            jwt = authToken;
          }
        }
      }

      // If JWT is detected in query parameters, attempt to log the user.
      if(jwt){
        req.user = SSOUser.fromJWT({jwt, certificates});
      } else {
        req.user = SSOUser.fromCookie(req.cookies || {}, certificates);
        req.refererUser = SSOUser.fromCookie(req.cookies || {}, certificates);
      }

      passport.authenticate('default', {
        session: false,
      }, (err, user, ticket) => {
        if (err) {
          return next(err);
        }

        if(ticket && ticket.access_token){
          user = new SSOUser({jwtSession: {ticket}});
        }

        // Authentication has failed.
        // If acceptAnonymousRequest is true => go on !
        // If disableAuthRedirection is true => send a 401 status
        // Else redirect to login page.
        if (!user) {
          SSOUser.clearCookie(res);

          if (acceptAnonymousRequest) {
            return next();
          } else if (disableAuthRedirection) {
            return res.sendStatus(401);
          } else {
            return res.redirect(`${baseURL}/auth/login`);
          }
        }

        // Set user as req.user and call afterLogin
        req.logIn(user, {session: false}, () => afterLogin({req, res, next, user}));
      })(req, res, next);
    };
  };

  app.use(passport.initialize({}));

  /**
   * SSO login route for oauth2 token registration.
   */
  app.get('/auth/login', (req, res, next) => {
    /** @namespace req.query.redirectURI */
    let redirectURI = req.query.redirectURI || '/';

    if (req.isAuthenticated()) {
      return res.redirect(redirectURI);
    } else {
      SSOUser.clearCookie(res);
      passport.authenticate('oauth2', {session: false}, null)(req, res, next);
    }
  });

  /**
   * SSO login route for oauth2 username/password registration.
   *
   * @api {post} /auth/login
   *
   * @apiDescription It takes a JSON as input, with username and password.
   * In case of success it returns HTTP 200 with the oauth credentials as JSON, transferred as is from keycloak.
   * In case of invalid input parameters it returns HTTP 400.
   *
   * @apiParam {String} username
   * @apiParam {String} password
   *
   * @apiParamExample {json}
   *    {
   *      "username": "myuser@domain.com",
   *      "password": "mypassword"
   *    }
   *
   * @apiSuccessExample {json}
   *    HTTP/1.1 200 OK
   *    "ticket": {
   *      "access_token": "hashaccesstoken",
   *      "refresh_token": "hashrefreshtoken",
   *      "expires": 1554978396
   *    }
   *
   * @apiError 400  The error 400 is returned in case of the following input errors : username is empty, password is empty.
   *                Or in case of another unexpected error (probably during keycloak mechanism).
   *                See the following example for the format of the JSON error payload.
   *
   * @apiErrorExample {json}
   *    HTTP/1.1 400 Bad Request
   *    {
   *      "errors": {
   *        "username": "Username is empty",
   *        "password": "Password is empty"
   *      }   
   *    }
   *
   * @apiErrorExample {json}
   *    HTTP/1.1 400 Bad Request
   *    {
   *      "error": "Invalid user credentials"
   *    }
   *
   */
  app.post('/auth/login', [
    check('username', 'Username is empty').exists({checkFalsy: true}),
    check('password', 'Password is empty').exists({checkFalsy: true}),
    sendValidationErrorsJSON,
  ], (req, res, next) => {
    passport.authenticate('local', {session: false}, (err, jwtSession, info) => {
      if (err || !jwtSession) {
        let reason = info ? info.message : 'Unable to login';

        logError(err);

        if (err && err.data) {
          reason = JSON.parse(err.data).error_description;
        }

        res.status(400).send({
          error: reason,
        });

        return next();
      }

      let user = new SSOUser({jwtSession});

      // Set user as req.user and call afterLogin
      req.logIn(user, {session: false}, () => afterLogin({req, res, user, info}));
    })(req, res, next);
  });

  /**
   * Default SSO login callback route.
   */
  app.get('/auth/login/callback', passport.authenticate('oauth2',
    {
      failureRedirect: '/auth/login',
      session: false
    }, null),
    async (req, res, next) => {
      // ⚠⚠⚠
      // @see https://github.com/artema/passport-oauth2-middleware
      // In this callback `req.user` contains the jwtSession format of authenticated user and NOT a SSOUser instance.
      // This is the middleware behaviour, `user` naming is very confusing.
      // ⚠⚠⚠

      let user = new SSOUser({jwtSession: req.user});
      user.toCookie(res);
      res.redirect('/');
    }
  );

  /**
   * Default SSO logout route.
   */
  app.get('/auth/logout', (req, res) => {
    let logoutCallbackUri = `${baseURL}/auth/logout/callback`;
    res.redirect(`${logoutURL}?redirect_uri=${encodeURIComponent(logoutCallbackUri)}`);
  });

  /**
   * Default SSO logout callback route.
   */
  app.get('/auth/logout/callback', async (req, res) => {
    SSOUser.clearCookie(res);
    res.redirect('/');
  });

  /**
   * Reinit password by mail for a given username
   *
   * @api {post} /auth/reset-password-by-mail
   *
   * @apiDescription This request a password reset by mail for a given username. It will trigger the sending of an email
   * to the user, allowing them to reset their password.
   *
   * @apiSuccessExample {json}
   *    HTTP/1.1 200 OK
   *
   * @apiError 401  The error 401 is returned in case of anonymous session.
   *
   * @apiErrorExample {json}
   *    HTTP/1.1 400 Bad Request
   *    {
   *      "errors": {
   *        "username": "Username is empty"
   *      }
   *    }
   *
   * @apiError 500  The error 500 is returned in case of an unexpected exception. The exception error message is given in the
   *                JSON payload (see the following example)
   *
   * @apiErrorExample {json}
   *    HTTP/1.1 500 Internal Server Error
   *    {
   *      "error": "MyExceptionType: An unexpected error occured"
   *    }
   */
  app.post('/auth/reset-password-by-mail', [
    sendValidationErrorsJSON,
    check('username', 'Username is empty').exists({checkFalsy: true}),
    attachDatastoreSession({datastoreAdapter: app.getDefaultDatastoreAdapter(), acceptAnonymousRequest: true}),
  ], async (req, res) => {
    /** @type {SynaptixDatastoreSession} datastoreSession */
    let datastoreSession = req.datastoreSession;
    let {username} = req.body;

    try {
      await datastoreSession.getSSOControllerService().resetPasswordByMail({
        username,
        redirectUri: req.body.redirectURI || baseURL,
        clientId: clientID
      });
      logDebug(`Password reset OK for ${username}`);
      res.sendStatus(200);
    } catch (e) {
      logError(`Password reset ERROR for ${username} :`, e);
      res.status(500).json({error: e.toString()});
    }
  });

  /**
   * Reinit password for logged user
   *
   * @api {post} /auth/reset-password
   *
   * @apiDescription This request a password reset for a logged user.
   *
   * @apiParam {String} oldPassword
   * @apiParam {String} newPassword
   * @apiParam {String} newPasswordConfirm
   *
   * @apiSuccessExample {json}
   *    HTTP/1.1 200 OK
   *
   * @apiError 401  The error 401 is returned in case of anonymous session.
   *
   * @apiError 500  The error 500 is returned in case of an unexpected exception. The exception error message is given in the
   *                JSON payload (see the following example)
   *
   * @apiErrorExample {json}
   *    HTTP/1.1 500 Internal Server Error
   *    {
   *      "error": "MyExceptionType: An unexpected error occured"
   *    }
   */
  app.post('/auth/reset-password', [
    sendValidationErrorsJSON,
    check('oldPassword', 'Old password is empty').exists({checkFalsy: true}),
    check('newPassword', 'New password is empty').exists({checkFalsy: true}),
    check('newPasswordConfirm', 'New password is empty').exists({checkFalsy: true}),
    attachDatastoreSession({datastoreAdapter: app.getDefaultDatastoreAdapter(), acceptAnonymousRequest: true}),
  ], async (req, res, next) => {
    /** @type {SynaptixDatastoreSession} datastoreSession */
    let datastoreSession = req.datastoreSession;

    let {oldPassword, newPassword, newPasswordConfirm} = req.body;

    if (newPassword !== newPasswordConfirm){
      res.status(500).json({error: "New password confirmation doesn't match new password."});
    }

    if (!datastoreSession.getContext().isAnonymous()) {
      /** @type {SSOUser} */
      let user = await datastoreSession.getContext().getUser();

      try {
        await datastoreSession.getSSOControllerService().resetPassword({oldPassword, newPassword});
        logDebug(`Password reset OK for ${user.getEmail()}`);
        res.sendStatus(200);
      } catch (e) {
        logError(`Password reset ERROR for ${user.getEmail()} :`, e);
        res.status(500).json({error: e.toString()});
      }
    } else {
      res.status(401).json({"error": "Anonymous session not allowed. User must be logged to reset is password."});
    }
  });

  if (registrationEnabled) {
    /**
     * Register account
     *
     * @api {post} /auth/register Register a new user
     *
     * @apiDescription This registers a new user. It takes a JSON as input, with username and password field mandatory.
     * The username should not already exist. In case of success it returns HTTP 200 with the created user as JSON. In
     * case of invalid input parameters it returns HTTP 400.
     *
     * @apiParam {String} username
     * @apiParam {String} password
     * @apiParam {String} [firstName]
     * @apiParam {String} [lastName]
     * @apiParam {Boolean} [isTemporaryPassword]
     *
     * @apiParamExample {json}
     *    {
     *      "username": "myuser@domain.com",
     *      "password": "mypassword",
     *      "firstName": "John",
     *      "lastName": "Doe"
     *    }
     *
     * @apiSuccess {String} username
     * @apiSuccess {String} password
     * @apiSuccess {String} firstName
     * @apiSuccess {String} lastName
     * @apiSuccessExample {json}
     *    HTTP/1.1 200 OK
     *    {
     *      "username": "myuser@domain.com",
     *      "password": "mypassword",
     *      "firstName": "John",
     *      "lastName": "Doe"
     *    }
     *
     * @apiError 400  The error 400 is returned in case of the following input errors : username is empty, password is empty,
     *                a user with the given username already exists. See the following example for the format of the JSON error payload.
     *
     * @apiErrorExample {json}
     *    HTTP/1.1 400 Bad Request
     *    {
     *      "errors": {
     *        "username": "Username is empty",
     *        "password": "Password is empty"
     *      }   
     *    }
     *
     * @apiError 500  The error 500 is returned in case of an unexpected exception. The exception error message is given in the
     *                JSON payload (see the following example)
     *
     * @apiErrorExample {json}
     *    HTTP/1.1 500 Internal Server Error
     *    {
     *      "error": "MyExceptionType: An unexpected error occured"
     *    }
     */
    app.post('/auth/register', [
        check('username', 'Username is empty').exists({checkFalsy: true}),
        check('password', 'Password is empty').exists({checkFalsy: true}),
        check('username').custom(async (username) => {
          if (await ssoApiClient.isUsernameExists(username)) {
            logWarning(`An account already exists with this email ${username}."`);
            return Promise.reject("An account already exists with this email");
          }
        }),
        sendValidationErrorsJSON,
      ],
      attachDatastoreSession({datastoreAdapter: app.getDefaultDatastoreAdapter(), acceptAnonymousRequest: true}),
      async (req, res) => {
        /** @type {SynaptixDatastoreSession} datastoreSession */
        let datastoreSession = req.datastoreSession;

        try {
          let user = await datastoreSession.getSSOControllerService().registerUserAccount({
            ...req.body,
            email: req.body.email || req.body.username,
            userAttributes: req.body.attributes
          });
          res.json(user.toJSON());
        } catch (e) {
          logError(`Account creation error : `, e);
          return res.status(500).json({
            error: e.message
          });
        }
      });
  }

  return {authenticate, passport};
};

/**
 * @callback SSOEndpointCallback
 * @param {Object} options - options
 * @param {ExpressApp} options.app - Express app
 * @returns {SSOEndpointDefinition}
 */
/**
 * @typedef {object} SSOEndpointDefinition
 * @property {Authenticator} passport - Passport instance
 * @property {AuthenticationMiddleware} authenticate - Authentication middleware used by Express to secure URLs.
 * @property {SSOApiClient} ssoApiClient - SSOApiClient instance.
 */
/**
 * @callback AuthenticationMiddleware
 * @param options
 * @param {boolean} [options.disableAuthRedirection] - Send 401 if auth fails.
 * @param {boolean} [options.acceptAnonymousRequest] - Continue if auth fails
 */