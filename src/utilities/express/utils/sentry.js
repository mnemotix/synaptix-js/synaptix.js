import * as Sentry from '@sentry/node';
import { I18nError } from '../../error/I18nError'

/**
 * Filter errors which are sent to Sentry
 * @param {*} ctx 
 * @returns 
 */
function filterErrorsCapturedBySentry(ctx) {
    return ctx.errors.filter(error => 
        !(error.originalError instanceof I18nError)
        || !!error.originalError.captureInSentry
    )
}


/**
 * Thanks to https://medium.com/@mahyor.sam/tracking-errors-in-apollo-graphql-with-sentry-549ae52c0c76
 * 
 * @returns 
 */
export function captureGqlErrorInSentry(ctx) {

    const errors = filterErrorsCapturedBySentry(ctx)

    // If we couldn't parse the operation (usually invalid queries)
    if (!ctx.operation) {
        for (const err of errors) {
            Sentry.withScope(scope => {
                scope.setExtra('query', ctx.request.query);
                Sentry.captureException(err);
            });
        }
        return;
    }

    for (const err of errors) {
        // Add scoped report details and send to Sentry
        Sentry.withScope(scope => {
            // Annotate whether failing operation was query/mutation/subscription
            scope.setTag('kind', ctx.operation.operation);

            // Log query and variables as extras (make sure to strip out sensitive data!)
            scope.setExtra('query', ctx.request.query);
            scope.setExtra('variables', JSON.stringify(ctx.request.variables));

            if (err.path) {
                // We can also add the path as breadcrumb
                scope.addBreadcrumb({
                    category: 'query-path',
                    message: err.path.join(' > '),
                    level: Sentry.Severity.Debug,
                });
            }

            const transactionId = ctx.request.http.headers.get(
                'x-transaction-id',
            );
            if (transactionId) {
                scope.setTransaction(transactionId);
            }

            Sentry.captureException(err);
        });
    }
}