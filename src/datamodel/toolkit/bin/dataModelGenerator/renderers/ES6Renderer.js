import AbstractRenderer from './AbstractRenderer';
import {
    HEADER_CODE
} from '../utils';


/**
 * Definition files renderer for ES6 format
 *
 */
export default class ES6Renderer extends AbstractRenderer {

    /**
     * @inheritdoc
     */
    renderDefinitionCode({
        synaptixImports,
        classesImports,
        linkDefinitionsImports,
        parentDefinitions,
        namespace,
        model,
        indexType
    }) {
        return `${HEADER_CODE}

import {
    ${synaptixImports.join(',\n    ')}
} from "@mnemotix/synaptix.js";
${classesImports.concat(linkDefinitionsImports).join('\n')}


export default class ${model.name}Definition extends ModelDefinitionAbstract {

${(!model.isInstantiable()) ? this._renderIsIntantiableMethod(false) : '' }
${(parentDefinitions.length > 0) ? this._renderGetParentDefinitionsMethod(parentDefinitions) : '' }

    /**
     * @inheritDoc
     */
    static getRdfType() {
        return "${namespace}:${model.name}";
    }

    /**
     * @inheritDoc
     */
    static getGraphQLDefinition() {
        return ${model.isInstantiable() ? 'GraphQLTypeDefinition' : 'GraphQLInterfaceDefinition'};
    }
    
    /**
     * @inheritDoc
     */
    static getIndexType() {
        return ${indexType};
    }

${(Object.values(model.links).length > 0) ? this._renderGetLinksMethod(model) : '' }
${(Object.values(model.labels).length > 0) ? this._renderGetLabelsMethod(model) : '' }
${(Object.values(model.literals).length > 0) ? this._renderGetLiteralsMethod(model) : '' }

};

`;
    }

    /**
     * @inheritdoc
     */
    renderDefinitionIndexCode(namespace, definitionsNames) {
        const uppercasedNamespace = namespace.charAt(0).toUpperCase() + namespace.slice(1);
        return `${HEADER_CODE}

import { DataModel } from "@mnemotix/synaptix.js";
${definitionsNames.map(definition => (
    `import ${definition} from "./${definition}";`
)).join('\n')}


export let ${uppercasedNamespace}ModelDefinitions = {
    ${definitionsNames.join(',\n    ')}
};

export let ${namespace}DataModel = new DataModel({
    modelDefinitions: ${uppercasedNamespace}ModelDefinitions
});
`;
    };


    /**
     * @inheritdoc
     */
    _renderIsIntantiableMethod(isInstantiable) {
        return `
    /**
     * @inheritDoc
     */
    static isInstantiable(){
        return ${isInstantiable};
    }`;
    }

    /**
     * @inheritdoc
     */
    _renderGetParentDefinitionsMethod(parentDefinitions) {
        return `
    /**
     * @inheritDoc
     */
    static getParentDefinitions(){
        return [${parentDefinitions.join(',')}];
    }`;
    }

    /**
     * @inheritdoc
     */
    _renderGetLinksMethod(model) {
        return `
    /**
     * @inheritDoc
     */
    static getLinks() {
        return [
            ${(model.hasSuperClass()) ? `...super.getLinks(),` : ''}
            ${Object.values(model.links).map(link => (
            `new LinkDefinition({
                linkName: '${link.name}',
                pathInIndex: '${link.name}',
                rdfObjectProperty: '${link.type.namespace}:${link.name}',
                relatedModelDefinition: ${(link.type.namespace === 'mnx')
                    ? `MnxOntologies.mnxAgent.ModelDefinitions.${link.type.value}Definition`
                    : `${link.type.value}Definition`
                },
                isCascadingUpdated: true,
                isCascadingRemoved: true,
                isPlural: ${link.isPlural()},
                ${link.isTargetInstantiable()
                    ? `graphQLInputName: '${link.name}Input${link.isPlural() ? 's' : ''}'`
                    : ''
                }
            })`)).join(',\n\t\t\t')}
        ]
    }`;
    }

    /**
     * @inheritdoc
     */
    _renderGetLabelsMethod(model) {`
    /**
     * @inheritDoc
     */
    static getLabels() {
        return [
            ${(model.hasSuperClass()) ? `...super.getLabels(),` : ``}
            ${Object.values(model.labels).map(label => (
            `new LabelDefinition({
                labelName: '${label.name}',
                pathInIndex: '${label.name}s',
                rdfDataProperty: '${label.namespace}:${label.name}'
            })`)).join(',\n\t\t\t')}
        ];
    }`;
    }

    /**
     * @inheritdoc
     */
    _renderGetLiteralsMethod(model) {
        return `
    /**
     * @inheritDoc
     */
    static getLiterals() {
        return [
            ...super.getLiterals(),
            ${Object.values(model.literals).map(literal => (
            `new LiteralDefinition({
                literalName: '${literal.name}',
                rdfDataProperty: '${literal.namespace}:${literal.name}',
                rdfDataType: '${literal.type.prefix}${literal.type.value}'
            })`)).join(',\n\t\t\t')}
        ];
    }`;
    }

};
