import {
    NetworkLayerAMQP,
    DataModel,
    GraphQLContext,
    MnxDatamodels,
    SynaptixDatastoreRdfSession
} from '../../../../../'; 
import { extractPrefixAndName } from '../utils';
import { RDFModel } from './RdfModel';
import { RDF_MODELS_QUERY } from './query';


/**
 * Create a Synaptix session
 * 
 */
export const createSession = async () => {
    // Init network layer.
    const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;
    const amqpNetworkLayer = new NetworkLayerAMQP(amqpURL, process.env.RABBITMQ_EXCHANGE_NAME, {}, {
        durable: !!parseInt(process.env.RABBITMQ_EXCHANGE_DURABLE || 1)
    });
    await amqpNetworkLayer.connect();

    let dataModel = new DataModel({})
        .mergeWithDataModels([MnxDatamodels.mnxCoreDataModel, MnxDatamodels.mnxAgentDataModel])
        .addDefaultSchemaTypeDefs();

    return new SynaptixDatastoreRdfSession({
        context: new GraphQLContext({
            anonymous: true,
            lang: "fr"
        }),
        networkLayer: amqpNetworkLayer,
        modelDefinitionsRegister: dataModel.generateModelDefinitionsRegister(),
        schemaNamespaceMapping: JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING),
        nodesNamespaceURI: process.env.NODES_NAMESPACE_URI,
        nodesPrefix: process.env.NODES_PREFIX,
    })
}


/**
 * Fetch all classes and their prperties bound to namespace in graph database
 * 
 */
export const inspectRdfStore = async ({ prefix, modelName }) => {    

    // Query returning properties of any object of type owl:Class,
    // filtered by namespace or model name
    const query = RDF_MODELS_QUERY(prefix, modelName);

    // Get a Synaptix datastore adapter
    const synaptixSession = await createSession();

    // Run query and get models in raw response
    const rdfPublisher = synaptixSession.getGraphControllerService().getGraphControllerPublisher();
    const response = await rdfPublisher.select({ query });

    const nodesByModel = {};

    // Group nodes by model name
    response.results.bindings.map(node => {
        const [prefix, nodeName] = extractPrefixAndName(node.model.value);
        if (!(nodeName in nodesByModel)) {
            nodesByModel[nodeName] = [];
        }
        nodesByModel[nodeName].push(node);
    });

    let rdfModels = {}
    // Create a RDFModel object for each set of nodes
    for (let modelName in nodesByModel)
        rdfModels[modelName] = new RDFModel(nodesByModel[modelName][0]);

    // Set inheritance an sameAs relationships for each model
    for (let modelName in nodesByModel) {
        const modelNodes = nodesByModel[modelName];
        let rdfModel = rdfModels[modelName]

        modelNodes.filter(node => typeof(node.equivalentClassName) !== 'undefined').map(node => {
            let [prefix, name] = extractPrefixAndName(node.model.value);
            let [_, equivalentClassName] = extractPrefixAndName(node.equivalentClassName.value);
            if (name !== equivalentClassName)
                rdfModels[name].addEquivalentClass(rdfModels[equivalentClassName])
        });

        modelNodes.filter(node => typeof(node.superClassName) !== 'undefined').map(node => {
            const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);

            let [prefix, name] = extractPrefixAndName(node.model.value);
            let [superPrefix, superName] = extractPrefixAndName(node.superClassName.value);

            // If prefix of superClass is not in namespace mapping, we don't generate inheritance relationship
            const superNamespace = Object.keys(prefixes).find(key => prefixes[key] === superPrefix)
            if (superNamespace) {
                rdfModel.setSuperClass(superNamespace, superName);
                if (superName in rdfModels) {
                    const namespace = Object.keys(prefixes).find(key => prefixes[key] === prefix);
                    rdfModels[superName].addSubClass(namespace, name);
                }     
            }  
        })
        rdfModels[modelName] = rdfModel;
    }

    // Set data / object properties to each model
    for (let modelName in nodesByModel) {
        const modelNodes = nodesByModel[modelName];
        let rdfModel = rdfModels[modelName]
        modelNodes
            .filter(node => (typeof(node.propType) !== 'undefined' && node.valueType && node.valueType.type !== 'bnode'))
            .map(node => rdfModel.addProperty(node, rdfModels));
        rdfModels[modelName] = rdfModel;
    }

    return rdfModels;
}