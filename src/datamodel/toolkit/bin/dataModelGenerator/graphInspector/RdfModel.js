import { LabelProperty, LiteralProperty, LinkProperty } from "./RdfProperty";
import { extractPrefixAndName } from "../utils";



/**
 * RDF model parsed from a SPARQL query
 *  
 */
export class RDFModel {

    /**
     * Create a object model from a SPARQL query response node 
     * 
     * @param {*} node 
     */
    constructor(node) {
        [this.prefix, this.name] = extractPrefixAndName(node.model.value);
        this.links = {};
        this.labels = {};
        this.literals = {};

        const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);
        this.namespace = Object.keys(prefixes).find(key => prefixes[key] === this.prefix);

        // Super class in data model
        this.superClass = null;
        // Sub classesin data model
        this.subClasses = [];
        // Equivalent classes in graph
        this.equivalentClasses = new Set();
        // Define if model is abstract or can be instantiated
        this.instantiable = true;
    }

    /**
     * Add a RDFProperty to model 
     * 
     * @param {*} node 
     */
    addProperty(node, rdfModels) {
        const [_, propName] = extractPrefixAndName(node.propName.value);
        const [__, propType] = extractPrefixAndName(node.propType.value);
        const [valuePrefix, valueType] = extractPrefixAndName(node.valueType.value);

        const LABELS_RDF_DATATYPES = ['string', 'Name', 'NCName'];
        if (propType === 'DatatypeProperty' && LABELS_RDF_DATATYPES.indexOf(valueType) > -1) {
            this.setLabel(node)
        } else if (propType === 'DatatypeProperty' && valueType !== 'string') {
            this.setLiteral(node)
        } else if (propType === 'ObjectProperty') {
            this.setLink(node, rdfModels)
        } else {
            throw new Error(
                `Property ${propName} of type ${this.name} is not accepted as literal, label or link...`
            );
        }
    }

    /**
     * Set info property contained in node about a iteral
     * @param {} node 
     */
    setLiteral(node) {
        const [___, propName] = extractPrefixAndName(node.propName.value);

        // if property is already defined in labels, we don't add it
        if (propName in this.labels) {
            //throw new Error(`Property ${propName} in type ${this.name} is already defined as label...`)
            return;
        }

        if (!(propName in this.literals)) {
            this.literals[propName] = new LiteralProperty(node);
        }
        let currentProperty = this.literals[propName];

        if (node.valueName?.['xml:lang']){
            currentProperty.addLabel(node.valueName['xml:lang'], node.valueName.value);
        }
    }

    /**
     * Set info property contained in node about a label
     * 
     * @param {*} node 
     */
    setLabel(node) {
        const [___, propName] = extractPrefixAndName(node.propName.value);

        // if property is already defined in literals, we don't add it
        if (propName in this.literals) {
            return;
        }

        if (!(propName in this.labels)) {
            this.labels[propName] = new LabelProperty(node);
        }
        let currentProperty = this.labels[propName];

        if(node.valueName?.['xml:lang']){
            currentProperty.addLabel(node.valueName['xml:lang'], node.valueName.value);
        }
    }

    /**
     * Set info property contained in node about a link
     * 
     * @param {*} node 
     */
    setLink(node, rdfModels) {
        const [___, propName] = extractPrefixAndName(node.propName.value);
        const [_, propType] = extractPrefixAndName(node.propType.value);
        const [valuePrefix, valueType] = extractPrefixAndName(node.valueType.value);

        if (!(propName in this.links)) {
            this.links[propName] = new LinkProperty(node);
        }
        let currentProperty = this.links[propName];
        if(node.valueName?.['xml:lang']){
            currentProperty.addLabel(node.valueName['xml:lang'], node.valueName.value);
        }

        if (node.cardinalityType && node.cardinality) {
            currentProperty.addCardinality(node.cardinalityType.value, node.cardinality.value)
        }

        // TODO: very ugly ! Behavior specific to links... refactor introspecter part of generator ! 
        if (propType === 'ObjectProperty') {
            const isTargetInstantiable = (rdfModels[valueType]) ? rdfModels[valueType].isInstantiable() : true;
            currentProperty.setTargetInstantiable(isTargetInstantiable)
        }
    }

    setSuperClass(namespace, name) {
        // if model name in argument is already a equivalent class, we don't consider it as a super class
        let equivalentClasses = this.getEquivalentClasses().map(model => model.name);
        if (equivalentClasses.indexOf(name) == -1)
            this.superClass = { namespace,  name };
    }

    hasSuperClass() {
        return this.superClass != null;
    }

    addSubClass(namespace, name) {
        // if model name in argument is already a equivalent class, we don't consider it as a sub class
        let equivalentClasses = this.getEquivalentClasses().map(model => model.name);
        if (equivalentClasses.indexOf(name) == -1) {
            this.subClasses[name] = { namespace, name }
            // If a model has a sub class, we consider it as abstract and not instantiable
            this.instantiable = false;
        }   
    }

    isInstantiable() {
        return this.instantiable;
    }

    addEquivalentClass(rdfModel) {
        if (!this.equivalentClasses.has(rdfModel)) {
            this.equivalentClasses.add(rdfModel)
            rdfModel.addEquivalentClass(this)
        }
    }

    getEquivalentClasses() {
        return [...this.equivalentClasses];
    }

}