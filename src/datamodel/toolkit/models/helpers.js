/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

/**
 * Parse a synaptix graphStore node to a  model.
 *
 * @param ModelClass
 * @param {Node} node
 * @returns {Model}
 */
export function createModelFromNode(ModelClass, node) {
  return new ModelClass(node.getId(), node.getUri(), node.getProperties(), node.getNodeType());
}

/**
 * Parse a synaptix indexStore document to a model
 * @param ModelClass
 * @param document
 * @returns {Model}
 */
export function createModelFromDocument(ModelClass, document) {
  // TODO: check the last property when index OK.
  return new ModelClass(document._id || document.id, document.uri, document, document.type);
}