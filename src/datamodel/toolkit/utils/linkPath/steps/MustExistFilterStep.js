/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Step} from "./Step";

export class MustExistFilterStep extends Step{
  /**
   * @param {LinkPath[]} linkPath
   */
  constructor({linkPath, subject, target}){
    super();
    this._subject = subject;
    this._target = target;
    this._linkPath = linkPath || throw new TypeError("Link path must be provided");
  }

  /**
   * @return {string}
   */
  getSubject() {
    if (!this._subject) {
      return null;
    }
    return this._subject.startsWith('<http://') ? this._subject : `?${this._subject}`;
  }

  /**
   * @return {string}
   */
  getTarget() {
    if (!this._target) {
      return null;
    }
    return this._target.startsWith('<http://') ? this._target : `?${this._target}`;
  }

  /**
   * @return {LinkPath[]}
   */
  getLinkPath() {
    return this._linkPath;
  }
}
