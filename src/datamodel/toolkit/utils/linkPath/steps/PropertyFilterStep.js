/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Step} from "./Step";
import {PropertyFilter} from "../../filter";

export class PropertyFilterStep extends Step{
  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {PropertyFilter} propertyFilter
   * @param {*} value - @deprecated Use propertyFilter instead
   */
  constructor({propertyDefinition, value, propertyFilter}){
    super();
    if(!value && !propertyFilter ){
      throw new TypeError("One of 'value' or 'propertyFilter' must be provided")
    }
    this._propertyDefinition = propertyDefinition || throw new TypeError("propertyDefinition must be provided");
    this._value = value;
    this._propertyFilter = propertyFilter
  }

  /**
   * @return {PropertyDefinitionAbstract}
   */
  getPropertyDefinition() {
    return this._propertyDefinition;
  }

  /**
   * @return {string|number}
   */
  getValue() {
    return this._value;
  }

  /**
   * @returns {PropertyFilter}
   */
  getPropertyFilter(){
    return this._propertyFilter;
  }

  /**
   * @return {boolean}
   */
  isLiteral(){
    return !!this._literalDefiniton;
  }
}