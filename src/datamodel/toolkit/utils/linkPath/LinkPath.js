/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {LinkStep} from "./steps/LinkStep";
import {PropertyStep} from "./steps/PropertyStep";
import {PropertyFilterStep} from "./steps/PropertyFilterStep";
import {BindMustExistStep} from "./steps/BindMustExistStep";
import {BindMustNotExistStep} from "./steps/BindMustNotExistStep";
import {BindStep, ConcatStep} from "./steps";
import {BindCountStep} from "./steps/BindCountStep";
import {Link} from "../../definitions";
import {LinkFilter} from "../filter";
import {UnionStep} from "./steps/UnionStep";
import { MustExistFilterStep } from "./steps/MustExistFilterStep";
import { MustNotExistFilterStep } from "./steps/MustNotExistFilterStep";

export class LinkPath{
  /**
   * @param {Link} indexedShortcutLink - Use this shortcut link to improve performance. Link must reference LinkDefinition tagged with option "inIndexOnly".
   * @param {LinkFilter} indexedShortcutLinkFilter - Use this shortcut link filter to improve performance. LinkFilter must reference LinkDefinition tagged with option "inIndexOnly".
   */
  constructor({indexedShortcutLink, indexedShortcutLinkFilter} = {}) {
    if (indexedShortcutLink instanceof Link && indexedShortcutLink.getLinkDefinition().isInIndexOnly()){
      this._indexedShortcutLink = indexedShortcutLink;
    }

    if(indexedShortcutLinkFilter instanceof LinkFilter){
      this._indexedShortcutLinkFilter = indexedShortcutLinkFilter;
    }

    this._steps = [];
  }

  /**
   * @return {Step[]}
   */
  getSteps(){
    return this._steps;
  }

  /**
   * @return {Link}
   */
  getIndexedShortcutLink() {
    return this._indexedShortcutLink;
  }

  /**
   * @return {LinkFilter}
   */
  getIndexedShortcutLinkFilter() {
    return this._indexedShortcutLinkFilter;
  }

  /**
   * @return {LinkPath}
   */
  clone(){
    return (new LinkPath()).append({linkPath: this})
  }

  /**
   * @param {LinkDefinition} linkDefinition
   * @param {string} [targetId]
   * @param {LinkFilter} [linkFilter]
   * @param {boolean} [reversed=false]
   * @param {boolean} [typeAssertionDiscarded] - Disable the assertion of link related target type.
   */
  step({linkDefinition, targetId, linkFilter, reversed, typeAssertionDiscarded}){
    if(linkDefinition.getLinkPath()){
      return (new LinkPath())
        .append({linkPath: this})
        .append({linkPath: linkDefinition.getLinkPath()})
    } else {
      this._steps.push(new LinkStep({linkDefinition, reversed, targetId, typeAssertionDiscarded, linkFilter}));
      return this;
    }
  }

  /**
   * @param {LinkPath[]} linkPaths
   */
  union({linkPaths}){
    this._steps.push(new UnionStep({linkPaths}));
    return this;
  }

  /**
   * @param {LinkPath[]} linkPaths
   * @param {string} separator
   */
  concat({linkPaths, separator}){
    this._steps.push(new ConcatStep({linkPaths, separator}));
    return this;
  }

  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {string} [rdfDataPropertyAlias]
   */
  property({propertyDefinition, rdfDataPropertyAlias}){
    if(propertyDefinition.getLinkPath()){
      return (new LinkPath())
        .append({linkPath: this})
        .append({linkPath: propertyDefinition.getLinkPath()});
    } else {
      this._steps.push(new PropertyStep({propertyDefinition, rdfDataPropertyAlias}));
      return this;
    }
  }

  /**
   * @param {LinkPath} linkPath
   * @param {string} bindAs
   * @param {string} rdfDataPropertyAlias
   */
  mustExistPropertyBinding({linkPath, bindAs, rdfDataPropertyAlias}){
    this._steps.push(new BindMustExistStep({linkPath, bindAs, rdfDataPropertyAlias}));
    return this;
  }

  /**
   * @param {LinkPath} linkPath
   * @param {string} bindAs
   * @param {string} rdfDataPropertyAlias
   */
  mustNotExistPropertyBinding({linkPath, bindAs, rdfDataPropertyAlias}){
    this._steps.push(new BindMustNotExistStep({linkPath, bindAs, rdfDataPropertyAlias}));
    return this;
  }

  /**
   * @param {LinkPath} linkPath
   * @param {string} bindAs
   * @param {string} rdfDataPropertyAlias
   */
  countPropertyBinding({linkPath, bindAs, rdfDataPropertyAlias}){
    this._steps.push(new BindCountStep({linkPath, bindAs, rdfDataPropertyAlias}));
    return this;
  }

  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {PropertyFilter} propertyFilter
   * @param {*} value - @deprecated Use propertyFilter instead
   */
  filterOnProperty({propertyDefinition, propertyFilter, value}){
    this._steps.push(new PropertyFilterStep({propertyDefinition, propertyFilter, value}));
    return this;
  }

  /**
   * @param {LinkPath[]} linkPath
   */
  filterOnExistsPath({linkPath, subject, target}){
    this._steps.push(new MustExistFilterStep({linkPath, subject, target}));
    return this;
  }

  /**
   * @param {LinkPath[]} linkPath
   */
  filterOnNotExistsPath({linkPath, subject, target}){
    this._steps.push(new MustNotExistFilterStep({linkPath, subject, target}));
    return this;
  }

  /**
   * Reverse the path
   */
  reverse(){
    this._steps = this._steps.reverse();
    return this;
  }

  /**
   * @return {Step}
   */
  getLastStep(){
    return this._steps?.[this._steps.length - 1];
  }

  /**
   * @return {LinkStep}
   */
  getLastLinkStep(){
    return this._steps.reverse().find(step => step instanceof LinkStep);
  }

  /**
   * @return {PropertyStep}
   */
  getLastPropertyStep(){
    return this._steps.reverse().find(step => step instanceof PropertyStep || step instanceof BindStep);
  }

  /**
   * Append step of another link path
   * @param {LinkPath} linkPath
   */
  append({linkPath}){
    this._steps = [].concat(this._steps, linkPath.getSteps());
    return this;
  }
}