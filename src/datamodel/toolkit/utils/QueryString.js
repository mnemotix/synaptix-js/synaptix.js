
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const QueryStringType = {
  MATCH: "MATCH",
  MATCH_PREFIX: "MATCH_PREFIX",
  TERM: "TERM",
  REGEX: "REGEX"
}

export class QueryString{
  /**
   * @param {PropertyDefinitionAbstract} [propertyDefinition]
   * @param {string} [value]
   * @param {string} [type=MATCH]
   */
  constructor({propertyDefinition, value, type = QueryStringType.MATCH}){
    this._propertyDefinition = propertyDefinition;
    this._type     = type;
    this._value = value;
  }

  get propertyDefinition() {
    return this._propertyDefinition;
  }

  get value() {
    return this._value;
  }

  get type() {
    return this._type;
  }


  set propertyDefinition(value) {
    this._propertyDefinition = value;
  }

  set value(value) {
    this._value = value;
  }

  set type(value) {
    this._type = value;
  }
}