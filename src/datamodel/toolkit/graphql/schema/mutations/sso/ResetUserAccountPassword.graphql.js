/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {I18nError} from "../../../../../../utilities/error/I18nError";
import {object, string, ref} from "yup";
import {FormValidationError} from "../../middlewares/formValidation/FormValidationError";

export let ResetUserAccountPasswordMutation = `
"""Reset account mutation payload"""
type ResetUserAccountPasswordPayload {
  success: Boolean
}

"""Reset account mutation input"""
input ResetUserAccountPasswordInput {
  oldPassword: String 
  newPassword: String 
  newPasswordConfirm: String 
}

extend type Mutation{
  """ 
  This mutation input parameters are inforced by Yup middleware and returns a FORM_VALIDATION_ERROR listing input error details.
  
  Possible i18n error keys are :
  
    - IS_REQUIRED
    - PASSWORD_TO_SHORT
    - PASSWORDS_DO_NOT_MATCH
    - WRONG_OLD_PASSWORD
  """
  resetUserAccountPassword(input: ResetUserAccountPasswordInput!): ResetUserAccountPasswordPayload
}
`;

export let ResetUserAccountPasswordMutationResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {string} redirectUri
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    resetUserAccountPassword: {
      validationSchema: object().shape({
        input: object().shape({
          oldPassword:  string().trim().required("IS_REQUIRED"),
          newPassword: string().trim().required("IS_REQUIRED").min(6, "PASSWORD_TOO_SHORT"),
          newPasswordConfirm: string().oneOf([ref('newPassword')], "PASSWORDS_DO_NOT_MATCH")
        })
      }),
      resolve: async (_, {input: {oldPassword, newPassword}}, synaptixSession) => {
        try{
          await synaptixSession.getSSOControllerService().resetPassword({oldPassword, newPassword});
        } catch (e) {
          // Simulate FormValidationError
          if(e instanceof I18nError && e.i18nKey === "INVALID_CREDENTIALS"){
            throw new FormValidationError(e.message, [{
              field: "input.oldPassword",
              errors: [
                "WRONG_OLD_PASSWORD"
              ]
            }])
          }
          throw e;
        }
        return {success: true};
      }
    }
  },
};