/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  batchUpdateObjectResolver,
  createObjectInConnectionResolver,
  updateObjectResolver
} from "../../helpers/resolverHelpers";
import lowerCase from "lodash/lowerCase";

/**
 * Generate "update" node mutation based on GraphQL type
 *
 * @param {string} type
 * @param {string} [extraInputArgs] extra inputs
 * @param {string} [description] A human readable mutation description to add to documentation.
 * @param {string} [batchDescription] A human readable batch mutation description to add to documentation.
 * @return {string}
 */
export const generateUpdateMutationDefinitionForType = (type, description, batchDescription, extraInputArgs) => `
input Update${type}Input {
  objectId: ID!
  objectInput: ${type}Input
  ${extraInputArgs || ""}
}

type Update${type}Payload {
  updatedObject: ${type}
  success: Boolean
}

input BatchUpdate${type}Input {
  objectIds: [ID!]
  filters: [String]
  objectsInput: ${type}Input
  ${extraInputArgs || ""}
}

type BatchUpdate${type}Payload {
  updatedObjects: [${type}]
  success: Boolean
}

extend type Mutation{
  """
  ${description || `Mutation to update a ${lowerCase(type)}.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.`}
  """
  update${type}(input: Update${type}Input!): Update${type}Payload
  
  """
  ${batchDescription || `Mutation to batch update a list of ${lowerCase(type)}.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.`}
  """
  batchUpdate${type}(input: BatchUpdate${type}Input!): BatchUpdate${type}Payload
}
`;

/**
 * Generate "update" node mutation based on ModelDefinition
 *
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {string} [description] A human readable mutation description to add to documentation.
 * @param {string} [batchDescription] A human readable batch mutation description to add to documentation.
 * @param {string} [extraInputArgs] extra inputs
 * @return {string}
 */
export const generateUpdateMutationDefinition = ({modelDefinition, description, batchDescription, extraInputArgs}) => generateUpdateMutationDefinitionForType(modelDefinition.getGraphQLType(), description, batchDescription, extraInputArgs);


/**
 * Generate "update" node resolver based on GraphQL type
 *
 * @param {string} type
 * @param {function} [resolver] Resolver function
 * @param {function} [batchResolver] Batch resolver function
 * @return {object}
 */
export const generateUpdateMutationResolverForType = (type, resolver, batchResolver) => ({
  Mutation: {
    [`update${type}`]: resolver || updateObjectResolver.bind(this, type),
    [`batchUpdate${type}`]: batchResolver || batchUpdateObjectResolver.bind(this, type)
  }
});

/**
 * Generate "update" node resolver based on ModelDefinition
 *
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {function} [resolver] Resolver function
 *  @param {function} [batchResolver] Batch resolver function
 * @return {object}
 */
export const generateUpdateMutationResolver = ({modelDefinition, resolver, batchResolver}) =>
  generateUpdateMutationResolverForType(
    modelDefinition.getGraphQLType(),
    resolver || updateObjectResolver.bind(this, modelDefinition),
    batchResolver || batchUpdateObjectResolver.bind(this, modelDefinition)
  );

/**
 * Generate "create" node mutation based on GraphQL type
 *
 * @param {string} type mutation type name
 * @param {string} [extraInputArgs] extra inputs
 * @param {string} [edgeType = type] returned edge type
 * @param {string} [description] A human readable mutation description to add to documentation.
 * @return {string}
 */
export const generateCreateMutationDefinitionForType = (type, extraInputArgs, edgeType, description) => `
input Create${type}Input {
  objectInput: ${edgeType || type}Input
  ${extraInputArgs || ""}
}

type Create${type}Payload {
  createdObject: ${type}
  createdEdge: ${edgeType || type}Edge
  success: Boolean
}

extend type Mutation{
  """
  ${description || `Mutation to create a ${lowerCase(type)}.
  
  This mutation input is protected by the Synaptix.js. Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.`}
  """
  create${type}(input: Create${type}Input!): Create${type}Payload
}
`;

/**
 * Generate "create" node mutation based on ModelDefinition
 *
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {string} [extraInputArgs] extra inputs
 * @param {string} [edgeType = type] returned edge type
 * @param {string} [description] A human readable mutation description to add to documentation.
 * @return {string}
 */
export const generateCreateMutationDefinition = ({modelDefinition, extraInputArgs, edgeType, description}) => generateCreateMutationDefinitionForType(modelDefinition.getGraphQLType(),  extraInputArgs, edgeType, description);

/**
 * Generate "create" node resolver based on GraphQL type
 *
 * @param {string} type
 * @param {LinkDefinition[]|function} [linksOrResolver] Array of definition
 * @return {object}
 */
export const generateCreateMutationResolverForType = (type, linksOrResolver) => ({
  Mutation: {
    [`create${type}`]: typeof linksOrResolver === "function" ? linksOrResolver : createObjectInConnectionResolver.bind(this, type, linksOrResolver)
  }
});

/**
 * Generate "create" node resolver based on ModelDefinition
 *
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {LinkDefinition[]} [linkDefinitions] Array of definition
 * @param {function} [resolver] Array of definition
 * @return {object}
 */
export const generateCreateMutationResolver = ({modelDefinition, linkDefinitions, resolver}) =>
  generateCreateMutationResolverForType(
    modelDefinition.getGraphQLType(),
    linkDefinitions || resolver || createObjectInConnectionResolver.bind(this, modelDefinition, linkDefinitions || resolver)
  );