/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import GraphQLProperty from './GraphQLProperty';
import { GraphQLCreateMutation, GraphQLUpdateMutation } from './GraphQLMutation';


/**
 * This module is an interface for defining a GraphQL type schema
 * 
 */
export default class GraphQLDefinition {

    /**
     * Return if graphQL type must be 
     */
    static isTypeGenerated() {
        throw new Error(`You must implement ${this.name}::isTypeGenerated method`);
    }

    /**
     * Return a type query
     * 
     * @returns {GraphQLQuery}
     */
    static getTypeQuery() {
        return null;
    }

    /**
     * Return a connection type mutation
     * 
     * @returns {GraphQLQuery}
     */
    static getTypeConnectionQuery() {
        return null;
    }

    /**
     * Return a create mutation
     * 
     * @returns {GraphQLCreateMutation}
     */
    static getCreateMutation() {
        return null;
    }

    /**
     * Return a update mutation
     * 
     * @returns {GraphQLUpdateMutation}
     */
    static getUpdateMutation() {
        return null;
    }

    /**
     * Return a remove mutation
     * If null, mutation removeObject can be used with id of object
     * 
     * @returns {GraphQLRemoveMutation}
     */
    static getRemoveMutation() {
        return null;
    }

    /**
     * Return extra field queries to add to GraphQL schema
     * (and rather related to defined type)
     * 
     * @returns {GraphQLQuery[]}
     */
    static getExtraQueries() {
        return [];
    }

    /**
     * Return extra field mutations to add to GraphQL schema
     * (and rather related to defined type)
     * 
     * @returns {GraphQLQuery[]}
     */
    static getExtraMutations() {
        return [];
    }
    
    /**
     * Return list of properties to override
     * Throw error if one of the properties is not existing in related model
     * 
     * @returns {GraphQLProperty[]}
     */
    static getOverridenProperties() {
        return [];
    }

    /**
     * Return list of properties to override
     * Throw error if one of the properties is already existing in related model
     * 
     * @returns {GraphQLProperty[]}
     */
    static getExtraProperties() {
        return [];
    }

    /**
     * Add extra GraphQL code specific to this model
     * @returns {String}
     */
    static getExtraGraphQLCode() {
        return ``;    
    }

    /**
     * Return extra resolvers to merge to the schema resolvers
     *
     * @returns {Object}
     */
    static getExtraResolvers() {
        return null;
    }

    /**
     * Return shield resolver rules for this model
     * For a model definition, can only defines rules for Query, Mutation and graphQL properties related
     * Example :
     * {
     *   Query: {
     *     <fieldKey>: <rule1>
     *   },
     *   Mutation: {
     *     <fieldKey>: <rule2>
     *   },
     *   <nodeType>: {
     *     <propertyKey>: <rule3>
     *   }
     * }
     *
     * @param {String} [graphQLType] Name of the graphQL type for which rules must be defined (optional)
     */
    static getRules(graphQLType) {
        return {};
    }

    /**
     * Return extra property corresponding to name if any
     * @param {String} name
     * @returns {GraphQLProperty}
     */
    static getExtraProperty(name) {
        let extraProperty = this.getExtraProperties().find(property => property._name === name);
        if (!extraProperty) {
            throw new Error(`Extra property ${name} does not exist for ${this.constructor.name} class`);
        }
        return extraProperty
    }
}