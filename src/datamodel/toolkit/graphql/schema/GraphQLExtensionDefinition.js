import GraphQLDefinition from "./GraphQLDefinition";


/**
 * This module defines a base specification for a GraphQL extension type schema
 * 
 */
export default class GraphQLExtensionDefinition extends GraphQLDefinition {

    /**
     * @inheritdoc
     */
    static isTypeGenerated() {
        return false;
    }

}