/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import env from "env-var";
import EntityDefinition from "../../../../../ontologies/mnx-common/EntityDefinition";

/**
 * Use this middleware to add a custom search filter and restrict results
 *
 * @param {string} [publicPolicyUri=env.get("PUBLIC_POLICY_ID")} - The URI of access policy tagged as public. Default to env var PUBLIC_POLICY_ID
 * @param {*} [replaceValue] - The replacement value
 */
export let restrictEntitiesWithPublicPolicyMiddleware = ({publicPolicyUri} = {}) => (
  /**
   * @param {function} resolve
   * @param {object} object
   * @param {ResolverArgs} args
   * @param {SynaptixDatastoreSession} synaptixSession
   * @param {object} info
   * @param {object} options
   */
  async (resolve, object, args, synaptixSession, info, options) => {
    if (!publicPolicyUri){
      publicPolicyUri = env.get("PUBLIC_POLICY_ID").required().asString() ;
    }

    // If user is not logged, we must ensure that no data leak appends.
    // Usecases:
    //  - Case 1 : Request for an entity based on its "id"
    //  - case 2 : Request for a list of entities
    if (!(await synaptixSession.isLoggedUser())){
      args.filters = (args.filters || []).concat([`${EntityDefinition.getLink("hasAccessPolicy").getLinkName()}:${synaptixSession.extractIdFromGlobalId(publicPolicyUri)}`])
    }

    return resolve(object, args, synaptixSession, info);
  }
);