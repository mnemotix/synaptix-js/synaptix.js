/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {isReadOnlyModeDisabledRule} from "../isReadOnlyModeDisabledRule";
import {ShieldError} from "../../../../../../../../utilities/error/ShieldError";

describe("Test isAuthenticatedRule", () => {
  it('should pass as default', async () => {
    let rule = isReadOnlyModeDisabledRule();
    let result = await rule.resolve();

    expect(result).toBe(true);
  });

  it('should block is env var READ_ONLY_MODE_ENABLED defined as true', async () => {
    process.env.READ_ONLY_MODE_ENABLED = "1";
    let rule = isReadOnlyModeDisabledRule();
    let exception = await rule.resolve();
    process.env.READ_ONLY_MODE_ENABLED = "0";

    expect(exception).toBeInstanceOf(ShieldError);
    expect(exception.i18nKey).toBe("READ_ONLY_MODE_ENABLED");

  });

});