/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {isAuthenticatedRule} from "../isAuthenticatedRule";
import ModelDefinitionsRegister from "../../../../../../definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../../../../../../../network/amqp/NetworkLayerAMQP";
import {PubSub} from "graphql-subscriptions";
import GraphQLContext from "../../../../../GraphQLContext";
import SynaptixDatastoreRdfSession from "../../../../../../../../datastores/SynaptixDatastoreRdfSession";
import SSOUser from "../../../../../../../../datamodules/drivers/sso/models/SSOUser";
import {ShieldError} from "../../../../../../../../utilities/error/ShieldError";

let modelDefinitionsRegister = new ModelDefinitionsRegister([]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();

let initSession = ({context}) => new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine,
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
});

describe("Test isAuthenticatedRule", () => {
  it('should block an anonymous user', async () => {
    let rule = isAuthenticatedRule();

    let synaptixSession = initSession({
      context: new GraphQLContext({
        anonymous: true, lang: 'fr'
      })
    });

    jest.spyOn(synaptixSession.getGraphControllerService().getGraphControllerPublisher(), 'ask');

    let exception = await rule.resolve({}, {}, synaptixSession, {});

    expect(exception).toBeInstanceOf(ShieldError);
    expect(exception.i18nKey).toBe("USER_MUST_BE_AUTHENTICATED");
  });

  it('should grant a valid user', async () => {
    let rule = isAuthenticatedRule();

    let synaptixSession = initSession({
      context: new GraphQLContext({
        user: new SSOUser({
          user: {
            id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
            username: 'test@domain.com',
            firstName: 'John',
            lastName: 'Doe'
          }
        }),
        lang: 'fr'
      })
    });

    jest.spyOn(synaptixSession, 'isLoggedUser').mockImplementation(() => true);

    let result = await rule.resolve({}, {}, synaptixSession, {});

    expect(result).toBe(true);
  });

  it('should block a disabled or unregistered user', async () => {
    let rule = isAuthenticatedRule();

    let synaptixSession = initSession({
      context: new GraphQLContext({
        user: new SSOUser({
          user: {
            id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
            username: 'test@domain.com',
            firstName: 'John',
            lastName: 'Doe'
          }
        }),
        lang: 'fr'
      })
    });

    jest.spyOn(synaptixSession, 'isLoggedUser').mockImplementation(() => false);

    let exception = await rule.resolve({}, {}, synaptixSession, {});

    expect(exception).toBeInstanceOf(ShieldError);
    expect(exception.i18nKey).toBe("USER_MUST_BE_AUTHENTICATED");
  });
});