/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import env from "env-var";
import { rule } from 'graphql-shield';
import {ShieldError} from "../../../../../../../utilities/error/ShieldError";

/**
 * @param parent
 * @param args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @return {*}
 */
let readOnlyModeDisabledRuleResolver = async (parent, args, synaptixSession) => {
  const readOnlyModeEnabled = env.get("READ_ONLY_MODE_ENABLED").default("0").asBool();

  if (readOnlyModeEnabled){
    return new ShieldError(` (Blocked by \`readOnlyModeDisabledRule\` 🛡 rule)`, "READ_ONLY_MODE_ENABLED", 401);
  }

  return true;
};

/**
 * @return {Rule}
 */
export let isReadOnlyModeDisabledRule = () => rule()(readOnlyModeDisabledRuleResolver);