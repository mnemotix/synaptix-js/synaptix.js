/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import fs from 'fs';
import {makeExecutableSchema, mergeSchemas as merge} from '@graphql-tools/schema';
import {introspectionQuery} from 'graphql/utilities';

import {graphqlSync, isSpecifiedDirective, isSpecifiedScalarType, print, printSchema} from 'graphql';

export function printSchemaWithDirectives(schema) {
  const str = Object
    .keys(schema.getTypeMap())
    .filter(k => !k.match(/^__/))
    .reduce((accum, name) => {
      const type = schema.getType(name);

      !isSpecifiedScalarType(type)
        ? accum += `${print(type.astNode)}\n`
        : accum;

      return accum;
    }, '');

  return schema
    .getDirectives()
    .reduce((accum, d) => {
      !isSpecifiedDirective(d)
        ? accum += `${print(d.astNode)}\n`
        : accum;

      return accum;
    }, str + `${print(schema.astNode)}\n`);
}

/**
 * @param typeDefs
 * @param resolvers
 * @param printSchemaPath
 * @param mergeSchemas
 * @return {GraphQLSchema}
 */
export function generateSchema({typeDefs, resolvers, printGraphQLSchemaPath, printJSONSchemaPath, mergeSchemas}) {
  let schema;
  try{
    schema = makeExecutableSchema({
      typeDefs: typeDefs.join("\n"),
      resolvers,
      inheritResolversFromInterfaces: true
    });
  } catch (e){
    if(e.name === "GraphQLError"){
      let errorSources = [];
      const graphqlSource = e.source.body.split("\n");

      for(const {line, column} of e.locations){
        errorSources.push({
          line,
          column,
          source: `
        | ${graphqlSource[line - 4]}
        | ${graphqlSource[line - 3]}
=> HERE | ${graphqlSource[line - 2]}
        | ${graphqlSource[line - 1]}   
        | ${graphqlSource[line]}
      `});
      }

      throw new Error(`GraphQL schema building error : ${e.message}
${errorSources.map(({line, column, source}) => `  - On position L${line}:C${column} ${source}`)}`);
    } else {
      throw e;
    }
  }


  if (mergeSchemas) {
    schema = merge({
      schemas: [schema, mergeSchemas]
    });
  }

  if (printGraphQLSchemaPath) {
    fs.writeFileSync(
      printGraphQLSchemaPath,
      printSchema(schema),
    );
  }

  if (printJSONSchemaPath) {
    let result = graphqlSync(schema, introspectionQuery);

    if (result.errors) {
      return console.error(result.errors);
    }

    fs.writeFileSync(printJSONSchemaPath, JSON.stringify(result, null, 2));
  }

  return schema;
}

export {merge as mergeSchemas};