/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
    generateCreateMutationResolver,
    generateUpdateMutationResolver,
    generateUpdateMutationDefinition,
    generateCreateMutationDefinition
 } from "./mutations/generators";
import { getFragmentDefinitionsRegisterFromGqlInfo } from "../helpers/fragmentHelpers";


const DEFAULT_REMOVE_OBJECT_INPUT = 'RemoveObjectInput'


/**
 * Class representing a GraphQL mutation
 * 
 */
export class GraphQLMutation {

    /**
     * @param {String} description Custom description of the mutation
     * @param {Function} resolver Resolver for this mutation
     */
    constructor({ name, description, resolver } = {}) {
        this._description = description;
        this._resolver = resolver;
    }

    /**
     * Returns field name for this mutation
     * @param {ModelDefinitionAbstract} modelDefinition 
     * @returns String
     */
    generateFieldName(modelDefinition) {
        throw new Error(`You must implement ${this.constructor.name}::generateFieldName`);
    }

    /**
     * Returns a GraphQL schema for this mutation
     * @param {ModelDefinitionAbstract} modelDefinition 
     */
    generateType(modelDefinition) {
        throw new Error('You must implement GraphQLMutation::generateType')
    }

    /**
     * Returns a GraphQL resolver object for this mutation
     * @param {ModelDefinitionAbstract} modelDefinition 
     */
    generateResolver(modelDefinition) {
        throw new Error('You must implement GraphQLMutation::generateResolver')
    }

    /**
     * Wrapper for mutation extension type
     *
     * @param content
     * @return {string}
     */
    _wrapQueryType(content) {
        return ` extend type Mutation { ${content} }`;
    }

    /**
     * Wrapper for mutation extension resolver
     * @param content
     * @return {object}
     */
    _wrapQueryResolver(content) {
        return {
            Mutation: content
        };
    }

    /**
     * @param {IRule} [rule] GraphQL shield rule
     */
    static getShieldRule(rule) {
        if(rule){
            return {
                Mutation: {
                    [this.generateFieldName()]: rule
                }
            }
        }
    }
}


/**
 * Class representing a create mutation
 * 
 * The resolver function in constructor takes two arguments :
 * - an input JS object containing followng keys :
 *   - "objectInput" : input JS object for item to create
 *   - any key defined in extraInputArgs (extra input GraphQL arguments)
 * - a synaptix session
 * It returns created object
 *
 */
export class GraphQLCreateMutation extends GraphQLMutation {

    /**
     * @param {String} [extraInputArgs] Extra input GraphQL arguments
     * @param {String} [edgeType] Type of the edge returned after mutation
     * @param {String} [description] Custom description of the mutation
     * @param {Function} resolver Resolver for this mutation
     * 
     */
    constructor({ extraInputArgs, edgeType, description, resolver } = {}) {
        super({ description, resolver });
        this._extraInputArgs = extraInputArgs;
        this._edgeType = edgeType;
    }

    /**
     * @inheritdoc
     */
    generateFieldName(modelDefinition) {
        return `create${modelDefinition.getGraphQLType()}`;
    }

    /**
     * @inheritdoc
     */
    generateType(modelDefinition) {
        return generateCreateMutationDefinition({
            modelDefinition,
            extraInputArgs: this._extraInputArgs,
            edgeType: this._edgeType,
            description: this._description
        });
    }

    /**
     * @inheritdoc
     */
    generateResolver(modelDefinition) {
        // If this._resolver is not null, we wrap it for using a homogeneous signature
        // with standard synaptix create resolver
        let wrapperResolver = (!!this._resolver) ?
            async (_, { input }, synaptixSession, info) => {
                
                // Parse fragments on returned object
                synaptixSession.storeFragmentDefinitionsRegister(
                    getFragmentDefinitionsRegisterFromGqlInfo({
                      info,
                      modelDefinitionsRegister: synaptixSession.getModelDefinitionsRegister(),
                      isConnection: false,
                      rootModelDefinition: modelDefinition,
                    })
                );
                
                let createdObject = await this._resolver(input, synaptixSession, info);
                return {
                    createdObject,
                    createdEdge: {
                        cursor: 0,
                        node: createdObject
                    },
                    success: true
                }
            } : null;

        return generateCreateMutationResolver({
            modelDefinition,
            resolver: wrapperResolver
        });
    }
}


/**
 * Class representing a update mutation
 * 
 * The resolver function in constructor takes two arguments :
 * - an input JS object containing following keys 
 *   - "objectId" : id of the object to updatde
 *   - "objectInput" : input JS object for object to update
 * - a synaptix session
 * It returns updated object
 */
export class GraphQLUpdateMutation extends GraphQLMutation {
    /**
     * @param {String} [extraInputArgs] Extra input GraphQL arguments
     * @param {String} [edgeType] Type of the edge returned after mutation
     * @param {String} [description] Custom description of the mutation
     * @param {Function} resolver Resolver for this mutation
     * @param {Function} batchResolver Resolver for this mutation applied to a batch of items
     *
     */
    constructor({ extraInputArgs, description, resolver, batchResolver } = {}) {
        super({ description, resolver });
        this._batchResolver = batchResolver
        this._extraInputArgs = extraInputArgs;
    }

    /**
     * @inheritdoc
     */
    generateFieldName(modelDefinition) {
        return `update${modelDefinition.getGraphQLType()}`;
    }

    /**
     * @inheritdoc
     */
    generateType(modelDefinition) {
        return generateUpdateMutationDefinition({
            modelDefinition,
            extraInputArgs: this._extraInputArgs,
            description: this._description
        });
    }

    /**
     * @inheritdoc
     */
    generateResolver(modelDefinition) {
        // If this._resolver is not null, we wrap it for using a homogeneous signature
        // with standard synaptix update resolver
        function decorateResolver(resolver, returnField) {
            return async (_, { input }, synaptixSession, info) => {
                // Parse gql fragments on returned object
                synaptixSession.storeFragmentDefinitionsRegister(
                    getFragmentDefinitionsRegisterFromGqlInfo({
                        info,
                        modelDefinitionsRegister: synaptixSession.getModelDefinitionsRegister(),
                        isConnection: false,
                        rootModelDefinition: modelDefinition,
                    })
                );
                
                return { 
                    [returnField]: await resolver(input, synaptixSession, info),
                    success: true
                };
            }
        }
        
        return generateUpdateMutationResolver({
            modelDefinition,
            resolver: this._resolver ? decorateResolver(this._resolver, 'updatedObject') : null,
            batchResolver: this._batchResolver ? decorateResolver(this._batchResolver, 'updatedObjects') : null
        });
    }
}


/**
 * Class representing a remove mutation
 * 
 * The resolver function in constructor takes by default two arguments :
 * - an input JS object containing following keys :
 *   - "objectId" : id of the object to updatde
 *   - "permanent" : boolean indicating if object is really destroyed or not
 * - a synaptix session
 * It returns id of the deleted item
 * 
 * Input type can be overriden by "customInputType" argument
 */
export class GraphQLRemoveMutation extends GraphQLMutation {
    /**
     * @param {String} [customInputType] Extra custom input type
     * @param {String} [description] Custom description of the mutation
     * @param {Function} resolver Resolver for this mutation
     *
     */
     constructor({ customInputType, extraInputArgs, description, resolver, batchResolver } = {}) {
        super({ description, resolver });
        this._removeInputType = customInputType || DEFAULT_REMOVE_OBJECT_INPUT;
        this._extraInputArgs = extraInputArgs;
        this._batchResolver = batchResolver
    }

    /**
     * @inheritdoc
     */
    generateFieldName(modelDefinition) {
        return `remove${modelDefinition.getGraphQLType()}`;
    }

    /**
     * @inheritdoc
     */
    generateType(modelDefinition) {
        const mutationDeclaration = this._wrapQueryType(`
            remove${modelDefinition.getGraphQLType()}(input: ${this._removeInputType}!): RemoveObjectPayload
            ${this._batchResolver ? `batchRemove${modelDefinition.getGraphQLType()}(input: Batch${this._removeInputType}!): RemoveObjectsPayload` : ''}
        `);
        if (this._removeInputType === DEFAULT_REMOVE_OBJECT_INPUT) {
            return mutationDeclaration;
        }

        const inputTypeDefinition = `
        """Remove ${modelDefinition.getNodeType()} mutation input"""
        input ${this._removeInputType} {
            objectId: ID!
            permanent: Boolean
            ${this._extraInputArgs}
        }
        
        """Batch remove ${modelDefinition.getNodeType()} mutation input"""
        input Batch${this._removeInputType} {
            objectIds: [ID]!
            permanent: Boolean
            ${this._extraInputArgs}
        }`
        ;

        return [inputTypeDefinition, mutationDeclaration].join('\n\n')

    }

    /**
     * @inheritdoc
     */
    generateResolver(modelDefinition) {
        return this._wrapQueryResolver({
            [`remove${modelDefinition.getGraphQLType()}`]: async (_, { input }, synaptixSession) => {
                return {
                    deletedId: await this._resolver(input, synaptixSession),
                    success: true
                }
            },
            ...(!this._batchResolver ? {} : {
                [`batchRemove${modelDefinition.getGraphQLType()}`]: async (_, { input }, synaptixSession) => {
                    return {
                        deletedIds: await this._batchResolver(input, synaptixSession),
                        success: true
                    }
                }
            })
        })
    }
}