import GraphQLDefinition from "./GraphQLDefinition";
import { GraphQLTypeQuery, GraphQLTypeConnectionQuery } from "./GraphQLQuery";


/**
 * This module defines a base specification for a GraphQL interface schema
 * 
 */
export default class GraphQLInterfaceDefinition extends GraphQLDefinition {

        /**
     * @inheritdoc
     */
    static isTypeGenerated() {
        return true;
    }

    /**
     * @inheritdoc
     */
    static getTypeQuery() {
        return new GraphQLTypeQuery();
    }

    /**
     * @inheritdoc
     */
    static getTypeConnectionQuery() {
        return new GraphQLTypeConnectionQuery();
    }

}