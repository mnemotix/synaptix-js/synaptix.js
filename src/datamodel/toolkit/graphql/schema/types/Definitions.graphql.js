/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {QueryStringType} from "../../../utils/QueryString";

export let Definitions = `
""" Information about pagination in a connection. """
type PageInfo {
  """ When paginating forwards, are there more items? """
  hasNextPage: Boolean!

  """ When paginating backwards, are there more items? """
  hasPreviousPage: Boolean!

  """ When paginating backwards, the cursor to continue. """
  startCursor: String

  """ When paginating forwards, the cursor to continue. """
  endCursor: String
}

input SortingInput {
  """ Sort by input """
  sortBy: String
  """ Is sort descending """
  isSortDescending: Boolean
  """ 
  Sorting params (stringified JSON) used for complex sortings (_script, _geo_distance...). 
  Need that "sortBy" parameter refers to a "SortingDefinition" defined in the contextual "ModelDefinition". 
  """
  sortParams: String
  """ Sorting filters @deprecated """
  sortFilters: [String]
}


enum QueryStringTypeEnum {
  ${Object.values(QueryStringType).join("  \n")}
}

input QueryStringInput {
  """ Searchable property to query"""
  property: String
  
  """ Query string value """
  value: String
  
  """ Query string type """
  type: QueryStringTypeEnum = MATCH
}

""" An aggregation is a representation of a type property occurence"""
type Aggregation{
  """ Aggregation name. Generally refers to type property."""
  name: String!
  
  """ Buckets """
  buckets: [AggregationBucket]
}

type AggregationBucket{
  key: String!
  count: Int
}

""" A representation of a localized label """
type Translation{
  """ Value of label """
  value: String
  """ Lang of label """
  lang: String
}

""" A representation of a localized label input"""
input TranslationInput{
  """ Value of label """
  value: String!
  """ Lang of label """
  lang: String!
}
`;
