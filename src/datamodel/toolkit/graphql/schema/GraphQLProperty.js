/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import { connectionArgs, filteringArgs } from "./types/generators";


/**
 * Encapsulates a GraphQL property for GraphQL schema
 * 
 */
export default class GraphQLProperty {

    /**
     * @param {String} name name of the property
     * @param {String} [description] description of property
     * @param {Boolean} [isConnection] is property a connection or not
     * @param {String} type type of property (or null if not a readable property)
     * @param {String} inputType  type of input property (or null if read-only property)
     * @param {String} inputArgs property arguments arguments
     * @param {String} [args] Extra args to add to property, if it's a property
     * @param {Function} typeResolver resolver function for this property
     */
    constructor({ name, description, isConnection, type, inputType, args, extraConnectionArgs, typeResolver, countResolver }) {
        if (!name) {
            throw new Error('GraphQL property must have a name');
        }
        this._name = name;
        this._description = description;
        this._isConnection = isConnection || false;
        this._type = type;
        this._inputType = inputType;
        this._typeResolver = typeResolver;
        this._countResolver = countResolver;
        this._extraConnectionArgs = extraConnectionArgs || null;
        this._args = args;
    }

    /**
     * @returns {String}
     */
    getName() {
        return this._name;
    }

    /**
     * @returns {String}
     */
    getDescription() {
        return this._description;
    }

    /**
     * @returns {String}
     */
    getType() {
        return this._type;
    }

    /**
     * @returns {String}
     */
    getInputType() {
        return this._inputType;
    }

    /**
     * @returns {Function}
     */
    getTypeResolver() {
        return this._typeResolver;
    }

    /**
     * 
     * @returns {Function}
     */
    getCountResolver() {
        return this._countResolver;
    }

    /**
     * Returns type string representation
     * @returns {String}
     */
    getTypeRepresentation() {
        if (!this._type)
            return '';
        if (this._isConnection) {
            // if property is a connection, we add stuff for connection handling and eventually extra arguments
            let extraArgs = this._extraConnectionArgs ? `, ${this._extraConnectionArgs}` : '';
            return `
""" ${this.description} """
${this._name}(${connectionArgs}, ${filteringArgs}, ${extraArgs}):${this._type}Connection

${this._countResolver ?
`""" ${this.description} (Count) """
${this._name}Count(${filteringArgs}): Int`
: ''}`
        } else {
            return `
""" ${this.description} """
${this._name}${this._args ? `(${this._args})` : ''}: ${this._type}
`
        }
    }

    /**
     * Returns input type string representation
     * @returns {String}
     */
    getInputTypeRepresentation() {
        if (!this._inputType)
            return '';
        let description = (this._description) ? `""" ${this._description} """` : '';
        return description + `\n${this._name} : ${this._inputType}`; 
    }
}