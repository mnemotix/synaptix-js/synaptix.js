/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import env from "env-var";

/**
 * These helpers are extracted from `graphql-relay` library.
 */

/**
 * @param {array} objects
 * @param {object} args
 * @returns {{edges, pageInfo: {hasNextPage: boolean, hasPreviousPage: boolean, endCursor: (*|null), startCursor: (*|null)}}}
 */
export function connectionFromArray({objects, args}) {
  return connectionFromArraySlice({
    arraySlice: objects || [],
    args
  });
}

/**
 * @param {Collection} collection
 * @param  {object} args
 */
export function connectionFromCollection({collection, args}){
  return connectionFromArraySlice({
    arraySlice: collection.objects,
    totalCount: collection.totalCount,
    aggregations : collection.aggregations,
    args
  });
}

/**
 * Given a slice (subset) of an array, returns a connection object for use in
 * GraphQL.
 *
 * This function is similar to `connectionFromArray`, but is intended for use
 * cases where you know the cardinality of the connection, consider it too large
 * to materialize the entire array, and instead wish pass in a slice of the
 * total result large enough to cover the range specified in `args`.
 */
export function connectionFromArraySlice({arraySlice, totalCount, aggregations = [], args = {}} = {}) {
  let after = args.after,
    before = args.before,
    first = args.first,
    last = args.last;
  let sliceStart = 0,
    arrayLength = arraySlice.arrayLength;

  let sliceEnd = sliceStart + arraySlice.length;
  let beforeOffset = getOffsetWithDefault(before, arrayLength);
  let afterOffset = getOffsetWithDefault(after, -1);

  let startOffset = Math.max(sliceStart - 1, afterOffset, -1) + 1;
  let endOffset = Math.min(sliceEnd, beforeOffset, arrayLength);
  if (typeof first === 'number') {
    if (first < 0) {
      throw new Error('Argument "first" must be a non-negative integer');
    }

    endOffset = Math.min(endOffset, startOffset + first);
  }
  if (typeof last === 'number') {
    if (last < 0) {
      throw new Error('Argument "last" must be a non-negative integer');
    }

    startOffset = Math.max(startOffset, endOffset - last);
  }

  let edges = arraySlice.slice(0, first).map(function (value, index) {
    return {
      cursor: offsetToCursor(startOffset + index),
      node: value
    };
  });

  let firstEdge = edges[0];
  let lastEdge = edges[edges.length - 1];
  let upperBound = before ? beforeOffset : arrayLength;
  return {
    totalCount,
    edges: edges,
    pageInfo: {
      startCursor: firstEdge ? firstEdge.cursor : null,
      endCursor: lastEdge ? lastEdge.cursor : null,
      hasPreviousPage: afterOffset > 0,
      hasNextPage: arraySlice.length > first
    },
    aggregations
  };
}


/**
 * Creates the cursor string from an offset.
 */
export function offsetToCursor(offset ) {
  return obfuscate(getPrefix() + offset);
}

/**
 * Rederives the offset from the cursor string.
 */
export function cursorToOffset(cursor) {
  return parseInt(unobfuscate(cursor).substring(getPrefix().length), 10);
}

/**
 * Given an optional cursor and a default offset, returns the offset
 * to use; if the cursor contains a valid offset, that will be used,
 * otherwise it will be the default.
 */
export function getOffsetWithDefault(cursor, defaultOffset) {
  if (typeof cursor !== 'string') {
    return defaultOffset;
  }
  let offset = cursorToOffset(cursor);
  return isNaN(offset) ? defaultOffset : offset;
}

/**
 * Takes a type name and an ID specific to that type name, and returns a
 * "global ID" that is unique among all types.
 */
export function toGlobalId(type, id) {
  return obfuscate([type, id].join(':'));
}

/**
 * Takes the "global ID" created by toGlobalID, and returns the type name and ID
 * used to create it.
 */
export function fromGlobalId(globalId) {
  const unbasedGlobalId = unobfuscate(globalId);
  const delimiterPos = unbasedGlobalId.indexOf(':');
  return {
    type: unbasedGlobalId.substring(0, delimiterPos),
    id: unbasedGlobalId.substring(delimiterPos + 1),
  };
}

function getPrefix(){
  return 'offset:';
}

function obfuscate(i) {
  if (env.get("USE_GRAPHQL_RELAY").default("0").asBool()){
    return Buffer.from(i, 'utf8').toString('base64');
  } else {
    return i;
  }
}

function unobfuscate(i) {
  if (env.get("USE_GRAPHQL_RELAY").default("0").asBool()) {
    return Buffer.from(i, 'base64').toString('utf8');
  } else {
    return i;
  }
}