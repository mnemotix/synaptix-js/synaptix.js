/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import merge from "lodash/merge";
import { getFragmentDefinitionsRegisterFromGqlInfo } from "./fragmentHelpers";
import uniq from "lodash/uniq";

/**
 * Generate GraphQL `type` resolver map that resolves basic properties
 * @param {string} typeName
 * @param {function|null} idFetcher
 * @return {object}
 */
export let generateTypeResolvers = (typeName, idFetcher = null) => ({
  id: (obj, args, synaptixSession, info) => {
    if (Array.isArray(obj)) {
      throw new Error(
        `Can't resolve the id while object is an Array. ${JSON.stringify(obj)}`
      );
    }

    return synaptixSession.stringifyGlobalId({
      type: typeName || info.parentType.name,
      id: idFetcher ? idFetcher(obj, synaptixSession, info) : obj.id,
    });
  },
});

/**
 * Generate GraphQL `interface` resolver map that resolves basic properties and the GraphQL object type.
 * @see https://www.apollographql.com/docs/graphql-tools/resolvers#unions-and-interfaces
 *
 * @return {object}
 */
export let generateInterfaceResolvers = (typeName, idFetcher = null) => ({
  ...generateTypeResolvers(typeName, idFetcher),
  __resolveType: getTypeResolver.bind(this),
});

/**
 * @param {string} objectId
 * @param {object} parent
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {GraphQLResolveInfo} info
 */
export let getObjectResolver = async (
  modelDefinition,
  parent,
  { id, ...args },
  synaptixSession,
  info
) => {
  synaptixSession.storeFragmentDefinitionsRegister(
    getFragmentDefinitionsRegisterFromGqlInfo({
      info,
      modelDefinitionsRegister: synaptixSession.getModelDefinitionsRegister(),
      rootModelDefinition: modelDefinition,
    })
  );

  const fragmentDefinitions = synaptixSession.getFragmentDefinitions(info);

  if (id) {
    return synaptixSession.getObject({
      objectId: synaptixSession.extractIdFromGlobalId(id),
      modelDefinition,
      lang: synaptixSession.getContext().getLang(),
      args,
      fragmentDefinitions,
    });
  } else {
    return synaptixSession.getObjects({
      modelDefinition,
      firstOne: true,
      lang: synaptixSession.getContext().getLang(),
      args,
      fragmentDefinitions,
    });
  }
};

/**
 * @param {ResolverArgs|object} args
 * @param {object} parent
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {GraphQLResolveInfo} info
 * @param {boolean} [asCollection] Returns Collection instance instead of GraphQL connection
 *
 * @return {object|Collection}
 */
export let getObjectsResolver = async (
  modelDefinition,
  parent,
  args,
  synaptixSession,
  info,
  asCollection = false
) => {
  synaptixSession.storeFragmentDefinitionsRegister(
    getFragmentDefinitionsRegisterFromGqlInfo({
      info,
      modelDefinitionsRegister: synaptixSession.getModelDefinitionsRegister(),
      isConnection: true,
      rootModelDefinition: modelDefinition,
    })
  );

  const fragmentDefinitions = synaptixSession.getFragmentDefinitions(info);

  let objects = await synaptixSession.getObjects({
    modelDefinition,
    args,
    fragmentDefinitions,
    asCollection: true,
  });

  return asCollection
    ? objects
    : synaptixSession.wrapObjectsIntoGraphQLConnection(objects || [], args);
};

/**
 * @param {ResolverArgs|object} args
 * @param {object} parent
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 *
 * @return {int}
 */
export let getObjectsCountResolver = async (
  modelDefinition,
  parent,
  args,
  synaptixSession
) => {
  return synaptixSession.getObjectsCount({ modelDefinition, args });
};

/**
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 * @param {GraphQLResolveInfo} info
 */
export let getLinkedObjectsResolver = async (
  linkDefinition,
  object,
  args,
  synaptixSession,
  info
) => {
  const fragmentDefinitions = synaptixSession.getFragmentDefinitions(info);

  let linkedObjects = await synaptixSession.getLinkedObjectsFor({
    object,
    linkDefinition,
    args,
    fragmentDefinitions,
    asCollection: true,
  });
  return synaptixSession.wrapObjectsIntoGraphQLConnection(linkedObjects, args);
};

/**
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 */
export let getLinkedObjectsCountResolver = async (
  linkDefinition,
  object,
  args,
  synaptixSession
) => {
  return synaptixSession.getLinkedObjectsCountFor({
    object,
    linkDefinition,
    args,
  });
};

/**
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 * @param {GraphQLResolveInfo} info
 */
export let getLinkedObjectResolver = async (
  linkDefinition,
  object,
  args,
  synaptixSession,
  info
) => {
  const fragmentDefinitions = synaptixSession.getFragmentDefinitions(info);

  return synaptixSession.getLinkedObjectsFor({
    object,
    linkDefinition,
    args,
    fragmentDefinitions,
  });
};

/**
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LabelDefinition} labelDefinition
 */
export let getLocalizedLabelResolver = async (
  labelDefinition,
  object,
  args = {},
  synaptixSession
) => {
  return synaptixSession.getLocalizedLabelFor({
    object,
    labelDefinition,
    lang: synaptixSession.getContext().getLang(),
    returnFirstOneIfNotExistForLang: true,
    langFlagEnabled: args?.langFlag === true,
  });
};

/**
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LabelDefinition} labelDefinition
 */
export let isLocalizedLabelTranslatedResolver = async (
  labelDefinition,
  object,
  args,
  synaptixSession
) => {
  return synaptixSession.isLocalizedLabelTranslated({
    object,
    labelDefinition,
    lang: synaptixSession.getContext().getLang(),
  });
};

/**
 * @param {object} root
 * @param {object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let getMeResolver = async (root, args, synaptixSession) => {
  return synaptixSession.getLoggedUserPerson();
};

/**
 * @param {Model} object - Model instance
 * @param {SynaptixDatastoreSession} synaptixSession
 * @return {string}
 */
export let getTypeResolver = async (object, synaptixSession) => {
  let modelDefinition = await synaptixSession.getModelDefinitionForModel({
    model: object,
  });

  if (modelDefinition) {
    return modelDefinition.getGraphQLType();
  } else {
    return object.type;
  }
};

/**
 * @param {Model} object - Model instance
 * @param {*} _
 * @param {SynaptixDatastoreRdfSession} synaptixSession
 * @return {string}
 */
export let getTypesResolver = async (object, _, synaptixSession) => {
  if (object.types?.length > 0) {
    return synaptixSession
      .getModelDefinitionForRdfType({ type: object.types, returnList: true })
      .map((modelDefinition) => modelDefinition.getGraphQLType());
  }

  return [await getTypeResolver(object, synaptixSession)];
};

/**
 * @param {...object} resolvers
 * @return {target}
 */
export let mergeResolvers = (...resolvers) => {
  return merge({}, ...resolvers);
};

/**
 * @param {typeof ModelDefinitionAbstract|string} modelDefinitionOrType
 * @param {object} _
 * @param {object} objectInput
 * @param {Link[]} links
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let createObjectInConnectionResolver = async (
  modelDefinitionOrType,
  links,
  _,
  { input: { objectInput } },
  synaptixSession,
  info
) => {
  if (typeof modelDefinitionOrType !== "string") {
    modelDefinitionOrType = modelDefinitionOrType.getGraphQLType();
  }

  // Parse gql fragments on returned object
  synaptixSession.storeFragmentDefinitionsRegister(
    getFragmentDefinitionsRegisterFromGqlInfo({
      info,
      modelDefinitionsRegister: synaptixSession.getModelDefinitionsRegister(),
      isConnection: false,
      rootModelDefinition: modelDefinitionOrType,
    })
  );

  let object = await synaptixSession.createObject({
    graphQLType: modelDefinitionOrType,
    links,
    objectInput,
    lang: synaptixSession.getContext().getLang(),
  });

  return {
    createdObject: object,
    createdEdge: {
      cursor: 0,
      node: object,
    },
    success: true
  };
};

/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {LinkDefinition} linkDefinition
 * @param {object} _
 * @param {string} objectId
 * @param {string} targetId
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let createEdgeResolver = async (
  modelDefinition,
  linkDefinition,
  _,
  { input: { objectId, targetId } },
  synaptixSession
) => {
  let { target } = await synaptixSession.createEdge(
    modelDefinition,
    synaptixSession.extractIdFromGlobalId(objectId),
    linkDefinition,
    synaptixSession.extractIdFromGlobalId(targetId)
  );

  return {
    createdEdge: {
      cursor: 0,
      node: target,
    },
  };
};

/**
 * @param {typeof ModelDefinitionAbstract|string} modelDefinitionOrType
 * @param {object} _
 * @param {ResolverArgs} args
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let updateObjectResolver = async (
  modelDefinitionOrType,
  _,
  args,
  synaptixSession,
  info
) => {
  const {
    input: { objectId, objectInput },
  } = args;

  // Parse gql fragments on returned object
  synaptixSession.storeFragmentDefinitionsRegister(
    getFragmentDefinitionsRegisterFromGqlInfo({
      info,
      modelDefinitionsRegister: synaptixSession.getModelDefinitionsRegister(),
      isConnection: false,
      rootModelDefinition: modelDefinitionOrType,
    })
  );

  let { type, id } = synaptixSession.parseGlobalId(objectId);

  if (!type) {
    type = await synaptixSession.getGraphQLTypeForId(id);
  }

  if (typeof modelDefinitionOrType === "string") {
    modelDefinitionOrType = synaptixSession
      .getModelDefinitionsRegister()
      .getModelDefinitionForGraphQLType(type);
  }

  // Use the synaptixSession.updateObjects method to add potential ACL filters
  // passed in args by a high order middleware.
  // @see securizeEntitiesMutationMiddleware
  let updatedObjects = await synaptixSession.updateObjects({
    modelDefinition: modelDefinitionOrType,
    objectIds: [id],
    args,
    updatingProps: objectInput,
    lang: synaptixSession.getContext().getLang(),
  });

  return {
    updatedObject: updatedObjects[0],
  };
};

/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {object} _
 * @param {ResolverArgs} args
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let batchUpdateObjectResolver = async (
  modelDefinition,
  _,
  args,
  synaptixSession
) => {
  const {
    input: { objectIds = [], filters, objectsInput },
  } = args;
  let types = [];
  let normalizedIds = [];

  for (const objectId of objectIds) {
    let { id, type } = synaptixSession.parseGlobalId(objectId);

    if (!type) {
      type = await synaptixSession.getGraphQLTypeForId(id);
    }

    types.push(type);
    normalizedIds.push(id);
  }

  if (uniq(types).length > 1) {
    throw new I18nError(
      `"batchUpdate***" mutations only support the batch update of entities having the same GraphQL type. Found following : ${uniq(
        types
      )}`
    );
  }

  // Use the whole "args" object to add potential ACL filters added by a high order middleware.
  // @see securizeEntitiesMutationMiddleware
  args = {
    ...args,
    filters,
  };

  const updatedObjects = await synaptixSession.updateObjects({
    modelDefinition: modelDefinition,
    objectIds: normalizedIds,
    args,
    updatingProps: objectsInput,
    lang: synaptixSession.getContext().getLang(),
  });

  return {
    updatedObjects,
  };
};
