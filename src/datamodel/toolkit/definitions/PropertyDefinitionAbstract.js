/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {PropertyStep} from "../utils/linkPath/steps/PropertyStep";
import {BindStep} from "../utils/linkPath/steps/BindStep";
import {UnionStep} from "../utils/linkPath/steps/UnionStep";

export class PropertyDefinitionAbstract {
  /**
   * @param {string} propertyName
   * @param {string} [description]
   * @param {string} [pathInIndex]
   * @param {string} [rdfDataProperty]
   * @param {string} [rdfLiteralType]
   * @param {*} [defaultValue]
   * @param {LinkPath} [linkPath]
   * @param {boolean} [isSearchable=false]
   * @param {number}  [searchBoost=1.0] Boost paramater to increase index score value
   * @param {string} [inputFormOptions] Input form options to use in form directive. @see {FormInputDirectiveType} definition
   * @param {boolean} [excludeFromInput] Exclude this literal from input generation
   * @param {boolean} [isRequired] Is literal required
   * @param {boolean} [isPlural] Is literal an array
   * @param {boolean} [isAggregable] Is literal eligible for index aggregations
   * @param {boolean} [inIndexOnly] - Use this property to precise that the link is only findable in index for performance reasons
   */
  constructor({propertyName, description, pathInIndex, pathInGraphstore, rdfDataProperty, linkPath, rdfDataType, isSearchable, inputFormOptions, excludeFromInput, isRequired, isPlural, isAggregable, searchBoost, inIndexOnly, defaultValue}) {
    if (!propertyName){
      throw new TypeError("A PropertyDefinition instance must have a propertyName defined.")
    }

    if (linkPath && !(linkPath.getLastStep() instanceof PropertyStep || linkPath.getLastStep() instanceof BindStep || linkPath.getLastStep() instanceof UnionStep)){
      throw new TypeError("Last step of the property definion linkPath must be a PropertyStep, a BindStep or an UnionStep.");
    }

    this._propertyName    = propertyName;
    this._description     = description;
    this._rdfDataProperty = rdfDataProperty;
    this._linkPath = linkPath;
    this._rdfDataType = rdfDataType || "rdfs:Literal";
    this._inputFormOptions = inputFormOptions;
    this._excludeFromInput = excludeFromInput;
    this._pathInGraphstore = pathInGraphstore;
    this._pathInIndex = pathInIndex;

    this._isRequired = isRequired;
    this._isPlural = isPlural || false;
    this._isAggregable = isAggregable || false;
    this._isSearchable = isSearchable;
    this._searchBoost = searchBoost;
    this._isHerited = false;
    this._inIndexOnly = inIndexOnly;
    this._defaultValue = defaultValue;
  }

  /**
   * Returns the propery name
   * @return {string}
   */
  getPropertyName(){
    return this._propertyName;
  }

  /**
   * Returns the property description
   * @return {string}
   */
  getDescription() {
    return this._description;
  }

  /**
   * Returns the path to retrieve literal in index
   * @return {string|UseIndexMatcherOfDefinition|ChildOfIndexDefinition}
   */
  getPathInIndex() {
    return this._pathInIndex || this._propertyName;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   */
  getPathInGraphstore() {
    return this._pathInGraphstore;
  }

  /**
   * Returns the RDF data property predicate used to retrieve literal or the link path alias.
   * @return {string}
   */
  getRdfDataProperty() {
    return this._rdfDataProperty;
  }

  /**
   * Returns the RDF data property predicate alias used to retrieve literal.
   * @return {string}
   */
   getRdfAliasDataProperty(){
    return this._linkPath?.getLastPropertyStep()?.getRdfDataPropertyAlias()
  
  }

  /**
   * Returns the RDF datatype.
   * @return {*}
   */
  getRdfDataType() {
    return this._rdfDataType;
  }

  /**
   * Returns the link path to access the data
   * @return {LinkPath}
   */
  getLinkPath() {
    return this._linkPath;
  }

  /**
   * Set literal as herited from parent definition
   */
  setIsHerited(){
    this._isHerited = true;
  }

  /**
   * Is literal herited from parent definition
   */
  isHerited(){
    return this._isHerited;
  }

  getSearchBoost() {
    return this._searchBoost;
  }

  /**
   * @return {*}
   */
  getDefaultValue() {
    return this._defaultValue;
  }

  /**
   * Is this property excluded from input generation ?
   */
  isExcludedFromInput(){
    return this._excludeFromInput;
  }

  /**
   * Return input form options to use in form directive.
   * @see {FormInputDirectiveType} definition
   * @return {string}
   */
  getInputFormOptions() {
    return this._inputFormOptions;
  }

  /**
   * Is this literal flagged as searchable ?
   * @return {boolean}
   */
  isSearchable() {
    return this._isSearchable;
  }

  /**
   * Is this link forbidded using this link in a node creation
   * @return {boolean}
   */
  isInIndexOnly() {
    return this._inIndexOnly;
  }

  
  /**
   * Is literal required
   */
  isRequired(){
    return this._isRequired;
  }

  /**
   * @return {boolean}
   */
  isPlural(){
    return this._isPlural;
  }

  /**
   * @returns {boolean}
   */
  isAggregable() {
    return this._isAggregable;
  }
  
  /**
   * Convert this literal into a SPARQL variable.
   * @param  {string} [suffix]
   * @return {string}
   */
  toSparqlVariable({suffix} = {}) {
    if (this._linkPath && this._linkPath.getLastPropertyStep() instanceof PropertyStep){
      return this._linkPath.getLastPropertyStep().getPropertyDefinition().toSparqlVariable()
    }

    return `?${this._propertyName}${suffix || ""}`;
  }

  /**
   * Convert this literal into a SPARQL variable.
   * @return {string}
   */
  toSparqlValue(value) {
    return `"${value}"`;
  }
}