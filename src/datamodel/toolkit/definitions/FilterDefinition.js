/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

/**
 */
export default class FilterDefinition{
  /**
   * @param {string} filterName
   * @param {function} indexFilter
   * @param {function} indexScriptFields -  @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-body.html#request-body-search-script-fields
   * @param {function} graphFilter
   */
  constructor({filterName, indexFilter, indexScriptFields, graphFilter}) {
    this._filterName = filterName;
    this._indexFilter = indexFilter;
    this._indexScriptFields = indexScriptFields;
    this._graphFilter = graphFilter;
  }

  /**
   * Get filter name
   */
  getFilterName() {
    return this._filterName;
  }

  /**
   * @param {*} params
   * @return {Function}
   */
  generateIndexFilter(params = {}){
    return this._indexFilter(params);
  }

  /**
   * Get script fields to add to index results.
   * @param {*} params
   * @return {array}
   */
  generateIndexScriptFields(params = {}) {
    return this._indexScriptFields ? this._indexScriptFields(params) : [];
  }

  /**
   * @param {*} params
   * @return {Function}
   */
  generateGraphFilter(params){
    return this._graphFilter(params);
  }
}