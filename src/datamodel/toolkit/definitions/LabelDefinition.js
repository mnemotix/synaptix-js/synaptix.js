/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {PropertyDefinitionAbstract} from "./PropertyDefinitionAbstract";

export default class LabelDefinition extends PropertyDefinitionAbstract{
  /**
   * @param {string} labelName
   * @param {string} [description]
   * @param {string} [pathInGraphstore]
   * @param {string} [pathInIndex]
   * @param {string} [rdfDataProperty]
   * @param {bool} [isHTML]
   * @param {*} [defaultValue]
   * @param {typeof ModelDefinitionAbstract} [relatedNodeDefinition]
   * @param {boolean} [isSearchable=true]
   * @param {number}  [searchBoost=1.0] Boost paramater to increase index score value
   * @param {string} [inputFormOptions] - Input form options to use in form directive. @see {FormInputDirectiveType} definition
   * @param {LinkPath} [linkPath]
   * @param {boolean} [isRequired] Is label required
   * @param {boolean} [isPlural] Is literal an array
   * @param {boolean} [isAggregable=false]
   * @param {boolean} [inIndexOnly] - Use this property to precise that the link is only findable in index for performance reasons
   */
  constructor({labelName, description, pathInGraphstore, rdfDataProperty, pathInIndex, isHTML, relatedNodeDefinition, isSearchable = true, inputFormOptions, isRequired, isPlural, isAggregable, linkPath, searchBoost, inIndexOnly, defaultValue} = {}) {
    super({propertyName: labelName, description, pathInGraphstore, rdfDataProperty, pathInIndex, isHTML, relatedNodeDefinition, isSearchable, inputFormOptions, isRequired, isPlural, linkPath, searchBoost, inIndexOnly, defaultValue, isAggregable});

    this._pathInGraphstore = pathInGraphstore;
    this._relatedNodeDefinition = relatedNodeDefinition;
  }

  /**
   * Get label name
   */
  getLabelName() {
    return this.getPropertyName();
  }

  getNodeType() {
    return this._relatedNodeDefinition.getNodeType();
  }

  getModelClass() {
    return this._relatedNodeDefinition.getModelClass();
  }

  /**
   * Convert this literal into a SPARQL value.
   * @return {string}
   */
  toSparqlValue(value) {
    return `"${value}"`;
  }

  /**
   * Convert this literal into a SPARQL value.
   * @return {string}
   */
  toSparqlLocalizedValue(value, lang) {
    return `"${value}"@${lang}`;
  }

  /**
   * Get GraphQL property name that indicates if the label is translated for contextual language
   */
  getTranslationStatusPropertyName() {
    return `${this.getLabelName()}Translated`;
  }
}
