/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import LabelDefinition from "./LabelDefinition";

/**
 * A ModelDefinitionFragmentDefinition is a portion of ModelDefinitionAbstract.
 */
export class FragmentDefinition {
  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {PropertyDefinitionAbstract[]} [properties]
   * @param {LinkDefinition[]} [links]
   */
  constructor({modelDefinition, properties, links}) {
    this._modelDefinition = modelDefinition;
    this._properties = properties || [];
    this._links = links || [];
  }

  /**
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinition(){
    return this._modelDefinition;
  }

  /**
   * @return {PropertyDefinitionAbstract[]}
   */
  getProperties() {
    return this._properties;
  }

  /**
   * @return {LinkDefinition[]}
   */
  getLinks() {
    return this._links;
  }

  /**
   * @param {PropertyDefinitionAbstract} property
   */
  addProperty(property){
    this._properties.push(property)
  }

  /**
   * @param {LinkDefinition} link
   */
  addLink(link){
    this._links.push(link)
  }

  /**
   * @param {PropertyDefinitionAbstract[]} properties
   */
  addProperties(properties = []){
    this._properties = this._properties.concat(properties)
  }

  /**
   * @param {LinkDefinition[]} links
   */
  addLinks(links = []){
    this._links = this._links.concat(links)
  }

  /**
   * @param {LinkDefinition} targetLink
   * @return {boolean}
   */
  hasLink(targetLink){
    return this._links.some(link => link.getLinkName() === targetLink.getLinkName())
  }

  /**
   * Return an array of keys findable in the related ModelDefinition index
   * @returns {*[]}
   */
  getIndexedKeys(){
    return [].concat(
      this.getProperties().flatMap(property => {
        let fields = [property.getPathInIndex()];
        if (property instanceof LabelDefinition) {
          fields = fields.concat(`${property.getPathInIndex()}_locales`);
        }
        return fields;
      }),
      this.getLinks().map(link => link.getPathInIndex())
    );
  }

  /**
   * @returns {boolean}
   */
  getIdOnly(){
    return this._links.length === 0 && this._properties.length === 0;
  }
}