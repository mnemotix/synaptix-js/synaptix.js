/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import OnlineContributionDefinition from "./OnlineContributionDefinition";
import ConceptDefinition from "../mnx-skos/ConceptDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";
import DeletionDefinition from "./DeletionDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath";

export default class TaggingDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [OnlineContributionDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition(){
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Tagging";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'tagging';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const parentLinks = super.getLinks();

    const hasDefaultDeletionLinkDefinition = parentLinks.find(
      link => link.getLinkName() === "hasDeletionAction"
    );

    if (hasDefaultDeletionLinkDefinition) {
      parentLinks.splice(
        parentLinks.indexOf(hasDefaultDeletionLinkDefinition),
        1
      );
    }

    const hasConceptLinkDefinition = new LinkDefinition({
      linkName: 'hasConcept',
      symmetricLinkName: 'isTaggedConceptOf',
      rdfObjectProperty: "mnx:hasConcept",
      relatedModelDefinition: ConceptDefinition,
      graphQLPropertyName: "concept",
      graphQLInputName: "conceptInput"
    });


    const hasEntityLinkDefinition = new LinkDefinition({
      linkName: 'hasEntity',
      rdfObjectProperty: "mnx:isTaggingOf",
      relatedModelDefinition: EntityDefinition,
      graphQLPropertyName: "entities",
      graphQLInputName: "entityInputs",
      isPlural: true
    });

    //
    // This is a reification class,
    // If one the reified object is deleted, reification must be deleted.
    //
    const hasDeletionLinkDefinition = new LinkDefinition({
      linkName: 'hasDeletionAction',
      relatedModelDefinition: DeletionDefinition,
      graphQLPropertyName: "deletionAction",
      linkPath: new LinkPath().union({
        linkPaths: [
          new LinkPath()
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasConceptLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasEntityLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition })
        ]
      })
    });

    return [
      ...parentLinks,
      hasConceptLinkDefinition,
      hasEntityLinkDefinition,
      hasDeletionLinkDefinition
    ];
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'weight',
        rdfDataProperty: "mnx:weight",
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#integer'
      })
    ]
  }
};