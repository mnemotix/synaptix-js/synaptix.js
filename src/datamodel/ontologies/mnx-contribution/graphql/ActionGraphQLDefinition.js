/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  GraphQLInterfaceDefinition,
  GraphQLProperty
} from "../../../toolkit/graphql/schema";
import {
  gatherIdsFromRdf4jJsonLdNode,
  parseObjectFromRdf4jJsonLdNode
} from "../../../../datamodules/services/graph/sparqlHelpers/parseObjectFromJsonLdNode";
import {logException} from "../../../../utilities/logger";
import ActionDefinition from "../ActionDefinition";

export default class ActionGraphQLDefinition extends GraphQLInterfaceDefinition {
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "entitySnapshot",
        type: "EntityInterface",
        description: "Pick an entity involved in the action and return it's snapshot.",
        args: `entityId: ID!`,
        /**
         * @param {Model} object
         * @param {string} entityId
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         * @return {*}
         */
        typeResolver: (object, {entityId: entityGlobalId}, synaptixSession) => {
          const entityId = synaptixSession.extractIdFromGlobalId(entityGlobalId);

          if(object.snapshot){
            const snapshot = JSON.parse(object.snapshot);
            const jsonLdNode = (snapshot || []).find(node => node?.["@id"] === entityId);

            if(jsonLdNode?.["@id"] && jsonLdNode?.["@type"]){
              return parseObjectFromRdf4jJsonLdNode({
                jsonLdNode,
                modelDefinition: synaptixSession.getModelDefinitionForRdfType({type: jsonLdNode["@type"]}),
                rdfPrefixesMappings: synaptixSession.getSchemaNamespaceMapping()}
              )
            }
          }
        }
      })
    ];
  }

  static getOverridenProperties(){
    return [
      new GraphQLProperty({
        name: "snapshot",
        type: "String",
        description: "A subgraph snapshot of the action",
        args: `
""" Returns a pretty version of snapshot """
pretty: Boolean`,
        /**
         * @param {Model} object
         * @param pretty
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         * @return {*}
         */
        typeResolver: async (object, {pretty}, synaptixSession) => {
          if(!pretty){
            return object.snapshot;
          }

          let relatedSnapshots;

          if(object.startedAtTime && object.hasPerson?.id){
             const relatedActions = (await synaptixSession.getObjects({
              modelDefinition: ActionDefinition,
              args: {
                filters: [`hasPerson.id = ${object.hasPerson?.id}`, `startedAtTime = ${object.startedAtTime}`]
              }
            })).filter(({id}) => id !== object.id);

            relatedSnapshots = relatedActions.map(relatedAction => relatedAction.snapshot)
          }

          return JSON.stringify(getPrettySnapshot({snapshot: object.snapshot, synaptixSession, relatedSnapshots}));
        }
      })
    ];
  }
}


function getPrettySnapshot({snapshot, synaptixSession, relatedSnapshots = []}){
  let processedSnapshot = [];

  let relatedObjects = relatedSnapshots.reduce((relatedObjects, snapshot) =>  [...relatedObjects, ...getPrettySnapshot({snapshot, synaptixSession})], []);

  if(snapshot){
    try {
      const jsonLdNodes = JSON.parse(snapshot);

      // First reconcile nodes between them.
      const objects = jsonLdNodes.reduce((objects, jsonLdNode) => {
        if (jsonLdNode?.["@type"]) {
          let object = parseObjectFromRdf4jJsonLdNode({
            jsonLdNode,
            modelDefinition: synaptixSession.getModelDefinitionForRdfType({type: jsonLdNode["@type"]}),
            rdfPrefixesMappings: synaptixSession.getSchemaNamespaceMapping(),
            includeNestedLinks: true,
            forcePrefixedIds: true,
            processIfIdNull: true
          });


          if(relatedObjects.length > 0){
            let touchedRelatedObjectsId = [];
            const modelDefinition = synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(object.type);

            for(let [property, value] of Object.entries(object)){
              const linkDefinition = modelDefinition?.getLinkFromGraphqlPropertyName(property);

              if(linkDefinition){
                const symmetricLinkDefinition = linkDefinition?.getSymmetricLinkDefinition();
                const relatedModelDefinition = linkDefinition?.getRelatedModelDefinition();

                // Remove linked object without id
                if(!Array.isArray(value)){
                  value = [value];
                }

                object[property] = value.filter(({id}) => !!id);

                if(symmetricLinkDefinition){
                  object[property] = object[property].concat(relatedObjects.filter(({id, type}) => {
                    if(relatedModelDefinition.getGraphQLType() === type){
                      touchedRelatedObjectsId.push(id);
                      return true;
                    }
                  }));
                }
              }
            }


            for(const relatedObject of relatedObjects.filter(({id}) => !touchedRelatedObjectsId.includes(id))){
              for(let [property, candidateObjects] of Object.entries(relatedObject)){
                const relatedModelDefinition = synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(relatedObject.type);
                const relatedLinkDefinition = relatedModelDefinition?.getLinkFromGraphqlPropertyName(property);
                const candidateModelDefinition = relatedLinkDefinition?.getRelatedModelDefinition();
                const symmetricLinkDefinition = relatedLinkDefinition?.getSymmetricLinkDefinition();

                if(
                  symmetricLinkDefinition &&
                  (
                    candidateModelDefinition?.isEqualOrDescendantOf(modelDefinition) ||
                    candidateModelDefinition?.getSubstitutionModelDefinition() === modelDefinition ||
                    modelDefinition.getSubstitutionModelDefinition() === candidateModelDefinition
                  )
                ){
                  if(!Array.isArray(candidateObjects)){
                    candidateObjects = [candidateObjects];
                  }

                  candidateObjects = candidateObjects.filter((candidateObject) => (candidateObject.id === object.id) || object.id === "?node");

                  if(!object[symmetricLinkDefinition.getGraphQLPropertyName()]){
                    object[symmetricLinkDefinition.getGraphQLPropertyName()] = [];
                  }

                  if(!Array.isArray(object[symmetricLinkDefinition.getGraphQLPropertyName()])){
                    object[symmetricLinkDefinition.getGraphQLPropertyName()] = [object[symmetricLinkDefinition.getGraphQLPropertyName()]];
                  }

                  object[symmetricLinkDefinition.getGraphQLPropertyName()].push(relatedObject);
                }
              }
            }
          }

          objects.push(object);
        }

        return objects;
      }, []);

      processedSnapshot = [...objects, ...relatedObjects];
    } catch (e){
      logException(e);
    }
  }

  return processedSnapshot;
}


function gatherTouchedIds({object, synaptixSession }){
  let ids = [];
  if(object.snapshot){
    try {
      return JSON.parse(object.snapshot).reduce((ids, jsonLdNode) => {
        if (jsonLdNode?.["@type"]) {
          ids = ids.concat(gatherIdsFromRdf4jJsonLdNode({
            jsonLdNode,
            modelDefinition: synaptixSession.getModelDefinitionForRdfType({type: jsonLdNode["@type"]}),
            rdfPrefixesMappings: synaptixSession.getSchemaNamespaceMapping(),
            forcePrefixedIds: true,
            processIfIdNull: true
          }));
        }

        return ids;
      }, []);
    } catch (e){
      logException(e);
    }
  }
  return ids;
}