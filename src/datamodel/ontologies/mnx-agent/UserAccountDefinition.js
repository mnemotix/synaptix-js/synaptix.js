/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import AccessTargetDefinition from "../mnx-acl/AccessTargetDefinition";
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import UserGroupDefinition from "./UserGroupDefinition";
import PersonDefinition from "./PersonDefinition";
import CreationDefinition from "../mnx-contribution/CreationDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath";
import UserAccountGraphQLDefinition from "./graphql/UserAccountGraphQLDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";

export default class UserAccountDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "sioc": "http://rdfs.org/sioc/ns#",
      "prov": "http://www.w3.org/ns/prov#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AccessTargetDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return UserAccountGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:UserAccount";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["sioc:User", "prov:Agent"];
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "access-target";
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    // While action index is shared between Creation/Update/Deletion, we must filter types
    return [{
      "term":  {
        "types": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserAccount"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasUserGroup',
        symmetricLinkName: 'hasUserAccount',
        rdfObjectProperty: "sioc:member_of",
        relatedModelDefinition: UserGroupDefinition,
        isPlural: true,
        graphQLPropertyName: "userGroups",
        graphQLInputName: "userGroupInputs",
        rdfPrefixesMapping: {
          "sioc": "http://rdfs.org/sioc/ns#"
        },
        defaultValue: "mnx:user-group/VisitorGroup"
      }),
      new LinkDefinition({
        linkName: 'hasPerson',
        symmetricLinkName: 'hasUserAccount',
        rdfObjectProperty: "mnx:isUserAccountOf",
        relatedModelDefinition: PersonDefinition,
        isPlural: false,
        graphQLPropertyName: "person",
        graphQLInputName: "personInput",
        rdfPrefixesMapping: {
          "sioc": "http://rdfs.org/sioc/ns#"
        },
      }),
      new LinkDefinition({
        linkName: 'hasInitiatedCreationAction',
        symmetricLinkName: "hasEntity",
        rdfReversedObjectProperty: "prov:wasAssociatedWith",
        relatedModelDefinition: CreationDefinition,
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    const hasPersonLinkPath =  new LinkPath()
      .step({linkDefinition: UserAccountDefinition.getLink("hasPerson")});

    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'userId',
        rdfDataProperty: "mnx:userId",
        excludeFromInput: true
      }),
      new LiteralDefinition({
        literalName: 'username',
        rdfDataProperty: "mnx:username",
        excludeFromInput: true,
        isSearchable: true
      }),
      new LiteralDefinition({
        literalName: 'isDisabled',
        rdfDataProperty: "mnx:isUserDisabled",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
      new LiteralDefinition({
        literalName: 'isUnregistered',
        rdfDataProperty: "mnx:isUserUnregistered",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
      new LiteralDefinition({
        literalName: 'firstName',
        linkPath: hasPersonLinkPath
          .clone()
          .property({
            propertyDefinition: PersonDefinition.getLiteral("firstName"),
            rdfDataPropertyAlias: 'mnx:firstName'
          }),
        isSearchable: true,
        inIndexOnly: true
      }),
      new LiteralDefinition({
        literalName: 'lastName',
        linkPath: hasPersonLinkPath
          .clone()
          .property({
            propertyDefinition: PersonDefinition.getLiteral("lastName"),
            rdfDataPropertyAlias: 'mnx:lastName'
          }),
        isSearchable: true,
        inIndexOnly: true
      }),
      new LiteralDefinition({
        literalName: 'fullName',
        linkPath: new LinkPath()
          .concat({
            linkPaths : [
              hasPersonLinkPath
                .clone()
                .property({
                  propertyDefinition: PersonDefinition.getLiteral("firstName"),
                }),
              hasPersonLinkPath
                .clone()
                .property({
                  propertyDefinition: PersonDefinition.getLiteral("lastName"),
                }),
              hasPersonLinkPath
                .clone()
                .property({
                  propertyDefinition: PersonDefinition.getLiteral("nickName"),
                }),
            ], separator: " "}),
        isSearchable: true,
        inIndexOnly: true
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'userGroupLabels',
        linkPath: new LinkPath()
          .step({linkDefinition: UserAccountDefinition.getLink("hasUserGroup")})
          .property({
            propertyDefinition: UserGroupDefinition.getLabel("label"),
            rdfDataPropertyAlias: 'rdfs:label'
          }),
        isAggregable: true,
        isPlural: true
      }),
    ]
  }
};

