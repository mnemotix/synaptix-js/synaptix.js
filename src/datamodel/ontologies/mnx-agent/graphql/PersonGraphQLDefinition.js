/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import {GraphQLProperty, GraphQLTypeDefinition} from "../../../toolkit/graphql/schema";
import PersonDefinition from "../PersonDefinition";
import {getMeResolver} from "../../../toolkit/graphql/helpers/resolverHelpers";

export default class PersonGraphQLDefinition extends GraphQLTypeDefinition {
  static getOverridenProperties() {
    return [
      new GraphQLProperty({
        name: "displayName",
        type: "String",
        description: "Display name",
        typeResolver: person => person.fullName
      }),
      new GraphQLProperty({
        name: "fullName",
        type: "String",
        description: "Full name (alias of display name)",
        typeResolver: person => {
          const {fullName, firstName, lastName, nickName} = person;

          if(fullName){
            return Array.isArray(fullName) ? fullName.join(" ") : fullName;
          }

          if (firstName || lastName || nickName) {
            return `${firstName || ""} ${lastName || ""} ${nickName || ""} `.trim();
          }
        }
      })
    ]
  }
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "mainEmail",
        type: "EmailAccount",
        description: "Main email account",
        typeResolver:  async (person, args, synaptixSession) => {
          let emails = await synaptixSession.getLinkedObjectsFor({
            object: person,
            linkDefinition: PersonDefinition.getLink('hasEmailAccount'),
            args
          });

          if (emails.length > 0) {
            return emails[0];
          }
        }
      })
    ];
  }


  static getExtraGraphQLCode(){
    return `
extend type Query{
  """ Get myself object """
  me: PersonInterface
}
`
  }

  static getExtraResolvers(){
    return {
      Query: {
        me: getMeResolver
      }
    }
  }
}
