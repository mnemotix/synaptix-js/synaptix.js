/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import AccessTargetDefinition from "../mnx-acl/AccessTargetDefinition";
import UserAccountDefinition from "./UserAccountDefinition";
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import UserGroupGraphQLDefinition from "./graphql/UserGroupGraphQLDefinition";

export default class UserGroupDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "sioc": "http://rdfs.org/sioc/ns#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AccessTargetDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return UserGroupGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:UserGroup";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["sioc:UserGroup"];
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "access-target";
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    // While action index is shared between Creation/Update/Deletion, we must filter types
    return [{
      "term":  {
        "types": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserGroup"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasUserAccount',
        rdfReversedObjectProperty: "sioc:member_of",
        symmetricLinkName: "hasUserGroup",
        relatedModelDefinition: UserAccountDefinition,
        isPlural: true,
        graphQLPropertyName: "userAccounts",
        graphQLInputName: "userAccountInputs"
      }),
      new LinkDefinition({
        linkName: 'hasTransitiveUserAccount',
        description: "This connection gathers direct and infered members of this group.",
        rdfObjectProperty: "mnx:hasTransitiveMember",
        relatedModelDefinition: UserAccountDefinition,
        isPlural: true,
        graphQLPropertyName: "transitiveUserAccounts",
        excludeFromInput: true
      }),
      new LinkDefinition({
        linkName: 'isSubGroupOf',
        description: "Parent group belonging",
        rdfObjectProperty: "mnx:isSubGroupOf",
        relatedModelDefinition: UserGroupDefinition,
        graphQLPropertyName: "subGroupOf",
        excludeFromInput: true,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'label',
        rdfDataProperty: "rdfs:label"
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: "dc:description"
      }),
    ]
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'asGlobalRole',
        rdfDataProperty: "mnx:asGlobalRole",
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#boolean'
      }),
      new LiteralDefinition({
        literalName: 'isInformativeGroup',
        description: "Is this group purely informative and not editable ? ",
        rdfDataProperty: "mnx:isInformativeGroup",
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#boolean'
      })
    ]
  }
};

