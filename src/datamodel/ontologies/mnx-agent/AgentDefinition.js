/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../mnx-common/EntityDefinition";
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import EmailAccountDefinition from "./EmailAccountDefinition";
import PhoneDefinition from "./PhoneDefinition";
import AddressDefinition from "./AddressDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import InvolvementDefinition from "../mnx-project/InvolvementDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import GraphQLInterfaceDefinition from "../../toolkit/graphql/schema/GraphQLInterfaceDefinition";
import AffiliationDefinition from "./AffiliationDefinition";
import GroupMembershipDefinition from "./GroupMembershipDefinition";

export default class AgentDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLInterfaceDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "foaf": "http://xmlns.com/foaf/0.1/"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Agent"
  }

  static getIndexType(){
    return 'agent';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasEmailAccount',
        rdfObjectProperty: "mnx:hasEmailAccount",
        relatedModelDefinition: EmailAccountDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        rdfPrefixesMapping: {
          "foaf": "http://xmlns.com/foaf/0.1/"
        },
        graphQLPropertyName: "emails",
        graphQLInputName: "emailAccountInputs"
      }),
      new LinkDefinition({
        linkName: 'hasPhone',
        rdfObjectProperty: "mnx:hasPhone",
        relatedModelDefinition: PhoneDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "phones",
        graphQLInputName: "phoneInputs"
      }),
      new LinkDefinition({
        linkName: 'hasAddress',
        rdfObjectProperty: "mnx:hasAddress",
        relatedModelDefinition: AddressDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "addresses",
        graphQLInputName: "addressInputs"
      }),
      new LinkDefinition({
        linkName: 'hasAffiliation',
        symmetricLinkName: 'hasPerson',
        rdfReversedObjectProperty: "mnx:hasPerson",
        relatedModelDefinition: AffiliationDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "affiliations",
        graphQLInputName: "affiliationInputs"
      }),
      new LinkDefinition({
        linkName: 'gathersMembership',
        symmetricLinkName: 'hasAgentMember',
        rdfReversedObjectProperty: "mnx:hasAgentMember",
        relatedModelDefinition: GroupMembershipDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "gathersMemberships",
        graphQLInputName: "gathersMembershipInputs",
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'avatar',
        rdfDataProperty: "mnx:avatar",
        inputFormOptions: 'type: "image"'
      }),
      new LiteralDefinition({
        literalName: 'displayName',
        rdfDataProperty: "mnx:displayName"
      })
    ];
  }
};
