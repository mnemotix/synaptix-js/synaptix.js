/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import ConceptDefinition from "./ConceptDefinition";
import VocabularyDefinition from "./VocabularyDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import {GraphQLTypeDefinition} from "../../toolkit/graphql/schema";
import SkosElementDefinition from "./SkosElementDefinition";

export default class SchemeDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [SkosElementDefinition];
  }
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'skos:ConceptScheme';
  }


  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Scheme';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'scheme';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasConcept',
        symmetricLinkName: "inScheme",
        rdfReversedObjectProperty: "skos:inScheme",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        isCascadingUpdated: true,
        graphQLPropertyName: "concepts",
        graphQLInputName: "conceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasTopConcept',
        symmetricLinkName: "topInScheme",
        rdfObjectProperty: "skos:hasTopConcept",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        graphQLPropertyName: "topConcepts",
        graphQLInputName: "topConceptInputs"
      }),
      new LinkDefinition({
        linkName: 'hasVocabulary',
        rdfObjectProperty: "mnx:schemeOf",
        relatedModelDefinition: VocabularyDefinition,
        isPlural: true,
        graphQLPropertyName: "vacabularies",
        graphQLInputName: "vocabularyInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'prefLabel',
        rdfDataProperty: "skos:prefLabel"
      }),
      new LabelDefinition({
        labelName: 'altLabel',
        rdfDataProperty: "skos:altLabel"
      }),
      new LabelDefinition({
        labelName: 'definition',
        rdfDataProperty: "skos:definition"
      }),
      // Deprecated
      new LabelDefinition({
        labelName: 'title',
        rdfDataProperty: "mnx:title"
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: "mnx:description"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      })
    ];
  }
};