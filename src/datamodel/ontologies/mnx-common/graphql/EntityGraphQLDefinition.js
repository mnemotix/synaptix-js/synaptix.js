/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import env from "env-var";
import {
  GraphQLInterfaceDefinition,
  GraphQLProperty
} from "../../../toolkit/graphql/schema";
import EntityDefinition from "../EntityDefinition";
import UserAccountDefinition from "../../mnx-agent/UserAccountDefinition";
import CreationDefinition from "../../mnx-contribution/CreationDefinition";
import PersonDefinition from "../../mnx-agent/PersonDefinition";
import {
  getLinkedObjectResolver,
  getObjectResolver, getTypesResolver
} from "../../../toolkit/graphql/helpers/resolverHelpers";
import { LinkPath } from "../../../toolkit/utils/linkPath";
import {RemoveEntityGraphQLMutation} from "./RemoveEntityGraphQLMutation";
import {RemoveEntitiesGraphQLMutation} from "./RemoveEntitiesGraphQLMutation";
import {RemoveEntityLinkGraphQLMutation} from "./RemoveEntityLinkGraphQLMutation";
import {Permissions} from "../../../toolkit/models/Permissions";
import {isLoggedUserObjectCreator} from "../../mnx-contribution/middlewares/aclValidation/rules/isCreatorRule";
import {isReadOnlyAccessTarget} from "../../mnx-acl/middlewares/aclValidation/rules/isReadOnlyAccessTargetRule";
import {isReadWriteAccessTarget} from "../../mnx-acl/middlewares/aclValidation/rules/isReadWriteAccessTargetRule";
import {RemoveEntitiesByFiltersGraphQLMutation} from "./RemoveEntitiesByFiltersGraphQLMutation";

export default class EntityGraphQLDefinition extends GraphQLInterfaceDefinition {
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "canUpdate",
        type: "Boolean",
        description: "Is the user allowed to update this entity",
        /**
         *
         * This property can be used in different ways.
         *
         * - With a global behaviour by overriding `SynaptixDatastoreSession::isLoggedUserAllowedToUpdateObject()` method in your own session. By default returns true.
         * - With a local behaviour by using `replaceFieldValueIfRuleFailsMiddleware` middleware.
         *
         * To override this value, you need to use selectivelly
         * @param {Model} object
         * @param __
         * @param {SynaptixDatastoreSession} synaptixSession
         * @return {*}
         */
        typeResolver: (object, __, synaptixSession) => {
          return synaptixSession.isLoggedUserAllowedToUpdateObject({
            object,
            modelDefinition: synaptixSession
              .getModelDefinitionsRegister()
              .getModelDefinitionForGraphQLType(object.type)
          });
        }
      }),
      new GraphQLProperty({
        name: "permissions",
        type: "Permissions",
        description: "The permissions granted to user",
        /**
         * To override this value, you need to use selectivelly
         * @param {Model} object
         * @param __
         * @param {SynaptixDatastoreSession} synaptixSession
         * @return {*}
         */
        typeResolver: async (object, __, synaptixSession) => {
          let read, update, del, grant;

          if(
            env.get("FULL_FEATURE_ENABLED").asBool() === true ||
            await synaptixSession.isLoggedUserInAdminGroup() ||
            await isLoggedUserObjectCreator({
              object,
              synaptixSession
            })
          ){
            read = update = del = grant = true;
          } else if (await synaptixSession.isLoggedUser()) {
            update = await isReadWriteAccessTarget({
              object,
              synaptixSession
            });

            read = update || await isReadOnlyAccessTarget({
              object,
              synaptixSession
            });
          }

          return new Permissions(read, update, del, grant);
        }
      })
    ];
  }

  static getExtraResolvers() {
    return {
      EntityInterface: {
        /**
         * @param {Model} object
         * @param __
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {GraphQLResolveInfo} info
         */
        creatorUserAccount: async (object, __, synaptixSession, info) => {
          const creationAction =
            object[
              EntityDefinition.getLink("hasCreationAction").getPathInIndex()
              ];

          if (creationAction) {
            return getLinkedObjectResolver(
              CreationDefinition.getLink("hasUserAccount"),
              await synaptixSession.getObject({
                modelDefinition: CreationDefinition,
                objectId: creationAction.id
              }),
              {},
              synaptixSession,
              info
            );
          } else {
            return getObjectResolver(
              UserAccountDefinition,
              object,
              {
                linkPaths: [
                  new LinkPath()
                    .step({
                      linkDefinition: UserAccountDefinition.getLink(
                        "hasInitiatedCreationAction"
                      )
                    })
                    .step({
                      linkDefinition: CreationDefinition.getLink("hasEntity"),
                      typeAssertionDiscarded: true,
                      targetId: object.id
                    })
                ]
              },
              synaptixSession,
              info
            );
          }
        },
        /**
         * @param {Model} object
         * @param __
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {GraphQLResolveInfo} info
         */
        creatorPerson: async (object, __, synaptixSession, info) => {
          const creationAction =
            object[
              EntityDefinition.getLink("hasCreationAction").getPathInIndex()
              ];

          if (creationAction) {
            return getLinkedObjectResolver(
              UserAccountDefinition.getLink("hasPerson"),
              await synaptixSession.getLinkedObjectFor({
                object: await synaptixSession.getObject({
                  modelDefinition: CreationDefinition,
                  objectId: creationAction.id
                }),
                linkDefinition: CreationDefinition.getLink("hasUserAccount")
              }),
              {},
              synaptixSession,
              info
            );
          } else {
            return getObjectResolver(
              PersonDefinition,
              object,
              {
                linkPaths: [
                  new LinkPath()
                    .step({
                      linkDefinition: PersonDefinition.getLink("hasUserAccount")
                    })
                    .step({
                      linkDefinition: UserAccountDefinition.getLink(
                        "hasInitiatedCreationAction"
                      )
                    })
                    .step({
                      linkDefinition: CreationDefinition.getLink("hasEntity"),
                      targetId: object.id,
                      typeAssertionDiscarded: true
                    })
                ]
              },
              synaptixSession,
              info
            );
          }
        },
        types: getTypesResolver
      },
      Query: {
        /**
         * @param {object} parent
         * @param {string} globalId
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {GraphQLResolveInfo} info
         * @return {Model}
         */
        entity: async (parent, { id: globalId }, synaptixSession, info) => {
          let { id, type } = synaptixSession.parseGlobalId(globalId);

          if (!type) {
            type = await synaptixSession.getGraphQLTypeForId(id);
          }

          let modelDefinition = synaptixSession
            .getModelDefinitionsRegister()
            .getModelDefinitionForGraphQLType(type);

          return getObjectResolver(
            modelDefinition,
            parent,
            { id },
            synaptixSession,
            info
          );
        }
      }
    };
  }

  /**
   * Return extra field mutations to add to GraphQL schema
   * (and rather related to defined type)
   *
   * @returns {GraphQLQuery[]}
   */
  static getExtraMutations() {
    return [
      new RemoveEntityGraphQLMutation(),
      new RemoveEntitiesGraphQLMutation(),
      new RemoveEntityLinkGraphQLMutation(),
      new RemoveEntitiesByFiltersGraphQLMutation()
    ];
  }
}
