/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GraphQLMutation} from "../../../toolkit/graphql/schema";
import {isAdminRule} from "../../mnx-agent/middlewares/aclValidation/rules";

export class RemoveEntitiesByFiltersGraphQLMutation extends GraphQLMutation{
  /**
   * @inheritdoc
   */
  generateFieldName() {
    return `removeEntitiesByFilters`;
  }

  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    return `
"""Remove entities mutation payload"""
type RemoveEntitiesByFiltersPayload {
  deletedIds: [ID]
}

"""Remove object mutation input"""
input RemoveEntitiesByFiltersInput {
  """ GraphQL type of entities to remove """
  type: String!
  """ List of filters to use to restrict entities (same as entities connection filters) """
  filters: [String]
  """ Permanent deletion, invalidation otherwise """
  permanent: Boolean
}

${this._wrapQueryType(`
  """
    Batch remove entities. Much faster than multiple removeEntity mutations.
  """
  ${this.generateFieldName()}(input: RemoveEntitiesByFiltersInput!): RemoveEntitiesByFiltersPayload
`)}   
`;
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      /**
       * @param {object} _
       * @param {string[]} ids
       * @param {boolean} permanent
       * @param {SynaptixDatastoreSession} synaptixSession
       */
      [this.generateFieldName()]: async (_, args, synaptixSession) => {
        const {input: {type, filters, permanent}} = args;
        const deletedIds = await synaptixSession.removeObjects({
          modelDefinition: synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type),
          args: {
            ...args,
            filters
          },
          permanentRemoval: permanent
        });

        return {deletedIds};
      }
    })
  }

  getShieldRule(){
    return isAdminRule();
  }
}