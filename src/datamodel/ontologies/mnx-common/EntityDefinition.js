/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import ExternalLinkDefinition from "../mnx-contribution/ExternalLinkDefinition";
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import ActionDefinition from "../mnx-contribution/ActionDefinition";
import AccessPolicyDefinition from "../mnx-acl/AccessPolicyDefinition";
import CreationDefinition from "../mnx-contribution/CreationDefinition";
import DeletionDefinition from "../mnx-contribution/DeletionDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath/LinkPath";
import UpdateDefinition from "../mnx-contribution/UpdateDefinition";
import UserAccountDefinition from "../mnx-agent/UserAccountDefinition";
import PersonDefinition from "../mnx-agent/PersonDefinition";
import TaggingDefinition from "../mnx-contribution/TaggingDefinition";
import EntityGraphQLDefinition from "./graphql/EntityGraphQLDefinition";
import ConceptDefinition from "../mnx-skos/ConceptDefinition";
import {FilterDefinition, LabelDefinition} from "../../toolkit/definitions";
import AccessTargetDefinition from "../mnx-acl/AccessTargetDefinition";
import {I18nError} from "../../../utilities/error/I18nError";

export default class EntityDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return EntityGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "prov": "http://www.w3.org/ns/prov#"
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Entity";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["prov:Entity"];
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const hasReadWriteAccessTargetLinkDefinition = new LinkDefinition({
      linkName: 'hasReadWriteAccessTarget',
      rdfObjectProperty: "mnx:hasReadWriteAccessTarget",
      relatedModelDefinition: AccessTargetDefinition,
      isPlural: true,
      graphQLPropertyName: "readWriteAccessTargets",
      graphQLInputName: "readWriteAccessTargetInputs",
    });

    const hasReadOnlyAccessTargetLinkDefinition =  new LinkDefinition({
      linkName: 'hasReadOnlyAccessTarget',
      rdfObjectProperty: "mnx:hasReadOnlyAccessTarget",
      relatedModelDefinition: AccessTargetDefinition,
      isPlural: true,
      graphQLPropertyName: "readOnlyAccessTargets",
      graphQLInputName: "readOnlyAccessTargetInputs",
    });

    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasExternalLink',
        rdfObjectProperty: "mnx:hasExternalLink",
        relatedModelDefinition: ExternalLinkDefinition,
        isPlural: true,
        graphQLPropertyName: "externalLinks",
        graphQLInputName: "externalLinkInputs"
      }),
      new LinkDefinition({
        linkName: 'hasAction',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: "prov:wasGeneratedBy",
        relatedModelDefinition: ActionDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "actions",
        excludeFromIndex: true,
        excludeFromInput: true
      }),
      new LinkDefinition({
        linkName: 'hasCreationAction',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: "mnx:hasCreation",
        relatedModelDefinition: CreationDefinition,
        graphQLPropertyName: "creationAction",
        excludeFromInput: true
      }),

      new LinkDefinition({
        linkName: 'hasUpdateAction',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: "mnx:hasUpdate",
        relatedModelDefinition: UpdateDefinition,
        isPlural: true,
        graphQLPropertyName: "updateActions",
        excludeFromInput: true
      }),
      new LinkDefinition({
        linkName: 'hasDeletionAction',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: "mnx:hasDeletion",
        relatedModelDefinition: DeletionDefinition,
        graphQLPropertyName: "deletionAction",
        excludeFromInput: true
      }),
      new LinkDefinition({
        linkName: 'hasAccessPolicy',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: "mnx:hasAccessPolicy",
        relatedModelDefinition: AccessPolicyDefinition,
        isCascadingRemoved: true,
        graphQLPropertyName: "accessPolicy",
        graphQLInputName: "accessPolicyInput",
        defaultValue: "mnx:access-policy/PrivatePolicy"
      }),
      hasReadWriteAccessTargetLinkDefinition,
      hasReadOnlyAccessTargetLinkDefinition,
      // Shortcuts links
      new LinkDefinition({
        linkName: 'hasCreatorUserAccount',
        relatedModelDefinition: UserAccountDefinition,
        linkPath: () => new LinkPath()
          .step({linkDefinition: EntityDefinition.getLink("hasCreationAction")})
          .step({linkDefinition: CreationDefinition.getLink("hasUserAccount" )}),
        graphQLPropertyName: "creatorUserAccount",
        excludeFromInput: true
      }),
      new LinkDefinition({
        linkName: 'hasCreatorPerson',
        relatedModelDefinition: PersonDefinition,
        linkPath: () => new LinkPath()
          .step({linkDefinition: EntityDefinition.getLink("hasCreationAction")})
          .step({linkDefinition: CreationDefinition.getLink("hasUserAccount")})
          .step({linkDefinition: UserAccountDefinition.getLink("hasPerson")}),
        graphQLPropertyName: "creatorPerson",
        excludeFromInput: true
      }),
      new LinkDefinition({
        linkName: 'hasUpdatePerson',
        relatedModelDefinition: PersonDefinition,
        linkPath: () => new LinkPath()
          .step({linkDefinition: EntityDefinition.getLink("hasUpdateAction")})
          .step({linkDefinition: CreationDefinition.getLink("hasUserAccount")})
          .step({linkDefinition: UserAccountDefinition.getLink("hasPerson")}),
        graphQLPropertyName: "updatePerson",
        excludeFromInput: true,
        excludeFromIndex: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    const hasCreatorPersonLink = EntityDefinition.getLink("hasCreatorPerson");

    return [
      new LiteralDefinition({
        isAggregable: true,
        literalName: "types",
        description: "List of types of this object (in the order of inheritance)",
        rdfDataProperty: "rdf:type",
        isPlural: true,
        excludeFromInput: true
      }),
      new LiteralDefinition({
        literalName: 'createdBy',
        isSearchable: true,
        isAggregable: true,
        linkPath: new LinkPath()
          .concat({
            linkPaths: [
              hasCreatorPersonLink.getLinkPath().clone().property({ propertyDefinition: new LiteralDefinition({
                literalName: 'firstName',
                rdfDataProperty: 'foaf:firstName',
              }) }),
              hasCreatorPersonLink.getLinkPath().clone().property({ propertyDefinition: new LiteralDefinition({
                literalName: 'lastName',
                rdfDataProperty: 'foaf:lastName',
              }) }),
              hasCreatorPersonLink.getLinkPath().clone().property({ propertyDefinition: new LiteralDefinition({
                literalName: 'nickName',
                rdfDataProperty: 'foaf:nick',
              }) }),
            ],
            separator: " "
          })
      }),
      new LiteralDefinition({
        literalName: 'createdAt',
        linkPath: new LinkPath()
          .step({linkDefinition: EntityDefinition.getLink("hasCreationAction")})
          .property({
            propertyDefinition: CreationDefinition.getLiteral("startedAtTime"),
            rdfDataPropertyAlias: 'mnx:createdAt'
          }),
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#dateTimeStamp'
      }),
      new LiteralDefinition({
        literalName: 'updatedAt',
        linkPath: new LinkPath()
          .step({linkDefinition: EntityDefinition.getLink("hasAction")})
          .property({
            propertyDefinition: CreationDefinition.getLiteral("startedAtTime"),
            rdfDataPropertyAlias: 'mnx:updatedAt'
          }),
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#dateTimeStamp'
      }),
      new LiteralDefinition({
        literalName: 'seeAlso',
        rdfDataProperty: "rdfs:seeAlso"
      }),
      new LiteralDefinition({
        literalName: 'comment',
        rdfDataProperty: "rdfs:comment"
      }),
    ];
  }

  static getLabels() {
    return [
      new LabelDefinition({
        labelName: "accessPolicyLabel",
        linkPath: new LinkPath()
          .step({linkDefinition: EntityDefinition.getLink("hasAccessPolicy")})
          .property({propertyDefinition: AccessPolicyDefinition.getLabel("label")}),
        defaultValue: "\\\"Privé\\\"@fr",
        isAggregable: true
      })
    ]
  }

  static getFilters(){
    return [
      // This custom filter is an attempt to securize an entity given :
      //  - its access policy (that must match with readableAccessPolicyIds parameter)
      //  - its readOnlyAccessTargetAccess/readWriteAccessTargetAccess  (that must match with accessTargetIds parameter)
      new FilterDefinition({
        filterName: "hasReadAccessGranted",
        indexFilter: ({accessTargetIds, readableAccessPolicyIds} = {}) => {
          if(!accessTargetIds){
            throw new I18nError("userAccountId must be passed")
          }

          if(!readableAccessPolicyIds){
            throw new I18nError("readableAccessPolicyIds must be passed")
          }

          return ({
            "bool": {
              "should" : [
                { "terms" : { [this.getLink("hasReadOnlyAccessTarget").getPathInIndex()] : accessTargetIds } },
                { "terms" : { [this.getLink("hasReadWriteAccessTarget").getPathInIndex()] : accessTargetIds } },
                { "terms" : { [this.getLink("hasAccessPolicy").getPathInIndex()] : readableAccessPolicyIds } }
              ],
            }
          });
        }
      }),
      // This custom filter is an attempt to securize an entity given its readOnlyAccessTargetAccess/readWriteAccessTargetAccess  (that must match with accessTargetIds parameter)
      new FilterDefinition({
        filterName: "hasWriteAccessGranted",
        indexFilter: ({accessTargetIds} = {}) => {
          if(!accessTargetIds){
            throw new I18nError("userAccountId must be passed")
          }

          return ({
            "bool": {
              "should" : [
                { "terms" : { [this.getLink("hasReadWriteAccessTarget").getPathInIndex()] : accessTargetIds } }
              ],
            }
          });
        }
      })
    ];
  }
};