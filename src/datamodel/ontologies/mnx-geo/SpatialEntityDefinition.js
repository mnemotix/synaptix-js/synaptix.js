/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import GeometryDefinition from "./GeometryDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import EntityDefinition from "../mnx-common/EntityDefinition";
import GraphQLInterfaceDefinition from "../../toolkit/graphql/schema/GraphQLInterfaceDefinition";
import {LiteralDefinition} from "../../toolkit/definitions";
import {LinkPath} from "../../toolkit/utils/linkPath";

export default class SpatialEntityDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLInterfaceDefinition;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:SpatialEntity";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasGeometry',
        rdfObjectProperty: 'http://www.opengis.net/ont/geosparql#hasGeometry',
        relatedModelDefinition: GeometryDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false,
        graphQLPropertyName: "geometry",
        graphQLInputName: "geometryInput"
      })
    ]
  }

  static getLiterals(){
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "geoloc",
        inIndexOnly: true,
        linkPath: new LinkPath()
          .step({ linkDefinition: this.getLink("hasGeometry") })
          .property({
            propertyDefinition : GeometryDefinition.getLiteral("asWKT")
          })
      })
    ]
  }
};

