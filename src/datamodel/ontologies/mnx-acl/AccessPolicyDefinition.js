/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../mnx-common/EntityDefinition";
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";

export default class AccessPolicyDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:AccessPolicy"
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "access-policy"
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasEntity',
        rdfReversedObjectProperty: "mnx:hasAccessPolicy",
        relatedModelDefinition: EntityDefinition,
        isPlural: true,
        graphQLPropertyName: "entities",
        graphQLInputName: "entityInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'label',
        rdfDataProperty: "rdfs:label"
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: "dc:description"
      }),
    ]
  }
};
