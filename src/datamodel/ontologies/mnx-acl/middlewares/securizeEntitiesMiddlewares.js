import {securizeEntitiesMutationMiddleware} from "./securizeEntitiesMutationMiddleware";
import {securizeEntitiesQueryMiddleware} from "./securizeEntitiesQueryMiddleware";

/**
 *  Use that middleware to securize both Query/Mutation entities.
 *
 *  @param {string} [publicPolicyId] - Mininum access policy ID to access entities. Set PUBLIC_POLICY_ID env variable if not using this parameter.
 */
export function securizeEntitiesMiddlewares({publicPolicyId} = {}){
  return [securizeEntitiesQueryMiddleware({publicPolicyId}), securizeEntitiesMutationMiddleware()];
}