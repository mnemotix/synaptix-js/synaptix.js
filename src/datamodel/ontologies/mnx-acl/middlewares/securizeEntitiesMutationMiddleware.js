/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import env from "env-var";
import EntityDefinition from "../../mnx-common/EntityDefinition";
import {LinkFilter, QueryFilter} from "../../../toolkit/utils/filter";
import {getModelDefinitionForGraphQLType} from "./securizeEntitiesQueryMiddleware";

/**
 * Use this middleware to add a custom search filter before a mutation
 */
export function securizeEntitiesMutationMiddleware(){
  /**
   * Default resolver.
   * @param {function} resolve
   * @param {object} object
   * @param {object} args
   * @param {SynaptixDatastoreSession} synaptixSession
   * @param {object} info
   */
  return async (
    resolve,
      object,
      args,
      synaptixSession,
      info
  )  => {
    if (env.get("FULL_FEATURE_ENABLED").asBool() === true) {
      return resolve(object, args, synaptixSession, info);
    }

    if (info.operation?.operation !== "mutation") {
      return resolve(object, args, synaptixSession, info);
    }

    const fieldName = info.fieldName.toString();

    // In case of removal or update mutations, add a select filter.
    if (fieldName === "removeEntitiesByFilters" || !!fieldName.match(/^(update|remove|batchUpdate)/)){
      const userAccountId = await synaptixSession.getLoggedUserAccountId();
      const userAccountGroupIds  = await synaptixSession.getLoggedUserAccountGroupIds();

      args.queryFilters = (args.queryFilters || []).concat([
        new QueryFilter({
          filterDefinition: EntityDefinition.getFilter("hasWriteAccessGranted"),
          filterGenerateParams: {
            accessTargetIds: [userAccountId, ...userAccountGroupIds]
          },
          isStrict: true,
        }),
      ]);
    }

    return resolve(object, args, synaptixSession, info);
  };
}