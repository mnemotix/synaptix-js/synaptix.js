/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import env from "env-var";
import EntityDefinition from "../../mnx-common/EntityDefinition";
import {LinkFilter, QueryFilter} from "../../../toolkit/utils/filter";

/**
 * Use this middleware to add a custom search filter and restrict results
 * @param {string} [publicPolicyId] - Mininum access policy ID to access entities. Set PUBLIC_POLICY_ID env variable if not using this parameter.
 */
export function securizeEntitiesQueryMiddleware({
  publicPolicyId
} = {}){
  /**
   * Default resolver.
   * @param {function} resolve
   * @param {object} object
   * @param {object} args
   * @param {SynaptixDatastoreSession} synaptixSession
   * @param {object} info
   */
  return async (
    resolve,
      object,
      args,
      synaptixSession,
      info
  )  => {
    if (env.get("FULL_FEATURE_ENABLED").asBool() === true) {
      return resolve(object, args, synaptixSession, info);
    }

    if (info.operation?.operation === "mutation") {
      return resolve(object, args, synaptixSession, info);
    }

    const fieldName = info.fieldName.toString();
    const parentType = info.parentType.toString();
    let returnType = info.returnType.toString();

    //
    // This is the case of removeEntitiesByFilters middleware
    //
    if (fieldName === "removeEntitiesByFilters"){
      returnType = args.input.type;
    }

    if (
      ["me", "pageInfo", "edges", "node"].includes(fieldName) ||
      parentType.match(/Edge$|Node|PageInfo$/)
    ) {
      return resolve(object, args, synaptixSession, info);
    }

    let matchingModelDefinition;

    // Case of type count
    // - If parentType is query, need to fetch
    if (returnType === "Int" && fieldName.match(/Count$/)) {
      if (parentType === "Query") {
        matchingModelDefinition = getModelDefinitionForConnectionCountName({
          synaptixSession,
          connectionCountName: fieldName,
        });
      } else {
        matchingModelDefinition = getModelDefinitionForParentTypeAndFieldName({
          synaptixSession,
          parentType,
          fieldName,
        });
      }
    } else if (
      !returnType
        .replace(/[\[\]\!]/g, "")
        .match(/^(Int|Float|String|Boolean|ID|Upload)$/)
    ) {
      matchingModelDefinition = getModelDefinitionForGraphQLType({
        synaptixSession,
        graphQLType: returnType,
      });
    }

    if (matchingModelDefinition?.isEqualOrDescendantOf(EntityDefinition)) {
      // prettier-ignore
      if(!publicPolicyId){
        publicPolicyId = synaptixSession.extractIdFromGlobalId(
          env.get("PUBLIC_POLICY_ID").required().asString()
        );
      }

      // Case 1.
      // If user is not logged (not authenticated requests),
      // Then restrict entities with PublicPolicy access policy
      if (!(await synaptixSession.isLoggedUser())) {
        args.linkFilters = (args.linkFilters || []).concat([
          new LinkFilter({
            linkDefinition: EntityDefinition.getLink("hasAccessPolicy"),
            id: publicPolicyId,
          }),
        ]);
      }
      // Case 2.
      // Else restrict entities with
      //  - PublicPolicy access policy
      //  - The ones which have the user has readOnlyAccessTarget | readWriteAccessTarget
      else {
        const userAccountId = await synaptixSession.getLoggedUserAccountId();
        const userAccountGroupIds  = await synaptixSession.getLoggedUserAccountGroupIds();

        args.queryFilters = (args.queryFilters || []).concat([
          new QueryFilter({
            filterDefinition: EntityDefinition.getFilter("hasReadAccessGranted"),
            filterGenerateParams: {
              accessTargetIds: [userAccountId, ...userAccountGroupIds],
              readableAccessPolicyIds: [publicPolicyId],
            },
            isStrict: true,
          }),
        ]);
      }
    }

    return resolve(object, args, synaptixSession, info);
  };
}

let cache = {};

export function getModelDefinitionForGraphQLType({ synaptixSession, graphQLType }) {
  const normalizedGraphQLType = graphQLType.replace("Connection", "");
  const cacheKey = `gqlType_${normalizedGraphQLType}`;
  if (!cache[cacheKey]) {
    try {
      cache[
        cacheKey
        ] = synaptixSession
        .getModelDefinitionsRegister()
        .getModelDefinitionForGraphQLType(normalizedGraphQLType);
    } catch (e) {}
  }

  return cache[cacheKey];
}

function getModelDefinitionForParentTypeAndFieldName({
  synaptixSession,
  parentType,
  fieldName,
}) {
  const cacheKey = `gqlType_${parentType}_${fieldName}`;
  if (!cache[cacheKey]) {
    const parentModelDefinition = getModelDefinitionForGraphQLType({
      synaptixSession,
      graphQLType: parentType,
    });

    cache[cacheKey] = parentModelDefinition
      ?.getLinks()
      .find(
        (linkDefinition) =>
          linkDefinition.getGraphQLPropertyName() ===
          fieldName.replace("Count", "")
      )
      ?.getRelatedModelDefinition();
  }

  return cache[cacheKey];
}

function getModelDefinitionForConnectionCountName({
  synaptixSession,
  connectionCountName,
}) {
  const cacheKey = `gqlConCountName_${connectionCountName}`;
  if (!cache[cacheKey]) {
    cache[cacheKey] = synaptixSession
      .getModelDefinitionsRegister()
      .getModelDefinitions()
      .find((modelDefinition) => {
        return (
          modelDefinition
            .getGraphQLDefinition()
            ?.getTypeConnectionQuery()
            ?.generateCountName(modelDefinition) === connectionCountName
        );
      });
  }

  return cache[cacheKey];
}
