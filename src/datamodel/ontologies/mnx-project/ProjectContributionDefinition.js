/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import ProjectDefinition from "./ProjectDefinition";
import InvolvementDefinition from "./InvolvementDefinition";
import AttachmentDefinition from "./AttachmentDefinition";
import DurationDefinition from "../mnx-time/DurationDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath";
import FileDefinition from "./FileDefinition";

export default class ProjectContributionDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [DurationDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ProjectContribution";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'project-contribution';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasProject',
        symmetricLinkName: 'hasProjectContribution',
        rdfObjectProperty: "mnx:hasProject",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true,
        isPlural: true,
        graphQLPropertyName: "projects",
        graphQLInputName: 'projectInputs'
      }),
      new LinkDefinition({
        linkName: 'hasMainProject',
        symmetricLinkName: 'hasProjectContribution',
        rdfObjectProperty: "mnx:hasProject",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true,
        graphQLPropertyName: "project",
        graphQLInputName: 'projectInput',
        description: "@deprecated. Use projects instead."
      }),
      new LinkDefinition({
        linkName: 'hasReply',
        symmetricLinkName: 'isReplyOf',
        rdfReversedObjectProperty: "mnx:isReplyOf",
        relatedModelDefinition: ProjectContributionDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "replies",
        graphQLInputName: 'replyInputs'

      }),
      new LinkDefinition({
        linkName: 'isReplyOf',
        symmetricLinkName: 'hasReply',
        rdfObjectProperty: "mnx:isReplyOf",
        relatedModelDefinition: ProjectContributionDefinition,
        graphQLPropertyName: "replyTo",
        graphQLInputName: 'replyToInput'

      }),
      new LinkDefinition({
        linkName: 'hasAttachment',
        symmetricLinkName: "hasProjectContribution",
        rdfObjectProperty: "mnx:hasAttachment",
        relatedModelDefinition: AttachmentDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "attachments",
        graphQLInputName: 'attachmentInputs'
      }),
      new LinkDefinition({
        linkName: 'hasInvolvement',
        symmetricLinkName: "hasProjectContribution",
        rdfObjectProperty: "mnx:hasInvolvement",
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "involvements",
        graphQLInputName: 'involvementInputs'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'title',
        rdfDataProperty: 'mnx:title'
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: 'mnx:description'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        rdfDataProperty: 'mnx:shortDescription'
      }),
      new LabelDefinition({
        labelName: 'memo',
        rdfDataProperty: 'mnx:memo'
      }),
      new LabelDefinition({
        labelName: 'gatheredAttachmentsFilenames',
        linkPath: new LinkPath()
          .step({ linkDefinition: this.getLink("hasAttachment") })
          .step({ linkDefinition: AttachmentDefinition.getLink("hasResource") })
          .property({ propertyDefinition: FileDefinition.getProperty("filename") }),
        isAggregable: true
      })
    ];
  }
};