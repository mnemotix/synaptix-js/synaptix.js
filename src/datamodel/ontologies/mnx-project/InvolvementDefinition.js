/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import AgentDefinition from "../mnx-agent/AgentDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";
import ProjectContributionDefinition from "./ProjectContributionDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import ProjectDefinition from "./ProjectDefinition";
import AccessPolicyDefinition from "../mnx-acl/AccessPolicyDefinition";
import { LinkPath } from "../../toolkit/utils/linkPath";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";
import DeletionDefinition from "../mnx-contribution/DeletionDefinition";

export default class InvolvementDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Involvement";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "involvement";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();

    const indexOf = parentLinks.findIndex(
      link => link.getLinkName() === "hasAccessPolicy"
    );
    if (indexOf > 0) {
      parentLinks.splice(indexOf, 1);
    }

    const hasDefaultDeletionLinkDefinition = parentLinks.find(
      link => link.getLinkName() === "hasDeletionAction"
    );

    if (hasDefaultDeletionLinkDefinition) {
      parentLinks.splice(
        parentLinks.indexOf(hasDefaultDeletionLinkDefinition),
        1
      );
    }

    const hasAgentLinkDefinition = new LinkDefinition({
      linkName: "hasAgent",
      symmetricLinkName: "hasInvolvement",
      rdfObjectProperty: "mnx:hasAgent",
      relatedModelDefinition: AgentDefinition,
      isCascadingUpdated: true,
      graphQLPropertyName: "agent",
      graphQLInputName: "agentInput"
    });

    const hasProjectContributionLinkDefinition = new LinkDefinition({
      linkName: "hasProjectContribution",
      symmetricLinkName: "hasInvolvement",
      rdfObjectProperty: "mnx:isInvolvementOf",
      relatedModelDefinition: ProjectContributionDefinition,
      isCascadingUpdated: true,
      graphQLPropertyName: "projectContribution",
      graphQLInputName: "projectContributionInput"
    });

    const hasProjectLinkDefinition = new LinkDefinition({
      linkName: "hasProject",
      symmetricLinkName: "hasInvolvement",
      rdfObjectProperty: "mnx:hasProject",
      relatedModelDefinition: ProjectDefinition,
      isCascadingUpdated: true,
      graphQLPropertyName: "project",
      graphQLInputName: "projectInput"
    });

    //
    // This is a reification class,
    // If one the reified object is deleted, reification must be deleted.
    //
    const hasDeletionLinkDefinition = new LinkDefinition({
      linkName: 'hasDeletionAction',
      relatedModelDefinition: DeletionDefinition,
      graphQLPropertyName: "deletionAction",
      linkPath: new LinkPath().union({
        linkPaths: [
          new LinkPath()
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasAgentLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasProjectContributionLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasProjectLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
        ]
      })
    });

    return [
      ...parentLinks,
      hasAgentLinkDefinition,
      hasProjectContributionLinkDefinition,
      hasProjectLinkDefinition,
      hasDeletionLinkDefinition,
      new LinkDefinition({
        linkName: "hasAccessPolicy",
        relatedModelDefinition: AccessPolicyDefinition,
        graphQLPropertyName: "accessPolicy",
        linkPath: () =>
          new LinkPath()
            .step({ linkDefinition: hasAgentLinkDefinition })
            .step({
              linkDefinition: EntityDefinition.getLink("hasAccessPolicy")
            })
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "role",
        rdfDataProperty: "mnx:role"
      })
    ];
  }
}
