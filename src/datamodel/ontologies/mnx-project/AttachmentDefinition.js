/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import ProjectContributionDefinition from "./ProjectContributionDefinition";
import ResourceDefinition from "./ResourceDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import AccessPolicyDefinition from "../mnx-acl/AccessPolicyDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";
import DeletionDefinition from "../mnx-contribution/DeletionDefinition";

export default class AttachmentDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Attachment";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'attachment';
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    return [{
      "exists":  {
        "field": this.getLink("hasResource").getPathInIndex()
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();

    const indexOfAccessPolicyLink = parentLinks.findIndex((link) => link.getLinkName() === "hasAccessPolicy");
    if(indexOfAccessPolicyLink > 0){
      parentLinks.splice(indexOfAccessPolicyLink, 1);
    }

    const hasDefaultDeletionLinkDefinition = parentLinks.find((link) => link.getLinkName() === "hasDeletionAction");

    if(hasDefaultDeletionLinkDefinition){
      parentLinks.splice(parentLinks.indexOf(hasDefaultDeletionLinkDefinition), 1);
    }

    const hasResourceLinkDefinition = new LinkDefinition({
      linkName: 'hasResource',
      rdfObjectProperty: "mnx:hasResource",
      relatedModelDefinition: ResourceDefinition,
      isCascadingUpdated: true,
      graphQLPropertyName: "resource",
      graphQLInputName: "resourceInput"
    });

    const hasProjectContributionLinkDefinition = new LinkDefinition({
      linkName: 'hasProjectContribution',
      rdfObjectProperty: "mnx:isAttachmentOf",
      relatedModelDefinition: ProjectContributionDefinition,
      isCascadingUpdated: true,
      graphQLPropertyName: "projectContribution",
      graphQLInputName: "projectContributionInput"
    });

    //
    // This is a reification class,
    // If one the reified object is deleted, reification must be deleted.
    //
    const hasDeletionLinkDefinition = new LinkDefinition({
      linkName: 'hasDeletionAction',
      relatedModelDefinition: DeletionDefinition,
      graphQLPropertyName: "deletionAction",
      linkPath: new LinkPath().union({
        linkPaths: [
          new LinkPath()
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasResourceLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
          new LinkPath()
            .step({linkDefinition: hasProjectContributionLinkDefinition })
            .step({linkDefinition: hasDefaultDeletionLinkDefinition }),
        ]
      })
    });

    return [
      ...parentLinks,
      hasResourceLinkDefinition,
      hasProjectContributionLinkDefinition,
      hasDeletionLinkDefinition,
      new LinkDefinition({
        linkName: 'hasAccessPolicy',
        relatedModelDefinition: AccessPolicyDefinition,
        graphQLPropertyName: "accessPolicy",
        linkPath: () => new LinkPath()
          .step({linkDefinition: hasResourceLinkDefinition})
          .step({linkDefinition: EntityDefinition.getLink("hasAccessPolicy")})
      }),

    ];
  }
};