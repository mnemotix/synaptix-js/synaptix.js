/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import ProjectDefinition from "./ProjectDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import AgentDefinition from "../mnx-agent/AgentDefinition";
import FileDefinition from "./FileDefinition";
import GraphQLInterfaceDefinition from "../../toolkit/graphql/schema/GraphQLInterfaceDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath";

export default class ProjectOutputDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLInterfaceDefinition;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ProjectOutput";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'project-output';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasProject',
        rdfObjectProperty: "mnx:isProjectOutputOf",
        relatedModelDefinition: ProjectDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        graphQLPropertyName: "projects",
        graphQLInputName: 'projectInputs'
      }),
      new LinkDefinition({
        linkName: 'hasPicture',
        rdfObjectProperty: "mnx:hasPicture",
        relatedModelDefinition: FileDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        graphQLPropertyName: "pictures",
        graphQLInputName: 'pictureInputs'
      }),
      new LinkDefinition({
        linkName: 'hasMainPicture',
        rdfObjectProperty: "mnx:hasMainPicture",
        relatedModelDefinition: FileDefinition,
        isCascadingUpdated: true,
        graphQLPropertyName: "mainPicture",
        graphQLInputName: 'mainPictureInput'
      }),
      new LinkDefinition({
        linkName: 'hasRepresentation',
        rdfObjectProperty: "mnx:hasRepresentation",
        relatedModelDefinition: ProjectOutputDefinition,
        isCascadingUpdated: true,
        isPlural: true,
        graphQLPropertyName: "representations",
        graphQLInputName: 'representationInputs'
      }),
      new LinkDefinition({
        linkName: 'hasAuthor',
        rdfObjectProperty: "mnx:hasAuthor",
        relatedModelDefinition: AgentDefinition,
        isCascadingUpdated: true,
        isPlural: true,
        graphQLPropertyName: "authors",
        graphQLInputName: 'authorInputs'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'title',
        rdfDataProperty: 'mnx:title',
        isSearchable: true,
        isRequired: true
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: 'mnx:description'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        rdfDataProperty: 'mnx:shortDescription'
      }),
      new LabelDefinition({
        labelName: 'projectTitles',
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("hasProject")
          })
          .property({
            propertyDefinition: ProjectDefinition.getProperty("title")
          }),
        isPlural: true,
        isAggregable: true
      }),
      new LabelDefinition({
        labelName: 'pictureUrls',
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("hasPicture")
          })
          .property({
            propertyDefinition: FileDefinition.getProperty("publicUrl")
          }),
        isPlural: true
      })
    ];
  }
};