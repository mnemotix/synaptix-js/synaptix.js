/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import AttachmentDefinition from "./AttachmentDefinition";
import ProjectDefinition from "./ProjectDefinition";
import FilterDefinition from "../../toolkit/definitions/FilterDefinition";
import GraphQLInterfaceDefinition from "../../toolkit/graphql/schema/GraphQLInterfaceDefinition";
import { LinkPath } from "../../toolkit/utils/linkPath";
import ProjectContributionDefinition from "./ProjectContributionDefinition";

export default class ResourceDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable() {
    return false;
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLInterfaceDefinition;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Resource";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "resource";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const attachmentLinkDefinition = new LinkDefinition({
      linkName: "hasAttachment",
      symmetricLinkName: "hasResource",
      rdfObjectProperty: "mnx:isResourceOf",
      relatedModelDefinition: AttachmentDefinition,
      isPlural: true,
      isCascadingRemoved: true,
      graphQLPropertyName: "attachments",
      graphQLInputName: "attachmentInputs"
    });

    const projectLinkContribution = new LinkDefinition({
      linkName: "hasProject",
      rdfObjectProperty: "mnx:hasProject",
      relatedModelDefinition: ProjectDefinition,
      graphQLPropertyName: "project",
      graphQLInputName: "projectInput"
    });

    const projectContributionLinkDefinition = new LinkDefinition({
      linkName: "hasProjectContribution",
      linkPath: new LinkPath()
        .step({ linkDefinition: attachmentLinkDefinition })
        .step({
          linkDefinition: AttachmentDefinition.getLink("hasProjectContribution")
        }),
      relatedModelDefinition: ProjectContributionDefinition,
      isPlural: true,
      inIndexOnly: true
    });

    const gatheredProjectsLinkDefinition = new LinkDefinition({
      linkName: "hasGatheredProjects",
      pathInIndex: "gatheredProjects",
      linkPath: new LinkPath().union({
        linkPaths: [
          new LinkPath()
            .append({
              linkPath: projectContributionLinkDefinition.getLinkPath()
            })
            .step({
              linkDefinition: ProjectContributionDefinition.getLink(
                "hasProject"
              )
            }),
          new LinkPath().step({
            linkDefinition: projectLinkContribution
          })
        ]
      }),
      relatedModelDefinition: ProjectDefinition,
      isPlural: true,
      inIndexOnly: true
    });

    return [
      ...super.getLinks(),
      attachmentLinkDefinition,
      projectLinkContribution,
      projectContributionLinkDefinition,
      gatheredProjectsLinkDefinition
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    const gatheredProjectsTitleLabelDefinition = new LabelDefinition({
      labelName: "gatheredProjectsTitles",
      linkPath: new LinkPath().union({
        linkPaths: [
          new LinkPath()
            .append({
              linkPath: this.getLink("hasProjectContribution").getLinkPath()
            })
            .step({
              linkDefinition: ProjectContributionDefinition.getLink("hasProject")
            })
            .property({ propertyDefinition: ProjectDefinition.getProperty("title")})
          ,
          new LinkPath()
            .step({
              linkDefinition: this.getLink("hasProject")
            })
            .property({ propertyDefinition: ProjectDefinition.getProperty("title")})
        ]
      }),
      isPlural: true,
      isAggregable: true
    });

    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "description",
        rdfDataProperty: "mnx:description"
      }),
      new LabelDefinition({
        labelName: "credits",
        rdfDataProperty: "mnx:credits"
      }),
      gatheredProjectsTitleLabelDefinition
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "publicUrl",
        rdfDataProperty: "mnx:publicUrl",
        isRequired: true
      }),
      new LiteralDefinition({
        literalName: "uploadTag",
        rdfDataProperty: "mnx:uploadTag",
        isRequired: true
      })
    ];
  }

  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "hasAttachment",
        indexFilter: () => ({
          exists: {
            field: ResourceDefinition.getLink("hasAttachment").getPathInIndex()
          }
        })
      }),
      new FilterDefinition({
        filterName: "hasProject",
        indexFilter: () => ({
          exists: {
            field: ResourceDefinition.getLink("hasGatheredProjects").getPathInIndex()
          }
        })
      })
    ];
  }
}
