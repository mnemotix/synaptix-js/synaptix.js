/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition  from "../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import ProjectContributionDefinition from "./ProjectContributionDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import ProjectOutputDefinition from "./ProjectOutputDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import UserGroupDefinition from "../mnx-agent/UserGroupDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath/LinkPath";
import FilterDefinition from "../../toolkit/definitions/FilterDefinition";
import InvolvementDefinition from "./InvolvementDefinition";
import ResourceDefinition from "./ResourceDefinition";
import PersonDefinition from "../mnx-agent/PersonDefinition";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";
import AgentDefinition from "../mnx-agent/AgentDefinition";
import DurationDefinition from "../mnx-time/DurationDefinition";

export default class ProjectDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [DurationDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Project";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'project';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const projectContributionLink = new LinkDefinition({
      linkName: 'hasProjectContribution',
      symmetricLinkName: "hasProject",
      rdfObjectProperty: "mnx:isProjectOf",
      relatedModelDefinition: ProjectContributionDefinition,
      isPlural: true,
      isCascadingUpdated: true,
      isCascadingRemoved: true,
      graphQLPropertyName: "projectContributions",
      graphQLInputName: 'projectContributionInputs'
    });

    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasAuthor',
        rdfReversedObjectProperty: "mnx:hasAuthor",
        relatedModelDefinition: PersonDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        graphQLPropertyName: "authors",
        graphQLInputName: 'authorInputs'
      }),
      new LinkDefinition({
        linkName: 'hasParentProject',
        symmetricLinkName: "hasSubProject",
        rdfObjectProperty: "mnx:hasParentProject",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true,
        graphQLPropertyName: "parentProject",
        graphQLInputName: 'parentProjectInput'
      }),
      new LinkDefinition({
        linkName: 'hasSubProject',
        symmetricLinkName: "hasParentProject",
        rdfReversedObjectProperty: "mnx:hasParentProject",
        relatedModelDefinition: ProjectDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        graphQLPropertyName: "subProjects",
        graphQLInputName: 'subProjectInputs'
      }),
      projectContributionLink,
      new LinkDefinition({
        linkName: 'hasInvolvement',
        symmetricLinkName: "hasProject",
        rdfReversedObjectProperty: "mnx:hasProject",
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "involvements",
        graphQLInputName: 'involvementInputs'
      }),
      new LinkDefinition({
        linkName: 'hasInvolvedAgents',
        pathInIndex: "involvedAgents",
        inIndexOnly: true,
        relatedModelDefinition: AgentDefinition,
        isPlural: true,
        graphQLPropertyName: "gatheredInvolvedAgents",
        linkPath: new LinkPath()
          .step({ linkDefinition: projectContributionLink })
          .step({ linkDefinition: ProjectContributionDefinition.getLink("hasInvolvement") })
          .step({ linkDefinition: InvolvementDefinition.getLink("hasAgent") })
      }),
      new LinkDefinition({
        linkName: 'hasProjectOutput',
        symmetricLinkName: "hasProject",
        rdfObjectProperty: "mnx:hasProjectOutput",
        relatedModelDefinition: ProjectOutputDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "projectOutputs",
        graphQLInputName: 'projectOutputInputs'
      }),
      new LinkDefinition({
        linkName: 'hasResource',
        symmetricLinkName: "hasProject",
        rdfReversedObjectProperty: "mnx:hasProject",
        relatedModelDefinition: ResourceDefinition,
        isPlural: true,
        graphQLPropertyName: "resources",
        graphQLInputName: 'resourceInputs'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'title',
        rdfDataProperty: 'mnx:title',
        isRequired: true,
        searchBoost: 2,
        isAggregable: true
      }),
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: 'mnx:description'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        rdfDataProperty: "mnx:shortDescription"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      }),
      new LiteralDefinition({
        literalName: 'image',
        rdfDataProperty: "mnx:image"
      }),
      new LiteralDefinition({
        literalName: 'isParentProject',
        linkPath: new LinkPath()
          .mustNotExistPropertyBinding({
            bindAs: "isParentProject",
            linkPath: new LinkPath()
              .step({linkDefinition: ProjectDefinition.getLink("hasParentProject")}),
            rdfDataPropertyAlias: "mnx:isParentProject"
          }),
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#boolean'
      })
    ];
  }

  static getFilters() {
    return [
      new FilterDefinition({
        filterName: "hasParentProject",
        indexFilter: () => ({
          "exists": {
            "field": "hasParentProject"
          }
        })
      })
    ]
  }
};