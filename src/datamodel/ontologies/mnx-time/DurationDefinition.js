/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import TemporalEntityDefinition from "./TemporalEntityDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import GraphQLInterfaceDefinition from "../../toolkit/graphql/schema/GraphQLInterfaceDefinition";

export default class DurationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition(){
    return GraphQLInterfaceDefinition
  }

  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [TemporalEntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Duration";
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'endDate',
        rdfDataProperty: 'mnx:endDate',
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#dateTimeStamp'
      }), new LiteralDefinition({
        literalName: 'startDate',
        rdfDataProperty: 'mnx:startDate',
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#dateTimeStamp'
      })
    ];
  }
};

