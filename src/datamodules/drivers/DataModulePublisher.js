/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { v4 as uuid }  from 'uuid';
import get from "lodash/get";
import {SynaptixError} from "./SynaptixError";
import {logError} from "../../utilities/logger";
import DataLoader from "dataloader";
import hash from "object-hash";
import env from "env-var";

/**
 * @typedef {object} SynaptixResponse
 * @property {string} command
 * @property {string} sender
 * @property {number} date
 * @property {*} body
 */

export default class DataModulePublisher {
  /**
   * @param {NetworkLayer} networkLayer
   * @param {string} [senderId]
   */
  constructor(networkLayer, senderId) {
    this._networkLayer = networkLayer;
    this._senderId = senderId;
    this._dataloaderDisabled = !!env.get("DATALOADER_DISABLED").asBool();

    if (!this._dataloaderDisabled){
      this._publishDataLoader = new DataLoader((batchParams) => {
        return Promise.all(batchParams.map((params) => this.publishRaw(params)))
      }, {
        cacheKeyFn: params => {
          return hash(params)
        }
      });
    }
  }

  /* istanbul ignore next */
  getName() {
    return uuid();
  }

  /**
   * Publish an RPC AMQP message.
   *
   * @param {string} command RPC command
   * @param {*} body RPC body
   * @param {object} context extra AMQP headers
   * @param {boolean} [expectingResponse=true]
   * @returns {*}
   */
  async publish(command, body, context = {}, expectingResponse = true) {
    if (!this._dataloaderDisabled) {
      return this._publishDataLoader.load({
        command,
        body,
        context,
        expectingResponse
      })
    } else {
      return this.publishRaw({
        command,
        body,
        context,
        expectingResponse
      })
    }
  }

  /**
   * @param command
   * @param body
   * @param context
   * @param expectingResponse
   * @return {Promise<*>}
   */
  async publishRaw({command, body, context, expectingResponse}) {
    if (!context) {
      context = {};
    }

    let payload = {
      headers: {
        command,
        sender: this._senderId || process.env.UUID || 'mnx:app:nodejs',
        timestamp: Date.now(),
        ...context
      },
      body
    };

    let logLevel = env.get("RABBITMQ_LOG_LEVEL").asString();

    if (expectingResponse) {
      let response = await this._networkLayer.request(command, payload);

      if (response) {
        let data = typeof response === "object" ? response : JSON.parse(response);
        let status = get(data, 'headers.status');
        if (!!status) {
          if ('OK' === status) {
            return data.body;
          } else {
            if (logLevel !== "NONE") {
              logError(`Something gone wrong with the RPC command ${JSON.stringify(payload)}. ${status} : ${JSON.stringify(data.body)}`);
            }
            throw new SynaptixError(data.body, data.command, status, payload.body);
          }
        }
      }

      if (logLevel !== "NONE") {
        logError(`Callback timeout has been raised with the RPC command ${payload.headers ? payload.headers.command : payload.command} ${JSON.stringify(payload)}.`);
      }

      throw  new SynaptixError("Callback timeout has been raised with the Synaptix command.", payload.command, "TIMEOUT", payload);
    } else {
      await this._networkLayer.publish(command, payload);
    }
  }
}