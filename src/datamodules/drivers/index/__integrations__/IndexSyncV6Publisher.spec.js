/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
jest.unmock('amqplib');
jest.setTimeout(60000);

import IndexSyncV6Publisher from '../IndexControllerV6Publisher';
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";

process.env.UUID = "integration-tests";
process.env.RABBITMQ_LOG_LEVEL = "DEBUG";

let networkLayer = new NetworkLayerAMQP("amqp://admin:Pa55w0rd@172.19.0.87:5672", "default");
// let networkLayer = new NetworkLayerAMQP("amqp://guest:mnxpowa!@localhost:5672", "local-cortex");

const indexSyncPublisher = new IndexSyncV6Publisher(networkLayer, "test");

describe('IndexSyncPublisher V6', () => {
  beforeEach(async () => {
    await networkLayer.connect();
  });

  it('raises exception on misformed', async (done) => {
    await expect(
      indexSyncPublisher.query({
        indicesAndTypes: {
          indices: [""],
          types: [""]
        },
        query: {
          query: {
            match_all: {}
          }
        }
      })
    ).rejects.toThrow(Error);

    done();
  });

  it('queries on types', async (done) => {
    let hits = await indexSyncPublisher.query({
      indicesAndTypes: {
        indices: ["synaptix"],
        types: ["person"]
      },
      query: {
        match_all: {}
      }
    });

    expect(hits.total).toEqual(200);
    done();
  });
});