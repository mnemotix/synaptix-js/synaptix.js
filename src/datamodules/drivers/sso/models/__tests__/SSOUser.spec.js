/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SSOUser from '../SSOUser';

describe('SSOUser', () => {
  test("User created from JWT session", () => {
    let user = new SSOUser({
      jwtSession: {
        ticket: {
          access_token: "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJZeTNncDViYmpZY3ZNdExFcHBaRDVtdTdfV3gwTmRQNXR3RHVNR3pVb3pJIn0.eyJqdGkiOiI1OTU3ODE1YS1lMDFjLTQzODItODVhOS1iYWI2NTg2ZjUzOTgiLCJleHAiOjE1NzA1MzcwMDgsIm5iZiI6MCwiaWF0IjoxNTcwNTM2OTQ4LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgxODEvYXV0aC9yZWFsbXMvZGRmIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU5ZThiMzU3LTEyNjItNDViYy04YzkzLTEwMjgzN2IyYjZkOSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImM4NGFlYzgzLWM3YTUtNDQ4Yi04ZDMzLTZjM2E2YTFhNjJlYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovLyoiLCJodHRwczovLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJNYXRoaWV1IFJvZ2VsamEiLCJhdHRyaWJ1dGVzIjp7InBlcnNvbklkIjpbImRkZnY6cGVyc29uLzgzYW9waW52ajZ4cG02Il0sInBpY3R1cmUiOlsiaHR0cDovL2dvb2dsZS5mciJdfSwicHJlZmVycmVkX3VzZXJuYW1lIjoibWF0aGlldS5yb2dlbGphQG1uZW1vdGl4LmNvbSIsImdpdmVuX25hbWUiOiJNYXRoaWV1IiwiZmFtaWx5X25hbWUiOiJSb2dlbGphIiwicGljdHVyZSI6Imh0dHA6Ly9nb29nbGUuZnIiLCJlbWFpbCI6Im1hdGhpZXUucm9nZWxqYUBtbmVtb3RpeC5jb20ifQ.FR_SENXMZDHk5etTZ-qJvlbd0SF3kF74-y3ydnxY6bwRtvYzixAK2YRWgIENEUs943peWvvoSIp0Y5TXWiev4L_vqMfnG3I-23No63DPrAKbZt09faRGJ8Lg__MVpnEydsCg70maJ7HnINky4-0CWn55eiDi9273BRbRNfojEoCj95RF86cUHmGUXtQqD37QTFuKdDl3oQNtG1q04LS9wCervqzCwJK-eLNB3p-_Gng8lDUGsh3H1CW-Ga2vdLNkrposODZ4il4V_EVJ6ArfgRVQN3tFXvhetgtz6Bezm7DUSQyYRz8WAnIIyKgN_MuReaDL5jHgwxx3NcPbXKRKFQ",
          refresh_token: ""
        }
      }
    });

    expect(user.toJSON()).toEqual({
      id: '59e8b357-1262-45bc-8c93-102837b2b6d9',
      username: 'mathieu.rogelja@mnemotix.com',
      email: 'mathieu.rogelja@mnemotix.com',
      firstName: 'Mathieu',
      lastName: 'Rogelja',
      attributes: {
        personId: 'ddfv:person/83aopinvj6xpm6',
        picture: 'http://google.fr'
      }
    });

    expect(user.ticket).toEqual({
      "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJZeTNncDViYmpZY3ZNdExFcHBaRDVtdTdfV3gwTmRQNXR3RHVNR3pVb3pJIn0.eyJqdGkiOiI1OTU3ODE1YS1lMDFjLTQzODItODVhOS1iYWI2NTg2ZjUzOTgiLCJleHAiOjE1NzA1MzcwMDgsIm5iZiI6MCwiaWF0IjoxNTcwNTM2OTQ4LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgxODEvYXV0aC9yZWFsbXMvZGRmIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU5ZThiMzU3LTEyNjItNDViYy04YzkzLTEwMjgzN2IyYjZkOSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImM4NGFlYzgzLWM3YTUtNDQ4Yi04ZDMzLTZjM2E2YTFhNjJlYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovLyoiLCJodHRwczovLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJNYXRoaWV1IFJvZ2VsamEiLCJhdHRyaWJ1dGVzIjp7InBlcnNvbklkIjpbImRkZnY6cGVyc29uLzgzYW9waW52ajZ4cG02Il0sInBpY3R1cmUiOlsiaHR0cDovL2dvb2dsZS5mciJdfSwicHJlZmVycmVkX3VzZXJuYW1lIjoibWF0aGlldS5yb2dlbGphQG1uZW1vdGl4LmNvbSIsImdpdmVuX25hbWUiOiJNYXRoaWV1IiwiZmFtaWx5X25hbWUiOiJSb2dlbGphIiwicGljdHVyZSI6Imh0dHA6Ly9nb29nbGUuZnIiLCJlbWFpbCI6Im1hdGhpZXUucm9nZWxqYUBtbmVtb3RpeC5jb20ifQ.FR_SENXMZDHk5etTZ-qJvlbd0SF3kF74-y3ydnxY6bwRtvYzixAK2YRWgIENEUs943peWvvoSIp0Y5TXWiev4L_vqMfnG3I-23No63DPrAKbZt09faRGJ8Lg__MVpnEydsCg70maJ7HnINky4-0CWn55eiDi9273BRbRNfojEoCj95RF86cUHmGUXtQqD37QTFuKdDl3oQNtG1q04LS9wCervqzCwJK-eLNB3p-_Gng8lDUGsh3H1CW-Ga2vdLNkrposODZ4il4V_EVJ6ArfgRVQN3tFXvhetgtz6Bezm7DUSQyYRz8WAnIIyKgN_MuReaDL5jHgwxx3NcPbXKRKFQ",
      "refresh_token": ""
    });
    expect(user.toJWTSession()).toEqual({
      "ticket": {
        "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJZeTNncDViYmpZY3ZNdExFcHBaRDVtdTdfV3gwTmRQNXR3RHVNR3pVb3pJIn0.eyJqdGkiOiI1OTU3ODE1YS1lMDFjLTQzODItODVhOS1iYWI2NTg2ZjUzOTgiLCJleHAiOjE1NzA1MzcwMDgsIm5iZiI6MCwiaWF0IjoxNTcwNTM2OTQ4LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgxODEvYXV0aC9yZWFsbXMvZGRmIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU5ZThiMzU3LTEyNjItNDViYy04YzkzLTEwMjgzN2IyYjZkOSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImM4NGFlYzgzLWM3YTUtNDQ4Yi04ZDMzLTZjM2E2YTFhNjJlYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovLyoiLCJodHRwczovLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJNYXRoaWV1IFJvZ2VsamEiLCJhdHRyaWJ1dGVzIjp7InBlcnNvbklkIjpbImRkZnY6cGVyc29uLzgzYW9waW52ajZ4cG02Il0sInBpY3R1cmUiOlsiaHR0cDovL2dvb2dsZS5mciJdfSwicHJlZmVycmVkX3VzZXJuYW1lIjoibWF0aGlldS5yb2dlbGphQG1uZW1vdGl4LmNvbSIsImdpdmVuX25hbWUiOiJNYXRoaWV1IiwiZmFtaWx5X25hbWUiOiJSb2dlbGphIiwicGljdHVyZSI6Imh0dHA6Ly9nb29nbGUuZnIiLCJlbWFpbCI6Im1hdGhpZXUucm9nZWxqYUBtbmVtb3RpeC5jb20ifQ.FR_SENXMZDHk5etTZ-qJvlbd0SF3kF74-y3ydnxY6bwRtvYzixAK2YRWgIENEUs943peWvvoSIp0Y5TXWiev4L_vqMfnG3I-23No63DPrAKbZt09faRGJ8Lg__MVpnEydsCg70maJ7HnINky4-0CWn55eiDi9273BRbRNfojEoCj95RF86cUHmGUXtQqD37QTFuKdDl3oQNtG1q04LS9wCervqzCwJK-eLNB3p-_Gng8lDUGsh3H1CW-Ga2vdLNkrposODZ4il4V_EVJ6ArfgRVQN3tFXvhetgtz6Bezm7DUSQyYRz8WAnIIyKgN_MuReaDL5jHgwxx3NcPbXKRKFQ",
        "refresh_token": "",
        "uid": "59e8b357-1262-45bc-8c93-102837b2b6d9"
      }
    });
    expect(user.getEmail()).toEqual("mathieu.rogelja@mnemotix.com");
    expect(user.getId()).toEqual("59e8b357-1262-45bc-8c93-102837b2b6d9");
    expect(user.id).toEqual("59e8b357-1262-45bc-8c93-102837b2b6d9");
    expect(user.getAccessToken()).toEqual("eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJZeTNncDViYmpZY3ZNdExFcHBaRDVtdTdfV3gwTmRQNXR3RHVNR3pVb3pJIn0.eyJqdGkiOiI1OTU3ODE1YS1lMDFjLTQzODItODVhOS1iYWI2NTg2ZjUzOTgiLCJleHAiOjE1NzA1MzcwMDgsIm5iZiI6MCwiaWF0IjoxNTcwNTM2OTQ4LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgxODEvYXV0aC9yZWFsbXMvZGRmIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU5ZThiMzU3LTEyNjItNDViYy04YzkzLTEwMjgzN2IyYjZkOSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImM4NGFlYzgzLWM3YTUtNDQ4Yi04ZDMzLTZjM2E2YTFhNjJlYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovLyoiLCJodHRwczovLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJNYXRoaWV1IFJvZ2VsamEiLCJhdHRyaWJ1dGVzIjp7InBlcnNvbklkIjpbImRkZnY6cGVyc29uLzgzYW9waW52ajZ4cG02Il0sInBpY3R1cmUiOlsiaHR0cDovL2dvb2dsZS5mciJdfSwicHJlZmVycmVkX3VzZXJuYW1lIjoibWF0aGlldS5yb2dlbGphQG1uZW1vdGl4LmNvbSIsImdpdmVuX25hbWUiOiJNYXRoaWV1IiwiZmFtaWx5X25hbWUiOiJSb2dlbGphIiwicGljdHVyZSI6Imh0dHA6Ly9nb29nbGUuZnIiLCJlbWFpbCI6Im1hdGhpZXUucm9nZWxqYUBtbmVtb3RpeC5jb20ifQ.FR_SENXMZDHk5etTZ-qJvlbd0SF3kF74-y3ydnxY6bwRtvYzixAK2YRWgIENEUs943peWvvoSIp0Y5TXWiev4L_vqMfnG3I-23No63DPrAKbZt09faRGJ8Lg__MVpnEydsCg70maJ7HnINky4-0CWn55eiDi9273BRbRNfojEoCj95RF86cUHmGUXtQqD37QTFuKdDl3oQNtG1q04LS9wCervqzCwJK-eLNB3p-_Gng8lDUGsh3H1CW-Ga2vdLNkrposODZ4il4V_EVJ6ArfgRVQN3tFXvhetgtz6Bezm7DUSQyYRz8WAnIIyKgN_MuReaDL5jHgwxx3NcPbXKRKFQ");
    expect(user.getAttributes()).toEqual({"personId": "ddfv:person/83aopinvj6xpm6", "picture": "http://google.fr"});
    expect(user.getFirstName()).toEqual("Mathieu");
    expect(user.getLastName()).toEqual("Rogelja");
    expect(user.getFullName()).toEqual("Mathieu Rogelja");
    expect(user.getUsername()).toEqual("mathieu.rogelja@mnemotix.com");
  });

  test("User created from user SSO representation", () => {
    let user = new SSOUser({
      user: {
        "id": "3c684e83-748d-42d7-bb78-5d668c1479fc",
        "createdTimestamp": 1567624676302,
        "username": "mathieu.rogelja@mnemotix.com",
        "enabled": true,
        "totp": false,
        "emailVerified": false,
        "firstName": "Mathieu",
        "lastName": "Rogelja",
        "disableableCredentialTypes": [],
        "requiredActions": [],
        "notBefore": 0,
        "attributes": {
          "personId": "mnxd:person/1234",
          "orgId": "mnxd:org/1234"
        }
      }
    });

    expect(user.toJSON()).toEqual({
      id: '3c684e83-748d-42d7-bb78-5d668c1479fc',
      username: 'mathieu.rogelja@mnemotix.com',
      email: 'mathieu.rogelja@mnemotix.com',
      firstName: 'Mathieu',
      lastName: 'Rogelja',
      attributes: {
        personId: 'mnxd:person/1234',
        orgId: "mnxd:org/1234"
      }
    });
  });

  test("SSOUser creation from cookie", () => {
    let user = SSOUser.fromCookie({
      [SSOUser.getSessionCookieName()]: "eyJ0aWNrZXQiOnsiYWNjZXNzX3Rva2VuIjoiZXlKaGJHY2lPaUpTVXpJMU5pSXNJblI1Y0NJZ09pQWlTbGRVSWl3aWEybGtJaUE2SUNKWmVUTm5jRFZpWW1wWlkzWk5kRXhGY0hCYVJEVnRkVGRmVjNnd1RtUlFOWFIzUkhWTlIzcFZiM3BKSW4wLmV5SnFkR2tpT2lJMU9UVTNPREUxWVMxbE1ERmpMVFF6T0RJdE9EVmhPUzFpWVdJMk5UZzJaalV6T1RnaUxDSmxlSEFpT2pFMU56QTFNemN3TURnc0ltNWlaaUk2TUN3aWFXRjBJam94TlRjd05UTTJPVFE0TENKcGMzTWlPaUpvZEhSd09pOHZiRzlqWVd4b2IzTjBPamd4T0RFdllYVjBhQzl5WldGc2JYTXZaR1JtSWl3aVlYVmtJam9pWVdOamIzVnVkQ0lzSW5OMVlpSTZJalU1WlRoaU16VTNMVEV5TmpJdE5EVmlZeTA0WXprekxURXdNamd6TjJJeVlqWmtPU0lzSW5SNWNDSTZJa0psWVhKbGNpSXNJbUY2Y0NJNkltRndhU0lzSW1GMWRHaGZkR2x0WlNJNk1Dd2ljMlZ6YzJsdmJsOXpkR0YwWlNJNkltTTROR0ZsWXpnekxXTTNZVFV0TkRRNFlpMDRaRE16TFRaak0yRTJZVEZoTmpKbFl5SXNJbUZqY2lJNklqRWlMQ0poYkd4dmQyVmtMVzl5YVdkcGJuTWlPbHNpYUhSMGNEb3ZMeW9pTENKb2RIUndjem92THlvaVhTd2ljbVZoYkcxZllXTmpaWE56SWpwN0luSnZiR1Z6SWpwYkltOW1abXhwYm1WZllXTmpaWE56SWl3aWRXMWhYMkYxZEdodmNtbDZZWFJwYjI0aVhYMHNJbkpsYzI5MWNtTmxYMkZqWTJWemN5STZleUpoWTJOdmRXNTBJanA3SW5KdmJHVnpJanBiSW0xaGJtRm5aUzFoWTJOdmRXNTBJaXdpYldGdVlXZGxMV0ZqWTI5MWJuUXRiR2x1YTNNaUxDSjJhV1YzTFhCeWIyWnBiR1VpWFgxOUxDSnpZMjl3WlNJNkltVnRZV2xzSUhCeWIyWnBiR1VpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbTVoYldVaU9pSk5ZWFJvYVdWMUlGSnZaMlZzYW1FaUxDSmhkSFJ5YVdKMWRHVnpJanA3SW5CbGNuTnZia2xrSWpwYkltUmtablk2Y0dWeWMyOXVMemd6WVc5d2FXNTJhalo0Y0cwMklsMHNJbkJwWTNSMWNtVWlPbHNpYUhSMGNEb3ZMMmR2YjJkc1pTNW1jaUpkZlN3aWNISmxabVZ5Y21Wa1gzVnpaWEp1WVcxbElqb2liV0YwYUdsbGRTNXliMmRsYkdwaFFHMXVaVzF2ZEdsNExtTnZiU0lzSW1kcGRtVnVYMjVoYldVaU9pSk5ZWFJvYVdWMUlpd2labUZ0YVd4NVgyNWhiV1VpT2lKU2IyZGxiR3BoSWl3aWNHbGpkSFZ5WlNJNkltaDBkSEE2THk5bmIyOW5iR1V1Wm5JaUxDSmxiV0ZwYkNJNkltMWhkR2hwWlhVdWNtOW5aV3hxWVVCdGJtVnRiM1JwZUM1amIyMGlmUS5GUl9TRU5YTVpESGs1ZXRUWi1xSnZsYmQwU0Yza0Y3NC15M3lkbnhZNmJ3UnR2WXppeEFLMllSV2dJRU5FVXM5NDNwZVd2dm9TSXAwWTVUWFdpZXY0TF92cU1mbkczSS0yM05vNjNEUHJBS2JadDA5ZmFSR0o4TGdfX01WcG5FeWRzQ2c3MG1hSjdIbklOa3k0LTBDV241NWVpRGk5MjczQlJiUk5mb2pFb0NqOTVSRjg2Y1VIbUdVWHRRcUQzN1FURnVLZERsM29RTnRHMXEwNExTOXdDZXJ2cXpDd0pLLWVMTkIzcC1fR25nOGxEVUdzaDNIMUNXLUdhMnZkTE5rcnBvc09EWjRpbDRWX0VWSjZBcmZnUlZRTjN0Rlh2aGV0Z3R6NkJlem03RFVTUXlZUno4V0FuSUl5S2dOX011UmVhREw1akhnd3h4M05jUGJYS1JLRlEiLCJyZWZyZXNoX3Rva2VuIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJZ09pQWlTbGRVSWl3aWEybGtJaUE2SUNKbU5EVTFZalU0TVMwek9HSmhMVFEzTnpjdFltUXpNUzAzTVdZd05tVmpaVEF4WmpFaWZRLmV5SnFkR2tpT2lJeFltUTFNalU0TXkwM01EUXlMVFJsT1RrdFltRmlaUzFqWlRnMVlUSTBNMlF3WVRNaUxDSmxlSEFpT2pFMU56QTFNemczTkRnc0ltNWlaaUk2TUN3aWFXRjBJam94TlRjd05UTTJPVFE0TENKcGMzTWlPaUpvZEhSd09pOHZiRzlqWVd4b2IzTjBPamd4T0RFdllYVjBhQzl5WldGc2JYTXZaR1JtSWl3aVlYVmtJam9pYUhSMGNEb3ZMMnh2WTJGc2FHOXpkRG80TVRneEwyRjFkR2d2Y21WaGJHMXpMMlJrWmlJc0luTjFZaUk2SWpVNVpUaGlNelUzTFRFeU5qSXRORFZpWXkwNFl6a3pMVEV3TWpnek4ySXlZalprT1NJc0luUjVjQ0k2SWxKbFpuSmxjMmdpTENKaGVuQWlPaUpoY0draUxDSmhkWFJvWDNScGJXVWlPakFzSW5ObGMzTnBiMjVmYzNSaGRHVWlPaUpqT0RSaFpXTTRNeTFqTjJFMUxUUTBPR0l0T0dRek15MDJZek5oTm1FeFlUWXlaV01pTENKeVpXRnNiVjloWTJObGMzTWlPbnNpY205c1pYTWlPbHNpYjJabWJHbHVaVjloWTJObGMzTWlMQ0oxYldGZllYVjBhRzl5YVhwaGRHbHZiaUpkZlN3aWNtVnpiM1Z5WTJWZllXTmpaWE56SWpwN0ltRmpZMjkxYm5RaU9uc2ljbTlzWlhNaU9sc2liV0Z1WVdkbExXRmpZMjkxYm5RaUxDSnRZVzVoWjJVdFlXTmpiM1Z1ZEMxc2FXNXJjeUlzSW5acFpYY3RjSEp2Wm1sc1pTSmRmWDBzSW5OamIzQmxJam9pWlcxaGFXd2djSEp2Wm1sc1pTSjkua3NWUG9MLUlzM2s4b1Z4V1hhcm1sdTBIdldINm50VkZBS09CaUx6OVo5VSIsInVpZCI6IjU5ZThiMzU3LTEyNjItNDViYy04YzkzLTEwMjgzN2IyYjZkOSJ9fQ=="
    });

    user.addAttributes({"tata": "toto"});

    expect(user.toJSON()).toEqual({
      id: '59e8b357-1262-45bc-8c93-102837b2b6d9',
      username: 'mathieu.rogelja@mnemotix.com',
      email: 'mathieu.rogelja@mnemotix.com',
      firstName: 'Mathieu',
      lastName: 'Rogelja',
      attributes: {
        personId: "ddfv:person/83aopinvj6xpm6",
        picture:  "http://google.fr",
        tata: "toto"
      }
    });
  });

  test("SSOUser creation from JWT", () => {
    let user = SSOUser.fromJWT({
      jwt: "eyJ0aWNrZXQiOnsiYWNjZXNzX3Rva2VuIjoiZXlKaGJHY2lPaUpTVXpJMU5pSXNJblI1Y0NJZ09pQWlTbGRVSWl3aWEybGtJaUE2SUNKWmVUTm5jRFZpWW1wWlkzWk5kRXhGY0hCYVJEVnRkVGRmVjNnd1RtUlFOWFIzUkhWTlIzcFZiM3BKSW4wLmV5SnFkR2tpT2lJMU9UVTNPREUxWVMxbE1ERmpMVFF6T0RJdE9EVmhPUzFpWVdJMk5UZzJaalV6T1RnaUxDSmxlSEFpT2pFMU56QTFNemN3TURnc0ltNWlaaUk2TUN3aWFXRjBJam94TlRjd05UTTJPVFE0TENKcGMzTWlPaUpvZEhSd09pOHZiRzlqWVd4b2IzTjBPamd4T0RFdllYVjBhQzl5WldGc2JYTXZaR1JtSWl3aVlYVmtJam9pWVdOamIzVnVkQ0lzSW5OMVlpSTZJalU1WlRoaU16VTNMVEV5TmpJdE5EVmlZeTA0WXprekxURXdNamd6TjJJeVlqWmtPU0lzSW5SNWNDSTZJa0psWVhKbGNpSXNJbUY2Y0NJNkltRndhU0lzSW1GMWRHaGZkR2x0WlNJNk1Dd2ljMlZ6YzJsdmJsOXpkR0YwWlNJNkltTTROR0ZsWXpnekxXTTNZVFV0TkRRNFlpMDRaRE16TFRaak0yRTJZVEZoTmpKbFl5SXNJbUZqY2lJNklqRWlMQ0poYkd4dmQyVmtMVzl5YVdkcGJuTWlPbHNpYUhSMGNEb3ZMeW9pTENKb2RIUndjem92THlvaVhTd2ljbVZoYkcxZllXTmpaWE56SWpwN0luSnZiR1Z6SWpwYkltOW1abXhwYm1WZllXTmpaWE56SWl3aWRXMWhYMkYxZEdodmNtbDZZWFJwYjI0aVhYMHNJbkpsYzI5MWNtTmxYMkZqWTJWemN5STZleUpoWTJOdmRXNTBJanA3SW5KdmJHVnpJanBiSW0xaGJtRm5aUzFoWTJOdmRXNTBJaXdpYldGdVlXZGxMV0ZqWTI5MWJuUXRiR2x1YTNNaUxDSjJhV1YzTFhCeWIyWnBiR1VpWFgxOUxDSnpZMjl3WlNJNkltVnRZV2xzSUhCeWIyWnBiR1VpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbTVoYldVaU9pSk5ZWFJvYVdWMUlGSnZaMlZzYW1FaUxDSmhkSFJ5YVdKMWRHVnpJanA3SW5CbGNuTnZia2xrSWpwYkltUmtablk2Y0dWeWMyOXVMemd6WVc5d2FXNTJhalo0Y0cwMklsMHNJbkJwWTNSMWNtVWlPbHNpYUhSMGNEb3ZMMmR2YjJkc1pTNW1jaUpkZlN3aWNISmxabVZ5Y21Wa1gzVnpaWEp1WVcxbElqb2liV0YwYUdsbGRTNXliMmRsYkdwaFFHMXVaVzF2ZEdsNExtTnZiU0lzSW1kcGRtVnVYMjVoYldVaU9pSk5ZWFJvYVdWMUlpd2labUZ0YVd4NVgyNWhiV1VpT2lKU2IyZGxiR3BoSWl3aWNHbGpkSFZ5WlNJNkltaDBkSEE2THk5bmIyOW5iR1V1Wm5JaUxDSmxiV0ZwYkNJNkltMWhkR2hwWlhVdWNtOW5aV3hxWVVCdGJtVnRiM1JwZUM1amIyMGlmUS5GUl9TRU5YTVpESGs1ZXRUWi1xSnZsYmQwU0Yza0Y3NC15M3lkbnhZNmJ3UnR2WXppeEFLMllSV2dJRU5FVXM5NDNwZVd2dm9TSXAwWTVUWFdpZXY0TF92cU1mbkczSS0yM05vNjNEUHJBS2JadDA5ZmFSR0o4TGdfX01WcG5FeWRzQ2c3MG1hSjdIbklOa3k0LTBDV241NWVpRGk5MjczQlJiUk5mb2pFb0NqOTVSRjg2Y1VIbUdVWHRRcUQzN1FURnVLZERsM29RTnRHMXEwNExTOXdDZXJ2cXpDd0pLLWVMTkIzcC1fR25nOGxEVUdzaDNIMUNXLUdhMnZkTE5rcnBvc09EWjRpbDRWX0VWSjZBcmZnUlZRTjN0Rlh2aGV0Z3R6NkJlem03RFVTUXlZUno4V0FuSUl5S2dOX011UmVhREw1akhnd3h4M05jUGJYS1JLRlEiLCJyZWZyZXNoX3Rva2VuIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJZ09pQWlTbGRVSWl3aWEybGtJaUE2SUNKbU5EVTFZalU0TVMwek9HSmhMVFEzTnpjdFltUXpNUzAzTVdZd05tVmpaVEF4WmpFaWZRLmV5SnFkR2tpT2lJeFltUTFNalU0TXkwM01EUXlMVFJsT1RrdFltRmlaUzFqWlRnMVlUSTBNMlF3WVRNaUxDSmxlSEFpT2pFMU56QTFNemczTkRnc0ltNWlaaUk2TUN3aWFXRjBJam94TlRjd05UTTJPVFE0TENKcGMzTWlPaUpvZEhSd09pOHZiRzlqWVd4b2IzTjBPamd4T0RFdllYVjBhQzl5WldGc2JYTXZaR1JtSWl3aVlYVmtJam9pYUhSMGNEb3ZMMnh2WTJGc2FHOXpkRG80TVRneEwyRjFkR2d2Y21WaGJHMXpMMlJrWmlJc0luTjFZaUk2SWpVNVpUaGlNelUzTFRFeU5qSXRORFZpWXkwNFl6a3pMVEV3TWpnek4ySXlZalprT1NJc0luUjVjQ0k2SWxKbFpuSmxjMmdpTENKaGVuQWlPaUpoY0draUxDSmhkWFJvWDNScGJXVWlPakFzSW5ObGMzTnBiMjVmYzNSaGRHVWlPaUpqT0RSaFpXTTRNeTFqTjJFMUxUUTBPR0l0T0dRek15MDJZek5oTm1FeFlUWXlaV01pTENKeVpXRnNiVjloWTJObGMzTWlPbnNpY205c1pYTWlPbHNpYjJabWJHbHVaVjloWTJObGMzTWlMQ0oxYldGZllYVjBhRzl5YVhwaGRHbHZiaUpkZlN3aWNtVnpiM1Z5WTJWZllXTmpaWE56SWpwN0ltRmpZMjkxYm5RaU9uc2ljbTlzWlhNaU9sc2liV0Z1WVdkbExXRmpZMjkxYm5RaUxDSnRZVzVoWjJVdFlXTmpiM1Z1ZEMxc2FXNXJjeUlzSW5acFpYY3RjSEp2Wm1sc1pTSmRmWDBzSW5OamIzQmxJam9pWlcxaGFXd2djSEp2Wm1sc1pTSjkua3NWUG9MLUlzM2s4b1Z4V1hhcm1sdTBIdldINm50VkZBS09CaUx6OVo5VSIsInVpZCI6IjU5ZThiMzU3LTEyNjItNDViYy04YzkzLTEwMjgzN2IyYjZkOSJ9fQ=="
    });

    user.addAttributes({"tata": "toto"});

    expect(user.toJSON()).toEqual({
      id: '59e8b357-1262-45bc-8c93-102837b2b6d9',
      username: 'mathieu.rogelja@mnemotix.com',
      email: 'mathieu.rogelja@mnemotix.com',
      firstName: 'Mathieu',
      lastName: 'Rogelja',
      attributes: {
        personId: "ddfv:person/83aopinvj6xpm6",
        picture:  "http://google.fr",
        tata: "toto"
      }
    });
  });

  test("SSOUser throws an exception", () => {

    expect(() => new SSOUser({})).toThrow("A valid user must be passed")
  })
});