/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerPublisher from '../GraphControllerPublisher';
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";

let graphControllerPublisher = new GraphControllerPublisher(new NetworkLayerAMQP("amqp://", "fake"), "fooSender");

describe('GraphControllerPublisher', () => {
  let publishSpyFn = jest.spyOn(graphControllerPublisher, 'publish');

  publishSpyFn.mockImplementation(() => {
  });

  let savedEnv = process.env.RDFSTORE_REPOSITORY_NAME;

  beforeAll(() => {
    process.env.RDFSTORE_REPOSITORY_NAME = "default_repo";
  });

  afterAll(() => {
    process.env.RDFSTORE_REPOSITORY_NAME = savedEnv;
  });


  it('should create triples', async () => {
    await graphControllerPublisher.createTriples({
      jsonLdContext: {
        "dc": "http://purl.org/dc/elements/1.1/",
        "ex": "http://example.org/vocab#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
      },
      jsonLdNodes: [
        {
          "@id": "http://example.org/library",
          "@type": "ex:Library",
          "ex:contains": "http://example.org/library/the-republic"
        },
        {
          "@id": "http://example.org/library/the-republic",
          "@type": "ex:Book",
          "dc:creator": "Plato",
          "dc:title": "The Republic",
          "ex:contains": "http://example.org/library/the-republic#introduction"
        },
        {
          "@id": "http://example.org/library/the-republic#introduction",
          "@type": "ex:Chapter",
          "dc:description": "An introductory chapter on The Republic.",
          "dc:title": "The Introduction"
        }
      ]
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.create.triples', {
      "@context": {
        "dc": "http://purl.org/dc/elements/1.1/",
        "ex": "http://example.org/vocab#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
      },
      "@graph": [{
        "@id": "http://example.org/library",
        "@type": "ex:Library",
        "ex:contains": "http://example.org/library/the-republic"
      }, {
        "@id": "http://example.org/library/the-republic",
        "@type": "ex:Book",
        "dc:creator": "Plato",
        "dc:title": "The Republic",
        "ex:contains": "http://example.org/library/the-republic#introduction"
      }, {
        "@id": "http://example.org/library/the-republic#introduction",
        "@type": "ex:Chapter",
        "dc:description": "An introductory chapter on The Republic.",
        "dc:title": "The Introduction"
      }]
    }, {"format": "JSON-LD", "repositories": ["default_repo"]});
  });

  it('should create triples in a named graph', async () => {
    await graphControllerPublisher.createTriples({
      jsonLdContext: {
        "dc": "http://purl.org/dc/elements/1.1/",
        "ex": "http://example.org/vocab#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
      },
      jsonLdNodes: [
        {
          "@id": "http://example.org/library",
          "@type": "ex:Library",
          "ex:contains": "http://example.org/library/the-republic"
        },
        {
          "@id": "http://example.org/library/the-republic",
          "@type": "ex:Book",
          "dc:creator": "Plato",
          "dc:title": "The Republic",
          "ex:contains": "http://example.org/library/the-republic#introduction"
        },
        {
          "@id": "http://example.org/library/the-republic#introduction",
          "@type": "ex:Chapter",
          "dc:description": "An introductory chapter on The Republic.",
          "dc:title": "The Introduction"
        }
      ],
      graphId: "_:graph"
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.create.triples', {
      "@context": {
        "dc": "http://purl.org/dc/elements/1.1/",
        "ex": "http://example.org/vocab#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
      },
      "@id": "_:graph",
      "@graph": [{
        "@id": "http://example.org/library",
        "@type": "ex:Library",
        "ex:contains": "http://example.org/library/the-republic"
      }, {
        "@id": "http://example.org/library/the-republic",
        "@type": "ex:Book",
        "dc:creator": "Plato",
        "dc:title": "The Republic",
        "ex:contains": "http://example.org/library/the-republic#introduction"
      }, {
        "@id": "http://example.org/library/the-republic#introduction",
        "@type": "ex:Chapter",
        "dc:description": "An introductory chapter on The Republic.",
        "dc:title": "The Introduction"
      }]
    }, {"format": "JSON-LD", "repositories": ["default_repo"]});
  });

  it('should create triple', async () => {
    await graphControllerPublisher.createTriple({
      jsonLdNode: {
        "@context": {
          "gr": "http://purl.org/goodrelations/v1#",
          "pto": "http://www.productontology.org/id/",
          "foaf": "http://xmlns.com/foaf/0.1/",
          "xsd": "http://www.w3.org/2001/XMLSchema#",
          "foaf:page": {
            "@type": "@id"
          },
          "gr:acceptedPaymentMethods": {
            "@type": "@id"
          },
          "gr:hasBusinessFunction": {
            "@type": "@id"
          },
          "gr:hasCurrencyValue": {
            "@type": "xsd:float"
          }
        },
        "@id": "http://example.org/cars/for-sale#tesla",
        "@type": "gr:Offering",
        "gr:name": "Used Tesla Roadster",
        "gr:description": "Need to sell fast and furiously",
        "gr:hasBusinessFunction": "gr:Sell",
        "gr:acceptedPaymentMethods": "gr:Cash",
        "gr:hasPriceSpecification": {
          "gr:hasCurrencyValue": "85000",
          "gr:hasCurrency": "USD"
        },
        "gr:includes": {
          "@type": [
            "gr:Individual",
            "pto:Vehicle"
          ],
          "gr:name": "Tesla Roadster",
          "foaf:page": "http://www.teslamotors.com/roadster"
        }
      }
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.create.triples', [{
      "@context": {
        "foaf": "http://xmlns.com/foaf/0.1/",
        "foaf:page": {"@type": "@id"},
        "gr": "http://purl.org/goodrelations/v1#",
        "gr:acceptedPaymentMethods": {"@type": "@id"},
        "gr:hasBusinessFunction": {"@type": "@id"},
        "gr:hasCurrencyValue": {"@type": "xsd:float"},
        "pto": "http://www.productontology.org/id/",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
      },
      "@id": "http://example.org/cars/for-sale#tesla",
      "@type": "gr:Offering",
      "gr:acceptedPaymentMethods": "gr:Cash",
      "gr:description": "Need to sell fast and furiously",
      "gr:hasBusinessFunction": "gr:Sell",
      "gr:hasPriceSpecification": {"gr:hasCurrency": "USD", "gr:hasCurrencyValue": "85000"},
      "gr:includes": {
        "@type": ["gr:Individual", "pto:Vehicle"],
        "foaf:page": "http://www.teslamotors.com/roadster",
        "gr:name": "Tesla Roadster"
      },
      "gr:name": "Used Tesla Roadster"
    }], {"format": "JSON-LD", "repositories": ["default_repo"]});
  });

  it('should delete triples', async () => {
    await graphControllerPublisher.deleteTriples({
      query: `PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
DELETE WHERE { 
  ?person foaf:givenName 'Fred'; ?property ?value 
}`
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.delete.triples', `PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
DELETE WHERE { 
  ?person foaf:givenName 'Fred'; ?property ?value 
}`, {"format": "SPARQL", "repositories": ["default_repo"]});
  });

  it('should update triples', async () => {
    await graphControllerPublisher.updateTriples({
      query: `PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
WITH <http://example/addresses>
DELETE { ?person foaf:givenName 'Bill' }
INSERT { ?person foaf:givenName 'William' }
WHERE
  { ?person foaf:givenName 'Bill' }`
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.update.triples', `PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
WITH <http://example/addresses>
DELETE { ?person foaf:givenName 'Bill' }
INSERT { ?person foaf:givenName 'William' }
WHERE
  { ?person foaf:givenName 'Bill' }`, {"format": "SPARQL", "repositories": ["default_repo"]});
  });

  it('should perform a "SELECT" query against the triplestore', async () => {
    await graphControllerPublisher.select({
      query: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
SELECT DISTINCT ?nom ?image ?description WHERE {
   ?personne rdf:type foaf:Person.
   ?personne foaf:name ?nom.
   ?image rdf:type foaf:Image.
   ?personne foaf:img ?image.
   ?image dc:description ?description
}`
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.select', `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
SELECT DISTINCT ?nom ?image ?description WHERE {
   ?personne rdf:type foaf:Person.
   ?personne foaf:name ?nom.
   ?image rdf:type foaf:Image.
   ?personne foaf:img ?image.
   ?image dc:description ?description
}`, {"format": "SPARQL", "repositories": ["default_repo"]});
  });

  it('should perform a "SELECT" query against the triplestore with a COUNT result', async () => {
    publishSpyFn.mockImplementation(() => ({
      "head": {
        "vars": [
          "count"
        ]
      },
      "results": {
        "bindings": [
          {
            "count": {
              "datatype": "http://www.w3.org/2001/XMLSchema#integer",
              "type": "literal",
              "value": "2"
            }
          }
        ]
      }
    }));

    let count = await graphControllerPublisher.count({
      query: `PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT (COUNT(*) AS ?count) WHERE {
 ?uri a foaf:Person.
}`, countVariable: "count"
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.select', `PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT (COUNT(*) AS ?count) WHERE {
 ?uri a foaf:Person.
}`, {"format": "SPARQL", "repositories": ["default_repo"]});

    expect(count).toEqual(2);

    count = await graphControllerPublisher.count({
      query: `PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT (COUNT(*) AS ?count) WHERE {
 ?uri a foaf:Person.
}`
    });

    expect(count).toEqual(2);

    publishSpyFn.mockImplementation(() => ({}));
  });

  it('should perform a "CONSTRUCT" query against the triplestore', async () => {
    await graphControllerPublisher.construct({
      query: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
CONSTRUCT {  
  ?x <http://example.org/hasLiteral> ?literal .  
}  
WHERE {  
  ?x ?p ?literal .  
  FILTER (isLiteral(?literal))  
}`
    });

    expect(publishSpyFn).toHaveBeenCalledWith('graph.construct', `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
CONSTRUCT {  
  ?x <http://example.org/hasLiteral> ?literal .  
}  
WHERE {  
  ?x ?p ?literal .  
  FILTER (isLiteral(?literal))  
}`, {"format": "SPARQL", "repositories": ["default_repo"]});
  });

  it('should perform a "ASK" query against the triplestore', async () => {
    publishSpyFn.mockImplementation(() => true);

    expect(await graphControllerPublisher.ask({
      query: `PREFIX prop: <http://dbpedia.org/property/>
ASK
{
  <http://dbpedia.org/resource/Amazon_River> prop:length ?amazon .
  <http://dbpedia.org/resource/Nile> prop:length ?nile .
  FILTER(?amazon > ?nile) .
}  `
    })).toEqual(true);

    expect(publishSpyFn).toHaveBeenCalledWith('graph.ask', `PREFIX prop: <http://dbpedia.org/property/>
ASK
{
  <http://dbpedia.org/resource/Amazon_River> prop:length ?amazon .
  <http://dbpedia.org/resource/Nile> prop:length ?nile .
  FILTER(?amazon > ?nile) .
}  `, {"format": "SPARQL", "repositories": ["default_repo"]});
  });

  it('should perform a "DESCRIBRE" query against the triplestore', async () => {
    publishSpyFn.mockImplementation(() => true);

    expect(await graphControllerPublisher.describe({
      query: `PREFIX prop: <http://dbpedia.org/property/>
ASK
{
  <http://dbpedia.org/resource/Amazon_River> prop:length ?amazon .
  <http://dbpedia.org/resource/Nile> prop:length ?nile .
  FILTER(?amazon > ?nile) .
}  `
    })).toEqual(true);

    expect(publishSpyFn).toHaveBeenCalledWith('graph.describe', `PREFIX prop: <http://dbpedia.org/property/>
ASK
{
  <http://dbpedia.org/resource/Amazon_River> prop:length ?amazon .
  <http://dbpedia.org/resource/Nile> prop:length ?nile .
  FILTER(?amazon > ?nile) .
}  `, {"format": "SPARQL", "repositories": ["default_repo"]});
  });
});