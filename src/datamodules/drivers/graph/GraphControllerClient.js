/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import env from "env-var";
import chalk from "chalk";
import got, {HTTPError} from "got";
import GraphControllerPublisher from "./GraphControllerPublisher";
import {logDebug, logError} from "../../../utilities/logger";
import {getDebugColor} from "../../../utilities/logger/debugColors";
import {I18nError} from "../../../utilities/error/I18nError";

/**
 *  @typedef {object} JsonLdNode - A JSON-LD node
 *  @property {string|object} [@context] - The context
 *  @property {string} [@type] - The type
 *  @property {string} [@id] - The URI
 *  @property {object} [...props] - The object and data properties.
 */

/**
 * Class used to publish commands to the Graphsync module (based on RDF triple store) on Synaptix.
 *
 *  @extends DataModulePublisher
 */
export default class GraphControllerClient extends GraphControllerPublisher {
  _endpoint;
  _username;
  _password;
  _defaultRepoName;

  /**
   * @param {NetworkLayer} networkLayer
   * @param {string} [senderId]
   */
  constructor(networkLayer, senderId) {
    super(networkLayer, senderId);

    this._endpoint = env.get("RDFSTORE_ROOT_URI").required().asString();
    this._username = env.get("RDFSTORE_USER").required().asString();
    this._password = env.get("RDFSTORE_PWD").required().asString();
    this._defaultRepoName = env.get("RDFSTORE_REPOSITORY_NAME").asString();
  }

  /**
   * @return {object}
   */
  getHeaders(){
    let headers = {};

    if(this._username && this._password){
      headers['Authorization'] = 'Basic ' + Buffer.from(this._username + ':' +  this._password).toString('base64')
    }

    return headers;
  }
  /**
   * Quuery method
   * @param query
   * @param repository
   * @param asJsonLd
   * @param infer
   * @param sameAs
   */
  async query({query, repository, asJsonLd = false, infer = true, sameAs = true}) {
    let headers = this.getHeaders();

    if(asJsonLd){
      headers["Accept"] = "application/ld+json"
    }

    try{
      return await got.post(`${this._endpoint}/repositories/${repository || this._defaultRepoName}`, {
        headers,
        form: {
          query,
          infer,
          sameAs
        },
        responseType: "json"
      })
    } catch (e) {
      throw e
    }
  }

  /**
   * Update method
   * @param update
   * @param repository
   * @param infer
   * @param sameAs
   */
  async update({update, repository, infer = true, sameAs = true}) {
    let headers = this.getHeaders();

    headers["content-type"] = "application/x-www-form-urlencoded; charset=UTF-8";

    return got.post(`${this._endpoint}/repositories/${repository || this._defaultRepoName}/statements`, {
      headers,
      form: {
        update,
        infer,
        sameAs
      },
      responseType: "json"
    })
  }

  /**
   * @param command
   * @param body
   * @param context
   */
  async publishRaw({command, body = {}, context} = {}) {
    const searchParams = {
      index: body.indices,
      body: body.source
    };

    const debugLevel = env.get("LOG_LEVEL").default(env.get('RABBITMQ_LOG_LEVEL')).asString();
    const startAt = Date.now();
    try {
      let response;

      if(command.match(/graph.delete|graph.update|graph.insert/)){
        response = await this.update({update: body})
      } else {
        response = await this.query({
          query: body,
          asJsonLd: command === "graph.construct"
        });
      }

      if (["DEBUG", "VERBOSE", "TRACE"].includes(debugLevel)) {
        const debugColor = getDebugColor();
        logDebug(chalk[debugColor](`[Graph request] ${JSON.stringify(body)}`));

        if (["VERBOSE", "TRACE"].includes(debugLevel)) {
          const takes = `${Date.now() - startAt}ms`;
          logDebug(chalk[debugColor](`[Graph response] [${response.statusCode}] [${takes}] ${JSON.stringify(response.body)}`));
        }
      }

      return response.body;
    } catch (e) {
      if (["DEBUG", "VERBOSE", "TRACE"].includes(debugLevel)) {
        const debugColor = getDebugColor();
        logError(chalk[debugColor](`[Graph request] ${JSON.stringify(body)}`));

        if (["VERBOSE", "TRACE"].includes(debugLevel)) {
          const takes = `${Date.now() - startAt}ms`;
          logError(chalk[debugColor](`[Graph response] [${takes}] [ERR ${e.code}] ${JSON.stringify(e)}`));
        }
      }

      if(e instanceof HTTPError){
        throw new I18nError(`GraphDB client error : ${e.message}`)
      } else {
        throw e;
      }
    }
  }
}