/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import DataModulePublisher from '../DataModulePublisher';
import NetworkLayerAMQP from "../../../network/amqp/NetworkLayerAMQP";

let now = Date.now();
jest.spyOn(Date, 'now').mockImplementation(() => now);

describe('Datamodule Publisher', () => {
  it('should publish message', async () => {
    let publisher = new DataModulePublisher(new NetworkLayerAMQP("amqp://", "fake"), "fooSender");
    let requestSpyFn = jest.spyOn(publisher._networkLayer, 'request').mockImplementation(() => JSON.stringify({
      headers: {
        status: "OK"
      },
      body: true
    }));

    await publisher.publish("foo.bar", {faz: "booz"},);

    expect(requestSpyFn).toHaveBeenCalledWith('foo.bar', {
      headers: {
        command: "foo.bar",
        sender: "fooSender",
        timestamp: now
      },
      body: {faz: "booz"}
    });
  });

  it('should publish message without expecting any response', async () => {
    let publisher = new DataModulePublisher(new NetworkLayerAMQP("amqp://", "fake"), "fooSender");
    let publishSpyFn = jest.spyOn(publisher._networkLayer, 'publish').mockImplementation(() => JSON.stringify({
      headers: {
        status: "OK"
      },
      body: true
    }));

    await publisher.publish("foo.bar", {faz: "booz"}, {}, false);

    expect(publishSpyFn).toHaveBeenCalledWith('foo.bar', {
      headers: {
        command: "foo.bar",
        sender: "fooSender",
        timestamp: now
      },
      body: {faz: "booz"}
    });
  });
  
  it('should raise an exception on ERROR status message', async () => {
    let publisher = new DataModulePublisher(new NetworkLayerAMQP("amqp://", "fake"), "fooSender");
    let requestSpyFn = jest.spyOn(publisher._networkLayer, 'request').mockImplementation(() => JSON.stringify({
      headers: {
        status: "ERROR"
      },
      body: "This is an error."
    }));

    await expect(publisher.publish("foo.bar", {faz: "booz"},)).rejects.toThrow();

    expect(requestSpyFn).toHaveBeenCalledWith('foo.bar', {
      headers: {
        command: "foo.bar",
        sender: "fooSender",
        timestamp: now
      },
      body: {faz: "booz"}
    });
  });
});