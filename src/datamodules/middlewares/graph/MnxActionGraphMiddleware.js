/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import dayjs from "dayjs";

import { GraphMiddleware } from "./GraphMiddleware";
import EntityDefinition from "../../../datamodel/ontologies/mnx-common/EntityDefinition";
import CreationDefinition from "../../../datamodel/ontologies/mnx-contribution/CreationDefinition";
import UpdateDefinition from "../../../datamodel/ontologies/mnx-contribution/UpdateDefinition";
import DeletionDefinition from "../../../datamodel/ontologies/mnx-contribution/DeletionDefinition";
import ActionDefinition from "../../../datamodel/ontologies/mnx-contribution/ActionDefinition";
import { parseObjectInputToJsonLdNode } from "../../services/graph/sparqlHelpers/parseObjectInputToJsonLdNode";

/**
 * This middleware is used to plug PROV-O ontology (embedded in MnxAction model).
 * The goal is to track data manipulation and know :
 *  - Who has done something
 *  - When did it occur
 *  - Which nodes are targeted
 *
 *  TODO: Since the 4.5.1 version, Creation/Update/Deletion actions are mutualised to save triples for each transaction.
 *        The only thing that still remain to implement is a link between Creation/Update/Deletion actions generated in a transaction.
 *        Some investigations are done with `prov:wasInformedBy` property.
 */
export class MnxActionGraphMiddleware extends GraphMiddleware {
  /**
   * Check if Action node should be applied to modelDefinition.
   * Two conditions :
   *
   * - modelDefinition MUST NOT be an ActionDefinition (or descendant)
   * - modelDefinition MUST NOT be the target of nested link of ActionDefinition, so :
   *    - MUST NOT be a UserAccountDefinition with a link existing and pointing on logged UserAccount id.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {Link[]} [extraLinks]
   * @private
   */
  async _isActionAppliedTo({ modelDefinition, extraLinks }) {
    let userId = await this._datastoreSession?.getLoggedUserAccountId();

    let isModelDefinitionAnAction = modelDefinition.isEqualOrDescendantOf(
      ActionDefinition
    );
    let isModelDefinitionRelatedActionUserAccount =
      userId &&
      modelDefinition ===
        ActionDefinition.getLink(
          "hasUserAccount"
        ).getRelatedModelDefinition() &&
      !!extraLinks.find(link => link.getTargetId() === userId);

    return (
      !isModelDefinitionAnAction && !isModelDefinitionRelatedActionUserAccount
    );
  }

  /**
   * @param objectId
   * @param objectInput
   * @param modelDefinition
   * @param links
   * @return {{"@type": *, "@id": string}}
   */
  getObjectSnapShot({ objectId, objectInput, modelDefinition, links }) {
    objectId = this._datastoreSession.normalizeId(objectId);

    // Convert objectInput into JSONLD snapshot
    const objectSnapshot = parseObjectInputToJsonLdNode({
      objectId,
      objectInput,
      links,
      modelDefinition,
      lang: this._datastoreSession.getContext().getLang(),
      normalizeUri: uri => this._datastoreSession.normalizeId(uri)
    });

    // Returns a snapshot only if it's relevant (mutated props else than @id and @type)
    if(Object.keys(objectSnapshot).length > 2){
      return objectSnapshot;
    }
  }

  /**
   * Creating an action link for a mutating object constits on :
   *  - Generating a link from the mutating Object to an ActionDefinition instance (CreationDefiniton, UpdateDefinition or DeletionDefinition)
   *  - Linking it to the userAccount responsible for the action.
   *  - Timestamping it with the current time.
   *  - Adding a snapshot of the mutating object.
   *
   * @param {string} objectId
   * @param {object} objectInput
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {Link[]} [links] -  List of links
   * @param {typeof ActionDefinition} ActionDefinition (CreationDefiniton|UpdateDefinition|DeletionDefinition)
   *
   * @return {Link}
   */
  async generateActionLinkFor({
    objectId,
    objectInput,
    modelDefinition,
    links,
    ActionDefinition
  }) {
    let entityActionLinkDefinition;

    switch (ActionDefinition) {
      case CreationDefinition:
        entityActionLinkDefinition = EntityDefinition.getLink(
          "hasCreationAction"
        );
        break;
      case UpdateDefinition:
        entityActionLinkDefinition = EntityDefinition.getLink(
          "hasUpdateAction"
        );
        break;
      case DeletionDefinition:
        entityActionLinkDefinition = EntityDefinition.getLink(
          "hasDeletionAction"
        );
    }

    if (entityActionLinkDefinition) {
      let targetObjectInput = {
        startedAtTime: dayjs().format()
      };

      let targetNestedLinks = [];

      const userId = await this._datastoreSession?.getLoggedUserAccountId();

      if (userId) {
        targetNestedLinks.push(
          ActionDefinition.getLink("hasUserAccount").generateLinkFromTargetId(
            userId
          )
        );
      }

      if (objectId) {
        const snapshot = this.getObjectSnapShot({
          objectId,
          objectInput,
          links,
          modelDefinition
        });

        if(snapshot){
          targetObjectInput.snapshot = JSON.stringify([snapshot]);
        }

        if(snapshot || ActionDefinition !== UpdateDefinition){
          return entityActionLinkDefinition.generateLinkFromTargetProps({
            targetModelDefinition: ActionDefinition,
            targetObjectInput,
            targetNestedLinks,
            sourceId: objectId
          });
        }
      }
    }
  }

  /**
   * Updating an existing action link for a mutating object consists on :
   *  - Concatenating a snapshot of the mutating object.
   *  - Adding a link from the action to the mutating object.
   *
   * @param {Link} actionLink
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {object} objectInput
   * @param {Link[]} [links] -  List of links
   * @return {*}
   */
  updateActionLinkFor({
    actionLink,
    modelDefinition,
    objectId,
    objectInput,
    links
  }) {
    if (objectId) {
      let globalSnapshot = JSON.parse(
        actionLink.getTargetObjectInput()?.snapshot || "[]"
      );

      // Convert objectInput into JSONLD snapshot
      let objectSnapshot = this.getObjectSnapShot({
        objectId,
        objectInput,
        links,
        modelDefinition
      });

      if (objectSnapshot) {
        globalSnapshot.push(objectSnapshot);

        actionLink.alterTargetObjectInput({
          snapshot: JSON.stringify(globalSnapshot)
        });
      }

      actionLink.addTargetNestedLinks([
        actionLink
          .getTargetModelDefinition()
          .getLink("hasEntity")
          .generateLinkFromTargetId(objectId)
      ]);
    }

    return actionLink;
  }

  /**
   * @inheritDoc
   */
  async handleObjectCreation({
    modelDefinition,
    objectId,
    objectInput = {},
    links = [],
    extraLinks = []
  } = {}) {
    if (await this._isActionAppliedTo({ modelDefinition, extraLinks })) {
      const existingActionLink = (extraLinks || []).find(link => {
        return link.getTargetModelDefinition() === CreationDefinition;
      });

      if (existingActionLink) {
        await this.updateActionLinkFor({
          actionLink: existingActionLink,
          modelDefinition,
          objectInput,
          objectId,
          links
        });
      } else {
        const actionLink  = await this.generateActionLinkFor({
          objectId,
          modelDefinition,
          links,
          objectInput,
          ActionDefinition: CreationDefinition
        });

        if (actionLink){
          extraLinks.push(actionLink);
        }
      }
    }

    return { objectInput, links, extraLinks};
  }

  /**
   * @inheritDoc
   */
  async handleObjectUpdate({
    modelDefinition,
    objectId,
    objectInput,
    links = [],
    extraLinks = []
  } = {}) {
    let isDeletion = !!extraLinks.find(extraLink =>
      extraLink.getTargetModelDefinition().isEqualOrDescendantOf(DeletionDefinition)
    );

    if (
      !isDeletion &&
      (await this._isActionAppliedTo({ modelDefinition, extraLinks }))
    ) {
      const existingActionLink = (extraLinks || []).find(link => {
        return link.getTargetModelDefinition() === UpdateDefinition;
      });

      if (existingActionLink) {
        await this.updateActionLinkFor({
          actionLink: existingActionLink,
          modelDefinition,
          objectInput,
          objectId,
          links
        });
      } else {
        const actionLink  = await this.generateActionLinkFor({
          objectId,
          modelDefinition,
          links,
          objectInput,
          ActionDefinition: UpdateDefinition
        });

        if (actionLink){
          extraLinks.push(actionLink);
        }
      }
    }

    return { objectInput, links, extraLinks };
  }

  /**
   * @inheritDoc
   */
  async handleObjectDeletion({
    modelDefinition,
    objectId,
    objectInput,
    links = [],
    extraLinks = []
  } = {}) {
    // By re-ijecting links as extraLinks, first generated Update action is reused.
    // @see GraphControllerService::removeNodes()
    if (await this._isActionAppliedTo({ modelDefinition, extraLinks })) {
      const existingActionLink = (extraLinks || []).find(link => {
        return link.getTargetModelDefinition() === DeletionDefinition;
      });

      if (existingActionLink) {
        this.updateActionLinkFor({
          actionLink: existingActionLink,
          modelDefinition,
          objectInput,
          objectId,
          links
        });
      } else {
        const actionLink = await this.generateActionLinkFor({
          objectId,
          modelDefinition,
          links,
          objectInput,
          ActionDefinition: DeletionDefinition
        })

        if (actionLink){
          extraLinks.push(actionLink);
        }
      }
    }

    return { objectInput, links, extraLinks };
  }

  /**
   * @inheritDoc
   */
  async handleObjectQuery({
    modelDefinition,
    propertyFilters,
    mustExistLinkFilters,
    mustNotExistLinkFilters
  }) {
    let isModelDefinitionAnAction = modelDefinition.isEqualOrDescendantOf(
      ActionDefinition
    );

    if (!isModelDefinitionAnAction) {
      mustNotExistLinkFilters = (mustNotExistLinkFilters || []).concat([
        {
          linkDefinition: EntityDefinition.getLink("hasDeletionAction")
        }
      ]);
    }

    return { propertyFilters, mustExistLinkFilters, mustNotExistLinkFilters };
  }
}
