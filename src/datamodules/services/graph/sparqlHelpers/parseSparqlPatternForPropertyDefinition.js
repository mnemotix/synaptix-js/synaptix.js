
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {parseSparqlPatternForLinkPath} from "./parseSparqlPatternForLinkPath";

/**
 * @param {PropertyDefinitionAbstract} propertyDefinition
 * @param {string}   sourceId
 * @param {object} rdfPrefixesMappings
 * @param {function} requestNodeFiltersMiddleware
 * @return {{whereTriples: array, optionalWhereTriples, templateTriples: array, operationTriples: array, bindingTriples: array, rdfPrefixesMappings: object}}
 */
export async function parseSparqlPatternForPropertyDefinition({propertyDefinition, sourceId, rdfPrefixesMappings, requestNodeFiltersMiddleware}){
  let whereTriples = [];
  let optionalWhereTriples = [];
  let operationTriples = [];
  let templateTriples = [];
  let bindingTriples = [];
  let variable = `?${propertyDefinition.getPropertyName()}`;

  if (propertyDefinition.getRdfDataProperty()) {
    let triple = {
      subject: sourceId,
      predicate: propertyDefinition.getRdfDataProperty(),
      object: variable
    };

    (propertyDefinition.isRequired() ? whereTriples : optionalWhereTriples).push(triple);
    templateTriples.push(triple);
  }

  if(propertyDefinition.getLinkPath()){
    let patterns = await parseSparqlPatternForLinkPath({
      sourceId,
      linkPath: propertyDefinition.getLinkPath(),
      rdfPrefixesMappings,
      isOptional: !propertyDefinition.isRequired(),
      requestNodeFiltersMiddleware
    });

    whereTriples = whereTriples.concat(patterns.whereTriples);
    optionalWhereTriples = optionalWhereTriples.concat(patterns.optionalWhereTriples);
    operationTriples = operationTriples.concat(patterns.operationTriples);
    templateTriples = templateTriples.concat(patterns.templateTriples);
    bindingTriples = bindingTriples.concat(patterns.bindingTriples);
    rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, patterns.rdfPrefixesMappings);
  }

  return {whereTriples, optionalWhereTriples, operationTriples, templateTriples, bindingTriples, rdfPrefixesMappings};
}