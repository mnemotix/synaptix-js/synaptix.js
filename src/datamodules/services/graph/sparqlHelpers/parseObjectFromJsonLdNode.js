/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import omit from "lodash/omit";
import get from "lodash/get";
import has from "lodash/has";
import dayjs from "dayjs";
import hash from "object-hash";
import {replaceAbsoluteNamespaceToPrefix, replacePrefixToAbsoluteNamespace} from "./sparqlPatternHelpers";
import LiteralDefinition from "../../../../datamodel/toolkit/definitions/LiteralDefinition";
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";

/**
 * @param {JsonLdNode} jsonLdNode - the node
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {object} rdfPrefixesMappings
 * @return {Model}
 */
export function parseObjectFromJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings}) {
  rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, modelDefinition.getRdfPrefixesMapping());

  if (Array.isArray(jsonLdNode)) {
    jsonLdNode = jsonLdNode[0];
  }

  if (jsonLdNode){
    if (jsonLdNode["@context"]) {
      return parseObjectFromJenaJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings});
    } else {
      return parseObjectFromRdf4jJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings});
    }
  }
}

/**
 * Parse object from JENA JSON-LD response.
 * @param jsonLdNode
 * @param modelDefinition
 * @param rdfPrefixesMappings
 * @return {Model}
 */
export function parseObjectFromJenaJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings}) {
  let ModelClass = modelDefinition.getModelClass();
  let id = jsonLdNode["@id"];

  if (id) {
    let uri = replacePrefixToAbsoluteNamespace(id, rdfPrefixesMappings);
    let context = jsonLdNode["@context"];

    let props = Object.entries(omit(jsonLdNode, ["@id", "@type", "@context"])).reduce((acc, [prop, value]) => {
      // There is a weird bug with graph-controller where sometime, prefix is not stripped.
      // It seems to appear if data property is a boolean.
      // Ie : mnx:isUserDisabled
      prop = prop.replace(/^\w+:/, '');

      let rdfProp = get(context, `${prop}.@id`);
      rdfProp = replaceAbsoluteNamespaceToPrefix(rdfProp, rdfPrefixesMappings);

      let normalizedProp;
      let datatype;

      let property = modelDefinition.getPropertyFromRdfDataProperty(rdfProp);

      if (property) {
        normalizedProp = property.getPropertyName();
        datatype = property.getRdfDataType() || property.getRdfAliasDataProperty();

        // If value is a literal
        if (property instanceof LiteralDefinition) {
          if (property.isPlural()) {
            if (!value) {
              value = [];
            }

            if (!Array.isArray(value)) {
              value = [value];
            }

            value = value.map(item => castValueForDataType({value: item, datatype}));
          } else {
            value = castValueForDataType({value, datatype});
          }
        }

        // If the value is a localized label with several languages
        if (property instanceof LabelDefinition) {
          let values = [];

          if (has(value, '0.@language')) {
            values = value.reduce((acc, localizedValue) => {
              acc.push({
                value: get(localizedValue, '@value'),
                lang: get(localizedValue, '@language')
              });

              return acc;
            }, []);
          }
          // If the value is a localized label with a single language
          else if (has(value, '@language')) {
            values.push({
              value: get(value, '@value'),
              lang: get(value, '@language')
            });
          }

          value = values;
        }
      }


      if (normalizedProp || prop) {
        acc[normalizedProp || prop] = value;
      }
      return acc;
    }, {});

    return new ModelClass(id, uri, props, modelDefinition.getNodeType());
  }
}

/**
 * Parse object from RDF4J JSON-LD response.
 *
 * @param jsonLdNode
 * @param modelDefinition
 * @param rdfPrefixesMappings
 * @param includeNestedLinks
 * @param forcePrefixedIds
 * @param processIfIdNull
 * @return {Model}
 */
export function parseObjectFromRdf4jJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings, includeNestedLinks, forcePrefixedIds, processIfIdNull}) {
  if (jsonLdNode["@id"] || processIfIdNull) {
    let ModelClass = modelDefinition.getModelClass();

    let uri = jsonLdNode["@id"];
    let id = uri;

    if(forcePrefixedIds && uri){
      id = replaceAbsoluteNamespaceToPrefix(uri, rdfPrefixesMappings);
    }


    let props = Object.entries(omit(jsonLdNode, ["@id", "@type"])).reduce((acc, [rdfProp, values]) => {
      rdfProp = replaceAbsoluteNamespaceToPrefix(rdfProp, rdfPrefixesMappings);

      let normalizedProp;
      let datatype;

      let property = modelDefinition.getPropertyFromRdfDataProperty(rdfProp);
      let value;

      if (property) {
        normalizedProp = property.getPropertyName();
        datatype = property.getRdfDataType() || property.getRdfAliasDataProperty();

        if (property instanceof LiteralDefinition) {
          if (property.isPlural()) {
            if (!values) {
              value = [];
            } else {
              value = values.map(item => castValueForDataType({value: item["@value"], datatype}))
            }
          } else {
            value = values[0]?.["@value"];

            if (value) {
              value = castValueForDataType({value, datatype});
            }
          }
        }

        if (property instanceof LabelDefinition) {
          value = values.map(value => ({
            lang: value["@language"],
            value: value["@value"]
          }));
        }
      } else if(includeNestedLinks){
        const link = modelDefinition.getLinkFromRdfObjectProperty(rdfProp);

        if(link){
          normalizedProp = link.getGraphQLPropertyName();
          if(!Array.isArray(values)){
            values = [values];
          }

          value = values.map(jsonLdNode => parseObjectFromRdf4jJsonLdNode({
            jsonLdNode,
            modelDefinition: link.getRelatedModelDefinition(),
            rdfPrefixesMappings,
            includeNestedLinks,
            forcePrefixedIds,
            processIfIdNull
          }));

          if(!link.isPlural()) {
            value = value[0];
          }
        }
      } else {
        value = values[0]?.["@value"];
      }

      if (rdfProp.includes(':')) {
        rdfProp = rdfProp.split(':')[1];
      }

      if (normalizedProp || rdfProp) {
        acc[normalizedProp || rdfProp] = value;
      }

      return acc;
    }, {});

    props["@ref"] = jsonLdNode["@ref"] || hash(omit(jsonLdNode, ["@id"]));
    props["@deletion"] = jsonLdNode["@deletion"];


    return new ModelClass(id, uri, props, modelDefinition.getNodeType());
  }
}

/**
 * Parse object from RDF4J JSON-LD response.
 *
 * @param jsonLdNode
 * @param modelDefinition
 * @param rdfPrefixesMappings
 * @param includeNestedLinks
 * @return {Array}
 */
export function gatherIdsFromRdf4jJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings}) {
  let id = jsonLdNode["@id"];

  let ids = Object.entries(omit(jsonLdNode, ["@id", "@type"])).reduce((ids, [rdfProp, values]) => {
    const link = modelDefinition.getLinkFromRdfObjectProperty( replaceAbsoluteNamespaceToPrefix(rdfProp, rdfPrefixesMappings));

    if(link){
      if(!Array.isArray(values)){
        values = [values];
      }

      values.map(jsonLdNode => {
        ids = [].concat(ids, gatherIdsFromRdf4jJsonLdNode({
          jsonLdNode,
          modelDefinition: link.getRelatedModelDefinition(),
          rdfPrefixesMappings,
        }));
      })
    }
    return ids;
  }, []);

  ids.push(id);
  return ids;
}

/**
 * @param datatype
 * @param value
 * @return {string|number|*}
 */
function castValueForDataType({datatype, value}) {
  switch (datatype) {
    case "http://www.w3.org/2001/XMLSchema#float":
    case "http://www.w3.org/2001/XMLSchema#double":
    case "http://www.w3.org/2001/XMLSchema#timestamp":
      return parseFloat(value);
    case "http://www.w3.org/2001/XMLSchema#int":
    case "http://www.w3.org/2001/XMLSchema#integer":
      return parseInt(value);
    case "http://www.w3.org/2001/XMLSchema#boolean":
      return typeof value === "boolean" ? value : value === "true";
    case "http://www.w3.org/2001/XMLSchema#date":
    case "http://www.w3.org/2001/XMLSchema#dateTime":
    case "http://www.w3.org/2001/XMLSchema#dateTimestamp":
      return dayjs(value).toISOString();
    default:
      return value;
  }
}
