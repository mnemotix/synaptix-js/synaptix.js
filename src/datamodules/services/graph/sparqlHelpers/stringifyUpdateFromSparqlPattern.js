/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Generator as SPARQL} from "sparqljs";
import {replacePrefixToAbsoluteNamespaceInTriples, convertWhereTriplesToOptional, normalizeTriples} from "./sparqlPatternHelpers";

/**
 * Create a SPARQL query (INSERT, DELETE).
 *
 * @param updateType
 * @param prefixes
 * @param insertTriples
 * @param whereTriples
 * @param optionalWhereTriples
 * @param optionalDeleteTriples
 * @param deleteTriples
 * @param graphId
 * @param operationTriples
 * @param options
 * @param stripNewlineCharInSparqlQuery
 * @return {*}
 */
export function stringifyUpdateFromSparqlPattern({
  graphId,
  updateType,
  prefixes = {},
  insertTriples = [],
  deleteTriples = [],
  whereTriples = [],
  optionalWhereTriples = [],
  optionalDeleteTriples = [],
  operationTriples = [],
  options = {},
  stripNewlineCharInSparqlQuery = false
} = {}) {
  let newline = "\n";
  let graph;

  /* istanbul ignore next */
  if (stripNewlineCharInSparqlQuery) {
    newline = " "
  }

  deleteTriples = normalizeTriples(deleteTriples, prefixes);
  insertTriples = normalizeTriples(insertTriples, prefixes, graphId);
  whereTriples  = normalizeTriples(whereTriples, prefixes);
  optionalWhereTriples = convertWhereTriplesToOptional(optionalWhereTriples, prefixes);
  optionalDeleteTriples = convertWhereTriplesToOptional(optionalDeleteTriples, prefixes);

  return (new SPARQL({
    indent: ' ',
    newline,
    allPrefixes: true,
    ...options
  })).stringify({
    type: "update",
    prefixes,
    updates: [{
      updateType,
      graph,
      insert: insertTriples,
      delete: [].concat(deleteTriples, optionalDeleteTriples),
      where: [].concat(whereTriples, optionalWhereTriples, operationTriples)
    }]
  });
}