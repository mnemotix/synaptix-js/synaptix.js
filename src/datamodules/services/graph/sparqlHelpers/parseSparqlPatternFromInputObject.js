
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import env from "env-var";

/**
 * Extract object input and return a SPARQL AST representation.
 *
 * @param modelDefinition
 * @param objectInput
 * @param lang
 * @return {insertTriples, deleteTriples, whereTriples, operationTriples}
 */
export function parseSparqlPatternFromInputObject({modelDefinition, objectInput, lang, globalVarsSuffix}) {
  const {id, ...objectProps} = objectInput;

  if(!lang){
    lang = env.get("DEFAULT_LOCALE").default("fr").asString();
  }

  let insertTriples = [];
  let deleteTriples = [];
  let whereTriples = [];
  let operationTriples = [];

  Object.entries(objectProps).map(([prop, value]) => {
    let literalDefinition = modelDefinition.getLiteral(prop);
    if (literalDefinition) {
      let deleteTriple = {
        subject: id,
        predicate: literalDefinition.getRdfDataProperty(),
        object: literalDefinition.toSparqlVariable({suffix: globalVarsSuffix})
      };

      deleteTriples.push(deleteTriple);
      whereTriples.push(deleteTriple);

      if (value != null && value !== "") {
        let sparqlValues = literalDefinition.toSparqlValue(value);

        if(!Array.isArray(sparqlValues)){
          sparqlValues = [sparqlValues];
        }

        sparqlValues.map(object => {
          insertTriples.push({
            subject: id,
            predicate: literalDefinition.getRdfDataProperty(),
            object
          })
        });
      }
    }

    let labelDefinition = modelDefinition.getLabel(prop.replace("Translations", ""));

    // See https://json-ld.org/spec/latest/json-ld/#language-indexing
    if (labelDefinition) {
      deleteTriples.push({
        subject: id,
        predicate: labelDefinition.getRdfDataProperty(),
        object: labelDefinition.toSparqlVariable({suffix: globalVarsSuffix})
      });

      // If value is an array of translations,
      //  - Delete all triples corresponding to this data property
      //  - Insert all triples corresponding of new translation
      if(Array.isArray(value)){
        whereTriples.push({
          type: "optional",
          patterns: [
            {
              type: "bgp",
              triples: [{
                subject: id,
                predicate: labelDefinition.getRdfDataProperty(),
                object: labelDefinition.toSparqlVariable({suffix: globalVarsSuffix})
              }]
            }
          ]
        });

        [...value].map((translation) => {
          insertTriples.push({
            subject: id,
            predicate: labelDefinition.getRdfDataProperty(),
            object: labelDefinition.toSparqlLocalizedValue(translation.value, translation.lang || lang),
          });
        });
      // If value is a string, the language context must be taken account of
      //  - Delete all triples corresponding to this data property filtered by the contextual language
      //  - Insert the triple corresponding of new label
      } else {
        whereTriples.push({
          type: "optional",
          patterns: [
            {
              type: "bgp",
              triples: [{
                subject: id,
                predicate: labelDefinition.getRdfDataProperty(),
                object: labelDefinition.toSparqlVariable({suffix: globalVarsSuffix})
              }]
            },
            {
              type: "filter",
              expression: {
                type: "operation",
                operator: "=",
                args: [
                  {
                    type: "operation",
                    operator: "lang",
                    args: [
                      labelDefinition.toSparqlVariable({suffix: globalVarsSuffix})
                    ]
                  },
                  `"${lang}"`
                ]
              }
            }
          ]
        });

        if (!!value && value !== "") {
          insertTriples.push({
            subject: id,
            predicate: labelDefinition.getRdfDataProperty(),
            object: labelDefinition.toSparqlLocalizedValue(value, lang)
          });
        }
      }
    }


     labelDefinition = modelDefinition.getLabel(prop.replace("Translations", ""));

    // See https://json-ld.org/spec/latest/json-ld/#language-indexing
    if (labelDefinition) {

    }
  });

  return {insertTriples, deleteTriples, whereTriples, operationTriples};
}