import {parseSparqlPatternForLinkPath} from "../parseSparqlPatternForLinkPath";
import {LinkPath} from "../../../../../datamodel/toolkit/utils/linkPath/LinkPath";
import FooDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";

describe('parseSparqlPatternForLinkPath', () => {
  it("parse must exists bind link path", async () => {
    let {bindingTriples, templateTriples} = await parseSparqlPatternForLinkPath(({
      sourceId: "test:1234",
      linkPath: new LinkPath()
        .mustExistPropertyBinding({
          bindAs: "isParentProject",
          rdfDataPropertyAlias: "mnx:isParentProject",
          linkPath: new LinkPath()
            .step({linkDefinition: FooDefinitionMock.getLink("hasBaz")})
        })
    }));

    expect(bindingTriples).toEqual([
      {
        "type": "bind",
        "variable": "?isParentProject",
        "expression": {
          "type": "operation",
          "operator": "exists",
          "args": [
            {
              "type": "bgp",
              "triples": [
                {
                  "subject": "?hasBaz_0",
                  "predicate": "mnx:hasBaz",
                  "object": "test:1234"
                },
                {
                  "subject": "?hasBaz_0",
                  "predicate": "rdf:type",
                  "object": "mnx:Baz"
                }
              ]
            }
          ]
        }
      }
    ]);

    expect(templateTriples).toEqual([{
      subject: 'test:1234',
      predicate: 'mnx:isParentProject',
      object: '?isParentProject'
    }]);
  });

  it("parse must not exists bind link path", async () => {
    let {bindingTriples, templateTriples} = await parseSparqlPatternForLinkPath(({
      sourceId: "test:1234",
      linkPath: new LinkPath()
        .mustNotExistPropertyBinding({
          bindAs: "isParentProject",
          rdfDataPropertyAlias: "mnx:isParentProject",
          linkPath: new LinkPath()
            .step({linkDefinition: FooDefinitionMock.getLink("hasBaz")})
        })
    }));

    expect(bindingTriples).toEqual([
      {
        "type": "bind",
        "variable": "?isParentProject",
        "expression": {
          "type": "operation",
          "operator": "notexists",
          "args": [
            {
              "type": "bgp",
              "triples": [
                {
                  "subject": "?hasBaz_0",
                  "predicate": "mnx:hasBaz",
                  "object": "test:1234"
                },
                {
                  "subject": "?hasBaz_0",
                  "predicate": "rdf:type",
                  "object": "mnx:Baz"
                }
              ]
            }
          ]
        }
      }
    ]);

    expect(templateTriples).toEqual([{
      subject: 'test:1234',
      predicate: 'mnx:isParentProject',
      object: '?isParentProject'
    }]);
  });
});