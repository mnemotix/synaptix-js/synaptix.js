/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";
import hash from "object-hash";

/**
 * Parse objectInput to JSON-LD object.
 *
 * @param {string} objectId
 * @param {object} objectInput
 * @param {Link[]} [links]
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {function} normalizeUri
 * @param {string} lang
 */
export function parseObjectInputToJsonLdNode({objectId, objectInput, links, modelDefinition, lang = "fr", normalizeUri = (uri) => uri}) {
  let jsonLdNode = {
    ...(objectId ? {"@id": objectId} : null),
    "@type": normalizeUri(modelDefinition.getRdfType())
  };

  Object.entries(objectInput || {}).map(([propertyName, propertyValue]) => {
    const property = modelDefinition.getProperty(propertyName);
    propertyValue = {
      "@value": propertyValue
    };

    if(property?.getRdfDataProperty()){
      const propName = normalizeUri(property.getRdfDataProperty());

      if(property instanceof LabelDefinition){
        propertyValue["@language"] = lang;
      }


      jsonLdNode[propName] = (jsonLdNode[ propName ] || []).concat([propertyValue]);
    }
  });

  (links || []).map((link) => {
    const linkDefinition = link.getLinkDefinition();
    let linkName = linkDefinition.getRdfObjectProperty() || linkDefinition.getRdfReversedObjectProperty();

    if(linkName) {
      linkName = normalizeUri(linkName);

      let linkValue = parseObjectInputToJsonLdNode({
        objectId: link.getTargetId() ? normalizeUri(link.getTargetId()) : null,
        objectInput: link.getTargetObjectInput(),
        modelDefinition: link.getTargetModelDefinition(),
        lang,
        normalizeUri
      });

      linkValue = {
        ...linkValue,
        ...(link.isDeletion() ? {"@deletion": true} : null),
        ...(link.isReversed() || linkDefinition.getRdfReversedObjectProperty() ? {"@reversed": true} : null),
      };

      jsonLdNode[linkName] = (jsonLdNode[ linkName ] || []).concat([linkValue]) ;
    }
  });

  jsonLdNode["@ref"] = hash(jsonLdNode);

  return jsonLdNode;
}