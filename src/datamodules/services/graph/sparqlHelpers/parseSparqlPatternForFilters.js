
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import LiteralDefinition from "../../../../datamodel/toolkit/definitions/LiteralDefinition";
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";

/**
 * @param {LinkFilter[]} [linkFilters]
 * @param {PropertyFilter[]} [propertyFilters]
 * @param {string} [sourceId]
 * @return {{whereTriples: array, operationTriples: array, rdfPrefixesMappings: object}}
 */
export function parseSparqlPatternForFilters({linkFilters = [], propertyFilters = [], sourceId} = {}) {
  let whereTriples = [];
  let operationTriples = [];
  let rdfPrefixesMappings = {};
  
  if (!sourceId){
    sourceId = "?uri";
  }
  
  if (linkFilters) {
    for (let linkFilter of linkFilters) {
      let rdfObjectProperty = linkFilter.linkDefinition.getRdfObjectProperty();
      let rdfReversedObjectProperty = linkFilter.linkDefinition.getRdfReversedObjectProperty();
      let reversePredicates =
        !!rdfObjectProperty && linkFilter.isReversed || rdfReversedObjectProperty && !linkFilter.isReversed;

      if (linkFilter.id) {
        whereTriples.push({
          subject: !reversePredicates ? sourceId : linkFilter.id,
          predicate: rdfObjectProperty || rdfReversedObjectProperty,
          object: !reversePredicates ? linkFilter.id : sourceId
        });
      }

      /* istanbul ignore next */
      if (linkFilter.rdfFilterFunction) {
        if (linkFilter.rdfFilterFunction?.literalDefinition) {
          let linkVariable = `?${linkFilter.linkDefinition.getLinkName()}`;

          whereTriples.push({
            subject: !reversePredicates ? sourceId : linkVariable,
            predicate: rdfObjectProperty || rdfReversedObjectProperty,
            object: !reversePredicates ? linkVariable : sourceId
          });

          whereTriples.push({
            subject: linkVariable,
            predicate: linkFilter.rdfFilterFunction.literalDefinition.getRdfDataProperty(),
            object: linkFilter.rdfFilterFunction.literalDefinition.toSparqlVariable({})
          });

          if (linkFilter.rdfFilterFunction?.generateRdfFilterDefinition) {
            let {args, rdfFunction, rdfOperator} = linkFilter.rdfFilterFunction.generateRdfFilterDefinition({
              literalVariable: linkFilter.rdfFilterFunction.literalDefinition.toSparqlVariable({})
            });

            operationTriples.push({
              type: "filter",
              expression: rdfFunction ? {
                type: "functioncall",
                function: rdfFunction,
                args
              } : {
                type: "operation",
                operator: rdfOperator,
                args
              }
            });
          }
        }
      }

      rdfPrefixesMappings = Object.assign(
        rdfPrefixesMappings,
        linkFilter.linkDefinition.getRdfPrefixesMapping(),
        linkFilter.linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping()
      );
    }
  }

  if (propertyFilters){
    for (let propertyFilter of propertyFilters) {
      const {propertyDefinition, value, lang} = propertyFilter;

      if (propertyDefinition instanceof LiteralDefinition){
        const {generateRdfFilterDefinition} = propertyFilter;

        /* istanbul ignore next */
        if (generateRdfFilterDefinition) {
          let {args, rdfFunction, rdfOperator} = generateRdfFilterDefinition({
            literalVariable: propertyDefinition.toSparqlVariable({}),
          });

          operationTriples.push({
            type: "filter",
            expression: rdfFunction ? {
              type: "functioncall",
              function: rdfFunction,
              args
            } : {
              type: "operation",
              operator: rdfOperator,
              args
            }
          });
        } else if (propertyDefinition.getLinkPath()) {
          operationTriples.push({
            type: "filter",
            expression: {
              type: "operation",
              operator: "=",
              args: [propertyDefinition.toSparqlVariable(), propertyDefinition.toSparqlValue(value)]
            }
          })
        } else {
          let rdfDataProperty = propertyDefinition.getRdfDataProperty();

          // TODO : Add any, isNeq, isGte, isGt, isLte, isLt capabilities.

          whereTriples.push({
            subject: sourceId,
            predicate: rdfDataProperty,
            object: propertyDefinition.toSparqlValue(value)
          });
        }
      }

      if (propertyDefinition instanceof LabelDefinition){
        let rdfDataProperty = propertyDefinition.getRdfDataProperty() || propertyDefinition.getRdfAliasDataProperty();

        whereTriples.push({
          subject: sourceId,
          predicate: rdfDataProperty,
          object: lang ? propertyDefinition.toSparqlLocalizedValue(value, lang) : propertyDefinition.toSparqlValue(value)
        });
      }
    }
  }

  return {whereTriples, operationTriples, rdfPrefixesMappings}
}