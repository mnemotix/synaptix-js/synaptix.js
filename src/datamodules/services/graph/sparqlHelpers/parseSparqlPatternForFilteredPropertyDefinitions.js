/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @param {PropertyDefinitionAbstract[]} propertyDefinitions
 * @param {string} value
 * @return {array}
 */
export function parseSparqlPatternForFilteredPropertyDefinitions({propertyDefinitions, value}) {
  let regexPattern = value.match(/^\/(?<pattern>[^\/]+)\/(?<flags>[smixq]*)$/);

  let expressions = propertyDefinitions
    .map(propertyDefinition => propertyDefinition.toSparqlVariable({}))
    .map(variable => ({
        type: "operation",
        operator: "regex",
        args: [
          {
            type: "operation",
            operator: "str",
            args: [variable]
          },
          `"${regexPattern?.groups.pattern || value}"`,
          ...(regexPattern?.groups?.flags ? [`"${regexPattern?.groups?.flags}"`] : [])
        ]
      })
    );

  if (expressions.length > 0) {
    return [{
      type: "filter",
      expression: expressions.reduce((acc, searchableExpression) => {
        // First iteration, return just the expression.
        if (!acc) {
          return searchableExpression
        }

        return {
          type: "operation",
          operator: "||",
          args: [searchableExpression, acc]
        };
      })
    }];
  } else {
    return [];
  }
}