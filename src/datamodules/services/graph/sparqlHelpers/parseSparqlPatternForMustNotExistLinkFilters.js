
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {replacePrefixToAbsoluteNamespaceInTriples} from "./sparqlPatternHelpers";

/**
 * @param {LinkFilter[]} mustNotExistLinkFilters
 * @param {object} rdfPrefixesMappings
 * @param {string} [sourceId]
 * @param {boolean} [isStrict]
 * @param {boolean} [isOptional]
 * @return {array}
 */
export function parseSparqlPatternForMustNotExistLinkFilters({mustNotExistLinkFilters, rdfPrefixesMappings, sourceId, isStrict, isOptional}) {
  let operationTriples = [];

  // TODO: implements mustExistLinkFilters, propertyFilters from "applyRequestNodeFiltersMiddlewares"
  if (mustNotExistLinkFilters && mustNotExistLinkFilters.length > 0) {
    let minusTriples = [];

    for (let linkFilter of mustNotExistLinkFilters) {
      let rdfObjectProperty = linkFilter.linkDefinition.getRdfObjectProperty();
      let rdfReversedObjectProperty = linkFilter.linkDefinition.getRdfReversedObjectProperty();
      let reversePredicates =
        !!rdfObjectProperty && linkFilter.isReversed || rdfReversedObjectProperty && !linkFilter.isReversed;

      if (!linkFilter.id) {
        linkFilter.id = linkFilter.linkDefinition.toSparqlVariable()
      }

      rdfPrefixesMappings = Object.assign(
        rdfPrefixesMappings,
        linkFilter.linkDefinition.getRdfPrefixesMapping(),
        linkFilter.linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping()
      );

      minusTriples.push({
        subject: !reversePredicates ? sourceId || '?uri' : linkFilter.id,
        predicate: rdfObjectProperty || rdfReversedObjectProperty,
        object: !reversePredicates ? linkFilter.id : sourceId || '?uri'
      });

      minusTriples.push({
        subject: linkFilter.id,
        predicate: "rdf:type",
        object: linkFilter.linkDefinition.getRelatedModelDefinition().getRdfType()
      })
    }

    if (isStrict) {
      operationTriples.push({
        type: "filter",
        expression: {
          type: "operation",
          operator: "notexists",
          args: [
            {
              type: "bgp",
              triples: replacePrefixToAbsoluteNamespaceInTriples(minusTriples, rdfPrefixesMappings)
            }
          ]
        }
      });
    } else {
      operationTriples.push({
        type: "minus",
        patterns: [
          {
            type: "bgp",
            triples: replacePrefixToAbsoluteNamespaceInTriples(minusTriples, rdfPrefixesMappings)
          }
        ]
      });
    }
  }

  if(isOptional){
    operationTriples = {
      type: "optional",
      patterns: [operationTriples]
    };
  }
  return operationTriples;
}