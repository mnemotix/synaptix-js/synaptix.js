/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import env from "env-var";
import LinkDefinition, {Link} from "../../../datamodel/toolkit/definitions/LinkDefinition";
import {LinkPath} from "../../../datamodel/toolkit/utils/linkPath/LinkPath";
import {stringifyQueryFromSparqlPattern} from "./sparqlHelpers/stringifyQueryFromSparqlPattern";
import {stringifyUpdateFromSparqlPattern} from "./sparqlHelpers/stringifyUpdateFromSparqlPattern";
import {parseObjectFromJsonLdNode} from "./sparqlHelpers/parseObjectFromJsonLdNode";
import {parseObjectFromObjectInput} from "./sparqlHelpers/parseObjectFromObjectInput";
import {parseSparqlPatternFromInputObject} from "./sparqlHelpers/parseSparqlPatternFromInputObject";
import {parseSparqlPatternFromLinks} from "./sparqlHelpers/parseSparqlPatternFromLinks";
import {parseSparqlPatternForMustNotExistLinkFilters} from "./sparqlHelpers/parseSparqlPatternForMustNotExistLinkFilters";
import {parseSparqlPatternForFilteredPropertyDefinitions} from "./sparqlHelpers/parseSparqlPatternForFilteredPropertyDefinitions";
import {parseSparqlPatternForLinkPath} from "./sparqlHelpers/parseSparqlPatternForLinkPath";
import {parseSparqlPatternForPropertyDefinition} from "./sparqlHelpers/parseSparqlPatternForPropertyDefinition";
import {parseSparqlPatternForFilters} from "./sparqlHelpers/parseSparqlPatternForFilters";
import sortBy from "lodash/sortBy";
import {Collection} from "../../../datamodel/toolkit/models/Collection";
import {replacePrefixToAbsoluteNamespace} from "./sparqlHelpers/sparqlPatternHelpers";
import GraphControllerPublisher from "../../drivers/graph/GraphControllerPublisher";
import GraphControllerClient from "../../drivers/graph/GraphControllerClient";

/**
 * Service used to perform CRUD on a RDF triple store.
 */
export default class GraphControllerService {
  /**
   * @param {NetworkLayerAMQP} networkLayer - The network layer.
   * @param {GraphQLContext} graphQLcontext - The graphQL session context .
   * @param {object} schemaNamespaceMapping - The namespace URI used to describe "mnx:" prefix.
   * @param {string} nodesNamespaceURI  - The URI namespace used to create new node (aka: to generate new URIs)
   * @param {object} namedGraphsMapping- Named graphs mapping
   * @param {string} [nodesPrefix=ex]   - The prefix used for nodesNamespaceURI
   * @param {function} [nodesTypeFormatter] - This function tweaks a node type into a urified version
   * @param {boolean} [stripNewlineCharInSparqlQuery=false] - Remove \n char in SPARQL query (usefull for testing)
   * @param {string} [defaultNamedGraphURI] - The URI of the global named graph where data are inserted
   * @param {GraphMiddleware[]} [middlewares}
   * @param {boolean} indexDisabled
   */
  constructor({networkLayer, graphQLcontext, schemaNamespaceMapping, namedGraphsMapping, nodesNamespaceURI, nodesPrefix, stripNewlineCharInSparqlQuery, middlewares, nodesTypeFormatter, defaultNamedGraphURI, indexDisabled}) {
    if (!graphQLcontext) {
      throw new Error(`You must provide a GrapQLContext to ${this.constructor.name}::constructor()`);
    }

    if (!schemaNamespaceMapping) {
      throw new Error(`You must provide a schemaNamespaceMapping to ${this.constructor.name}::constructor()`);

    }

    if (!nodesNamespaceURI) {
      throw new Error(`You must provide a nodesNamespaceURI to ${this.constructor.name}::constructor()`);
    }

    let senderId = graphQLcontext.getUser() ? graphQLcontext.getUser().getId() : process.env.UUID;

    this._networkLayer = networkLayer;
    this._nodesNamespaceURI = nodesNamespaceURI;
    this._schemaNamespaceMapping = schemaNamespaceMapping;
    this._namedGraphsMapping = namedGraphsMapping || {};
    this._nodesPrefix = nodesPrefix || "ex";
    this._stripNewlineCharInSparqlQuery = stripNewlineCharInSparqlQuery || false;
    this._middlewares = middlewares;
    this._nodesTypeFormatter = nodesTypeFormatter;
    this._defaultNamedGraphURI = defaultNamedGraphURI;
    this._indexDisabled = indexDisabled;

    switch (env.get("GRAPH_CONTROLLER_VERSION").asString()) {
      case "PUBLISHER":
        this._graphControllerPublisher = new GraphControllerPublisher(networkLayer, senderId);
        break;
      default:
        this._graphControllerPublisher = new GraphControllerClient(networkLayer, senderId);
    }
    this._namedGraphsMapping = namedGraphsMapping;
  }

  /**
   * @param {string} defaultNamedGraphURI
   */
  setDefaultNamedGraphURI(defaultNamedGraphURI) {
    this._defaultNamedGraphURI = defaultNamedGraphURI;
  }

  setNamedGraphsMapping(value) {
    this._namedGraphsMapping = value;
  }

  /**
   * Apply middlewares on node creation.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId - Object id
   * @param {object} [objectInput] - Object input
   * @param {Link[]} [links] -  List of links
   * @param {Link[]} [extraLinks] -  List of extra auto-generated links from middlewares.
   * @return {{links: *, objectInput: *, extraLinks: *}}
   */
  async applyNodeCreationMiddlewares({modelDefinition, objectId, objectInput = {}, links = [], extraLinks = []}) {
    for (let middleware of this._middlewares || []) {
      ({objectInput, links, extraLinks} = await middleware.handleObjectCreation({
        modelDefinition,
        objectId,
        objectInput,
        links,
        extraLinks
      }));
    }
    return {objectInput, links, extraLinks};
  }

  /**
   * Apply middlewares on node update.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {object} [objectInput] - Object input
   * @param {Link[]} [links] -  List of links
   * @param {Link[]} [extraLinks]  -  List of extra auto-generated links from middlewares.
   * @return {{links: *, objectInput: *}}
   */
  async applyNodeUpdateMiddlewares({modelDefinition, objectId, objectInput = {}, links = [], extraLinks = []}) {
    for (let middleware of this._middlewares || []) {
      ({objectInput, links} = await middleware.handleObjectUpdate({
        modelDefinition,
        objectId,
        objectInput,
        links,
        extraLinks
      }));
    }

    return {objectInput, links, extraLinks};
  }

  /**
   * Apply middlewares on node deletion.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId - Object id
   * @param {object} [objectInput] - Object input
   * @param {Link[]} [links] -  List of links used in deletion
   * @param {Link[]} [extraLinks] -  List of extra auto-generated links from middlewares.
   * @return {{links: *, objectInput: *}}
   */
  async applyNodeDeletionMiddlewares({modelDefinition, objectId, objectInput = {}, links = [], extraLinks = []}) {
    for (let middleware of this._middlewares || []) {
      ({objectInput, links} = await middleware.handleObjectDeletion({
        modelDefinition,
        objectInput,
        objectId,
        links,
        extraLinks
      }));
    }

    return {objectInput, extraLinks};
  }

  /**
   * Apply middlewares on node deletion.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {PropertyFilter[]} [propertyFilters] -  List of label filters
   * @param {LinkFilter[]} [mustExistLinkFilters] -  List of link filters
   * @param {LinkFilter[]} [mustNotExistLinkFilters] -  List of link filters
   * @returns {{propertyFilters: PropertyFilter[], mustExistLinkFilters: LinkFilter[], mustNotExistLinkFilters: LinkFilter[]}}
   */
  async applyRequestNodeFiltersMiddlewares({modelDefinition, propertyFilters, mustExistLinkFilters, mustNotExistLinkFilters}) {
    propertyFilters = propertyFilters || [];
    mustExistLinkFilters = mustExistLinkFilters || [];
    mustNotExistLinkFilters = mustNotExistLinkFilters || [];

    for (let middleware of this._middlewares || []) {
      ({
        propertyFilters,
        mustExistLinkFilters,
        mustNotExistLinkFilters
      } = await middleware.handleObjectQuery({
        modelDefinition,
        propertyFilters,
        mustExistLinkFilters,
        mustNotExistLinkFilters
      }));
    }

    return {propertyFilters, mustExistLinkFilters, mustNotExistLinkFilters};
  }

  /**
   * @param {NodeSerialization[]} nodes
   */
  async waitForIndexSyncingOf(nodes) {
  }

  /* istanbul ignore next */

  /**
   * @return {GraphControllerPublisher}
   */
  getGraphControllerPublisher() {
    return this._graphControllerPublisher;
  }

  /**
   *
   * @return {object}
   */
  getDefaultRdfMappings() {
    return {
      ...this._schemaNamespaceMapping,
      [this._nodesPrefix]: this._nodesNamespaceURI,
    }
  }

  /**
   * Is node exists
   * @param id
   * @param modelDefinition
   * @return {boolean}
   */
  async isNodeExists({id, modelDefinition}) {
    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    let {mustNotExistLinkFilters} = await this.applyRequestNodeFiltersMiddlewares({modelDefinition});

    let operationTriples = parseSparqlPatternForMustNotExistLinkFilters({
      mustNotExistLinkFilters,
      rdfPrefixesMappings,
      isStrict: true,
      sourceId: id
    });

    return this._graphControllerPublisher.ask({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "ASK",
        prefixes: rdfPrefixesMappings,
        whereTriples: [{
          subject: id,
          predicate: "rdf:type",
          object: modelDefinition.getRdfType()
        }],
        operationTriples
      })
    });
  }

  /**
   * Is object has a link with another one defined by it's id ?
   *
   * @param {string} id
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LinkDefinition|LinkDefinition[]} linkDefinitions
   * @param {string} targetId
   * @return {boolean}
   */
  async isNodeLinkedToTargetId({id, modelDefinition, linkDefinitions, targetId}) {
    let whereTriples = [];
    let operationTriples = [];
    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    if (linkDefinitions instanceof LinkDefinition) {
      linkDefinitions = [linkDefinitions];
    }

    let lastTargetId = id;

    for (let [index, linkDefinition] of linkDefinitions.entries()) {
      let stepSourceId = lastTargetId;
      let stepTargetId = linkDefinition.toSparqlVariable();

      if (index === linkDefinitions.length - 1) {
        stepTargetId = targetId;
      } else {
        whereTriples.push({
          subject: stepTargetId,
          predicate: "rdf:type",
          object: linkDefinition.getRelatedModelDefinition().getRdfType()
        });
      }
      rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping());

      whereTriples.push({
        subject: !!linkDefinition.getRdfObjectProperty() ? stepSourceId : stepTargetId,
        predicate: linkDefinition.getRdfObjectProperty() || linkDefinition.getRdfReversedObjectProperty(),
        object: !!linkDefinition.getRdfObjectProperty() ? stepTargetId : stepSourceId
      });

      let {mustNotExistLinkFilters} = await this.applyRequestNodeFiltersMiddlewares({modelDefinition: linkDefinition.getRelatedModelDefinition()});

      operationTriples = operationTriples.concat(parseSparqlPatternForMustNotExistLinkFilters({
        mustNotExistLinkFilters,
        rdfPrefixesMappings,
        sourceId: stepTargetId
      }));

      lastTargetId = stepTargetId;
    }

    return this._graphControllerPublisher.ask({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "ASK",
        prefixes: rdfPrefixesMappings,
        whereTriples,
        operationTriples
      })
    });
  }

  /**
   * Is object has a link with another one defined by it's id ?
   *
   * @param {string} id - Node id
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} linkPath
   * @return {boolean}
   */
  async isNodeExistsForLinkPath({id, modelDefinition, linkPath}) {
    let whereTriples = [];
    let operationTriples = [];
    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    let {whereTriples: linkPathWhereTriples, operationTriples: linkPathOperationTriples, rdfPrefixesMappings: linkPathRdfPrefixesMappings} = await this._parseSparqlPatternForLinkPath({
      sourceId: id,
      linkPath,
      rdfPrefixesMappings
    });

    rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, linkPathRdfPrefixesMappings);
    whereTriples = whereTriples.concat(linkPathWhereTriples);
    operationTriples = operationTriples.concat(linkPathOperationTriples);

    return this._graphControllerPublisher.ask({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "ASK",
        prefixes: rdfPrefixesMappings,
        whereTriples,
        operationTriples
      })
    });
  }

  /**
   * Returns a RDF type given an URI
   * @param {string} uri
   * @return {string|string[]}
   */
  async getRdfTypeForURI({uri}) {
    let nodes = await this._graphControllerPublisher.construct({
      query: `
${Object.entries(this.getDefaultRdfMappings()).map(([prefix, namespace]) => `PREFIX ${prefix}: <${namespace}>`).join("\n")}
CONSTRUCT { <${uri}> a $type } WHERE{
  <${uri}> a $type
  FILTER (!isBlank($type))
}      
`
    });

    return Array.isArray(nodes) ? nodes?.[0]?.["@type"] : nodes?.["@type"];
  }

  /**
   * Generic method to get a node
   *
   * @param id
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [lang]
   * @param {LinkFilter[]} [linkFilters] - A list of link filters.
   * @param {PropertyFilter[]} [propertyFilters] - A list of property filters.
   * @param {FragmentDefinition[]} fragmentDefinitions - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @returns {*}
   */
  async getNode({id, modelDefinition, lang, linkFilters, propertyFilters, fragmentDefinitions}) {
    let typeTriple = {
      subject: id,
      predicate: "rdf:type",
      object: modelDefinition.getRdfType()
    };

    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());
    let templateTriples = [typeTriple];
    let whereTriples = [typeTriple];
    let optionalWhereTriples = [];
    let operationTriples = [];
    let bindingTriples = [];
    let properties = [];

    // If fragments, we add their literals to properties to return
    if (fragmentDefinitions && fragmentDefinitions.length > 0) {
      for (let fragmentDefinition of fragmentDefinitions) {
        if (fragmentDefinition.getModelDefinition().isEqualOrDescendantOf(modelDefinition)) {
          properties = properties.concat(fragmentDefinition.getProperties());
        }
      }
    } else {
      properties = modelDefinition.getProperties().filter(property => !property.isInIndexOnly())
    }

    for (let propertyDefinition of properties) {
      let patterns = await this._parseSparqlPatternForPropertyDefinition({
        propertyDefinition,
        sourceId: id,
        rdfPrefixesMappings
      });

      whereTriples = whereTriples.concat(patterns.whereTriples);
      optionalWhereTriples = optionalWhereTriples.concat(patterns.optionalWhereTriples);
      operationTriples = operationTriples.concat(patterns.operationTriples);
      templateTriples = templateTriples.concat(patterns.templateTriples);
      bindingTriples = bindingTriples.concat(patterns.bindingTriples);
      rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, patterns.rdfPrefixesMappings);
    }

    let {
      whereTriples: filtersWhereTriples,
      operationTriples: filtersOperationTriples,
      rdfPrefixesMappings: filtersRdfPrefixesMappings
    } = parseSparqlPatternForFilters({
      linkFilters,
      propertyFilters,
      sourceId: id
    });

    whereTriples = whereTriples.concat(filtersWhereTriples);
    operationTriples = operationTriples.concat(filtersOperationTriples);
    Object.assign(rdfPrefixesMappings, filtersRdfPrefixesMappings);

    let {mustNotExistLinkFilters} = await this.applyRequestNodeFiltersMiddlewares({modelDefinition});

    operationTriples = operationTriples.concat(parseSparqlPatternForMustNotExistLinkFilters({
      mustNotExistLinkFilters,
      rdfPrefixesMappings,
      sourceId: id,
      isStrict: true
    }));

    let jsonLdNode = await this._graphControllerPublisher.construct({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "CONSTRUCT",
        prefixes: rdfPrefixesMappings,
        template: templateTriples,
        whereTriples: whereTriples,
        optionalWhereTriples: optionalWhereTriples,
        operationTriples: [...operationTriples, ...bindingTriples]
      })
    });

    return this._parseObjectFromJsonLdNode({jsonLdNode, modelDefinition});
  }

  /**
   * Generic method to get a list of nodes
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [qs] - A query string
   * @param {QueryString[]} [queries] - A list of query strings
   * @param {number} [limit=10]
   * @param {number} [offset]
   * @param {Sorting[]} [sortings] - A list of sorting
   * @param {LinkFilter[]} [linkFilters] - A list of link filters.
   * @param {PropertyFilter[]} [propertyFilters] - A list of property filters.
   * @param {LinkPath} [linkPaths] - A list  link paths filters
   * @param {boolean} [discardTypeAssertion=false] - Discard RDF type assertion. This this usefull when URI reference are stored in triple store but belongs to another triple store (Ex: Geonames)
   * @param {FragmentDefinition[]} fragmentDefinitions - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @param {boolean} [justCount]
   * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
   * @returns {*}
   */
  async getNodes({modelDefinition, qs, queries, limit, offset, sortings, linkFilters, propertyFilters, linkPaths, discardTypeAssertion, justCount, fragmentDefinitions, asCollection}) {
    let typeAssertionTriples = [{
      subject: '?uri',
      predicate: "rdf:type",
      object: modelDefinition.getRdfType()
    }];

    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());
    let whereTriples = [];
    let optionalWhereTriples = [];
    let templateTriples = [];
    let operationTriples = [];
    let bindingTriples = [];
    let properties = [];

    // If fragments, we add their literals to properties to return
    if (Array.isArray(fragmentDefinitions)) {
      for (let fragmentDefinition of fragmentDefinitions) {
        if (fragmentDefinition.getModelDefinition().isEqualOrDescendantOf(modelDefinition)) {
          properties = properties.concat(fragmentDefinition.getProperties());
        }
      }
    } else {
      properties = properties.concat(
        modelDefinition.getProperties().filter(property => !property.isInIndexOnly())
      );
    }

    if (qs && qs !== "") {
      let searchableProperties = modelDefinition.getSearchableProperties().filter(property => !property.isInIndexOnly());

      // Be sure that searchableProperties are listed in properties
      for (let searchableProperty of searchableProperties) {
        if (!properties.find(property => property.getPropertyName() === searchableProperty.getPropertyName())) {
          properties.push(searchableProperty);
        }
      }

      let searchableFilters = parseSparqlPatternForFilteredPropertyDefinitions({
        value: qs,
        propertyDefinitions: searchableProperties
      });

      operationTriples = operationTriples.concat(searchableFilters);
    }

    if(queries){
      for(const query of queries){
        let searchableFilters = parseSparqlPatternForFilteredPropertyDefinitions({
          value: query.value,
          propertyDefinitions: query.propertyDefinition
        });

        operationTriples = operationTriples.concat(searchableFilters);
      }
    }

    if (linkPaths) {
      for (let linkPath of linkPaths) {
        let {whereTriples: linkPathWhereTriples, operationTriples: linkPathOperationTriples, rdfPrefixesMappings: linkPathRdfPrefixesMappings, bindingTriples: linkPathBindingTriples} = await this._parseSparqlPatternForLinkPath({
          sourceId: "?uri",
          linkPath,
          rdfPrefixesMappings
        });

        rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, linkPathRdfPrefixesMappings);
        whereTriples = whereTriples.concat(linkPathWhereTriples);
        operationTriples = operationTriples.concat(linkPathOperationTriples);
        bindingTriples = bindingTriples.concat(linkPathBindingTriples);
      }
    }

    let {
      whereTriples: filtersWhereTriples,
      operationTriples: filtersOperationTriples,
      rdfPrefixesMappings: filtersRdfPrefixesMappings
    } = parseSparqlPatternForFilters({
      linkFilters,
      propertyFilters
    });

    whereTriples = whereTriples.concat(filtersWhereTriples);
    operationTriples = operationTriples.concat(filtersOperationTriples);
    Object.assign(rdfPrefixesMappings, filtersRdfPrefixesMappings);

    let {
      mustNotExistLinkFilters
    }
      = await this.applyRequestNodeFiltersMiddlewares({modelDefinition});

    operationTriples = operationTriples.concat(parseSparqlPatternForMustNotExistLinkFilters({
      mustNotExistLinkFilters,
      rdfPrefixesMappings
    }));

    if (sortings && sortings.length > 0) {
      // Be sure that sorting property is listed in properties
      for (let sorting of sortings) {
        if (sorting.getPropertyDefinition()) {
          if (!properties.find(property => property.getPropertyName() === sorting.getPropertyDefinition().getPropertyName())) {
            properties.push(sorting.getPropertyDefinition());
          }
        }
      }
    }

    for (let propertyDefinition of properties.reverse()) {
      let patterns = await this._parseSparqlPatternForPropertyDefinition({
        propertyDefinition,
        sourceId: "?uri",
        rdfPrefixesMappings
      });

      whereTriples = patterns.whereTriples.concat(whereTriples);
      optionalWhereTriples = patterns.optionalWhereTriples.concat(optionalWhereTriples);
      operationTriples = patterns.operationTriples.concat(operationTriples);
      templateTriples = patterns.templateTriples.concat(templateTriples);
      bindingTriples = patterns.bindingTriples.concat(bindingTriples);
      rdfPrefixesMappings = Object.assign(patterns.rdfPrefixesMappings, rdfPrefixesMappings);
    }

    if (!discardTypeAssertion) {
      whereTriples = typeAssertionTriples.concat(whereTriples);
    }

    let totalCount;


    if(asCollection || justCount){
      totalCount = await this._graphControllerPublisher.count({
        query: this._stringifyQueryFromSparqlPattern({
          queryType: "SELECT",
          prefixes: rdfPrefixesMappings,
          variables: [{
            expression: {
              expression: "?uri",
              type: "aggregate",
              aggregation: "count",
              distinct: true
            },
            variable: "?count"
          }],
          distinct: true,
          whereTriples,
          operationTriples
        })
      });
    }

    if (justCount) {
      return totalCount;
    } else {
      let uris;

      if (limit || (sortings || []).length > 0) {
        let order;
        let variables = ["?uri"];
        if ((sortings || []).length > 0) {
          order = [];
          for (let sorting of sortings) {
            if (sorting.getPropertyDefinition()) {
              order.push({
                expression: sorting.getPropertyDefinition().toSparqlVariable(),
                descending: sorting.isDescending()
              });
              variables.push(sorting.getPropertyDefinition().toSparqlVariable());
            }
          }
        }

        let {results: {bindings}} = await this._graphControllerPublisher.select({
          query: this._stringifyQueryFromSparqlPattern({
            queryType: "SELECT",
            prefixes: rdfPrefixesMappings,
            variables,
            whereTriples,
            optionalWhereTriples: order?.length > 0 || operationTriples?.length > 0 ? optionalWhereTriples : [], // Add optional operationTriples only if operationTriples or sorting is applied.
            operationTriples: [...bindingTriples, ...operationTriples],
            limit: limit,
            offset: offset || 0,
            order: order?.length > 0 ? order : null,
            distinct: true,
          })
        });

        uris = bindings.map(binding => binding.uri?.value);
        operationTriples = [{
          type: "filter",
          expression: {
            type: "operation",
            operator: "in",
            args: ["?uri", uris]
          }
        }];
      }

      let jsonLdNodes = await this._graphControllerPublisher.construct({
        query: this._stringifyQueryFromSparqlPattern({
          queryType: "CONSTRUCT",
          prefixes: rdfPrefixesMappings,
          template: [].concat(typeAssertionTriples, templateTriples),
          optionalWhereTriples,
          whereTriples,
          operationTriples: [...bindingTriples, ...operationTriples]
        })
      });

      let objects = [];

      // Case of RDF4J
      if (Array.isArray(jsonLdNodes)) {
        objects = jsonLdNodes.map(jsonLdNode => this._parseObjectFromJsonLdNode({
          jsonLdNode,
          modelDefinition
        }));
        // Case of JENA
      } else {
        if (Object.keys(jsonLdNodes).length <= 1) {
          objects = [];
        } else if (jsonLdNodes["@graph"]) {
          objects = jsonLdNodes["@graph"].map((jsonLdNode) => this._parseObjectFromJsonLdNode({
            jsonLdNode: {
              "@context": jsonLdNodes["@context"],
              ...jsonLdNode
            },
            modelDefinition
          }));
        } else {
          objects = [this._parseObjectFromJsonLdNode({
            jsonLdNode: jsonLdNodes,
            modelDefinition
          })];
        }
      }

      if ((uris || []).length > 0) {
        objects = objects.sort(({uri: uri1}, {uri: uri2}) => {
          return uris.indexOf(uri1) - uris.indexOf(uri2);
        })
      }

      if(asCollection){
        return new Collection({
          objects,
          totalCount // TODO: Find a way to add totalCount here
        })
      }

      return objects;
    }
  }

  /**
   * Get a count of nodes.
   *
   * @param modelDefinition
   * @return {number}
   */
  async countNodes({modelDefinition}) {
    let rdfPrefixesMapping = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    let whereTriples = [
      {
        subject: '?uri',
        predicate: "rdf:type",
        object: modelDefinition.getRdfType()
      }
    ];

    return this._graphControllerPublisher.count({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "SELECT",
        prefixes: rdfPrefixesMapping,
        variables: [{
          expression: {
            expression: "?uri",
            type: "aggregate",
            aggregation: "count",
            distinct: true
          },
          variable: "?count"
        }],
        optionalWhereTriples: whereTriples
      })
    });

  }

  /**
   * Create a node
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition -
   * @param {Link[]} links -  List of links
   * @param {object} [objectInput] - Object input
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {string} [graphId] - Named graph id.
   */
  async createNode({modelDefinition, links = [], objectInput, lang, uri, graphId}) {
    let extraLinks = [];

    let indexable = [];
    let prefixes = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    // Create URI if not provided.
    let objectId = !!uri ? uri : modelDefinition.generateURI({
      nodesNamespaceURI: `${this._nodesPrefix}:`,
      nodeTypeFormatter: this._nodesTypeFormatter
    });

    ({objectInput, links, extraLinks} = await this.applyNodeCreationMiddlewares({modelDefinition, objectId, objectInput, links}));

    if (!this._indexDisabled && modelDefinition.isIndexed()) {
      indexable.push({
        id: objectId,
        type: modelDefinition.getRdfType()
      })
    }

    let insertTriples = [{
      subject: objectId,
      predicate: "rdf:type",
      object: modelDefinition.getRdfType()
    }];

    objectInput = {
      id: objectId,
      ...objectInput
    };

    // Get the object JSON-LD node based on object input
    let {insertTriples: propertiesTriples} = parseSparqlPatternFromInputObject({
      modelDefinition,
      objectInput,
      lang
    });

    insertTriples = insertTriples.concat(propertiesTriples);

    let {insertTriples: linksInsertTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await this._parseSparqlPatternFromLinks({
      modelDefinition,
      objectId,
      links,
      extraLinks,
      lang
    });

    insertTriples = insertTriples.concat(linksInsertTriples);
    prefixes = Object.assign(prefixes, linksPrefixes);
    if (!this._indexDisabled) {
      indexable = indexable.concat(linksIndexable);
    }

    insertTriples = sortBy(insertTriples, ["subject"]);

    await this._graphControllerPublisher.insertTriples({
      query: this._stringifyUpdateFromSparqlPattern({
        updateType: "insert",
        prefixes,
        insertTriples,
        graphId: this.getNamedGraphForModelDefinition(modelDefinition) || graphId
      }),
      messageContext: {
        indexable
      }
    });

    return this._parseObjectFromObjectInput({
      objectInput,
      modelDefinition,
      lang
    });
  }

  /**
   * Create an edge.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId, graphId) {
    let targetModelDefinition = linkDefinition.getRelatedModelDefinition();
    let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
    let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();
    let indexable = [];
    let prefixes = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping(), linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping());

    if (!rdfObjectProperty && !rdfReversedObjectProperty) {
      throw new Error(`A rdfObjectProperty or rdfReversedObjectProperty must be set in the "${linkDefinition.getLinkName()}" link definition from "${modelDefinition.name}"`)
    }

    if (!this._indexDisabled && modelDefinition.isIndexed()) {
      indexable.push({
        type: modelDefinition.getRdfType(),
        id: objectId
      })
    }

    if (!this._indexDisabled && targetModelDefinition.isIndexed()) {
      indexable.push({
        type: targetModelDefinition.getRdfType(),
        id: targetId
      })
    }

    return this._graphControllerPublisher.insertTriples({
      query: this._stringifyUpdateFromSparqlPattern({
        updateType: "insert",
        prefixes,
        insertTriples: [{
          subject: rdfObjectProperty ? objectId : targetId,
          predicate: rdfObjectProperty || rdfReversedObjectProperty,
          object: rdfObjectProperty ? targetId : objectId,
        }],
        graphId: this.getNamedGraphForModelDefinition(modelDefinition) || graphId
      }),
      messageContext: {
        indexable
      }
    });
  }

  /**
   * Delete all edges.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   */
  async deleteEdges(edges) {
    edges = Array.isArray(edges) ? edges : [edges];
    
    const deleteTriples = edges.map(edge => {
      const {modelDefinition, objectId, linkDefinition, targetId} = edge;
      let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
      let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();

      if (!rdfObjectProperty && !rdfReversedObjectProperty) {
        throw new Error(`A rdfObjectProperty or rdfReversedObjectProperty must be set in the "${linkDefinition.getLinkName()}" link definition from "${modelDefinition.name}"`)
      }

      return {
        subject: rdfObjectProperty ? objectId : targetId,
        predicate: rdfObjectProperty || rdfReversedObjectProperty,
        object: rdfObjectProperty ? targetId : objectId
      };
    });

    const targetPrefixesMapping = edges.map(edge => {
      const {modelDefinition, linkDefinition} = edge;
      let targetModelDefinition = linkDefinition.getRelatedModelDefinition();
      return Object.assign({}, modelDefinition.getRdfPrefixesMapping(), targetModelDefinition.getRdfPrefixesMapping());
    });

    await this._graphControllerPublisher.deleteTriples({
      query: this._stringifyUpdateFromSparqlPattern({
        updateType: "delete",
        prefixes: Object.assign({}, this.getDefaultRdfMappings(), ...targetPrefixesMapping),
        deleteTriples: deleteTriples,
        whereTriples: deleteTriples,
        insertTriples: []
      }),
      messageContext: {}
    });
  }

  /**
   * Generic method to update a node.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {Link[]} links -  List of links
   * @param {Link[]} [extralinks] -  List of extra links autogenerated by middlewares.
   * @param {object} objectInput
   * @param {string|null} [lang]
   */
  async updateNode({modelDefinition, objectInput, links, extraLinks = [], lang, graphId}) {
    if (!objectInput.id) {
      throw new Error(`Impossible to update node, "id" property is not defined in "objectInput". Got ${objectInput}`)
    }

    let indexable = [];

    ({objectInput, links} = await this.applyNodeUpdateMiddlewares({
      modelDefinition,
      objectInput,
      objectId: objectInput.id,
      links,
      extraLinks
    }));

    // Get the object JSON-LD node based on object input
    let {insertTriples, deleteTriples, whereTriples, operationTriples} = parseSparqlPatternFromInputObject({
      modelDefinition,
      objectInput,
      lang
    });

    let prefixes = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    let {insertTriples: linksInsertTriples, deleteTriples: linksDeleteTriples, whereTriples: linksWhereTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await this._parseSparqlPatternFromLinks({
      modelDefinition,
      objectId: objectInput.id,
      links,
      extraLinks,
      lang
    });

    insertTriples = insertTriples.concat(linksInsertTriples);
    deleteTriples = deleteTriples.concat(linksDeleteTriples);
    whereTriples = whereTriples.concat(linksWhereTriples);
    prefixes = Object.assign(prefixes, linksPrefixes);
    if (!this._indexDisabled) {
      indexable = indexable.concat(linksIndexable);
    }

    let cascadingUpdatingLinkDefinitions = modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingUpdated());

    for (let cascadingUpdatingLinkDefinition of cascadingUpdatingLinkDefinitions) {
      // TODO: Set indexation infos here.
    }

    insertTriples = sortBy(insertTriples, ["subject"]);

    if (insertTriples.length !== 0 || deleteTriples.length !== 0) {
      await this._graphControllerPublisher.updateTriples({
        query: this._stringifyUpdateFromSparqlPattern({
          updateType: "insertdelete",
          prefixes,
          insertTriples,
          deleteTriples,
          optionalWhereTriples: whereTriples,
          operationTriples,
          graphId: this.getNamedGraphForModelDefinition(modelDefinition) || graphId
        }),
        messageContext: {
          indexable
        }
      });
    }

    return this._parseObjectFromObjectInput({modelDefinition, objectInput, lang});
  }

  /**
   * Generic method to update a node.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string[]} ids
   * @param {LinkPath[]} linkPaths
   * @param {object} objectsInput
   * @param {Link[]} [links] -  List of links
   * @param {Link[]} [extraLinks] -  List of extra links autogenerated by middlewares
   * @param {string|null} [lang]
   * @param {LinkFilter[]} [linkFilters] - A list of link filters.
   * @param {PropertyFilter[]} [propertyFilters] - A list of property filters.
   */
  async updateNodes({modelDefinition, ids = [], linkFilters = [], propertyFilters = [], linkPaths = [], objectsInput, links, lang, extraLinks = [], graphId} = {}) {
    const updatingNodesVariable = "?node";

    let indexable = [],
      insertTriples = [],
      deleteTriples = [],
      whereTriples = [{
        subject: updatingNodesVariable,
        predicate: "rdf:type",
        object: modelDefinition.getRdfType()
      }],
      optionalWhereTriples = [],
      operationTriples = [],
      prefixes = {};

    if (linkPaths?.length > 0 || propertyFilters?.length > 0 || linkFilters?.length > 0) {
      for (let linkPath of linkPaths) {
        let {whereTriples: linkPathWhereTriples, operationTriples: linkPathOperationTriples, rdfPrefixesMappings: linkPathRdfPrefixesMappings, bindingTriples: linkPathBindingTriples} = await this._parseSparqlPatternForLinkPath({
          sourceId: updatingNodesVariable,
          linkPath,
          rdfPrefixesMappings
        });

        prefixes = Object.assign(prefixes, linkPathRdfPrefixesMappings);
        whereTriples = [].concat(whereTriples, linkPathWhereTriples);
        operationTriples = [].concat(operationTriples, linkPathOperationTriples);
      }

      const {
        whereTriples: filtersWhereTriples,
        operationTriples: filtersOperationTriples,
        rdfPrefixesMappings: filtersPrefixes
      } = parseSparqlPatternForFilters({
        linkFilters,
        propertyFilters,
        sourceId: updatingNodesVariable
      });

      whereTriples = [].concat(whereTriples, filtersWhereTriples);
      operationTriples = [].concat(operationTriples, filtersOperationTriples);
      prefixes = Object.assign(prefixes, filtersPrefixes);
    }

    let objectInput = {
      id: updatingNodesVariable,
      ...objectsInput
    };

    ({objectInput, links} = await this.applyNodeUpdateMiddlewares({
      modelDefinition,
      objectInput,
      objectId: updatingNodesVariable,
      links,
      extraLinks
    }));

    // Get the object JSON-LD node based on object input
    let {
      insertTriples: objectInsertTriples,
      deleteTriples: objectDeleteTriples,
      whereTriples: objectWhereTriples,
      operationTriples: objectOperationTriples
    } = parseSparqlPatternFromInputObject({
      modelDefinition,
      objectInput,
      lang
    });

    let {insertTriples: linksInsertTriples, deleteTriples: linksDeleteTriples, whereTriples: linksWhereTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await this._parseSparqlPatternFromLinks({
      modelDefinition,
      objectId: objectInput.id,
      links,
      extraLinks,
      lang,
    });

    prefixes = Object.assign(prefixes, linksPrefixes, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    insertTriples = [].concat(insertTriples, objectInsertTriples, linksInsertTriples);
    deleteTriples = [].concat(deleteTriples, objectDeleteTriples, linksDeleteTriples);
    operationTriples = [].concat(operationTriples, objectOperationTriples);
    optionalWhereTriples = [].concat(optionalWhereTriples, objectWhereTriples, linksWhereTriples);

    if (!this._indexDisabled) {
      indexable = indexable.concat(linksIndexable);
    }

    if(ids?.length > 0){
      operationTriples.push({
        type: "filter",
        expression: {
          type: "operation",
          operator: "in",
          args: [
            updatingNodesVariable,
            ids.map(id => replacePrefixToAbsoluteNamespace(id, prefixes))
          ]
        }
      });
    }

    if (insertTriples.length !== 0 || deleteTriples.length !== 0) {
      await this._graphControllerPublisher.updateTriples({
        query: this._stringifyUpdateFromSparqlPattern({
          updateType: "insertdelete",
          prefixes,
          insertTriples,
          deleteTriples,
          whereTriples,
          optionalWhereTriples,
          operationTriples,
          graphId: this.getNamedGraphForModelDefinition(modelDefinition) || graphId
        }),
        messageContext: {
          indexable
        }
      });
    }

    return ids.map(id => this._parseObjectFromObjectInput({
      modelDefinition,
      objectInput: {
        id,
        ...objectsInput
      },
      lang
    }));
  }

  /**
   * Generic method to remove a node
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} id
   * @param {boolean} permanentRemoval
   */
  async removeNode({modelDefinition, id, permanentRemoval, graphId}) {
    if (!permanentRemoval) {
      // If no middleware apply do nothing.
      let objectInput = {id};
      let links = [];
      let extraLinks = [];

      ({objectInput, links, extraLinks} = await this.applyNodeDeletionMiddlewares({modelDefinition, objectInput, objectId: id, links}));

      return this.updateNode({modelDefinition, objectInput, links, extraLinks});
    } else {
      // Get the object JSON-LD node based on object input
      let deleteBaseTriple = {
        subject: id,
        predicate: '?p',
        object: '?v'
      };

      let deleteTriples = [deleteBaseTriple];
      let whereTriples = [deleteBaseTriple]
      let optionalWhereTriples = [];
      let indexable = [];

      let cascadingRemovingLinkDefinitions = modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingRemoved());

      for (let [index, linkDefinition] of Object.entries(cascadingRemovingLinkDefinitions)) {
        let targetModelDefinition = linkDefinition.getRelatedModelDefinition();
        let targetId = targetModelDefinition.toSparqlVariable();

        let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
        let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();

        let deleteLinkTriples = [{
          subject: rdfObjectProperty ? id : targetId,
          predicate: rdfObjectProperty || rdfReversedObjectProperty,
          object: rdfObjectProperty ? targetId : id
        }];
        deleteTriples = deleteTriples.concat(...deleteLinkTriples);
        optionalWhereTriples = optionalWhereTriples.concat(...deleteLinkTriples);

        // deleteTriples.push({
        //   subject: targetId,
        //   predicate: `?p${index}`,
        //   object: `?v${index}`,
        // }); 
      }

      let cascadingUpdatingLinkDefinitions = modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingUpdated());

      for (let cascadingUpdatingLinkDefinition of cascadingUpdatingLinkDefinitions) {
        if (cascadingUpdatingLinkDefinition.getRelatedModelDefinition().isIndexed()) {
          // indexable.push({
          //   type: cascadingUpdatingLinkDefinition.getRelatedModelDefinition().getRdfType(),
          //   id: ""
          // })
        }
      }

      await this._graphControllerPublisher.deleteTriples({
        query: this._stringifyUpdateFromSparqlPattern({
          updateType: "insertdelete",
          prefixes: Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping()),
          deleteTriples,
          whereTriples,
          optionalWhereTriples,
          insertTriples: [],
          graphId: this.getNamedGraphForModelDefinition(modelDefinition) || graphId
        }),
        messageContext: {
          // TODO: Set indexation infos here.
        }
      });
    }
  }

  /**
   * Generic method to remove a node
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} id
   * @param {boolean} permanentRemoval
   */
  async removeNodes({modelDefinition, ids, permanentRemoval, graphId}) {
    let errors = [];
    let removedIds = [];
    let indexable = [];

    if (!permanentRemoval) {
      // Keep an external declaration of links to mutualize Deletion object (in case of ActionMiddleware declared).
      let extraLinks = [];

      for (const id of ids) {
        if(!this._indexDisabled){
          indexable.push({
            id,
            type: modelDefinition.getRdfType()
          });
        }

        // @see MnxActionGraphMiddleware notes :
        // Deletion object is only created on the first iteration, and reused for other deleted nodes.
        ({extraLinks} = await this.applyNodeDeletionMiddlewares({
          modelDefinition,
          objectId: id,
          extraLinks
        }));

        removedIds.push(id);
      }

      let prefixes = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

      let {insertTriples, deleteTriples, whereTriples, prefixes: linksPrefixes} = await this._parseSparqlPatternFromLinks({
        modelDefinition,
        extraLinks
      });

      prefixes = Object.assign(prefixes, linksPrefixes);

      if (insertTriples.length !== 0 || deleteTriples.length !== 0) {
        await this._graphControllerPublisher.updateTriples({
          query: this._stringifyUpdateFromSparqlPattern({
            updateType: "insertdelete",
            prefixes,
            insertTriples,
            deleteTriples,
            optionalWhereTriples: whereTriples,
            operationTriples: []
          }),
          messageContext: {
            indexable
          }
        });
      }
    } else {
      let deleteTriples = [];
      let optionalDeleteTriples = [];

      for (const [index, id] of Object.entries(ids)) {
        deleteTriples.push({
          subject: id,
          predicate: `?p${index}`,
          object: `?o${index}`
        });

        optionalDeleteTriples.push({
          subject: `?s${index}`,
          predicate: `?rp${index}`,
          object: id
        });

        // TODO: Cascading removal / update
        removedIds.push(id);
      }

      await this._graphControllerPublisher.deleteTriples({
        query: this._stringifyUpdateFromSparqlPattern({
          updateType: "deletewhere",
          prefixes: Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping()),
          deleteTriples,
          optionalDeleteTriples: optionalDeleteTriples,
          whereTriples: deleteTriples,
          optionalWhereTriples: optionalDeleteTriples,
          insertTriples: [],
          graphId: this.getNamedGraphForModelDefinition(modelDefinition) || graphId
        }),
        messageContext: {
          // TODO: Set indexation infos here.
        }
      });

    }

    return removedIds;
  }

  /**
   * Is label translated for lang.
   * @param {Model} sourceNode Source node
   * @param {LabelDefinition} labelDefinition
   * @param {string} lang Lang
   *
   * @return {boolean}
   */
  async isLocalizedLabelTranslatedForNode({sourceNode, lang, labelDefinition}) {
    let labels = sourceNode[labelDefinition.getLabelName()] || {};

    return !!labels[lang];
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param {string} lang
   * @param {boolean} [returnFirstOneIfNotExistForLang] Only if labelDefinition.isPlural() is false
   * @param {boolean} [langFlagEnabled=false] Add the language flag (ex @fr) to the label
   * @return {string}
   */
  async getLocalizedLabelForNode({sourceNode, labelDefinition, lang, returnFirstOneIfNotExistForLang, langFlagEnabled}) {
    let labels = sourceNode[labelDefinition.getLabelName()] || {};

    if (typeof labels === "string") {
      return labelDefinition.isPlural() ? [labels] : labels;
    } else if (Array.isArray(labels)) {
      if (labelDefinition.isPlural()){
        return labels.map(({value, lang}) => {
          if(langFlagEnabled){
            value = `${value}@${lang}`;
          }
          return value;
        })
      } else {
        let label = labels.find(label => label.lang === lang);

        if (!label && returnFirstOneIfNotExistForLang) {
          label = labels[0];
        }

        let value = label?.value || "";

        if(langFlagEnabled){
          value = `${value}@${lang}`;
        }

        return value;
      }
    }

    if(labelDefinition.isPlural()){
      return [];
    }
  }

/////////
// UTILS
/////////

  /**
   * Create a SPARQL query (SELECT, CONSTRUCT, ASK).
   *
   * @see documentation here : https://github.com/RubenVerborgh/SPARQL.js
   *
   * @param queryType
   * @param template
   * @param prefixes
   * @param variables
   * @param triples
   * @param options
   * @param operationTriples
   * @param order
   * @param distinct
   * @return {*}
   */
  _stringifyQueryFromSparqlPattern({
    queryType,
    template,
    prefixes,
    variables,
    whereTriples,
    optionalWhereTriples,
    options,
    limit,
    offset,
    operationTriples,
    order,
    distinct
  }) {
    return stringifyQueryFromSparqlPattern({
      queryType,
      template,
      prefixes,
      variables,
      whereTriples,
      optionalWhereTriples,
      options,
      limit,
      offset,
      operationTriples,
      order,
      distinct,
      stripNewlineCharInSparqlQuery: this._stripNewlineCharInSparqlQuery
    });
  }

  /**
   * Create a SPARQL query (INSERT, DELETE).
   *
   * @param updateType
   * @param prefixes
   * @param insertTriples
   * @param whereTriples
   * @param optionalWhereTriples
   * @param optionalDeleteTriples
   * @param deleteTriples
   * @param graphId
   * @param operationTriples
   * @param options
   * @return {*}
   */
  _stringifyUpdateFromSparqlPattern({
    updateType,
    prefixes,
    insertTriples,
    whereTriples,
    optionalWhereTriples,
    optionalDeleteTriples,
    deleteTriples,
    operationTriples,
    options,
    graphId
  }) {
    if (!graphId) {
      graphId = this._defaultNamedGraphURI;
    }

    return stringifyUpdateFromSparqlPattern({
      updateType,
      prefixes,
      insertTriples,
      whereTriples,
      optionalWhereTriples,
      optionalDeleteTriples,
      deleteTriples,
      operationTriples,
      graphId,
      options,
      stripNewlineCharInSparqlQuery: this._stripNewlineCharInSparqlQuery
    });
  }

  /**
   * @param {JsonLdNode} jsonLdNode - the node
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @return {Model}
   */
  _parseObjectFromJsonLdNode({jsonLdNode, modelDefinition}) {
    return parseObjectFromJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings: this.getDefaultRdfMappings()})
  }

  /**
   *
   * @param objectInput
   * @param modelDefinition
   * @param lang
   * @private
   */
  _parseObjectFromObjectInput({objectInput, modelDefinition, lang}) {
    return parseObjectFromObjectInput({
      objectInput,
      modelDefinition,
      lang,
      rdfPrefixesMapping: this.getDefaultRdfMappings()
    });
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {Link[]} links      -  List of links to update
   * @param {Link[]} extraLinks -  List of extra links autogenerated by middlewares
   * @param {string} lang
   * @param {string} [globalVarsSuffix]
   * @return {{prefixes: object, insertTriples: [object], deleteTriples: [object], whereTriples: [object], indexable: [object]}}
   */
  async _parseSparqlPatternFromLinks({modelDefinition, objectId, links, extraLinks, lang, globalVarsSuffix}) {
    return parseSparqlPatternFromLinks({
      modelDefinition, objectId, links, extraLinks, lang, globalVarsSuffix,
      nodesPrefix: this._nodesPrefix,
      nodeTypeFormatter: this._nodesTypeFormatter,
      nodeCreationMiddleware: this.applyNodeCreationMiddlewares.bind(this),
      nodeUpdateMiddleware: this.applyNodeUpdateMiddlewares.bind(this),
      nodeDeletionMiddleware: this.applyNodeDeletionMiddlewares.bind(this)
    });
  }

  /**
   * @param {boolean} isOptional
   * @param {LinkPath} linkPath
   * @param {string}   sourceId
   * @param {object} rdfPrefixesMappings
   * @return {{whereTriples: array, optionalWhereTriples, templateTriples: array, operationTriples: array, rdfPrefixesMappings: object, bindingTriples: array}}
   * @private
   */
  async _parseSparqlPatternForLinkPath({sourceId, linkPath, rdfPrefixesMappings}) {
    return parseSparqlPatternForLinkPath({
      sourceId,
      linkPath,
      rdfPrefixesMappings,
      isOptional: false,
      requestNodeFiltersMiddleware: this.applyRequestNodeFiltersMiddlewares.bind(this)
    })
  }

  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {string}   sourceId
   * @param {object} rdfPrefixesMappings
   * @return {{whereTriples: array, optionalWhereTriples, templateTriples: array, operationTriples: array, bindingTriples: array, rdfPrefixesMappings: object}}
   * @private
   */
  async _parseSparqlPatternForPropertyDefinition({propertyDefinition, sourceId, rdfPrefixesMappings}) {
    return parseSparqlPatternForPropertyDefinition({
      propertyDefinition,
      sourceId,
      rdfPrefixesMappings,
      requestNodeFiltersMiddleware: this.applyRequestNodeFiltersMiddlewares.bind(this)
    });
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   */
  getNamedGraphForModelDefinition(modelDefinition){
    return this._namedGraphsMapping[modelDefinition.getRdfType()] || this._defaultNamedGraphURI;
  }
}
