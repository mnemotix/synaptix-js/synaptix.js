/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerService from "../GraphControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";
import generateId from "nanoid/generate";
import LinkDefinition, {Link} from "../../../../datamodel/toolkit/definitions/LinkDefinition";
import {MnxActionGraphMiddleware} from "../../../middlewares/graph/MnxActionGraphMiddleware";
import CreationDefinition from "../../../../datamodel/ontologies/mnx-contribution/CreationDefinition";
import EntityDefinition from "../../../../datamodel/ontologies/mnx-common/EntityDefinition";
import dayjs from "dayjs";
import SSOUser from "../../../drivers/sso/models/SSOUser";
import ModelDefinitionAbstract from "../../../../datamodel/toolkit/definitions/ModelDefinitionAbstract";
import LiteralDefinition from "../../../../datamodel/toolkit/definitions/LiteralDefinition";
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";
import {Model} from "../../../../datamodel/toolkit/models/Model";
import {BarDefinitionMock, BazDefinitionMock} from "../../../../datamodel/__tests__/mocks/definitions";
jest.mock("nanoid/generate");
jest.mock("dayjs", () => jest.fn((...args) => ({
  format: () => "now"
})));

let datastoreSession = new (jest.fn().mockImplementation(() => ({
  getLoggedUserAccountId: async () => "test:user-account/12345",
  normalizeId: (id) => id,
  getContext: () => ({
    getLang: () => "fr"
  })
})));

let mnxActionGraphMiddleware = new MnxActionGraphMiddleware();
mnxActionGraphMiddleware.attachSession(datastoreSession);

let graphControllerService = new GraphControllerService({
  networkLayer: new NetworkLayerAMQP("amqp://", "fake"),
  graphQLcontext: new GraphQLContext({
    user: new SSOUser({
      user: {
        id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
        username: 'test@domain.com',
        barLiteral1: 'John',
        barLiteral2: 'Doe'
      }
    }), lang: "fr"
  }),
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  stripNewlineCharInSparqlQuery: false,
  middlewares: [mnxActionGraphMiddleware],
  indexDisabled: true
});

class MockDefinition extends ModelDefinitionAbstract {
  static getRdfType() {
    return "mnx:Mock"
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'fooLink',
        rdfObjectProperty: "mnx:fooLink",
        relatedModelDefinition: MockDefinition
      }),
      new LinkDefinition({
        linkName: 'barLink',
        rdfObjectProperty: "mnx:barLink",
        relatedModelDefinition: MockDefinition
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'fooLabel',
        rdfDataProperty: "mnx:fooLabel"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'fooLiteral',
        rdfDataProperty: 'mnx:fooLiteral',
        isSearchable: true,
        isRequired: true
      }),
    ];
  }
}

describe('GraphControllerServiceWithMnxActionCrudMiddleware', () => {
  let selectSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'select');
  let countSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'count');
  let constructSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'construct');
  let createTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'insertTriples');
  let updateTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'updateTriples');
  let askTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'ask');
  let deleteTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'deleteTriples');


  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });


  let now = "now";

  it('should create an edge', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.createEdge(BarDefinitionMock, "test:bar/1", BarDefinitionMock.getLink("hasBaz"), "test:baz/1");

    expect(createTriplesSpyFn).toHaveBeenCalledWith({
      "messageContext": {
        indexable: []
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>. }`
    });
  });

  it('shoud create a simple node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      lang: 'fr'
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }]
    }, "Bar"));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        indexable: []
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/2>.
 <http://ns.mnemotix.com/instances/creation/2> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/1\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}]}]\".
}`
    });
  });

  it('shoud create a node and connect it to an existing target node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        new Link({
          linkDefinition: BarDefinitionMock.getLink("hasBaz"),
          targetId: 'test:baz/1'
        })
      ],
      lang: 'fr',
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }]
    }, "Bar"));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        indexable: []
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/2>.
 <http://ns.mnemotix.com/instances/creation/2> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/1\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasBaz\\\":[{\\\"@id\\\":\\\"test:baz/1\\\",\\\"@type\\\":\\\"mnx:Baz\\\"}]}]\".
}`
    });
  });

  it('shoud update a node', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        id: "test:bar/1",
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        // This is a plural link with subgraph creation
        BarDefinitionMock.getLink("hasFoo").generateLinkFromTargetProps({
          targetObjectInput: {
            fooLabel1: "06070809010",
          }
        }),
        // This is a single link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            bazLabel1: "derek@mnemotix.com",
          }
        }),
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetId(
          "test:baz/065544333"
        ),
      ],
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
DELETE {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1;
  mnx:hasFoo ?hasFoo.
}
INSERT {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 \"John\";
  mnx:barLiteral2 \"Doe\";
  mnx:barLabel1 \"Blabla en français\"@fr;
  mnx:hasFoo <http://ns.mnemotix.com/instances/foo/1>;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/2>, <http://ns.mnemotix.com/instances/baz/065544333>;
  mnx:hasUpdate <http://ns.mnemotix.com/instances/update/3>.
 <http://ns.mnemotix.com/instances/baz/065544333> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/3>.
 <http://ns.mnemotix.com/instances/baz/2> rdf:type mnx:Baz;
  mnx:bazLabel1 \"derek@mnemotix.com\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/4>.
 <http://ns.mnemotix.com/instances/creation/4> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:foo/1\\\",\\\"@type\\\":\\\"mnx:Foo\\\",\\\"mnx:fooLabel1\\\":[{\\\"@value\\\":\\\"06070809010\\\",\\\"@language\\\":\\\"fr\\\"}]},{\\\"@id\\\":\\\"test:baz/2\\\",\\\"@type\\\":\\\"mnx:Baz\\\",\\\"mnx:bazLabel1\\\":[{\\\"@value\\\":\\\"derek@mnemotix.com\\\",\\\"@language\\\":\\\"fr\\\"}]}]\".
 <http://ns.mnemotix.com/instances/foo/1> rdf:type mnx:Foo;
  mnx:fooLabel1 \"06070809010\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/4>.
 <http://ns.mnemotix.com/instances/update/3> rdf:type mnx:Update;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/1\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasFoo\\\":[{\\\"@type\\\":\\\"mnx:Foo\\\",\\\"mnx:fooLabel1\\\":[{\\\"@value\\\":\\\"06070809010\\\",\\\"@language\\\":\\\"fr\\\"}]}],\\\"mnx:hasBaz\\\":[{\\\"@type\\\":\\\"mnx:Baz\\\",\\\"mnx:bazLabel1\\\":[{\\\"@value\\\":\\\"derek@mnemotix.com\\\",\\\"@language\\\":\\\"fr\\\"}]},{\\\"@id\\\":\\\"test:baz/065544333\\\",\\\"@type\\\":\\\"mnx:Baz\\\"}]}]\".
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:hasFoo ?hasFoo. }
}`,
      messageContext: {
        indexable: []
      },
    });
  });

  it('shoud create a node and connect it to an existing target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: BazDefinitionMock.getLink("hasBar"),
          targetId: 'test:baz/1'
        })).reverse({
          targetModelDefinition: BazDefinitionMock
        }),
        new Link({
          linkDefinition: BarDefinitionMock.getLink("hasReversedFoo"),
          targetId: 'test:foo/1'
        })
      ],
      lang: 'fr',
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }]
    }, "Bar"));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        indexable: []
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 \"John\";
  mnx:barLiteral2 \"Doe\";
  mnx:barLabel1 \"Blabla en français\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/2>.
 <http://ns.mnemotix.com/instances/baz/1> mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>.
 <http://ns.mnemotix.com/instances/creation/2> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/1\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasBar\\\":[{\\\"@id\\\":\\\"test:baz/1\\\",\\\"@type\\\":\\\"mnx:Baz\\\",\\\"@reversed\\\":true},{\\\"@id\\\":\\\"test:foo/1\\\",\\\"@type\\\":\\\"mnx:Foo\\\",\\\"@reversed\\\":true}]}]\".
 <http://ns.mnemotix.com/instances/foo/1> mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>.
}`
    });
  });

  it('shoud create a node and connect it to an new target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: BazDefinitionMock.getLink("hasBar"),
          targetObjectInput: {
            bazLabel1: "123456",
          }
        })).reverse({
          targetModelDefinition: BazDefinitionMock
        })
      ],
      lang: 'fr',
    })).toEqual(new Model('test:bar/1', 'http://ns.mnemotix.com/instances/bar/1', {
      barLiteral1: "John",
      barLiteral2: "Doe",
      barLabel1: [{
        lang: "fr",
        value: "Blabla en français"
      }]
    }, "Bar"));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        indexable: []
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/3>.
 <http://ns.mnemotix.com/instances/baz/2> rdf:type mnx:Baz;
  mnx:bazLabel1 \"123456\"@fr;
  mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/3>.
 <http://ns.mnemotix.com/instances/creation/3> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/1\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasBar\\\":[{\\\"@type\\\":\\\"mnx:Baz\\\",\\\"mnx:bazLabel1\\\":[{\\\"@value\\\":\\\"123456\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"@reversed\\\":true}]},{\\\"@id\\\":\\\"test:baz/2\\\",\\\"@type\\\":\\\"mnx:Baz\\\",\\\"mnx:bazLabel1\\\":[{\\\"@value\\\":\\\"123456\\\",\\\"@language\\\":\\\"fr\\\"}]}]\".
}`
    });
  });

  it('shoud delete single edge', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.deleteEdges([{
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:bazAccount/1",
      linkDefinition: BarDefinitionMock.getLink("hasBaz")
    }]);

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE DATA { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/bazAccount/1>. }`,
      messageContext: {},
    });
  });

  it.only("shoud delete multiple edges", async () => {
    deleteTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.deleteEdges([{
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:bazAccount/1",
      linkDefinition: BarDefinitionMock.getLink("hasBaz")
    }, {
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:foo/2",
      linkDefinition: BarDefinitionMock.getLink("hasFoo")
    }]);

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE DATA {
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/bazAccount/1>;
  mnx:hasFoo <http://ns.mnemotix.com/instances/foo/2>.
}`,
      messageContext: {}
    });
  });

  it('shoud remove a node', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.removeNode({
      modelDefinition: BarDefinitionMock,
      id: "test:bar/1"
    });

    expect(deleteTriplesSpyFn).not.toBeCalled();
    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT {
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasDeletion <http://ns.mnemotix.com/instances/deletion/1>.
 <http://ns.mnemotix.com/instances/deletion/1> rdf:type mnx:Deletion;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>.
}
WHERE {  }`,
      messageContext: {
        indexable: []
      },
    });
  });

  it("should get a node and check it's not deleted", async () => {
    constructSpyFn.mockImplementation(() => ({}));

    await graphControllerService.getNode({
      id: "test:mock/134",
      modelDefinition: MockDefinition,
    });

    expect(constructSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/mock/134> rdf:type mnx:Mock.
 <http://ns.mnemotix.com/instances/mock/134> mnx:fooLiteral ?fooLiteral.
 <http://ns.mnemotix.com/instances/mock/134> mnx:fooLabel ?fooLabel.
}
WHERE {
 <http://ns.mnemotix.com/instances/mock/134> rdf:type mnx:Mock.
 <http://ns.mnemotix.com/instances/mock/134> mnx:fooLiteral ?fooLiteral.
 OPTIONAL { <http://ns.mnemotix.com/instances/mock/134> mnx:fooLabel ?fooLabel. }
 FILTER(NOT EXISTS {
  <http://ns.mnemotix.com/instances/mock/134> mnx:hasDeletion ?hasDeletionAction.
  ?hasDeletionAction rdf:type mnx:Deletion.
 })
}`
    });
  });

  it("should get list of nodes minus deleted", async () => {
    selectSpyFn.mockImplementation(() => ([]));
    constructSpyFn.mockImplementation(() => ([]));

    await graphControllerService.getNodes({
      modelDefinition: MockDefinition,
    });

    expect(constructSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
CONSTRUCT {
 ?uri rdf:type mnx:Mock.
 ?uri mnx:fooLiteral ?fooLiteral.
 ?uri mnx:fooLabel ?fooLabel.
}
WHERE {
 ?uri rdf:type mnx:Mock.
 ?uri mnx:fooLiteral ?fooLiteral.
 OPTIONAL { ?uri mnx:fooLabel ?fooLabel. }
 MINUS {
  ?uri mnx:hasDeletion ?hasDeletionAction.
  ?hasDeletionAction rdf:type mnx:Deletion.
 }
}`
    });
  });

  it("should count a list of nodes minus deleted", async () => {
    countSpyFn.mockImplementation(() => ([]));

    await graphControllerService.getNodes({
      modelDefinition: MockDefinition,
      justCount: true
    });

    expect(countSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
SELECT DISTINCT (COUNT(DISTINCT ?uri) AS ?count) WHERE {
 ?uri rdf:type mnx:Mock.
 ?uri mnx:fooLiteral ?fooLiteral.
 MINUS {
  ?uri mnx:hasDeletion ?hasDeletionAction.
  ?hasDeletionAction rdf:type mnx:Deletion.
 }
}`
    });
  });

  it("should gather creation nodes.", async () => {
    let links = [
      BarDefinitionMock.getLink("hasFoo").generateLinkFromTargetProps({
        targetObjectInput: {
          number: "06070809010",
          label: "Pro"
        }
      }),
    ];

    let extraLinks = [
      EntityDefinition.getLink("hasCreationAction").generateLinkFromTargetProps({
        targetModelDefinition: CreationDefinition,
        targetObjectInput: {
          startedAtTime: "now"
        }
      })
    ];

    let {insertTriples} = await graphControllerService._parseSparqlPatternFromLinks({
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/12345",
      links,
      extraLinks,
      lang: "fr"
    });

    expect(insertTriples).toEqual([
      {
        subject: "test:bar/12345",
        predicate: "mnx:hasFoo",
        object: "test:foo/1"
      },{
        subject : "test:bar/12345",
        predicate: "mnx:hasCreation",
        object: "test:creation/2"
      },{
        subject: "test:creation/2",
        predicate: "rdf:type",
        object: "mnx:Creation"
      }, {
        subject: "test:creation/2",
        predicate: "prov:startedAtTime",
        object: "\"now\"^^http://www.w3.org/2001/XMLSchema#dateTimeStamp"
      }, {
        subject: "test:foo/1",
        predicate: "rdf:type",
        object: "mnx:Foo"
      }, {
        object: "test:creation/2",
        predicate: "mnx:hasCreation",
        subject: "test:foo/1"
      }]
    );
  });

  it("should test if node deleted", async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeExists({
      modelDefinition: MockDefinition,
      id: "test:mock/1234"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
ASK WHERE {
 <http://ns.mnemotix.com/instances/mock/1234> rdf:type mnx:Mock.
 FILTER(NOT EXISTS {
  <http://ns.mnemotix.com/instances/mock/1234> mnx:hasDeletion ?hasDeletionAction.
  ?hasDeletionAction rdf:type mnx:Deletion.
 })
}`
    });
  });

  it("should test if node connected to another and is not deleted", async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: MockDefinition,
      id: "test:mock/1234",
      linkDefinitions: [
        MockDefinition.getLink("fooLink"),
      ],
      targetId: "test:whatever/4567"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
ASK WHERE {
 <http://ns.mnemotix.com/instances/mock/1234> mnx:fooLink <http://ns.mnemotix.com/instances/whatever/4567>.
 MINUS {
  <http://ns.mnemotix.com/instances/whatever/4567> mnx:hasDeletion ?hasDeletionAction.
  ?hasDeletionAction rdf:type mnx:Deletion.
 }
}`
    });
  });

  it("should test if node connected to another and no one is deleted between them", async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: MockDefinition,
      id: "test:mock/1234",
      linkDefinitions: [
        MockDefinition.getLink("fooLink"),
        MockDefinition.getLink("barLink"),
      ],
      targetId: "test:whatever/4567"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
ASK WHERE {
 ?fooLink rdf:type mnx:Mock.
 <http://ns.mnemotix.com/instances/mock/1234> mnx:fooLink ?fooLink.
 ?fooLink mnx:barLink <http://ns.mnemotix.com/instances/whatever/4567>.
 MINUS {
  ?fooLink mnx:hasDeletion ?hasDeletionAction.
  ?hasDeletionAction rdf:type mnx:Deletion.
 }
 MINUS {
  <http://ns.mnemotix.com/instances/whatever/4567> mnx:hasDeletion ?hasDeletionAction.
  ?hasDeletionAction rdf:type mnx:Deletion.
 }
}`
    });
  });

  it("shoud batch update a list of node", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNodes({
      modelDefinition: BarDefinitionMock,
      ids: ["test:bar/1", "test:bar/2", "test:bar/3"],
      objectsInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        // This is a plural link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            bazLabel1: "06070809010",
          }
        }),
      ],
      lang: "fr"
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
DELETE {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1_0;
  mnx:barLiteral2 ?barLiteral2_0;
  mnx:barLabel1 ?barLabel1_0.
 <http://ns.mnemotix.com/instances/bar/2> mnx:barLiteral1 ?barLiteral1_1;
  mnx:barLiteral2 ?barLiteral2_1;
  mnx:barLabel1 ?barLabel1_1.
 <http://ns.mnemotix.com/instances/bar/3> mnx:barLiteral1 ?barLiteral1_2;
  mnx:barLiteral2 ?barLiteral2_2;
  mnx:barLabel1 ?barLabel1_2.
}
INSERT {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 \"John\";
  mnx:barLiteral2 \"Doe\";
  mnx:barLabel1 \"Blabla en français\"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>;
  mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>.
 <http://ns.mnemotix.com/instances/baz/1> rdf:type mnx:Baz;
  mnx:bazLabel1 \"06070809010\"@fr;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/3>.
 <http://ns.mnemotix.com/instances/creation/3> rdf:type mnx:Creation;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:baz/1\\\",\\\"@type\\\":\\\"mnx:Baz\\\",\\\"mnx:bazLabel1\\\":[{\\\"@value\\\":\\\"06070809010\\\",\\\"@language\\\":\\\"fr\\\"}]}]\".
 <http://ns.mnemotix.com/instances/update/2> rdf:type mnx:Update;
  prov:wasAssociatedWith <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime \"now\"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/1\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasBaz\\\":[{\\\"@type\\\":\\\"mnx:Baz\\\",\\\"mnx:bazLabel1\\\":[{\\\"@value\\\":\\\"06070809010\\\",\\\"@language\\\":\\\"fr\\\"}]}]}]\".
 <http://ns.mnemotix.com/instances/bar/2> mnx:barLiteral1 \"John\";
  mnx:barLiteral2 \"Doe\";
  mnx:barLabel1 \"Blabla en français\"@fr.
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>.
 <http://ns.mnemotix.com/instances/bar/2> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>;
  mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>.
 <http://ns.mnemotix.com/instances/baz/1> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/3>.
 <http://ns.mnemotix.com/instances/update/2> rdf:type mnx:Update;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/2\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasBaz\\\":[{\\\"@id\\\":\\\"test:baz/1\\\",\\\"@type\\\":\\\"mnx:Baz\\\"}]}]\".
 <http://ns.mnemotix.com/instances/bar/3> mnx:barLiteral1 \"John\";
  mnx:barLiteral2 \"Doe\";
  mnx:barLabel1 \"Blabla en français\"@fr.
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>.
 <http://ns.mnemotix.com/instances/bar/2> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>.
 <http://ns.mnemotix.com/instances/bar/3> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>;
  mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>.
 <http://ns.mnemotix.com/instances/baz/1> mnx:hasUpdate <http://ns.mnemotix.com/instances/update/2>, <http://ns.mnemotix.com/instances/update/2>;
  mnx:hasCreation <http://ns.mnemotix.com/instances/creation/3>.
 <http://ns.mnemotix.com/instances/update/2> rdf:type mnx:Update;
  prov:value \"[{\\\"@id\\\":\\\"test:bar/2\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasBaz\\\":[{\\\"@id\\\":\\\"test:baz/1\\\",\\\"@type\\\":\\\"mnx:Baz\\\"}]},{\\\"@id\\\":\\\"test:bar/3\\\",\\\"@type\\\":\\\"mnx:Bar\\\",\\\"mnx:barLiteral1\\\":[{\\\"@value\\\":\\\"John\\\"}],\\\"mnx:barLiteral2\\\":[{\\\"@value\\\":\\\"Doe\\\"}],\\\"mnx:barLabel1\\\":[{\\\"@value\\\":\\\"Blabla en français\\\",\\\"@language\\\":\\\"fr\\\"}],\\\"mnx:hasBaz\\\":[{\\\"@id\\\":\\\"test:baz/1\\\",\\\"@type\\\":\\\"mnx:Baz\\\"}]}]\".
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1_0. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2_0. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1_0.
  FILTER((LANG(?barLabel1_0)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/2> mnx:barLiteral1 ?barLiteral1_1. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/2> mnx:barLiteral2 ?barLiteral2_1. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/bar/2> mnx:barLabel1 ?barLabel1_1.
  FILTER((LANG(?barLabel1_1)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/3> mnx:barLiteral1 ?barLiteral1_2. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/3> mnx:barLiteral2 ?barLiteral2_2. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/bar/3> mnx:barLabel1 ?barLabel1_2.
  FILTER((LANG(?barLabel1_2)) = "fr")
 }
}`,
      messageContext: {
        indexable: []
      }
    });
  });
});