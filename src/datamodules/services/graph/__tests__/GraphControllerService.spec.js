/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerService from "../GraphControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";

import ModelDefinitionAbstract from "../../../../datamodel/toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition, {
  Link
} from "../../../../datamodel/toolkit/definitions/LinkDefinition";
import generateId from "nanoid/generate";
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";
import LiteralDefinition from "../../../../datamodel/toolkit/definitions/LiteralDefinition";
import { LinkPath } from "../../../../datamodel/toolkit/utils/linkPath/LinkPath";
import FooDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";
import BazDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
import BarDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/BarDefinitionMock";
import { FragmentDefinition } from "../../../../datamodel/toolkit/definitions/FragmentDefinition";
import NotInstantiableDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/NotInstantiableDefinitionMock";
import LinkPathPropertyDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/LinkPathPropertyDefinitionMock";
import { Sorting } from "../../../../datamodel/toolkit/utils/Sorting";
import LinkPathPropertyBindingsDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/LinkPathPropertyBindingsDefinitionMock";
import {LinkFilter, PropertyFilter} from "../../../../datamodel/toolkit/utils/filter";
import { Model } from "../../../../datamodel/toolkit/models/Model";
import HeterogeneousDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/HeterogeneousDefinitionMock";

jest.mock("nanoid/generate");

let graphControllerService = new GraphControllerService({
  networkLayer: new NetworkLayerAMQP("amqp://", "fake"),
  graphQLcontext: new GraphQLContext({ anonymous: true, lang: "fr" }),
  schemaNamespaceMapping: {
    mnx: "http://ns.mnemotix.com/onto/"
  },
  namedGraphsMapping: {

  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  stripNewlineCharInSparqlQuery: false
});

describe("GraphControllerService", () => {
  let selectSpyFn = jest.spyOn(
    graphControllerService.getGraphControllerPublisher(),
    "select"
  );
  let countSpyFn = jest.spyOn(
    graphControllerService.getGraphControllerPublisher(),
    "count"
  );
  let constructSpyFn = jest.spyOn(
    graphControllerService.getGraphControllerPublisher(),
    "construct"
  );
  let createTriplesSpyFn = jest.spyOn(
    graphControllerService.getGraphControllerPublisher(),
    "insertTriples"
  );
  let updateTriplesSpyFn = jest.spyOn(
    graphControllerService.getGraphControllerPublisher(),
    "updateTriples"
  );
  let askTriplesSpyFn = jest.spyOn(
    graphControllerService.getGraphControllerPublisher(),
    "ask"
  );
  let deleteTriplesSpyFn = jest.spyOn(
    graphControllerService.getGraphControllerPublisher(),
    "deleteTriples"
  );

  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  let now = Date.now();
  Date.now = jest.fn().mockImplementation(() => now);

  it("should get an object from a json-ld node", async () => {
    expect(
      graphControllerService._parseObjectFromJsonLdNode({
        modelDefinition: BarDefinitionMock,
        jsonLdNode: {
          "@context": {
            barLiteral1: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLiteral2: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral2",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLabel1: {
              "@id": "http://ns.mnemotix.com/onto/barLabel1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            mnx: "http://ns.mnemotix.com/onto/",
            test: "http://ns.mnemotix.com/instances/"
          },
          "@type": "mnx:Bar",
          "@id": "test:bar/1",
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              "@language": "en",
              "@value": "Blabla in english"
            },
            {
              "@language": "fr",
              "@value": "Blabla en français"
            }
          ]
        },
        lang: "en"
      })
    ).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              value: "Blabla in english",
              lang: "en"
            },
            {
              value: "Blabla en français",
              lang: "fr"
            }
          ]
        },
        "Bar"
      )
    );

    expect(
      graphControllerService._parseObjectFromJsonLdNode({
        modelDefinition: BarDefinitionMock,
        jsonLdNode: {
          "@context": {
            barLiteral1: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLiteral2: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral2",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLabel1: {
              "@id": "http://ns.mnemotix.com/onto/barLabel1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            mnx: "http://ns.mnemotix.com/onto/",
            test: "http://ns.mnemotix.com/instances/"
          },
          "@type": "mnx:Bar",
          "@id": "test:bar/1",
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              "@language": "en",
              "@value": "Blabla in english"
            },
            {
              "@language": "fr",
              "@value": "Blabla en français"
            }
          ]
        }
      })
    ).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              value: "Blabla in english",
              lang: "en"
            },
            {
              value: "Blabla en français",
              lang: "fr"
            }
          ]
        },
        "Bar"
      )
    );
  });

  it("should get an void from a bad json-ld node", async () => {
    expect(
      graphControllerService._parseObjectFromJsonLdNode({
        modelDefinition: BarDefinitionMock,
        jsonLdNode: {
          "@context": {
            barLiteral1: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLiteral2: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral2",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLabel1: {
              "@id": "http://ns.mnemotix.com/onto/barLabel1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            mnx: "http://ns.mnemotix.com/onto/",
            test: "http://ns.mnemotix.com/instances/"
          }
        },
        lang: "en"
      })
    ).toBeUndefined();

    expect(
      graphControllerService._parseObjectFromJsonLdNode({
        modelDefinition: BarDefinitionMock,
        jsonLdNode: {
          "@context": {
            barLiteral1: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLiteral2: {
              "@id": "http://xmlns.com/foaf/0.1/barLiteral2",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            barLabel1: {
              "@id": "http://ns.mnemotix.com/onto/barLabel1",
              "@type": "http://www.w3.org/2001/XMLSchema#string"
            },
            mnx: "http://ns.mnemotix.com/onto/",
            test: "http://ns.mnemotix.com/instances/"
          },
          "@type": "mnx:Bar",
          "@id": "test:bar/1",
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              "@language": "en",
              "@value": "Blabla in english"
            },
            {
              "@language": "fr",
              "@value": "Blabla en français"
            }
          ]
        }
      })
    ).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              value: "Blabla in english",
              lang: "en"
            },
            {
              value: "Blabla en français",
              lang: "fr"
            }
          ]
        },
        "Bar"
      )
    );
  });

  it("should test if a node exists", async () => {
    askTriplesSpyFn.mockImplementation(() => true);

    let node = await graphControllerService.isNodeExists({
      id: "test:bar/1",
      modelDefinition: BarDefinitionMock
    });

    expect(askTriplesSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
ASK WHERE { <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar. }`
    });
  });

  it("should get a node", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@id": "test:bar/1",
      barLabel1: [
        {
          "@language": "en",
          "@value": "Blabla in english"
        },
        {
          "@language": "fr",
          "@value": "Blabla en français"
        }
      ],
      barLiteral1: "John",
      barLiteral2: "Doe",
      "@context": {
        barLabel1: {
          "@id": "http://ns.mnemotix.com/onto/barLabel1"
        },
        barLiteral2: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral2"
        },
        barLiteral1: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral1"
        },
        test: "http://ns.mnemotix.com/instances/",
        rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        owl: "http://www.w3.org/2002/07/owl#",
        xsd: "http://www.w3.org/2001/XMLSchema#",
        fn: "http://www.w3.org/2005/xpath-functions#",
        rdfs: "http://www.w3.org/2000/01/rdf-schema#",
        sesame: "http://www.openrdf.org/schema/sesame#",
        mnx: "http://ns.mnemotix.com/onto/",
        foaf: "http://xmlns.com/foaf/0.1/"
      }
    }));

    let node = await graphControllerService.getNode({
      id: "test:bar/1",
      modelDefinition: BarDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar.
 <http://ns.mnemotix.com/instances/bar/1> mnx:avatar ?avatar.
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1.
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2.
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral3 ?barLiteral3.
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1.
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel2 ?barLabel2.
}
WHERE {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar.
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1.
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:avatar ?avatar. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral3 ?barLiteral3. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel2 ?barLabel2. }
}`
    });

    expect(node).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              value: "Blabla in english",
              lang: "en"
            },
            {
              value: "Blabla en français",
              lang: "fr"
            }
          ]
        },
        "Bar"
      )
    );
  });

  it("should get a node without inIndexOnly properties", async () => {
    constructSpyFn.mockImplementation(() => ({}));

    await graphControllerService.getNode({
      id: "test:hetero/1",
      modelDefinition: HeterogeneousDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/hetero/1> rdf:type mnx:Hetero.
 <http://ns.mnemotix.com/instances/hetero/1> mnx:heteroLiteral1 ?heteroLiteral1.
}
WHERE {
 <http://ns.mnemotix.com/instances/hetero/1> rdf:type mnx:Hetero.
 OPTIONAL { <http://ns.mnemotix.com/instances/hetero/1> mnx:heteroLiteral1 ?heteroLiteral1. }
}`
    });
  });

  it("should get a list of nodes", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [
        {
          "@id": "test:bar/1",
          barLabel1: [
            {
              "@language": "en",
              "@value": "Blabla in english"
            },
            {
              "@language": "fr",
              "@value": "Blabla en français"
            }
          ],
          barLiteral1: "John",
          barLiteral2: "Doe"
        },
        {
          "@id": "test:bar/1",
          barLiteral1: "Mathieu"
        }
      ],
      "@context": {
        barLabel1: {
          "@id": "http://ns.mnemotix.com/onto/barLabel1"
        },
        barLiteral2: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral2"
        },
        barLiteral1: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral1"
        },
        test: "http://ns.mnemotix.com/instances/",
        rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        owl: "http://www.w3.org/2002/07/owl#",
        xsd: "http://www.w3.org/2001/XMLSchema#",
        fn: "http://www.w3.org/2005/xpath-functions#",
        rdfs: "http://www.w3.org/2000/01/rdf-schema#",
        sesame: "http://www.openrdf.org/schema/sesame#",
        mnx: "http://ns.mnemotix.com/onto/",
        foaf: "http://xmlns.com/foaf/0.1/"
      }
    }));

    let node = await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:barLiteral1 ?barLiteral1.
 ?uri mnx:barLiteral2 ?barLiteral2.
 ?uri mnx:barLiteral3 ?barLiteral3.
 ?uri mnx:barLabel1 ?barLabel1.
 ?uri mnx:barLabel2 ?barLabel2.
}
WHERE {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:barLiteral1 ?barLiteral1.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL { ?uri mnx:barLiteral3 ?barLiteral3. }
 OPTIONAL { ?uri mnx:barLabel1 ?barLabel1. }
 OPTIONAL { ?uri mnx:barLabel2 ?barLabel2. }
}`
    });

    expect(node).toEqual([
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              value: "Blabla in english",
              lang: "en"
            },
            {
              value: "Blabla en français",
              lang: "fr"
            }
          ]
        },
        "Bar"
      ),
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "Mathieu"
        },
        "Bar"
      )
    ]);
  });

  it("should get a list of node with pagination", async () => {
    selectSpyFn.mockImplementation(() => ({
      results: {
        bindings: [
          { uri: { value: "http://ns.mnemotix.com/instances/foo/1" } },
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" } },
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" } }
        ]
      }
    }));
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: FooDefinitionMock,
      limit: 3,
      offset: 2
    });

    expect(selectSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT DISTINCT ?uri WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:fooLabel1 ?fooLabel1.
}
OFFSET 2
LIMIT 3`
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:fooLabel1 ?fooLabel1.
 ?uri mnx:fooLabel2 ?fooLabel2.
}
WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:fooLabel1 ?fooLabel1.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:fooLabel2 ?fooLabel2. }
 FILTER(?uri IN(<http://ns.mnemotix.com/instances/foo/1>, <http://ns.mnemotix.com/instances/foo/2>, <http://ns.mnemotix.com/instances/foo/2>))
}`
    });
  });

  it("should get a list of nodes with required properties", async () => {
    constructSpyFn.mockImplementation(() => ({}));

    class RequiredPropModelDef extends ModelDefinitionAbstract {
      static getRdfType() {
        return "mnx:Bar";
      }

      static getLiterals() {
        return [
          ...super.getLiterals(),
          new LiteralDefinition({
            literalName: "foo1",
            rdfDataProperty: "mnx:foo1",
            isRequired: true
          })
        ];
      }

      static getLabels() {
        return [
          ...super.getLabels(),
          new LabelDefinition({
            labelName: "foo2",
            rdfDataProperty: "mnx:foo2",
            isRequired: true
          })
        ];
      }
    }

    let node = await graphControllerService.getNodes({
      modelDefinition: RequiredPropModelDef
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:foo1 ?foo1.
 ?uri mnx:foo2 ?foo2.
}
WHERE {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:foo1 ?foo1.
 ?uri mnx:foo2 ?foo2.
}`
    });
  });

  it("should get a list of nodes without type assertion", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [
        {
          "@id": "test:bar/1",
          barLabel1: [
            {
              "@language": "en",
              "@value": "Blabla in english"
            },
            {
              "@language": "fr",
              "@value": "Blabla en français"
            }
          ],
          barLiteral1: "John",
          barLiteral2: "Doe"
        },
        {
          "@id": "test:bar/1",
          barLiteral1: "Mathieu"
        }
      ],
      "@context": {
        barLabel1: {
          "@id": "http://ns.mnemotix.com/onto/barLabel1"
        },
        barLiteral2: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral2"
        },
        barLiteral1: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral1"
        },
        test: "http://ns.mnemotix.com/instances/",
        rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        owl: "http://www.w3.org/2002/07/owl#",
        xsd: "http://www.w3.org/2001/XMLSchema#",
        fn: "http://www.w3.org/2005/xpath-functions#",
        rdfs: "http://www.w3.org/2000/01/rdf-schema#",
        sesame: "http://www.openrdf.org/schema/sesame#",
        mnx: "http://ns.mnemotix.com/onto/",
        foaf: "http://xmlns.com/foaf/0.1/"
      }
    }));

    let node = await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      discardTypeAssertion: true
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:barLiteral1 ?barLiteral1.
 ?uri mnx:barLiteral2 ?barLiteral2.
 ?uri mnx:barLiteral3 ?barLiteral3.
 ?uri mnx:barLabel1 ?barLabel1.
 ?uri mnx:barLabel2 ?barLabel2.
}
WHERE {
 ?uri mnx:barLiteral1 ?barLiteral1.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL { ?uri mnx:barLiteral3 ?barLiteral3. }
 OPTIONAL { ?uri mnx:barLabel1 ?barLabel1. }
 OPTIONAL { ?uri mnx:barLabel2 ?barLabel2. }
}`
    });

    expect(node).toEqual([
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              value: "Blabla in english",
              lang: "en"
            },
            {
              value: "Blabla en français",
              lang: "fr"
            }
          ]
        },
        "Bar"
      ),
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "Mathieu"
        },
        "Bar"
      )
    ]);
  });

  it("should get a list of nodes without inIndexOnly properties", async () => {
    constructSpyFn.mockImplementation(() => ({}));

    let node = await graphControllerService.getNodes({
      modelDefinition: HeterogeneousDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Hetero.
 ?uri mnx:heteroLiteral1 ?heteroLiteral1.
}
WHERE {
 ?uri rdf:type mnx:Hetero.
 OPTIONAL { ?uri mnx:heteroLiteral1 ?heteroLiteral1. }
}`
    });
  });

  it("should get a void list of 1 node", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@id": "test:bar/1",
      barLabel1: [
        {
          "@language": "en",
          "@value": "Blabla in english"
        },
        {
          "@language": "fr",
          "@value": "Blabla en français"
        }
      ],
      barLiteral1: "John",
      barLiteral2: "Doe",
      "@context": {
        barLabel1: {
          "@id": "http://ns.mnemotix.com/onto/barLabel1"
        },
        barLiteral2: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral2"
        },
        barLiteral1: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral1"
        },
        test: "http://ns.mnemotix.com/instances/",
        rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        owl: "http://www.w3.org/2002/07/owl#",
        xsd: "http://www.w3.org/2001/XMLSchema#",
        fn: "http://www.w3.org/2005/xpath-functions#",
        rdfs: "http://www.w3.org/2000/01/rdf-schema#",
        sesame: "http://www.openrdf.org/schema/sesame#",
        mnx: "http://ns.mnemotix.com/onto/",
        foaf: "http://xmlns.com/foaf/0.1/"
      }
    }));

    let nodes = await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock
    });

    expect(nodes).toEqual([
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              value: "Blabla in english",
              lang: "en"
            },
            {
              value: "Blabla en français",
              lang: "fr"
            }
          ]
        },
        "Bar"
      )
    ]);
  });

  it("should get a searched list of node ", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "Derek"
    });

    expect(constructSpyFn.mock.calls[0][0].query).toMatch(
      `FILTER((REGEX(STR(?barLabel1), "Derek")) || ((REGEX(STR(?barLiteral2), "Derek")) || (REGEX(STR(?barLiteral1), "Derek"))))`
    );

    await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "/^Derek$/"
    });

    expect(constructSpyFn.mock.calls[1][0].query).toMatch(
      `(REGEX(STR(?barLabel1), "^Derek$")) || ((REGEX(STR(?barLiteral2), "^Derek$")) || (REGEX(STR(?barLiteral1), "^Derek$")))`
    );

    await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      qs: "/^Derek$/im"
    });

    expect(constructSpyFn.mock.calls[2][0].query).toMatch(
      `FILTER((REGEX(STR(?barLabel1), "^Derek$", "im")) || ((REGEX(STR(?barLiteral2), "^Derek$", "im")) || (REGEX(STR(?barLiteral1), "^Derek$", "im"))))`
    );
  });

  it("should get a filtered list of node ", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock,
      propertyFilters: [
        new PropertyFilter({
          propertyDefinition: BarDefinitionMock.getLiteral("barLiteral1"),
          value: "Derek"
        }),
        new PropertyFilter({
          propertyDefinition: BarDefinitionMock.getLiteral("barLiteral2"),
          value: "Devel"
        }),
        new PropertyFilter({
          propertyDefinition: BarDefinitionMock.getLabel("barLabel1"),
          lang: "fr",
          value: "Devel"
        }),
        new PropertyFilter({
          propertyDefinition: BarDefinitionMock.getLabel("barLabel1"),
          value: "Not localized Devel"
        })
      ]
    });

    expect(constructSpyFn.mock.calls[0][0].query).toMatch(
      `?uri mnx:barLiteral1 "Derek".`
    );
    expect(constructSpyFn.mock.calls[0][0].query).toMatch(
      `?uri mnx:barLiteral2 "Devel".`
    );
    expect(constructSpyFn.mock.calls[0][0].query).toMatch(
      `?uri mnx:barLabel1 "Devel"@fr.`
    );
    expect(constructSpyFn.mock.calls[0][0].query).toMatch(
      `?uri mnx:barLabel1 "Not localized Devel".`
    );
  });

  it("should get a list of node filtered by linkPath", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: FooDefinitionMock,
      linkPaths: [
        new LinkPath()
          .step({ linkDefinition: FooDefinitionMock.getLink("hasBaz") })
          .step({
            linkDefinition: BazDefinitionMock.getLink("hasBar"),
            targetId: "test:bar/1234"
          })
      ]
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:fooLabel1 ?fooLabel1.
 ?uri mnx:fooLabel2 ?fooLabel2.
}
WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:fooLabel1 ?fooLabel1.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasBar <http://ns.mnemotix.com/instances/bar/1234>.
 <http://ns.mnemotix.com/instances/bar/1234> rdf:type mnx:Bar.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:fooLabel2 ?fooLabel2. }
}`
    });
  });

  it("should get a list of node linked to another one defined by linkPath with filterOnProperty", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: FooDefinitionMock,
      linkPaths: [
        new LinkPath()
          .step({ linkDefinition: FooDefinitionMock.getLink("hasBaz") })
          .step({ linkDefinition: BazDefinitionMock.getLink("hasBar") })
          .filterOnProperty({
            propertyDefinition: BarDefinitionMock.getLabel("barLabel1"),
            value: "^mnx$"
          })
      ]
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:fooLabel1 ?fooLabel1.
 ?uri mnx:fooLabel2 ?fooLabel2.
}
WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:fooLabel1 ?fooLabel1.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasBar ?hasBar_1.
 ?hasBar_1 rdf:type mnx:Bar.
 ?hasBar_1 mnx:barLabel1 ?barLabel1.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:fooLabel2 ?fooLabel2. }
 FILTER(REGEX(STR(?barLabel1), "^mnx$"))
}`
    });
  });

  it("should get a list of node enhanced by fragments", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: NotInstantiableDefinitionMock,
      fragmentDefinitions: [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        })
      ]
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:NotInstantiableMock.
 ?uri mnx:fooLabel1 ?fooLabel1.
 ?uri mnx:barLiteral1 ?barLiteral1.
}
WHERE {
 ?uri rdf:type mnx:NotInstantiableMock.
 ?uri mnx:fooLabel1 ?fooLabel1.
 ?uri mnx:barLiteral1 ?barLiteral1.
}`
    });
  });

  it("should get a node enhanced by fragments", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNode({
      id: "test:foo/123",
      modelDefinition: NotInstantiableDefinitionMock,
      fragmentDefinitions: [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("fooLabel1")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        })
      ]
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:NotInstantiableMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:fooLabel1 ?fooLabel1.
 <http://ns.mnemotix.com/instances/foo/123> mnx:barLiteral1 ?barLiteral1.
}
WHERE {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:NotInstantiableMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:fooLabel1 ?fooLabel1.
 <http://ns.mnemotix.com/instances/foo/123> mnx:barLiteral1 ?barLiteral1.
}`
    });
  });

  it("should get a node with link path property", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNode({
      id: "test:foo/123",
      modelDefinition: LinkPathPropertyDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:aliasLabel1 ?fooLabel1_2.
}
WHERE {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz <http://ns.mnemotix.com/instances/foo/123>.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:fooLabel1 ?fooLabel1_2.
}`
    });
  });

  it("should get a list of node with link path property", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: LinkPathPropertyDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?uri mnx:aliasLabel1 ?fooLabel1_2.
}
WHERE {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:fooLabel1 ?fooLabel1_2.
}`
    });
  });

  it("should get a list of node with link path property with sorting", async () => {
    selectSpyFn.mockImplementation(() => ({
      results: {
        bindings: [
          { uri: { value: "http://ns.mnemotix.com/instances/foo/1" } },
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" } },
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" } }
        ]
      }
    }));

    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: LinkPathPropertyDefinitionMock,
      sortings: [
        new Sorting({
          propertyDefinition: LinkPathPropertyDefinitionMock.getProperty(
            "linkPathLabel1"
          ),
          descending: true
        })
      ]
    });

    expect(selectSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT DISTINCT ?uri ?fooLabel1 WHERE {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:fooLabel1 ?fooLabel1_2.
}
ORDER BY DESC (?fooLabel1)`
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?uri mnx:aliasLabel1 ?fooLabel1_2.
}
WHERE {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:fooLabel1 ?fooLabel1_2.
 FILTER(?uri IN(<http://ns.mnemotix.com/instances/foo/1>, <http://ns.mnemotix.com/instances/foo/2>, <http://ns.mnemotix.com/instances/foo/2>))
}`
    });
  });

  it("should get a node with link path property", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNode({
      id: "test:foo/123",
      modelDefinition: LinkPathPropertyDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:aliasLabel1 ?fooLabel1_2.
}
WHERE {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz <http://ns.mnemotix.com/instances/foo/123>.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:fooLabel1 ?fooLabel1_2.
}`
    });
  });

  it("should get a node with link path must exist binding property", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNode({
      id: "test:foo/123",
      modelDefinition: LinkPathPropertyBindingsDefinitionMock
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyBindingMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:mustExistProperty ?mustExistProperty.
 <http://ns.mnemotix.com/instances/foo/123> mnx:mustNotExistProperty ?mustNotExistProperty.
}
WHERE {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyBindingMock.
 BIND(EXISTS {
  ?hasBaz_0 mnx:hasBaz <http://ns.mnemotix.com/instances/foo/123>;
   rdf:type mnx:Baz;
   mnx:hasFoo ?hasFoo_1.
  ?hasFoo_1 rdf:type mnx:Foo.
 } AS ?mustExistProperty)
 BIND(NOT EXISTS {
  ?hasBaz_0 mnx:hasBaz <http://ns.mnemotix.com/instances/foo/123>;
   rdf:type mnx:Baz.
 } AS ?mustNotExistProperty)
}`
    });
  });

  it("should get a void list of node", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@context": {
        barLabel1: {
          "@id": "http://ns.mnemotix.com/onto/barLabel1"
        },
        barLiteral2: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral2"
        },
        barLiteral1: {
          "@id": "http://xmlns.com/foaf/0.1/barLiteral1"
        },
        test: "http://ns.mnemotix.com/instances/",
        rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        owl: "http://www.w3.org/2002/07/owl#",
        xsd: "http://www.w3.org/2001/XMLSchema#",
        fn: "http://www.w3.org/2005/xpath-functions#",
        rdfs: "http://www.w3.org/2000/01/rdf-schema#",
        sesame: "http://www.openrdf.org/schema/sesame#",
        mnx: "http://ns.mnemotix.com/onto/",
        foaf: "http://xmlns.com/foaf/0.1/"
      }
    }));

    let nodes = await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock
    });

    expect(nodes).toEqual([]);
  });

  it("should get a void list of node if response empty", async () => {
    constructSpyFn.mockImplementation(() => ({}));

    let nodes = await graphControllerService.getNodes({
      modelDefinition: BarDefinitionMock
    });

    expect(nodes).toEqual([]);
  });

  it("should count a list of node", async () => {
    countSpyFn.mockImplementation(() => 2);

    let count = await graphControllerService.countNodes({
      modelDefinition: BarDefinitionMock
    });

    expect(countSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT (COUNT(DISTINCT ?uri) AS ?count) WHERE { OPTIONAL { ?uri rdf:type mnx:Bar. } }`
    });

    expect(count).toEqual(2);
  });

  it("should create an edge", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.createEdge(
      BarDefinitionMock,
      "test:bar/1",
      BarDefinitionMock.getLink("hasBaz"),
      "test:baz/1"
    );

    expect(createTriplesSpyFn).toHaveBeenCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:bar/1",
            type: "mnx:Bar"
          }
        ]
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>. }`
    });
  });

  it("raise an error on edge creation", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    class BlablaDefinition extends ModelDefinitionAbstract {
      static getLinks() {
        return [
          new LinkDefinition({
            linkName: "bar",
            pathInIndex: "bar",
            relatedModelDefinition: BarDefinitionMock
          })
        ];
      }
    }

    await expect(
      graphControllerService.createEdge(
        BlablaDefinition,
        "blabla:123",
        BlablaDefinition.getLink("bar"),
        "bar:123"
      )
    ).rejects.toThrow(
      'A rdfObjectProperty or rdfReversedObjectProperty must be set in the "bar" link definition from "BlablaDefinition"'
    );
  });

  it("shoud create a simple node", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    expect(
      await graphControllerService.createNode({
        modelDefinition: BarDefinitionMock,
        objectInput: {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: "Blabla en français"
        },
        lang: "fr"
      })
    ).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              lang: "fr",
              value: "Blabla en français"
            }
          ]
        },
        "Bar"
      )
    );

    expect(createTriplesSpyFn).toBeCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:bar/1",
            type: "mnx:Bar"
          }
        ]
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr.
}`
    });
  });

  it("shoud create a simple node in a named graph", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    graphControllerService.setNamedGraphsMapping({
      "mnx:Bar": "http://namedGraph"
    });

    await graphControllerService.createNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      lang: "fr"
    });

    expect(createTriplesSpyFn).toBeCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:bar/1",
            type: "mnx:Bar"
          }
        ]
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 GRAPH <http://namedGraph> {
  <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar.
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 "John".
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 "Doe".
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 "Blabla en français"@fr.
 }
}`
    });

    graphControllerService.setNamedGraphsMapping({});
  });

  it("shoud create a node and connect it to an existing target node", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    expect(
      await graphControllerService.createNode({
        modelDefinition: BarDefinitionMock,
        objectInput: {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: "Blabla en français"
        },
        links: [
          new Link({
            linkDefinition: BarDefinitionMock.getLink("hasBaz"),
            targetId: "test:baz:1"
          })
        ],
        lang: "fr"
      })
    ).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              lang: "fr",
              value: "Blabla en français"
            }
          ]
        },
        "Bar"
      )
    );

    expect(createTriplesSpyFn).toBeCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:bar/1",
            type: "mnx:Bar"
          }
        ]
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz:1>.
}`
    });
  });

  it("shoud throw an exception if link points to a non instantiable model definition", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await expect(
      graphControllerService.createNode({
        modelDefinition: BazDefinitionMock,
        objectInput: {
          bazLabel1: "Boss",
          startDate: now
        },
        links: [
          new Link({
            linkDefinition: BazDefinitionMock.getLink("hasNotInstantiable"),
            targetObjectInput: {
              barLiteral1: "John",
              barLiteral2: "Doe",
              barLabel1: "Blabla en français"
            }
          })
        ],
        lang: "fr"
      })
    ).rejects.toThrow(
      'The model definition "NotInstantiableDefinitionMock" is not instantiable. Please specify a "targetModelDefinition" property in the link "BazDefinitionMock" ~> "hasNotInstantiable"'
    );
  });

  it("shoud create a node and connect it to a new target node", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    expect(
      await graphControllerService.createNode({
        modelDefinition: BazDefinitionMock,
        objectInput: {
          bazLabel1: "Boss"
        },
        links: [
          new Link({
            linkDefinition: BazDefinitionMock.getLink("hasBar"),
            targetModelDefinition: BarDefinitionMock,
            targetObjectInput: {
              barLiteral1: "John",
              barLiteral2: "Doe",
              barLabel1: "Blabla en français"
            }
          })
        ],
        lang: "fr"
      })
    ).toEqual(
      new Model(
        "test:baz/1",
        "http://ns.mnemotix.com/instances/baz/1",
        {
          bazLabel1: [
            {
              lang: "fr",
              value: "Boss"
            }
          ]
        },
        "Baz"
      )
    );

    expect(createTriplesSpyFn).toBeCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:bar/2",
            type: "mnx:Bar"
          }
        ]
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/2> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/baz/1> rdf:type mnx:Baz;
  mnx:bazLabel1 "Boss"@fr;
  mnx:hasBar <http://ns.mnemotix.com/instances/bar/2>.
}`
    });
  });

  it("should return a type", async () => {
    constructSpyFn.mockImplementation(() => ({
      "@id": "dinv:0/en/0",
      "@type": "ddf:Verb",
      "@context": {
        dinv: "http://data.dictionnairedesfrancophones.org/dict/inv/entry/",
        owl: "http://www.w3.org/2002/07/owl#",
        rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        ddf: "http://data.dictionnairedesfrancophones.org/ontology/ddf#",
        lexicog: "http://www.w3.org/ns/lemon/lexicog#",
        xsd: "http://www.w3.org/2001/XMLSchema#",
        fn: "http://www.w3.org/2005/xpath-functions#",
        rdfs: "http://www.w3.org/2000/01/rdf-schema#",
        ontolex: "http://www.w3.org/ns/lemon/ontolex#",
        sesame: "http://www.openrdf.org/schema/sesame#",
        lexinfo: "http://www.lexinfo.net/ontology/2.0/lexinfo#",
        mnx: "http://ns.mnemotix.com/ontologies/crossdomain/"
      }
    }));

    let type = await graphControllerService.getRdfTypeForURI(
      "http://data.dictionnairedesfrancophones.org/dict/inv/entry/0/en/0"
    );

    expect(type).toBe("ddf:Verb");
  });

  it("shoud create a node and connect it to an existing target node with reversed property on link", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    expect(
      await graphControllerService.createNode({
        modelDefinition: BarDefinitionMock,
        objectInput: {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: "Blabla en français"
        },
        links: [
          new Link({
            linkDefinition: BazDefinitionMock.getLink("hasBar"),
            targetId: "test:bar/1"
          }).reverse({
            targetModelDefinition: BazDefinitionMock
          })
        ],
        lang: "fr"
      })
    ).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              lang: "fr",
              value: "Blabla en français"
            }
          ]
        },
        "Bar"
      )
    );

    expect(createTriplesSpyFn).toBeCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:bar/1",
            type: "mnx:Bar"
          }
        ]
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>.
}`
    });
  });

  it("shoud create a node and connect it to an new target node with reversed property on link", async () => {
    createTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    expect(
      await graphControllerService.createNode({
        modelDefinition: BarDefinitionMock,
        objectInput: {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: "Blabla en français"
        },
        links: [
          new Link({
            linkDefinition: BazDefinitionMock.getLink("hasBar"),
            targetObjectInput: {
              startDate: 123456
            }
          }).reverse({
            targetModelDefinition: BazDefinitionMock
          })
        ],
        lang: "fr"
      })
    ).toEqual(
      new Model(
        "test:bar/1",
        "http://ns.mnemotix.com/instances/bar/1",
        {
          barLiteral1: "John",
          barLiteral2: "Doe",
          barLabel1: [
            {
              lang: "fr",
              value: "Blabla en français"
            }
          ]
        },
        "Bar"
      )
    );

    expect(createTriplesSpyFn).toBeCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:bar/1",
            type: "mnx:Bar"
          }
        ]
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/bar/1> rdf:type mnx:Bar;
  mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/baz/2> rdf:type mnx:Baz;
  mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>.
}`
    });
  });

  it("shoud delete single edge", async () => {
    deleteTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.deleteEdges({
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:bazAccount/1",
      linkDefinition: BarDefinitionMock.getLink("hasBaz")
    });

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE DATA { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/bazAccount/1>. }`,
      messageContext: {}
    });
  });

  it("shoud delete multiple edges", async () => {
    deleteTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.deleteEdges([{
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:bazAccount/1",
      linkDefinition: BarDefinitionMock.getLink("hasBaz")
    }, {
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/1",
      targetId: "test:foo/2",
      linkDefinition: BarDefinitionMock.getLink("hasFoo")
    }]);

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE DATA {
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/bazAccount/1>;
  mnx:hasFoo <http://ns.mnemotix.com/instances/foo/2>.
}`,
      messageContext: {}
    });
  });

  it("shoud not remove a node is not permanentRemoval selected", async () => {
    deleteTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);
    updateTriplesSpyFn.mockImplementation(() => []);

    await graphControllerService.removeNode({
      modelDefinition: BarDefinitionMock,
      id: "test:bar/1"
    });

    expect(updateTriplesSpyFn).not.toBeCalled();
    expect(deleteTriplesSpyFn).not.toBeCalled();
  });

  it("shoud not remove a list of node is not permanentRemoval selected", async () => {
    deleteTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);
    updateTriplesSpyFn.mockImplementation(() => []);

    await graphControllerService.removeNodes({
      modelDefinition: BarDefinitionMock,
      ids: ["test:bar/1", "test:bar/2", "test:bar/3"]
    });

    expect(updateTriplesSpyFn).not.toBeCalled();
    expect(deleteTriplesSpyFn).not.toBeCalled();
  });

  it("shoud not remove a list of node is permanentRemoval selected", async () => {
    deleteTriplesSpyFn.mockImplementation(() => []);

    await graphControllerService.removeNodes({
      modelDefinition: BarDefinitionMock,
      ids: ["test:bar/1", "test:bar/2", "test:bar/3"],
      permanentRemoval: true
    });

    expect(deleteTriplesSpyFn).toBeCalledWith({
      "messageContext": {},
      "query" : `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE WHERE {
 <http://ns.mnemotix.com/instances/bar/1> ?p0 ?o0.
 <http://ns.mnemotix.com/instances/bar/2> ?p1 ?o1.
 <http://ns.mnemotix.com/instances/bar/3> ?p2 ?o2.
 OPTIONAL { ?s0 ?rp0 <http://ns.mnemotix.com/instances/bar/1>. }
 OPTIONAL { ?s1 ?rp1 <http://ns.mnemotix.com/instances/bar/2>. }
 OPTIONAL { ?s2 ?rp2 <http://ns.mnemotix.com/instances/bar/3>. }
}`
    });
  });

  it("should parse links into triples", async () => {
    let {
      insertTriples,
      deleteTriples,
      indexable,
      prefixes
    } = await graphControllerService._parseSparqlPatternFromLinks({
      modelDefinition: BarDefinitionMock,
      objectId: "test:bar/12345",
      links: [
        // This is a plural link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            number: "06070809010",
            label: "Pro"
          }
        }),
        // This is a plural link with simple edge creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetId(
          "test:baz/aff2"
        ),
        // This is a single link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            bazLabel1: "derek@mnemotix.com"
          }
        }),
        // This is a plural link with subgraph creation and nested links
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            bazLabel1: "Boss"
          },
          targetNestedLinks: [
            BazDefinitionMock.getLink("hasFoo").generateLinkFromTargetProps({
              targetObjectInput: {
                fooLiteral1: "mnx"
              }
            })
          ]
        })
      ],
      lang: "fr"
    });

    expect(insertTriples).toEqual([
      {
        subject: "test:bar/12345",
        predicate: "mnx:hasBaz",
        object: "test:baz/1"
      },
      {
        subject: "test:bar/12345",
        predicate: "mnx:hasBaz",
        object: "test:baz/aff2"
      },
      {
        subject: "test:bar/12345",
        predicate: "mnx:hasBaz",
        object: "test:baz/2"
      },
      {
        subject: "test:bar/12345",
        predicate: "mnx:hasBaz",
        object: "test:baz/3"
      },
      { subject: "test:baz/1", predicate: "rdf:type", object: "mnx:Baz" },
      { subject: "test:baz/2", predicate: "rdf:type", object: "mnx:Baz" },
      {
        subject: "test:baz/2",
        predicate: "mnx:bazLabel1",
        object: '"derek@mnemotix.com"@fr'
      },
      { subject: "test:baz/3", predicate: "rdf:type", object: "mnx:Baz" },
      { subject: "test:baz/3", predicate: "mnx:hasFoo", object: "test:foo/4" },
      {
        subject: "test:baz/3",
        predicate: "mnx:bazLabel1",
        object: '"Boss"@fr'
      },
      { subject: "test:foo/4", predicate: "rdf:type", object: "mnx:Foo" }
    ]);

    expect(deleteTriples).toEqual([{
      subject: "test:baz/3",
      predicate: "mnx:hasFoo",
      object: "?hasFoo_nested_3" }
    ]);
  });

  it("shoud update a node", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        id: "test:bar/1",
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        // This is a plural link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            number: "06070809010",
            label: "Pro"
          }
        }),
        // This is a single link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            userId: "derek@mnemotix.com",
            username: "derekd"
          }
        }),
        // This is a plural link with subgraph creation and nested links
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps(
          {
            targetObjectInput: {
              bazLabel1: "Boss"
            },
            targetNestedLinks: [
              BazDefinitionMock.getLink("hasFoo").generateLinkFromTargetProps({
                targetObjectInput: {
                  fooLiteral1: "mnx"
                }
              })
            ]
          }
        ),
        // This is a plural link with subgraph update and nested links
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps(
          {
            targetId: "test:baz/toUpdate",
            targetObjectInput: {
              bazLabel1: "Boss 2"
            },
            targetNestedLinks: [
              BazDefinitionMock.getLink("hasFoo").generateLinkFromTargetProps({
                targetId: "test:foo/toUpdate",
                targetObjectInput: {
                  fooLiteral1: "mnx2"
                }
              })
            ]
          }
        ),
        // This is a deleted link
        BarDefinitionMock.getLink("hasBaz").generateDeletionLink({
          targetId: "test:baz/1234"
        })
      ],
      lang: "fr"
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1.
 <http://ns.mnemotix.com/instances/baz/3> mnx:hasFoo ?hasFoo_nested_2.
 <http://ns.mnemotix.com/instances/baz/toUpdate> mnx:hasFoo ?hasFoo_nested_3;
  mnx:bazLabel1 ?bazLabel1.
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1234>.
}
INSERT {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>, <http://ns.mnemotix.com/instances/baz/2>, <http://ns.mnemotix.com/instances/baz/3>, <http://ns.mnemotix.com/instances/baz/toUpdate>.
 <http://ns.mnemotix.com/instances/baz/1> rdf:type mnx:Baz.
 <http://ns.mnemotix.com/instances/baz/2> rdf:type mnx:Baz.
 <http://ns.mnemotix.com/instances/baz/3> rdf:type mnx:Baz;
  mnx:hasFoo <http://ns.mnemotix.com/instances/foo/4>;
  mnx:bazLabel1 "Boss"@fr.
 <http://ns.mnemotix.com/instances/baz/toUpdate> rdf:type mnx:Baz;
  mnx:hasFoo <http://ns.mnemotix.com/instances/foo/toUpdate>;
  mnx:bazLabel1 "Boss 2"@fr.
 <http://ns.mnemotix.com/instances/foo/4> rdf:type mnx:Foo.
 <http://ns.mnemotix.com/instances/foo/toUpdate> rdf:type mnx:Foo.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/baz/3> mnx:hasFoo ?hasFoo_nested_2. }
 OPTIONAL { <http://ns.mnemotix.com/instances/baz/toUpdate> mnx:hasFoo ?hasFoo_nested_3. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/baz/toUpdate> mnx:bazLabel1 ?bazLabel1.
  FILTER((LANG(?bazLabel1)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1234>. }
}`,
      messageContext: {
        indexable: []
      }
    });
  });

  it("shoud remove data properties of a node", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        id: "test:bar/1",
        barLiteral2: null,
        barLabel1: null
      },
      lang: "fr"
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE {
 <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
}`,
      messageContext: {
        indexable: []
      }
    });
  });

  it("shoud remove a node link in case of 1:1 relationship", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNode({
      modelDefinition: BazDefinitionMock,
      objectInput: {
        id: "test:baz/1"
      },
      links: [BazDefinitionMock.getLink("hasBar").generateDeletionLink()],
      lang: "fr"
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE {
 <http://ns.mnemotix.com/instances/baz/1> mnx:hasBar ?hasBar.
 ?hasBar mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/baz/1> mnx:hasBar ?hasBar. }
 OPTIONAL { ?hasBar mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>. }
}`,
      messageContext: {
        indexable: []
      }
    });
  });

  it("shoud not remove a node link in case of 1:N relationship", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        id: "test:bar/1"
      },
      links: [BarDefinitionMock.getLink("hasBaz").generateDeletionLink()],
      lang: "fr"
    });

    expect(updateTriplesSpyFn).not.toBeCalled();
  });

  it("shoud ask if two reversed nodes are connected", async () => {
    askTriplesSpyFn.mockImplementation(() => []);

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: BazDefinitionMock,
      id: "test:baz/1",
      linkDefinitions: BazDefinitionMock.getLink("hasBar"),
      targetId: "test:bar/1"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
ASK WHERE { <http://ns.mnemotix.com/instances/baz/1> mnx:hasBar <http://ns.mnemotix.com/instances/bar/1>. }`
    });
  });

  it("shoud ask if two nodes are connected", async () => {
    askTriplesSpyFn.mockImplementation(() => []);

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: BarDefinitionMock,
      id: "test:bar/1",
      linkDefinitions: BarDefinitionMock.getLink("hasBaz"),
      targetId: "test:baz/123"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
ASK WHERE { <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz <http://ns.mnemotix.com/instances/baz/123>. }`
    });
  });

  it("shoud ask if two nodes are connected between a path", async () => {
    askTriplesSpyFn.mockImplementation(() => []);

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: BarDefinitionMock,
      id: "test:bar/1",
      linkDefinitions: [
        BarDefinitionMock.getLink("hasBaz"),
        BazDefinitionMock.getLink("hasFoo")
      ],
      targetId: "test:foo/123"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
ASK WHERE {
 ?hasBaz rdf:type mnx:Baz.
 <http://ns.mnemotix.com/instances/bar/1> mnx:hasBaz ?hasBaz.
 ?hasBaz mnx:hasFoo <http://ns.mnemotix.com/instances/foo/123>.
}`
    });
  });

  it("shoud ask if node exists given a link path", async () => {
    askTriplesSpyFn.mockImplementation(() => []);

    await graphControllerService.isNodeExistsForLinkPath({
      id: "test:foo/2222",
      modelDefinition: FooDefinitionMock,
      linkPath: new LinkPath()
        .step({ linkDefinition: FooDefinitionMock.getLink("hasBaz") })
        .step({
          linkDefinition: BazDefinitionMock.getLink("hasBar"),
          targetId: "test:bar/1234"
        })
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
ASK WHERE {
 ?hasBaz_0 mnx:hasBaz <http://ns.mnemotix.com/instances/foo/2222>.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasBar <http://ns.mnemotix.com/instances/bar/1234>.
 <http://ns.mnemotix.com/instances/bar/1234> rdf:type mnx:Bar.
}`
    });
  });

  it("shoud batch update a list of node", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNodes({
      modelDefinition: BarDefinitionMock,
      ids: ["test:bar/1", "test:bar/2", "test:bar/3"],
      objectsInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        // This is a plural link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            number: "06070809010",
            label: "Pro"
          }
        }),
      ],
      lang: "fr"
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
DELETE {
 ?node mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1.
}
INSERT {
 ?node mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>.
 <http://ns.mnemotix.com/instances/baz/1> rdf:type mnx:Baz.
}
WHERE {
 ?node rdf:type mnx:Bar.
 OPTIONAL { ?node mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { ?node mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  ?node mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
 FILTER(?node IN(<http://ns.mnemotix.com/instances/bar/1>, <http://ns.mnemotix.com/instances/bar/2>, <http://ns.mnemotix.com/instances/bar/3>))
}`,
      messageContext: {
        indexable: []
      }
    });
  });

  it("shoud batch update a list of node given a list of filter", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNodes({
      modelDefinition: BarDefinitionMock,
      linkFilters: [new LinkFilter({
        linkDefinition: BarDefinitionMock.getLink("hasBaz"),
        id: "test:baz/1"
      })],
      objectsInput: {
        barLiteral1: "John",
        barLiteral2: "Doe",
        barLabel1: "Blabla en français"
      },
      links: [
        // This is a plural link with subgraph creation
        BarDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            number: "06070809010",
            label: "Pro"
          }
        }),
      ],
      lang: "fr"
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
DELETE {
 ?node mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1.
}
INSERT {
 ?node mnx:barLiteral1 "John";
  mnx:barLiteral2 "Doe";
  mnx:barLabel1 "Blabla en français"@fr;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>.
 <http://ns.mnemotix.com/instances/baz/1> rdf:type mnx:Baz.
}
WHERE {
 ?node rdf:type mnx:Bar;
  mnx:hasBaz <http://ns.mnemotix.com/instances/baz/1>.
 OPTIONAL { ?node mnx:barLiteral1 ?barLiteral1. }
 OPTIONAL { ?node mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL {
  ?node mnx:barLabel1 ?barLabel1.
  FILTER((LANG(?barLabel1)) = "fr")
 }
}`,
      messageContext: {
        indexable: []
      }
    });
  });


  it("shoud update all translations of a node", async () => {
    updateTriplesSpyFn.mockImplementation(() => []);
    selectSpyFn.mockImplementation(() => []);

    await graphControllerService.updateNode({
      modelDefinition: BarDefinitionMock,
      objectInput: {
        id: "test:bar/1",
        barLabel1Translations: [
          {
            lang: "fr",
            value: "Blabla en français"
          },
          {
            lang: "en",
            value: "Blabla in english"
          }
        ]
      }
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
DELETE { <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1. }
INSERT { <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 "Blabla en français"@fr, "Blabla in english"@en. }
WHERE { OPTIONAL { <http://ns.mnemotix.com/instances/bar/1> mnx:barLabel1 ?barLabel1. } }`,
      messageContext: {
        indexable: []
      }
    });
  });
});
