/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import env from "env-var";
import { parseObjectFromEsHit } from "./esHelpers/parseObjectFromEsHit";
import {
  ConcatStep,
  LinkStep,
  PropertyFilterStep
} from "../../../datamodel/toolkit/utils/linkPath/steps";
import { Model } from "../../../datamodel/toolkit/models/Model";
import IndexControllerV7Publisher from "../../drivers/index/IndexControllerV7Publisher";
import IndexControllerV7Client from "../../drivers/index/IndexControllerV7Client";
import {logError, logWarning} from "../../../utilities/logger";
import {FragmentDefinition} from "../../../datamodel/toolkit/definitions/FragmentDefinition";
import {LinkFilter} from "../../../datamodel/toolkit/utils/filter";
import {Collection} from "../../../datamodel/toolkit/models/Collection";
import {Aggregation} from "../../../datamodel/toolkit/models/Aggregation";
import {AggregationBucket} from "../../../datamodel/toolkit/models/AggregationBucket";
import {LabelDefinition} from "../../../datamodel/toolkit/definitions";
import {parseQueryStringToEsQuery} from "./esHelpers/parseQueryStringToEsQuery";
import {QueryString, QueryStringType} from "../../../datamodel/toolkit/utils/QueryString";
import {parseRawQsToPartialQueryString} from "./esHelpers/parseRawQsToPartialQueryString";

export default class IndexControllerService {
  /**
   * Constructor
   *
   * @param {NetworkLayerAMQP} networkLayer
   * @param {GraphQLContext} graphQLcontext
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {string} [typesPrefix] - In V2 or LEGACY version, prepend types defined in ModelDefinitions with a prefix.  In V6 version. prepend types defined in ModelDefinitions with a prefix and use the result as an index name.
   */
  constructor({
    networkLayer,
    graphQLcontext,
    typesPrefix,
    modelDefinitionsRegister
  } = {}) {
    this._modelDefinitionsRegister = modelDefinitionsRegister;

    if (!graphQLcontext) {
      throw new Error(
        `You must provide a GrapQLContext to ${this.constructor.name}::constructor()`
      );
    }

    let senderId = graphQLcontext.getUser()
      ? graphQLcontext.getUser().getId()
      : process.env.UUID;

    switch (env.get("INDEX_CONTROLLER_VERSION").asString()) {
      case "PUBLISHER":
      case "PUBLISHER_V7":
      case "V7":
        this._indexPublisher = new IndexControllerV7Publisher({
          networkLayer,
          typesPrefix,
          senderId
        });
        break;
      default:
        this._indexPublisher = new IndexControllerV7Client({
          networkLayer,
          typesPrefix,
          senderId
        });
    }
  }

  /**
   * @return {IndexControllerPublisherAbstract}
   */
  getIndexPublisher() {
    return this._indexPublisher;
  }

  static _isDSLQuery(qs) {
    return !!qs.match(
      /\+|-|=|&&|\|\|>|<\\!|\(|\)|{|}|\[|\\]|\^|"|\\~|\*|\?|:|\\|\/| OR | AND /
    );
  }

  /**
   * Get a node given an ID.
   *
   * @param id
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [lang]
   * @param {FragmentDefinition[]} [fragmentDefinitions] - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @param {LinkFilter[]} [linkFilters] - A list of link filters.
   * @param {PropertyFilter[]} [propertyFilters] - A list of property filters.
   * @param {QueryFilter[]} [queryFilters] - A  list of query filters
   * @param {ResolverArgs} [args={}]
   * @returns {*}
   */
  async getNode({
    id,
    modelDefinition,
    lang,
    fragmentDefinitions,
    linkFilters,
    propertyFilters,
    queryFilters
  }) {
    const {
      filter: boolQueryFilterFragments,
      must: boolQueryMustFragments,
      mustNot: boolQueryMustNotFragments,
      should: boolQueryShouldFragments
    } = this._convertFiltersIntoBoolQueryFragments({
      linkFilters,
      queryFilters,
      propertyFilters
    });

    boolQueryMustFragments.push({
      ids: { values: [id] }
    });

    const query = {
      bool: {
        ...(boolQueryMustFragments.length > 0
          ? { must: boolQueryMustFragments }
          : null),
        ...(boolQueryMustNotFragments.length > 0
          ? { must_not: boolQueryMustNotFragments }
          : null),
        ...(boolQueryShouldFragments.length > 0
          ? { should: boolQueryShouldFragments, minimum_should_match: 1 }
          : null),
        ...(boolQueryFilterFragments.length > 0
          ? { filter: boolQueryFilterFragments }
          : null)
      }
    };

    const queryPayload = this.buildQueryPayload({
      modelDefinition,
      query,
      lang,
      fragmentDefinitions,
      queryFilters
    })

    const result = await this._indexPublisher.query(queryPayload);

    let hit = result?.hits?.[0];

    if (hit) {
      return parseObjectFromEsHit({
        modelDefinition,
        hit,
        fragmentDefinitions
      });
    }
  }

  async getNodes({
    /**
     * Generic method to get a list of nodes
     *
     * @param {typeof ModelDefinitionAbstract} modelDefinition
     * @param {string} [qs] - A query string
     * @param {QueryString[]} [queries] - A list of QueryString
     * @param {number} [qsFuzziness] - Fuzzy level for returning approximating matches
     * @param {number} [limit=10]
     * @param {number} [offset]
     * @param {Sorting[]} [sortings] - A list of sorting
     * @param {string} [lang] - A lang code (fr/en...) used for sorting of localized labels.
     * @param {String[]} [idsFilters] - A list of ids
     * @param {LinkFilter[]} [linkFilters] - A list of link filters.
     * @param {PropertyFilter[]} [propertyFilters] - A list of property filters.
     * @param {QueryFilter[]} [queryFilters] - A  list of query filters
     * @param {LinkPath} [linkPaths] - A list  link paths filters
     * @param {FragmentDefinition[]} [fragmentDefinitions] - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
     * @param {boolean} [justCount=false] - Just count the number of hits without fetching any hit.
     * @param {boolean} [fetchIdsOnly=false] - Return hits fetching only ids.
     * @param {function} [buildQuery]
     * @param {boolean} [rawResult]
     * @param {object} [aggs] - ES Aggregations
     * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
     *
     * @param @deprecated {function} [getExtraQuery]
     * @param @deprecated {function} [getExtraQueryParams]
     * @param @deprecated {function} [getRootQueryWrapper] - Function used to wrap query into custom one. For example a function_score.
     *
     * @returns {Model|Model[]|Number|Collection}
     */
    modelDefinition,
    qs,
    queries,
    qsFuzziness,
    limit,
    offset,
    sortings,
    idsFilters,
    linkFilters,
    propertyFilters,
    queryFilters,
    linkPaths,
    justCount,
    fetchIdsOnly,
    fragmentDefinitions,
    getRootQueryWrapper,
    buildQuery,
    aggs,
    rawResult,
    asCollection,
    lang,
    getExtraQuery,
    getExtraQueryParams,
  } = {}) {
    if (linkPaths) {
      for (let linkPath of linkPaths) {
        let stepNodes = [];

        // Highly experimental. Don't use.
        if (linkPath.getIndexedShortcutLink()) {
          return this.getLinkedNodesFor({
            object: linkPath.getIndexedShortcutLink().getTargetId(),
            linkDefinition: linkPath
              .getIndexedShortcutLink()
              .getLinkDefinition(),
            justCount,
            fragmentDefinitions
          });
        // Highly experimental. Don't use.
        } else if (linkPath.getIndexedShortcutLinkFilter()) {
          linkFilters.push(linkPath.getIndexedShortcutLinkFilter());
        } else {
          const steps =  []
            .concat(linkPath.getSteps())
            .reverse();
          //
          // Update from 4.8.0.
          // Optimizations have been made on stepNodes recovering (aka: deep navigation across indices).
          // We use to get the whole documents that resulted in huge processing and poor performances.
          // We now request only document ids (without _source) that results in fast processings
          // The only drawback of this method is (maybe) a less effective use of ES requests where nested ids could have found in document linked properties.
          // Example :
          //   Before requesting a userAccount document made possible to get the related person id  (aka: userAccount.person: <personId>) and then request the person index with this id (fast ES request)
          //   Now, we only get userAccount.id, so getting related person must involved a term request (fast but maybe less than "id" request, maybe...)
          //
          for (let [index, step] of steps.entries()) {
            if (step instanceof LinkStep) {
              const previousStep = steps[index - 1];

              let stepModelDefinition = step
                .getLinkDefinition()
                .getRelatedModelDefinition();

              // Case 1.
              // This is first step, and a targetId is available from source node.
              if (index === 0 && step.getTargetId()) {
                if(step.isReversed() && !step.getLinkDefinition().getSymmetricLinkDefinition()){
                  logError(`To use first step of LinkPath with a reversed "${step.getLinkDefinition().getLinkName()}" link, you must declare a symmetric link`);
                }
                stepNodes = [ new Model(step.getTargetId(), step.getTargetId()) ];
              // Case 2.
              // This is an intermediate (or last) step BUT the previous one is a link step with a (in)existance filtering (aka "... = *" or "... != *")
              } else if (previousStep instanceof LinkStep && (previousStep.getLinkFilter()?.any || previousStep.getLinkFilter()?.isNeq)) {
                stepNodes = await this.getNodes({
                  modelDefinition: stepModelDefinition,
                  linkFilters: [previousStep.getLinkFilter()],
                  fetchIdsOnly: true
                });
              // Case 3.
              // This is an intermediate (or last) step BUT the previous one is a filter on a property.
              } else if (previousStep instanceof PropertyFilterStep) {
                stepNodes = await this.getNodes({
                  modelDefinition: stepModelDefinition,
                  propertyFilters: [previousStep.getPropertyFilter()],
                  fetchIdsOnly: true
                });
              // Case 4.
              // This is an intermediate (or last) step.
              // At this point, we should has a bunch of "stepNodes" ids to go deeper an the indices navigation.
              } else {
                // Case 4.1
                // If not, we consider that linPath filtering returns nothing and should stop here.
                if (stepNodes?.length === 0) {
                  return justCount ? 0 : [];
                // Case 4.2
                // Previous step refers to a reversed step. We must iterate on each step node, request linked nodes defined by linkDefinition, and gathered them.
                // The resulted nodes will be used as next step nodes.
                } else if (previousStep.isReversed()) {
                  let foundNodes = [];
                  for (let stepNode of stepNodes) {
                    foundNodes = foundNodes.concat(
                      await this.getLinkedNodesFor({
                        object: stepNode,
                        linkDefinition: previousStep.getLinkDefinition(),
                        fetchIdsOnly: true
                      })
                    );
                  }
                // Case 4.3
                // Previous step refers to a reversed link. We mus request nodes with fragment containing targets leading to next step
                } else if (!!previousStep.getLinkDefinition().getRdfReversedObjectProperty()) {
                  const sourceModelDefinition = previousStep.getLinkDefinition().getRelatedModelDefinition();
                  const symmetricLinkDefinition = previousStep.getLinkDefinition().getSymmetricLinkDefinition();
                  stepNodes = await this.getNodes({
                    modelDefinition: sourceModelDefinition,
                    idsFilters: stepNodes.map(node => node.id),
                    fragmentDefinitions: [new FragmentDefinition({
                      modelDefinition: sourceModelDefinition,
                      links: [symmetricLinkDefinition]
                    })]
                  });
                  stepNodes = stepNodes.flatMap(stepNodes => stepNodes[symmetricLinkDefinition.getPathInIndex()] || []);
                // Case 4.4
                // Previous step refers to a straight link. We must request linked nodes, defined by linkDefinition and filtered by step nodes gathered ids.
                // The resulted nodes will be used as next step nodes.
                } else {
                  stepNodes = await this.getNodes({
                    modelDefinition: stepModelDefinition,
                    linkFilters: [
                      {
                        linkDefinition: previousStep.getLinkDefinition(),
                        id: stepNodes.reduce((ids, stepNode) => ids.concat(Array.isArray(stepNode.id) ? stepNode.id : [stepNode.id ]), [])
                      }
                    ],
                    fetchIdsOnly: true
                  });
                }
              }

              // Case 5.
              // This is last step.
              if (index === steps.length - 1) {
                const lastStep = steps[steps.length - 1];
                if (!!lastStep.getLinkDefinition().getRdfReversedObjectProperty()) {
                  // Case 5.1
                  // Last step refers to a reversed link. We must request current step nodes with fragment containing targets to filter
                  const symmetricLinkDefinition = lastStep.getLinkDefinition().getSymmetricLinkDefinition();
                  stepNodes = await this.getNodes({
                    modelDefinition: stepModelDefinition,
                    idsFilters: stepNodes.map(node => node.id),
                    fragmentDefinitions: [new FragmentDefinition({
                      modelDefinition: stepModelDefinition,
                      links: [symmetricLinkDefinition]
                    })]
                  });
                  let linkPathIdsFilters = stepNodes.flatMap(stepNode => {
                    const target = stepNode[symmetricLinkDefinition.getPathInIndex()];
                    return target ? (Array.isArray(target) ? target.map(t => t.id) : target.id) : []
                  });
                  
                  idsFilters = (idsFilters || []).length > 0
                    ? idsFilters.filter(id => linkPathIdsFilters.includes(id))
                    : linkPathIdsFilters;
                } else {
                  // Case 5.2
                  // A new LinkFilter, defined by step link definition and last step nodes gathered ids, is added to existing ones (or not).
                  linkFilters = [].concat(linkFilters || [], [
                    new LinkFilter({
                      linkDefinition: step.getLinkDefinition(),
                      id: stepNodes.reduce((ids, stepNode) => ids.concat(Array.isArray(stepNode.id) ? stepNode.id : [stepNode.id ]), [])
                    })
                  ]);
                }
              }
            }
          }
        }
      }
    }

    let {
      filter: boolQueryFilterFragments,
      must: boolQueryMustFragments,
      mustNot: boolQueryMustNotFragments,
      should: boolQueryShouldFragments
    } = this._convertFiltersIntoBoolQueryFragments({
      idsFilters,
      linkFilters,
      queryFilters,
      propertyFilters
    });

    let sorts = [];

    // @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html for more details
    if (qs && qs !== "") {
      sorts.push("_score");

      const queryString = parseRawQsToPartialQueryString(qs);

      // Filter all searchable properties.
      let searchableProperties = modelDefinition.getSearchableProperties();

      // As inherited model definitions can be merged into same ES index,
      // to ensure that fulltext queries are consistents, we must gather all searchable properties of the hierarchy.
      // Previously fragment definitions were used to add inherited properties. This was not enough as "count" queries don't
      // provide those and leaded to different results than "nodes" queries.
      this._modelDefinitionsRegister
        .getInheritedModelDefinitionsFor(modelDefinition)
        .forEach(inheritedModelDefinition => {
          if (
            modelDefinition.getIndexType() ===
            inheritedModelDefinition.getIndexType()
          ) {
            inheritedModelDefinition.getProperties().forEach(property => {
              if (
                !property.isHerited() &&
                property.isSearchable() &&
                !searchableProperties.includes(property)
              ) {
                searchableProperties.push(property);
              }
            });
          }
        });

      // If fuzziness is undefined, we put a factor 0 by default
      // To tweak it globally setup FULLTEXT_FUZZYNESS env variable.
      const fuzziness = isNaN(qsFuzziness)
        ? env
          .get("FULLTEXT_FUZZYNESS")
          .default(0)
          .asInt()
        : qsFuzziness;

      for (const propertyDefinition of searchableProperties) {
        queryString.propertyDefinition = propertyDefinition;

        boolQueryShouldFragments.push(parseQueryStringToEsQuery({
          queryString,
          fuzziness
        }));
      }

      if(searchableProperties.length > 0 && ![QueryStringType.TERM, QueryStringType.REGEX].includes(queryString.type)){
        boolQueryShouldFragments.push({
          "simple_query_string": {
            "query": queryString.value,
            "fields": searchableProperties.map(property =>  property.getPathInIndex()),
            "default_operator": "and",
            boost: 50
          }
        });
      }
    }

    if(queries){
      for(let queryString of queries){
        boolQueryShouldFragments.push(parseQueryStringToEsQuery({
          queryString
        }))
      }
    }

    if ((modelDefinition.getIndexFilters()?.length || 0) > 0) {
      boolQueryFilterFragments = boolQueryFilterFragments.concat(
        modelDefinition.getIndexFilters()
      );
    }

    if ((sortings || []).length > 0) {
      for (let sorting of sortings) {
        if (sorting.getPropertyDefinition()) {
          let path = sorting.getPropertyDefinition().getPathInIndex();

          // LabelDefinition fields have this king of mapping :
          // {
          //   [analyzedFieldName]: {
          //     "type": "nested",
          //      "properties": {
          //        "lang": {
          //          "type": "keyword",
          //        },
          //        "value": {
          //          "type": "text",
          //          "fields": {
          //            "keyword": {
          //              "type": "keyword",
          //              "normalizer": "lowercase_no_accent"
          //            },
          //            "keyword_not_normalized": {
          //              "type": "keyword",
          //            }
          //          }
          //        }
          //      }
          //   },
          // }
          if(env.get("ES_NESTED_LOCALIZED_LABEL").asBool() && sorting.getPropertyDefinition() instanceof LabelDefinition){
            sorts.unshift({
              [`${path}.value.keyword`]: {
                order : sorting.isDescending() ? "desc" : "asc",
                ...(lang ? {
                  nested: {
                    path: path,
                    filter: {
                      "term" : { [`${path}.lang`] : lang }
                    }
                  }
                }: null)
              }
            });
          } else {
            // Since searchable properties are analyzed fields, sorting by them will make ES throw this error :
            // "Fielddata is disabled on text fields by default. Set fielddata=true on [***] in order to load fielddata in memory by uninverting the inverted index. Note that this can however use significant memory. Alternatively use a keyword field instead."
            // By default, we follow the ES convention for analyzed fields dynamic mappings that is to say :
            // {
            //   [analyzedFieldName]: {
            //     type: "text",
            //     fields: {
            //       keyword: {
            //         type: "keyword"
            //       },
            //       keyword_not_normalized: {
            //         type: "keyword",
            //       }
            //     },
            //   },
            // }
            if (sorting.getPropertyDefinition().isSearchable()) {
              if(sorting.getPropertyDefinition().getLinkPath()?.getLastStep() instanceof ConcatStep){
                path += `.keyword_not_normalized`;
              }else {
                path += `.keyword`;
              }
            }

            sorts.unshift({
              [path]: sorting.isDescending() ? "desc" : "asc"
            });
          }
        }
        if (sorting.getSortingDefinition()) {
          sorts.unshift(
            sorting.getSortingDefinition().generateIndexSorting({
              direction: sorting.isDescending() ? "desc" : "asc",
              ...sorting.getParams()
            })
          );
        }
      }
    }

    if (justCount === true) {
      limit = 0;
    }

    // @see https://www.elastic.co/guide/en/elasticsearch/reference/7.15/search-fields.html#script-fields
    let scriptFields = this._extractScriptFields({ queryFilters });

    // @see https://www.elastic.co/guide/en/elasticsearch/reference/7.15/runtime.html
    let runtimeFields = [];

    let query = {
      bool: {
        ...(boolQueryMustFragments.length > 0
          ? { must: boolQueryMustFragments }
          : null),
        ...(boolQueryMustNotFragments.length > 0
          ? { must_not: boolQueryMustNotFragments }
          : null),
        ...(boolQueryShouldFragments.length > 0
          ? { should: boolQueryShouldFragments, minimum_should_match: 1 }
          : null),
        ...(boolQueryFilterFragments.length > 0
          ? { filter: boolQueryFilterFragments }
          : null)
      }
    };

    // @deprecated
    if (getRootQueryWrapper ) {
      query = getRootQueryWrapper({ query });
    }

    if(!buildQuery){
      buildQuery = (query) => {
        return {
          ...query,
          ...getExtraQuery?.()
        }
      }
    }

    const queryPayload = this.buildQueryPayload({
      modelDefinition,
      fragmentDefinitions,
      lang,
      query: buildQuery(query),
      aggs,
      excludeAllFields: justCount || fetchIdsOnly,
      limit: limit ?? 1000,
      offset,
      sorts,
      queryFilters,
      ...getExtraQueryParams?.()
    });

    let result = await this._indexPublisher.query(queryPayload);

    if (rawResult) {
      return result;
    } else if (justCount === true) {
      return result?.total;
    } else {
      let objects = result?.hits.map(hit =>
        parseObjectFromEsHit({
          modelDefinition,
          hit,
          fragmentDefinitions
        })
      );

      if (idsFilters?.length > 0 && sorts.length === 0) {
        objects.sort((objectA, objectB) =>
          idsFilters.indexOf(objectA.id) > idsFilters.indexOf(objectB.id)
            ? 1
            : -1
        );
      }

      if(asCollection){
        return new Collection({
          objects,
          totalCount: result?.total,
          aggregations: Object.entries(result?.aggregations || {}).map(([name, {buckets, nestedAggs}]) => (
            new Aggregation({
              name,
              buckets: (nestedAggs?.termsAggs?.buckets || buckets || []).map(({key_as_string, key, doc_count}) => {
                return new AggregationBucket({key: key_as_string || key, count : doc_count})
              })
            }))
          )
        })
      } else {
        return objects;
      }
    }
  }

  /**
   * Get documents linked to another document
   *
   * There are two different was to request a linked node.
   *
   * - Case 1. If the link is single AND source object has an indexed reference to the target URI, then perform a basic request on target object index filtered by the id.
   * - Case 2. If the link is plural OR is is single but source object doesn't have an indexed reference to the target URI AND target index keep references to source object index, then then perform a basic request on target object index filtered by symmetric link definition.
   *
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {FragmentDefinition[]} [fragmentDefinitions] - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @param {LinkFilter[]} [linkFilters] - A list of link filters.
   * @param {PropertyFilter[]} [propertyFilters] - A list of property filters.
   * @param {QueryFilter[]} [queryFilters] - A  list of query filters
   * @param {LinkPath[]} [linkPaths] - A list  link paths filters
   * @param {boolean} [justCount]
   * @param {string} [qs] - A query string
   * @param {QueryString[]} [queries] - A list of QueryStrings
   * @param {number} [qsFuzziness] - Fuzzy level for returning approximating matches
   * @param {Sorting[]} [sortings] - A list of sorting
   * @param {string} [lang] - A lang code (fr/en...) used for sorting of localized labels.
   * @param {number} [limit]
   * @param {number} [offset]
   * @param {boolean} [fetchIdsOnly] - Optimise ES request by excluding all properties execpt id
   * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
   * @return {Model[]|Model|Collection|Number}
   */
  async getLinkedNodesFor({
    object,
    linkDefinition,
    justCount,
    fragmentDefinitions,
    qs,
    queries,
    lang,
    qsFuzziness,
    limit,
    offset,
    sortings,
    linkFilters = [],
    propertyFilters = [],
    queryFilters = [],
    linkPaths = [],
    fetchIdsOnly,
    asCollection
  }) {
    if (!object) {
      return linkDefinition.isPlural() ? [] : null;
    }
    let relatedModelDefinition = linkDefinition.getRelatedModelDefinition();
    let nodes;

    // Case 1.
    // If symmetric link is indexed that is to say :
    //   - symmetricLinkName property is defined.
    //   - symmetricLinkDefinition defined a rdfObjectProperty (and not ar rdfReversedObjectProperty that won't be indexed **for now**).
    if (linkDefinition.getSymmetricLinkDefinition()?.getRdfObjectProperty() || linkDefinition.getSymmetricLinkDefinition()?.getLinkPath()) {
      nodes = await this.getNodes({
        modelDefinition: relatedModelDefinition,
        fragmentDefinitions,
        linkFilters: [
          {
            id: object.id,
            linkDefinition: linkDefinition
              .getRelatedModelDefinition()
              .getLink(linkDefinition.getSymmetricLinkName())
          },
          ...linkFilters
        ],
        propertyFilters,
        queryFilters,
        linkPaths,
        justCount,
        qs,
        queries,
        qsFuzziness,
        lang,
        limit,
        offset,
        sortings,
        fetchIdsOnly,
        asCollection
      });
    } else if (object[linkDefinition.getPathInIndex()]) {
      const ids = this._extractIdsFromNestedNodes({
        nodes: [object],
        path: linkDefinition.getPathInIndex()
      });

      const existingFragmentDefinitions = (fragmentDefinitions || []).filter(fragmentDefinition => fragmentDefinition.getModelDefinition().isEqualOrDescendantOf(relatedModelDefinition));

      // Optimization possible here if fragmentDefinition exists on modelDefinition but
      //  - only id is requested
      //  - related model definition is not extensible (because we need "types" field to infer its type)
      // Then just mimic an ES search.
      const optimizeRequest = !relatedModelDefinition.isExtensible() && [].concat(propertyFilters, queryFilters, linkPaths, linkFilters).length === 0 && (fetchIdsOnly || existingFragmentDefinitions.length > 0 && !existingFragmentDefinitions.some(fragmentDefinition => !fragmentDefinition.getIdOnly()))


      if(optimizeRequest){
        nodes = ids.map(id => parseObjectFromEsHit({modelDefinition: relatedModelDefinition, fragmentDefinitions, hit: {_id: id}}))
      } else {
        nodes = await this.getNodes({
          modelDefinition: relatedModelDefinition,
          fragmentDefinitions,
          idsFilters: ids,
          linkFilters,
          propertyFilters,
          queryFilters,
          linkPaths,
          qs,
          queries,
          qsFuzziness,
          lang,
          justCount,
          limit,
          offset,
          sortings,
          fetchIdsOnly,
          asCollection
        });

      }
    }
    // Case 2.
    else {
      nodes = justCount ? 0 : asCollection ? new Collection() : [];
      // throw new Error(`Neither LinkDefinition ${object.type}Definition.getLink("${linkDefinition.getLinkName()}").getSymmetricLinkName() is defined nor model property ${object.type}.${linkDefinition.getPathInIndex()} is found among ${JSON.stringify(object)}`);
    }

    if (nodes != null) {
      if (justCount || linkDefinition.isPlural()) {
        return nodes;
      } else if(nodes instanceof Collection){
        return nodes.getAt(0);
      } else {
        return nodes[0];
      }
    }
  }

  /**
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [percoField=percoLabel] - Name on virtual field where to insert "text" to percolate.
   * @param {string} text - Text to percolate
   * @param {boolean} [justCount] - Return count only
   * @param {number} limit
   * @param {number} offset
   * @return {Model[]}
   */
  async percolateNodes({
    modelDefinition,
    text,
    percoField,
    justCount,
    limit,
    offset
  }) {
    let result = await this._indexPublisher.percolate({
      types: modelDefinition.getIndexType(),
      text,
      percoField,
      size: justCount ? 0 : limit != null ? limit : 10,
      ...(offset != null ? { from: offset } : null)
    });

    if (justCount) {
      return result?.hits?.total || 0;
    }

    return (result?.hits?.hits || []).map(hit =>
      parseObjectFromEsHit({
        modelDefinition,
        hit
      })
    );
  }

  /**
   * @param {Model[]} nodes
   * @param {string} [path]
   * @private
   */
  _extractIdsFromNestedNodes({ nodes, path }) {
    let ids = [];
    nodes.map(stepNode => {
      if (Array.isArray(stepNode[path])) {
        ids = ids.concat(stepNode[path].map(({ id }) => id));
      } else {
        ids.push(stepNode[path].id);
      }
    });
    return ids;
  }

  /**
   * @param {String[]} [idsFilters] - A list of ids
   * @param {LinkFilter[]} [linkFilters] - A list of link filters.
   * @param {PropertyFilter[]} [propertyFilters] - A list of property filters.
   * @param {QueryFilter[]} [queryFilters] - A  list of query filters
   * idsFilters, linkFilters, propertyFilters, queryFilters
   */
  _convertFiltersIntoBoolQueryFragments({
    idsFilters,
    linkFilters,
    propertyFilters,
    queryFilters
  }) {
    let boolQueryFilterFragments = [];
    let boolQueryShouldFragments = [];
    let boolQueryMustNotFragments = [];
    let boolQueryMustFragments = [];

    if (idsFilters) {
      boolQueryFilterFragments.push({
        ids: { values: idsFilters }
      });
    }

    if (linkFilters) {
      for (let linkFilter of linkFilters) {
        if (linkFilter.any === true) {
          if (linkFilter.isNeq) {
            boolQueryFilterFragments.push({
              bool: {
                must_not: {
                  exists: {
                    field: linkFilter.linkDefinition.getPathInIndex()
                  }
                }
              }
            });
          } else {
            boolQueryFilterFragments.push({
              exists: {
                field: linkFilter.linkDefinition.getPathInIndex()
              }
            });
          }
        } else {
          boolQueryFilterFragments.push({
            terms: {
              [linkFilter.linkDefinition.getPathInIndex()]: Array.isArray(
                linkFilter.id
              )
                ? linkFilter.id
                : [linkFilter.id]
            }
          });
        }
      }
    }

    if (propertyFilters) {
      for (let {
        propertyDefinition,
        value,
        any,
        isNeq,
        isLte,
        isLt,
        isGte,
        isGt,
        isStrict
      } of propertyFilters) {
        if (any === true) {
          const existsFilter = {
            exists: {
              field: propertyDefinition.getPathInIndex()
            }
          };

          if (isNeq) {
            boolQueryFilterFragments.push({
              bool: {
                must_not: existsFilter
              }
            });
          } else {
            boolQueryFilterFragments.push(existsFilter);
          }
        } else if (isLte || isLt || isGte || isGt) {
          let operator;

          if (isLte) {
            operator = "lte";
          } else if (isLt) {
            operator = "lt";
          } else if (isGte) {
            operator = "gte";
          } else {
            operator = "gt";
          }

          boolQueryFilterFragments.push({
            range: {
              [propertyDefinition.getPathInIndex()]: {
                [operator]: value
              }
            }
          });
        } else {
          let property = propertyDefinition.getPathInIndex();
          let termFilter;

          if(env.get("ES_NESTED_LOCALIZED_LABEL").asBool() && propertyDefinition instanceof LabelDefinition){
            termFilter = {
              nested: {
                path: property,
                query: {
                  term: {
                    [`${property}.value.${isStrict ? "keyword_not_normalized" : "keyword"}`]: value
                  }
                }
              }
            };
          } else {
            if (propertyDefinition.isSearchable()) {
              if(isStrict ||  propertyDefinition.getLinkPath()?.getLastStep() instanceof ConcatStep){
                property += `.keyword_not_normalized`;
              }else {
                property += `.keyword`;
              }
            }

            termFilter = {
              [Array.isArray(value) ? "terms": "term"]: {
                [property]: value
              }
            };
          }

          if (isNeq === true) {
            boolQueryFilterFragments.push({
              bool: {
                must_not: termFilter
              }
            });
          } else {
            boolQueryFilterFragments.push(termFilter);
          }
        }
      }
    }

    if (queryFilters) {
      for (let {
        filterGenerateParams,
        filterDefinition,
        isStrict,
        isNeq
      } of queryFilters) {
        if (isNeq) {
          boolQueryMustNotFragments.push(
            filterDefinition.generateIndexFilter(filterGenerateParams)
          );
        } else if (isStrict) {
          boolQueryFilterFragments.push(
            filterDefinition.generateIndexFilter(filterGenerateParams)
          );
        } else {
          boolQueryShouldFragments.push(
            filterDefinition.generateIndexFilter(filterGenerateParams)
          );
        }
      }
    }

    return {
      should: boolQueryShouldFragments,
      must: boolQueryMustFragments,
      mustNot: boolQueryMustNotFragments,
      filter: boolQueryFilterFragments
    };
  }

  /**
   * Tweak the ES query to include/exclude fields.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {FragmentDefinition[]} fragmentDefinitions - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @param {boolean} [excludeAllFields]
   * @private
   */
  _convertFragmentDefinitionsToFieldsRestriction({
    modelDefinition,
    fragmentDefinitions = [],
    excludeAllFields
  }) {
    if(excludeAllFields){
      return { excludes: "*" };
    }

    if(fragmentDefinitions.length > 0){
      return {
        includes: fragmentDefinitions.reduce((includes, fragmentDefinition) => includes.concat(fragmentDefinition.getIndexedKeys()), ["types"])
      }
    } else {
      return { includes: "*" };
    }
  }

  /**
   * @param {QueryFilter[]} [queryFilters] - A  list of query filters
   * @return {object}
   */
  _extractScriptFields({ queryFilters = [] } = {}) {
    if (queryFilters.length > 0) {
      let scriptFields = {};

      for (let {
        filterGenerateParams,
        filterDefinition,
      } of queryFilters) {
        Object.assign(
          scriptFields,
          filterDefinition.generateIndexScriptFields(filterGenerateParams)
        );
      }

      return scriptFields;
    }
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {FragmentDefinition[]} [fragmentDefinitions] - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @private
   */
  _extractRuntimeFields({modelDefinition, fragmentDefinitions}){
    let propertyDefinitions;

    if(fragmentDefinitions.length > 0){
      propertyDefinitions = fragmentDefinitions.reduce((propertyDefinitions, fragmentDefinition) => (
         propertyDefinitions.concat(fragmentDefinition.getProperties())
      ), [])
    } else {
      propertyDefinitions = modelDefinition.getProperties();
    }

    return propertyDefinitions.reduce((runtimeFields, properyDefinition) => {
      const linkPathStep = properyDefinition.getLinkPath()?.getLastStep();

      if(linkPathStep instanceof ConcatStep){
        if(!runtimeFields){
          runtimeFields = {};
        }
        const fieldName = properyDefinition.getPathInIndex();
        const separator  = linkPathStep.getSeparator().replace("'", "\\'");
        // See explainations of runtime fields in buildQueryPayload function
        runtimeFields[properyDefinition.isSearchable() ? `${fieldName}.keyword_not_normalized` : fieldName] = {
          type: "keyword",
          script: `if(params['_source'].containsKey('${fieldName}')) { def field = params['_source']['${fieldName}']; if(field instanceof List){ emit(field.stream().distinct().collect(Collectors.toList()).join('${separator}')); } else { emit(field); }}`
        }
      }

      return runtimeFields;
    }, null);
  }

  /**
   * @param {FragmentDefinition[]} [fragmentDefinitions] - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @param {string} [lang] - Lang
   * @private
   */
  _extractAggregations({fragmentDefinitions, lang}){
    if(fragmentDefinitions.length > 0){
      return fragmentDefinitions.reduce((aggregations, fragmentDefinition) => {
        for (const propertyDefinition of fragmentDefinition.getProperties()) {
          if(propertyDefinition.isAggregable()){
            if(!aggregations){
              aggregations = {};
            }

            const path = propertyDefinition.getPathInIndex();
            const aggType   =  AGGREGATION_RDFTYPE_MAPPING[propertyDefinition.getRdfDataType()] || "terms";

            if(env.get("ES_NESTED_LOCALIZED_LABEL").asBool() && propertyDefinition instanceof LabelDefinition){
              aggregations[propertyDefinition.getPropertyName()] = {
                nested: {
                  path
                },
                aggs: {
                  nestedAggs: {
                    filter: lang ? {
                      bool: {
                        filter: [
                          {
                            term: {
                              [`${path}.lang`]: lang
                            }
                          }
                        ]
                      }
                    } : {match_all: {}},
                    aggs: {
                      termsAggs: {
                        [aggType]: {
                          field: `${path}.value.keyword_not_normalized`,
                          size: 1000
                        },
                      }
                    }
                  }
                }
              };
            } else {
              const fieldName = propertyDefinition.isSearchable() ? `${path}.keyword_not_normalized`: path;

              aggregations[propertyDefinition.getPropertyName()] = {
                [aggType]: {
                  field: fieldName
                }
              };

              switch (aggType){
                case "date_histogram":
                  aggregations[propertyDefinition.getPropertyName()][aggType].calendar_interval = "month";
                  break;
                case "histogram":
                  aggregations[propertyDefinition.getPropertyName()][aggType].interval = "10";
                  break;
                case "terms":
                  aggregations[propertyDefinition.getPropertyName()][aggType].size = 1000;
              }
            }
          }
        }

        return aggregations;
      }, null)
    }
  }


  /**
   * Build Elastic query
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object}  query - ES Query
   * @param {FragmentDefinition[]} [fragmentDefinitions] - Force a list of requestable properties instead of adding the entire list of `modelDefinition` properties
   * @param {object} [aggs] - ES Aggregations
   * @param {string} [lang]
   * @param {boolean} excludeAllFields
   * @param limit
   * @param offset
   * @param sorts
   * @param {QueryFilter[]} [queryFilters] - A  list of query filters
   * @returns {{types, source: (*&{size: (*|number), runtime_mappings: *, query, script_fields: *, _source: ({excludes: string}|{includes: unknown}|{includes: string}), from?: *, sort?: *, track_total_hits: boolean})}}
   */
  buildQueryPayload({modelDefinition, query, lang, fragmentDefinitions, excludeAllFields, limit, offset, sorts, queryFilters, aggs}){
    const filteredFragmentDefinitions = (fragmentDefinitions || []).filter(fragmentDefinition =>
      fragmentDefinition.getModelDefinition().isEqualOrDescendantOf(modelDefinition) ||
      modelDefinition.isEqualOrDescendantOf(fragmentDefinition.getModelDefinition())
    );

    const scriptFields = this._extractScriptFields({ queryFilters });
    const runtimeFields = this._extractRuntimeFields({
      modelDefinition,
      fragmentDefinitions: filteredFragmentDefinitions
    });
    const aggregations = this._extractAggregations({
      fragmentDefinitions: filteredFragmentDefinitions,
      lang
    });

    let source = {
      track_total_hits: true, // See if there is a huge hole in speed with this param always truthy.
      query: query,
      _source: this._convertFragmentDefinitionsToFieldsRestriction({
        modelDefinition,
        fragmentDefinitions: filteredFragmentDefinitions,
        excludeAllFields
      })
    }

    if(aggs){
      source.aggs = aggs;
    }

    if(limit != null){
      source.size = limit;
    }

    if(offset != null){
      source.from = offset;
    }

    if(scriptFields) {
      source.script_fields = scriptFields;
    }

    // @see https://www.elastic.co/guide/en/elasticsearch/reference/master/runtime-override-values.html
    // The goal of running fields is to add dynamic computation on particular fields such one defined by LinkPath.concat().
    // For example:
    // Each person as a "fullName" field which is the concatenation of "firstName", "lastName".
    // In an ES document, this results in a multivalued field. Aka fullName : ["Mylène", "Leitzelman"]
    // With a running field, we can override this value by computing a value like ["Mylène", "Leitzelman"].join(" ")
    // and get in a response a document that looks like :
    // {
    //   "_id": "...",
    //   "_score": X.X,
    //   "_source": {
    //      "fullName": ["Mylène", "Leitzelman"]
    //    },
    //    "fields": {
    //      "fullName": ["Mylène Leitzelman"]
    //    }
    // }
    //
    //
    if(runtimeFields){
      source.runtime_mappings = runtimeFields;
      source.fields = Object.keys(runtimeFields);
    }

    if(sorts?.length > 0){
      source.sort = sorts;
    }

    if(aggregations){
      source.aggs = {
        ...(source.aggs || {}),
        ...aggregations
      };
    }

    return {
      types: modelDefinition.getIndexType(),
      source
    }
  }
}

/**
 * Type syntax converter from RDF to GraphQL
 *
 */
export const AGGREGATION_RDFTYPE_MAPPING = {
  // default : "terms"
  'http://www.w3.org/2001/XMLSchema#int': 'histogram',
  'http://www.w3.org/2001/XMLSchema#integer': 'histogram',
  'http://www.w3.org/2001/XMLSchema#float': 'histogram',
  'http://www.w3.org/2001/XMLSchema#date': 'date_histogram',
  'http://www.w3.org/2001/XMLSchema#dateTime': 'date_histogram',
  'http://www.w3.org/2001/XMLSchema#dateTimeStamp': 'date_histogram',
  'http://www.w3.org/2001/XMLSchema#gMonth': 'date_histogram',
  'http://www.opengis.net/ont/geosparql#wktLiteral': 'geotile_grid',
  'http://www.opengis.net/ont/geosparql#gmlLiteral': 'geotile_grid'
};