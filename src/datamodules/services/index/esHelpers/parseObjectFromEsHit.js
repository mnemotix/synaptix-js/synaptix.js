/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import omit from "lodash/omit";
import LiteralDefinition from "../../../../datamodel/toolkit/definitions/LiteralDefinition";
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";

/**
 * @param {object} hit - The ElasticSearch response hit.
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {FragmentDefinition[]} [fragmentDefinitions]
 * @return {Model}
 */
export function parseObjectFromEsHit({hit, modelDefinition, fragmentDefinitions} = {}) {
  const ModelClass = modelDefinition.getModelClass();
  const source = hit["_source"] || {};
  const scriptFields = hit["fields"];
  const id = hit._id || hit.id || source?.id;
  let props = [];

  if(scriptFields){
    Object.entries(scriptFields).map(([field, value]) => {
      source[field.replace(/\.keyword.*$/, "")] = value;
    })
  }

  const existingFragmentDefinitions = (fragmentDefinitions || []).filter(fragmentDefinition =>
    fragmentDefinition.getModelDefinition().isEqualOrDescendantOf(modelDefinition) ||
    modelDefinition.isEqualOrDescendantOf(fragmentDefinition.getModelDefinition())
  );

  if(existingFragmentDefinitions.length > 0){
    existingFragmentDefinitions.map(existingFragmentDefinition => {
      props = existingFragmentDefinition.getProperties().reduce((props, property) => {
        const field = property.getPathInIndex();

        if(source[field]){
          props[property.getPropertyName()] = parseProperty({property, value: source[field], field, source, fragmentDefinitions});
        }

        return props;
      }, props);
    });

    existingFragmentDefinitions.map(existingFragmentDefinition => {
      props = existingFragmentDefinition.getLinks().reduce((props, link) => {
        if (source[link.getPathInIndex()]) {
          props[link.getPathInIndex()] = parseLink({link, value: source[link.getPathInIndex()], fragmentDefinitions});
        }
        return props;
      }, props);
    });
  } else {
    props = Object.entries(omit(source, ["id", "types", "entityId"])).reduce((acc, [field, value]) => {
      if(field.includes("_locales")){
        return acc;
      }

      // 1. Check if the field is a link.
      let link = modelDefinition.getLinkFromIndexedField(field);

      // If value is a link then we assume that this is a URI (or list of URI) of the object in link with.
      // The goal is to create a Model instance with this URI (or list of Model instances)
      if (link) {
        acc[link.getPathInIndex()] = parseLink({link, value, fragmentDefinitions});
        return acc;
      }

      // 2. Check if the field is a data property (literal or label).
      let property = modelDefinition.getPropertyFromIndexedField(field); // TODO check this : || modelDefinition.getPropertyFromIndexedField(field.replace('.', '_'));

      if (property && property.getPropertyName()) {
        acc[property.getPropertyName()] = parseProperty({property, value, field, source, fragmentDefinitions});
        return acc;
      }

      // 3. Fallback on fragments
      if(fragmentDefinitions?.length > 0){
        for(const fragmentDefinition of fragmentDefinitions){
          // 3.1 First links
          let fragmentedLink = fragmentDefinition.getModelDefinition().getLinkFromIndexedField(field);

          if(fragmentedLink){
            acc[fragmentedLink.getPathInIndex()] = parseLink({link: fragmentedLink, value, fragmentDefinitions});
            return acc;
          }

          let fragmentedProperty = fragmentDefinition.getModelDefinition().getPropertyFromIndexedField(field);

          if(fragmentedProperty && fragmentedProperty.getPropertyName()){
            acc[fragmentedProperty.getPropertyName()] = parseProperty({property: fragmentedProperty, value, field, source, fragmentDefinitions});
            return acc;
          }
        }
      }

      return acc;
    }, {});
  }

  if (source?.types){
    props.types = source.types ;
  }

  return new ModelClass(id, hit.uri || id, props, modelDefinition.getNodeType());
}


/**
 * @param {LinkDefinition} link
 * @param {*} value
 * @param {FragmentDefinition[]} [fragmentDefinitions]
 * @return {*}
 */
function parseLink({link, value, fragmentDefinitions}){
  if (link.isPlural()) {
    let values;

    if (!Array.isArray(value)) {
      values = [value];
    } else {
      values = value;
    }

    return values.map(value => parseObjectFromEsHit({
      modelDefinition: link.getRelatedModelDefinition(),
      hit: {
        _id: link.isNested() ? value.id : value,
        _source: link.isNested() ? value : {}
      },
      fragmentDefinitions
    }));
  } else {
    if (Array.isArray(value)) {
      value = value[0];
    }

    return parseObjectFromEsHit({
      modelDefinition: link.getRelatedModelDefinition(),
      hit: {
        _id: link.isNested() ? value.id : value,
        _source: link.isNested() ? value : {}
      },
      fragmentDefinitions
    });
  }
}

/**
 * @param {PropertyDefinitionAbstract} property
 * @param {string} field
 * @param {*} value
 * @param {object} source
 * @return {*}
 */
function parseProperty({property, field, value, source}){
  // If value is a literal. Normalize value ensuring that :
  //   - Flagged plural values returns an array.
  //   - Flagged non plural values returns a literal.
  if (property instanceof LiteralDefinition) {
    if (property.isPlural()) {
      if (!value) {
        value = [];
      }

      if (!Array.isArray(value)) {
        value = [value];
      }
    } else if (Array.isArray(value)) {
      value = value[0];
    }
  }

    // If value is a localized label. Normalize value ensuring that the resulting format is an array like:
    // [
    //   {
    //      "lang": "fr",
    //      "value": "..."
    //   }
    //   {
    //      "lang": "en",
    //      "value": "..."
    //   }
  // ]
  else if (property instanceof LabelDefinition) {
    // This is the case where value is a simple string. Fallback to a default locale.
    if (typeof value === "string") {
      let lang = source[`${field}_locales`] || "fr";

      if (Array.isArray(lang)){
        lang = lang[0];
      }

      value = [{
        lang,
        value
      }];
    } else if (Array.isArray(value)) {
      let locales = source[`${field}_locales`];

      if(locales && !Array.isArray(locales)){
        locales = [locales];
      }

      // This is the case where index is flattened. Label values and locale are stored in two different properties.
      if (locales && locales.length === value.length) {
        let normalizedValue = [];
        for (let [index, locale] of locales.entries()) {
          normalizedValue.push({
            lang: locale,
            value: value[index]
          })
        }
        value = normalizedValue;
        // Fallback getting first value to a default locale.
      } else {
        let values = value;
        value = values.map(value => {
          if (typeof value === "string") {
            return {
              lang: "fr",
              value
            };
          } else {
            return value;
          }
        });
      }
    } else if(typeof value === "object"){
      value = [{
        value: value.value || "",
        lang : value.lang || "fr"
      }];
    }
  }

  return value;
}