/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import env from "env-var";
import {LabelDefinition} from "../../../../datamodel/toolkit/definitions";
import {QueryStringType} from "../../../../datamodel/toolkit/utils/QueryString";

/**
 * @param {QueryString} queryString
 * @param {number} fuzziness
 */
export function parseQueryStringToEsQuery({queryString, fuzziness: defaultFuzzyness}) {
  const {propertyDefinition, value, type, fuzziness} = queryString;
  let matchOptions = {};

  let path = propertyDefinition.getPathInIndex();

  if ([QueryStringType.MATCH].includes(type)) {
    matchOptions.fuzziness = fuzziness || defaultFuzzyness;
  }

  if ([QueryStringType.MATCH_PREFIX, QueryStringType.MATCH].includes(type) && propertyDefinition.getSearchBoost()) {
    matchOptions.boost = propertyDefinition.getSearchBoost();
  }

  if (env.get("ES_NESTED_LOCALIZED_LABEL").asBool() && propertyDefinition instanceof LabelDefinition) {
    path += ".value";
  }

  let queryPayload = {};
  let queryType;

  switch (type){
    case QueryStringType.REGEX:
      queryType = "regexp";
      queryPayload = {
        value: value || "",
        flags: "ALL",
        ...matchOptions
      };
      break;
    case QueryStringType.MATCH_PREFIX:
      queryType = "match_bool_prefix";
      queryPayload = {
        query: value || "",
        ...matchOptions
      };
      break;
    default:
      queryType = "match" ;
      queryPayload = {
        query: value || "",
        ...matchOptions
      };
  }
  // If path is nested, apply a nested query.
  // @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-nested-query.html
  if (path.includes(".")) {
    const nestedPath = path.slice(0, path.lastIndexOf("."));
    return {
      nested: {
        path: nestedPath,
        query: {
          [queryType]: {
            [path]: queryPayload
          }
        }
      }
    };
  } else {
    return {
      [queryType]: {
        [path]: queryPayload
      }
    }
  }
}