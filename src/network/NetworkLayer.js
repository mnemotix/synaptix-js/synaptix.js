/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

export default class NetworkLayer {
  constructor() {
    if (new.target === NetworkLayer) {
      throw new TypeError("Cannot construct Abstract instances directly");
    }

    if (this.connect === NetworkLayer.prototype.connect) {
      throw new TypeError(`You must implement ${this.constructor.name}::connect()`);
    }

    if (this.request === NetworkLayer.prototype.request) {
      throw new TypeError(`You must implement ${this.constructor.name}::request()`);
    }

    if (this.listen === NetworkLayer.prototype.listen) {
      throw new TypeError(`You must implement ${this.constructor.name}::listen()`);
    }
  }

  /* istanbul ignore next */

  /**
   * Abstract method to connect the network layer.
   * @param {*} [extras] - Extra parameters
   */
  async connect(...extras) {
  }

  /* istanbul ignore next */

  /**
   * Abstract method to send a request message and wait a response.
   *
   * @async
   * @param {string} command - Request command
   * @param {object} payload - Request message payload
   * @param {*} [extras] - Extra parameters
   * @return {string|object}
   */
  async request(command, payload, ...extras) {
  }

  /* istanbul ignore next */

  /**
   * Asbract method to listen on broadcasted messages filtered by a routingKey.
   *
   * @param {string} routingKey - Filter message with a routingKey. Default to #, don't filter.
   * @param {function|function[]} callbacks - A callback function (or an array).
   * @param {*} [extras] - Extra parameters
   */
  listen(routingKey, callbacks, ...extras) {
  }
}