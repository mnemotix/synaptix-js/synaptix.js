/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import NetworkLayer from '../NetworkLayer';

class NetworkLayerOkTest extends NetworkLayer {
  async connect() {
  }

  async createCallbackQueue(callbackQueueName, callbackFunction, routingKey = '#') {
  }

  async publish(command, payload) {
  }

  async request(command, payload) {
  }

  listen(routingKey, callbacks) {
  }
}

describe('NetworkLayerAbstract', () => {

  it('throws an exception if instantied as abstract', () => {
    expect(() => {
      let networkLayer = new NetworkLayer();
      networkLayer.connect();
    }).toThrow(TypeError);

  });

  it('throws an exception if extended with missing abstract method definitions', () => {
    expect(() => {
      class NetworkLayerNokTest extends NetworkLayer {
        async request(command, payload) {
        }

        listen(routingKey, callbacks) {
        }
      }

      new NetworkLayerNokTest();
    }).toThrow(TypeError);

    expect(() => {
      class NetworkLayerNokTest extends NetworkLayer {
        async connect() {
        }

        listen(routingKey, callbacks) {
        }
      }

      new NetworkLayerNokTest();
    }).toThrow(TypeError);

    expect(() => {
      class NetworkLayerNokTest extends NetworkLayer {
        async connect() {
        }

        async request(command, payload) {
        }
      }

      new NetworkLayerNokTest();
    }).toThrow(TypeError);
  });

  it('should instantiate a well formed extended class.', () => {
    new NetworkLayerOkTest();
  });
});




