# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.17.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.16.2...4.17.0)

### Commits

[`9147117`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/91471173490a3b2b9f848b625d12ef546d8054ca) Added named graphs mapping in NAMED_GRAPHS_MAPPING variable.

## [4.16.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.16.1...4.16.2) - 2023-03-13

### Commits

[`0cbaf34`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0cbaf34c98f40d0821efa6f77b8ac948b309ed98) Fixed bug on parseObjectFromEsHit.js when flattenized locales fields is not an array

## [4.16.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.16.0...4.16.1) - 2023-03-02

### Commits

[`4696a01`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4696a0191a19a775ed75eb852b5626a99e0a372a) Fixed bug on SSO. Cookie wasn't destroyed when JWT is outdated and not refreshable.

## [4.16.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.7...4.16.0) - 2023-02-26

### Commits

[`59bd7cc`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/59bd7cc330d10242769112a95a06a4a58bc8ecf1) Fixed miscloses ES connections

## [4.15.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.6...4.15.7) - 2023-02-16

## [4.15.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.5...4.15.6) - 2023-02-16

### Commits

[`6a4a1a3`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6a4a1a36226b463c9a7655be57537df002ca8e58) Updated Yarn version.
[`02d2f7f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/02d2f7f5bd2c4e8ce3e0220659bd373b1d1bbbc9) Fixed typo

## [4.15.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.4...4.15.5) - 2023-02-16

### Commits

[`891e7d6`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/891e7d6578d41e11e3298259cc64d5928a7f85e6) Fixed mishandled exceptions

## [4.15.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.3...4.15.4) - 2023-02-14

### Commits

[`5a5f6a0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5a5f6a008ea158abf30029f37793a439d91b7155) Updated yarn version

## [4.15.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.2...4.15.3) - 2023-01-23

### Commits

[`f495ce7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f495ce762ae351e3fb58e7a1293bb4e39e32c899) Fixed bug on property filter when value is an array

## [4.15.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.1...4.15.2) - 2023-01-17

### Commits

[`f373c8f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f373c8ffbae6699a49fac8eb5491ba8878330c11) Added "success" in Update/Create/Delete mutation payloads.
[`2dcbe88`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2dcbe888cdab7f9870c8d04c64ac19ca74913cc6) Fixed unit test

## [4.15.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.15.0...4.15.1) - 2023-01-06

### Commits

[`830d4f5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/830d4f52dbe35c22617cd65314238e0a30586d01) Added RGPD right to erasure to UnregisterUserAccount.graphql.js

## [4.15.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.8...4.15.0) - 2022-12-23

### Commits

[`523e56f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/523e56f58061ca9e1181857c448b9e007b5d0f55) Improve GraphQL fulltext search in queries by introducing fine grain QueryStringInput type and new "queries" parameter.

## [4.14.8](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.7...4.14.8) - 2022-12-22

### Commits

[`e1cb6ca`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e1cb6ca0e48eed9215d1849132da67364c36d6dc) Fixed unhandled GraphControllerClient HTTPErro
Fixed bug when GraphControllerClient password contains weird chars

## [4.14.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.6...4.14.7) - 2022-12-19

### Commits

[`7d30f99`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7d30f997a12a885310ed65b643b9bebb8b3a5773) Added a GRAPHQL_CSRF_PREVENTION_DISABLED variables to disable CSRF prevention on apollo server.

## [4.14.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.5...4.14.6) - 2022-12-16

### Commits

[`ffd6bf4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ffd6bf43b3f15a2649df72e13e832323e59706b1) Added express-winston. Activate it passing HTTP_REQUESTS_LOGGING_ENABLED=1

## [4.14.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.4...4.14.5) - 2022-12-08

### Commits

[`15113aa`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/15113aa803ad45a64f67e3586e1ac6b4bc5735a6) Added some shortcuts in DatastoreSession

## [4.14.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.4-rc...4.14.4) - 2022-12-06

### Merged

[`#83`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/83) Allow token exchanging for passwordless authentication

## [4.14.4-rc](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.3...4.14.4-rc) - 2022-11-28

### Commits

[`244fea4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/244fea4ff03c9872f8dfb9958ac66ab382deacb4) Expand jest timeout for unit test

## [4.14.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.2...4.14.3) - 2022-11-25

### Commits

[`28d8f8b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/28d8f8b523ffb21e02f64b2e851ee86d34bc9f32) Fixed SSOApiClient.js

## [4.14.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.1...4.14.2) - 2022-11-25

### Commits

[`23d9daf`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/23d9daf36ab1003dc00609fa5a205bd66b821467) Fixed GraphControllerClient.js error log

## [4.14.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.14.0...4.14.1) - 2022-11-24

## [4.14.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.13.2...4.14.0) - 2022-11-22

### Merged

[`#85`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/85) Native GraphController client

### Commits

[`569010b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/569010bef77843881b89efb5143591327baa1a37) WIP
[`f8e70ed`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f8e70ed0fdb88ec6b4166acf9add5bffdc9c644a) Removed legacy stuff.
Added GraphControllerClient.js
NetworkLayer is now optional
[`16fb30c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/16fb30c75751138f7169f232c54a3b2deef2fe4e) Upgraded GotJS version
Fixed some bugs
[`8648f20`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8648f20270890311933c94f695b1d57fca179bea) Removed legacy stuff.
Added GraphControllerClient.js
NetworkLayer is now optional
[`7b7fd14`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7b7fd14b4f766edc4cd659ae5d3e4dfa3712935f) Updated docs

## [4.13.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.13.1...4.13.2) - 2022-10-25

### Commits

[`b5bbcf3`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b5bbcf3cd490db6f5fa79892c894887ddadc0862) Fixed unit test
Fixed multivalued LiteralDefinition insertion
[`c2c7b54`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c2c7b545547d44756570ca5f527554ed1fccb21b) Fixed unproper AMQP connection/channel closing reconnection

## [4.13.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.13.0...4.13.1) - 2022-10-25

### Commits

[`3ac0061`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3ac00618b2a32f85d0abd5402285d3a1ea3ae3d1) Added some OWASP security layers

- Escaped SPARQL variables from injections
- Added CSRF double submission cookie mechanism
- Updated CORS parameters

## [4.13.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.9...4.13.0) - 2022-10-24

### Commits

[`aa8cdfb`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/aa8cdfb89bd12d45d52c36913e7d8b3319398507) Fixed RefreshToken flow.

## [4.12.9](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.8...4.12.9) - 2022-10-14

## [4.12.8](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.7...4.12.8) - 2022-10-11

### Merged

[`#84`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/84) Big wipe

### Commits

[`b12d8f4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b12d8f4716cd9ddf1a9345990ae12b7401cc23ad) Fixed unit test

## [4.12.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.6...4.12.7) - 2022-09-29

### Commits

[`c6bf2c1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c6bf2c14dfaf5c4377a7c5602e1f542645330e0b) Improved Gql filter

## [4.12.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.5...4.12.6) - 2022-09-27

### Commits

[`65d3872`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/65d3872e00324209ed4e0315a4b6b5dac09bf7d6) Removed IndexMatchers
[`f0b8ff8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f0b8ff8f8930fd758ccd0ee0d058b88b726ca628) Added the literalDefinition as URI case

## [4.12.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.4...4.12.5) - 2022-09-23

### Commits

[`af42ef9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/af42ef9ce82dbf0bd0313a515202e52eeaf06d45) Improved full_text queries

## [4.12.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.3...4.12.4) - 2022-09-13

### Commits

[`615b97a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/615b97a46804994b96633af6508cb895d36ff4fa) Updated UserAccountDefinition.js

## [4.12.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.2...4.12.3) - 2022-09-06

### Commits

[`4e408c0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4e408c095e7fa22176c1dca8b0b86eaa21e7b7a0) Replaced match_phrase_prefix with new more accurate match_bool_prefix query.

## [4.12.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.1...4.12.2) - 2022-08-18

### Commits

[`40cf9cb`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/40cf9cbc5f852054f302b93010b4dc23a7d3e990) Updated PersonDefinition.js

## [4.12.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.12.0...4.12.1) - 2022-07-08

### Commits

[`72844ed`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/72844ed765edf8a9667918acccc72adcdac44371) Updated ontologie.
Fixed bug on concept graph update when a symmetric link is not plural.

## [4.12.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.11.2-rc...4.12.0) - 2022-07-06

### Commits

[`eb4d9bd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/eb4d9bdec11ca8d0f7e059afc9b8dec2f4393a43) Added "Translations" suffixed properties and inputs in GraphQL schema. This feature enables to query/mutation all values for a given LabelDefinition related field.
[`8394fdd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8394fdd3daba8f1c786fa080d7f1ec16ad45cf7d) Improved Collection.js to append and prepend objects.
Improved getObjectsResolver to return a Collection instance instead of a GraphQL connection object.

## [4.11.2-rc](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.11.1...4.11.2-rc) - 2022-06-06

### Commits

[`a610233`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a6102330cd911b54b7c3c29f04803a4f4f07c836) Don't capture I18nError errors in Sentry by default

## [4.11.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.11.0...4.11.1) - 2022-05-30

### Commits

[`7547e88`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7547e88f9881f17bff6319ff3837c323c4a7dfab) Fixed bug on agent affiliation

## [4.11.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.10.6...4.11.0) - 2022-05-23

### Merged

[`#82`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/82) Improved mnx:Action snapshots middleware and resolvers.

## [4.10.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.10.5...4.10.6) - 2022-05-03

### Commits

[`22d4ff8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/22d4ff86f1fd00ee4c1dfb36d5504f45956b101f) Capture apollo graphql error in Sentry if enabled

## [4.10.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.10.4...4.10.5) - 2022-04-08

### Commits

[`07ea92d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/07ea92db880db8ef0c3097055505daa84c0f5357) Fix localized labels getter for plural label

## [4.10.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.10.3...4.10.4) - 2022-03-29

### Commits

[`a744dc3`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a744dc325aa835c52cef09423b0180fab4b81059) Added aggregation on ProjectDefinition::title

## [4.10.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.10.2...4.10.3) - 2022-03-28

### Commits

[`ac4a0a0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ac4a0a0a8f5f67a63413b6b6f192a9ccaecf6bc5) Fixed bug on single link deletion. The bug appeared when a link is registered with a rdfObjectProperty in the LinkDefinition but is saved in the graph with the owl:inverseOf notation.
[`ba1119b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ba1119b36eb63c40779d1c15da9019bd11f82db8) Upgraded dependencies. Required GraphqlJS version is now 15.7.2.

## [4.10.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.10.1...4.10.2) - 2022-03-18

### Commits

[`06c2b55`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/06c2b55a30f31607174d0615fdb7e8c440ede9d1) Improved logging options.
Moved `RABBITMQ_LOG_LEVEL` env var to `LOG_LEVEL`
Added `LOG_LEVEL`, `LOG_FILTER` and `LOG_FORMAT` env var.
@see https://gitlab.com/mnemotix/synaptix.js/-/blob/master/docs/pages/environment.md#logging

## [4.10.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.10.0...4.10.1) - 2022-03-17

### Commits

[`3ff5fb8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3ff5fb89fdfc757efd3faae4d036a56e220006cd) Updated EntityDefinition.js

## [4.10.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.1...4.10.0) - 2022-03-15

### Merged

[`#81`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/81) Added "nested localized label" in ES.

### Commits

[`31219ba`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/31219ba5ebb153882d52273600433528e06ab8e9) **Breaking change:** Added "nested localized label" in ES.
This feature enables to index localized labels in nested way instead of using dual fields "_locales". It fixed two issues :

- Sorting that doesn't work well if multiple languages are used in the same field.
- Terms aggregations that doesn't work well because gathers all languages terms without filtering those for current lang.

```
title: [{value: "...", lang: "fr"}, {value: "...", lang: "en"}]
```

This corresponding ES mapping must be such form :

```
"title": {
"type": "nested",
"properties": {
"lang": {
"type": "keyword"
},
"value": {
"type": "text",
"fields": {
"keyword": {
"type": "keyword",
"normalizer": "[...custom normalizer...]"
},
"keyword_not_normalized": {
"type": "keyword"
}
},
"analyzer": "[...custom analyzer...]"
}
}
}
```

As this features is BREAKING, it is not enabled by default. To activate it, add env variable : `ES_NESTED_LOCALIZED_LABEL=1`
[`14d829b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/14d829b48b7f9fd721638cc086986a4662ba4c56) Added unit tests for nested localized labeld.
[`71cba64`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/71cba6467a9be1e6ff6aa0ea850b1365f279a2fb) Updated ProjectOutputDefinition.js

## [4.9.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.1-hde-fix...4.9.1) - 2022-03-11

## [4.9.1-hde-fix](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.1-hde...4.9.1-hde-fix) - 2022-03-14

### Commits

[`88b817d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/88b817d9df89419c8093a4788fa4fc1f09c08a9c) Fix fragment key parser for batch update

## [4.9.1-hde](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0...4.9.1-hde) - 2022-03-14

### Commits

[`ffd0432`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ffd0432bd52ef26abd1152c62efd6e945105ae9e) Fixed ids normalization issues when using mixed Graph and Index queries. Solution is to normalize ids in absolute URI form.
[`1e4e424`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1e4e424b84489f0c5f321e018cf2a6390f036e5c) Consider updatedObjects as root of a gql path in fragmment parsing

## [4.9.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.27...4.9.0) - 2022-03-08

### Merged

[`#80`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/80) Merge 4.9 source code in master.

### Commits

[`0d0859d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0d0859dac872b84bec8480b927504ca12f20bb33) **Breaking change:** Removing Figlet
Potential BREAKING CHANGE : CJS transpilation now in dist/ and not dist/cjs
Escaping AMQP broker password in log.
[`8c43afb`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8c43afb2d79507ee191e09529237fab3d0f95f59) Disabling Apollo cache by default because since Apollo 3, everthing is PUBLIC...

## [4.9.0-beta.27](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.26...4.9.0-beta.27) - 2022-03-04

### Commits

[`6eb8763`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6eb87631102b50719f254575f99ab01993ebd178) Add ApiValidationError in graphql framework + add batch resolver on GraphQLRemoveMutation

## [4.9.0-beta.26](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.25...4.9.0-beta.26) - 2022-03-03

### Commits

[`60f30b4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/60f30b446461e0f500875819132094ac38a695e4) Fixed bug on runtime field that crashed when field type was not a List.
Fixed bug on sorting that was not recognized for extended modelDefinition properties.

## [4.9.0-beta.25](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.24...4.9.0-beta.25) - 2022-03-02

### Commits

[`09d0595`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/09d0595fe2847b31be7b7ca32525f38862871e11) Add possibility to define a batch resolver for a graphql update mutation
[`4c5599c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4c5599c9de74bfc900de72d6d09040a8b51e0b11) Add environment variable for customizing body parser max size

## [4.9.0-beta.24](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.23...4.9.0-beta.24) - 2022-02-25

### Commits

[`121e091`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/121e09100ae57ba7e738466f588a3d16040493a8) Fixed securization middlewares by adding user groups in queries.

## [4.9.0-beta.23](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.22...4.9.0-beta.23) - 2022-02-21

### Commits

[`42c50ef`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/42c50ef44ad2c2d9b45816b5197bfa9792fc5bcd) Fixed fragmentHelpers.js when using GraphQL aliases in deep connections.

## [4.9.0-beta.22](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.21...4.9.0-beta.22) - 2022-02-21

### Commits

[`7264b9b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7264b9b31c4e2bc8cf75c260623a190480d52120) Fix fragment definitions building, when build is called at a sublevel of a graphql query

## [4.9.0-beta.21](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.20...4.9.0-beta.21) - 2022-02-21

### Commits

[`6f385e4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6f385e4175ea72d0ff70e4094ca60b424b544209) Modified SPARQL update query to batch update a bunch of objects.
Securized updateObjectResolver by using SynaptixDatastoreSession::updateObjects method.

## [4.9.0-beta.20](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.19...4.9.0-beta.20) - 2022-02-01

### Commits

[`30f387d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/30f387de6bd44242c164928e42b383f9fcf0c8bf) Updated securizeEntitiesQueryMiddleware.js and securizeEntitiesMutationMiddleware.js.

## [4.9.0-beta.19](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.18...4.9.0-beta.19) - 2022-01-24

### Commits

[`237fcf9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/237fcf951e17c2076b2ace7796f31ace152da3b8) Updated UserGroup model to add inheritance and transitivity.
Updated UserAccount model to add default userGroup at indexation time.

## [4.9.0-beta.18](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.17...4.9.0-beta.18) - 2022-01-19

### Commits

[`27be50f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/27be50f2f22adda803d545db1718260a988ca9a3) Fix GroupMembership.hasAgentMember link + fix rule parser on gql schema generator

## [4.9.0-beta.17](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.15...4.9.0-beta.17) - 2022-01-14

### Merged

[`#79`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/79) Added group membership in model

### Commits

[`72117f9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/72117f91c35d6067fb2d9d404e3125bd80a388b2) **Breaking change:** BREAKING CHANGE : Updated GraphQL to 15.6.1 (required as peer dependencies)
BREAKING CHANGE : Removed FormInputDirective.js so @formInput(type:...) in GraphQL scheme is no longer supported.
Removed graphql-tools in favour of @graphql-tools/*
[`607da7b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/607da7bcca90afb181460d0f26b452d2ae0b5d8b) **Breaking change:** Potential BREAKING CHANGE : Updated Apollo Server to version 3. See https://www.apollographql.com/docs/apollo-server/migration
[`d6e3516`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d6e35168142f45bf845dc6e4c7f332cdc042e6e0) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`d1fdd73`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d1fdd73c0d14c9e9e7056645c8df0d3a11d00b7a) **Breaking change:** Potential BREAKING CHANGE. Changed ES text nested mapping "keyword_case_sentitive" by "keyword_not_normalized".
[`36ffed4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/36ffed409af9fdb5a253741b069a759bc7960e94) Added RemoveEntitiesByFiltersGraphQLMutation.js mutation to batch delete entities precising a GraphQL type and a list of filters to search for ids to delete.
Added securizeEntitiesMutationMiddleware.js to securize batch deletion according ACL filters.
Gathered securizeEntitiesMutationMiddleware.js and securizeEntitiesQueryMiddleware.js below exported securizeEntitiesMiddlewares.js to use in application.
[`5bd4f4d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5bd4f4dba09a5d4b17c2e57e2ab6360274c8a698) wip
[`a677f46`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a677f46f3ec36e425d1877db2b74580f82adc8c2) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`bd5ba00`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/bd5ba009658e564ac14028340992ed69d371e841) Added some shields rules related to access targets
Added securization middleware related to access targets.
Added "Permissions" GraphQL type and "permissions" property to EntityInterface GraphQL interface.
[`b5273e5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b5273e527940822e34daccf0bd4a4787983f0d6f) Added new LinkPath step called ConcatStep.js that defines a computed property resulting in the concatenation of a list of propertyStep.
Added runtime fields feature for Literal defined by that ConcatStep (Used in PersonDefinition::fullName and EntityDefinition::createdBy properties)
[`fe050d1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fe050d1ad6a89ab0c4d4a1650c73f25294144a00) Removed useless ACL classes
Added new ACL classes
[`cb27b36`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cb27b368ee1215955ca8f0fdc24ca30affa68ab0) Added totalCount property to GraphQL connection in order to avoid useless additional ES request (this piece of information is always sent by ES). Working too with GraphStore (without any optimization while no similar feature is available in SPARQL).
[`cd0a47f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cd0a47f37236cc590ce6e0be12aef773a8a9e153) Fixed bug on runtime fields (that broke fulltext search)
Fixed bug on SynaptixDatastoreSession::parseRawFilters method when modelDefinition is extensible (inherited model definitions properties were not recognized)
Added aggregations fields on ResourceDefinition.js and FileDefinition.js
[`5141391`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5141391b71a1512ae850d1a7be3694fda47f767a) Add memberships links between agents and people + set GroupMembership as instantiable
[`e564565`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e5645659d142fcc5171fe607de033aee601894cb) Fixed tests.
[`4a7f6ce`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4a7f6ce44008b069b727f428208d5b66a586377a) Added "hasReadAccessGranted" filter on EntityDefinition.js.
[`5491a15`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5491a15ada8fad31c8a63386f98adaaccb99c0ec) Used SYNAPTIX_USER_SESSION_COOKIE_NAME env variable to define session cookie name.
Make UserAccountDefinition.js and UserGroupDefinition.js inherit from AccessTargetDefinition.js and gather them inside a same index.
[`e9fc0f0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e9fc0f00dac6664a60661911640bee554eaec25b) Added "langFlag" parameter on GraphQL multilang properties. Returns "maison@fr" instead of "maison" if set to true.
[`9e2cf53`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9e2cf53b73b8966ec7fef6ac263efc016840808f) Gathered UserAccount and UserGroup under same index "access-target" (like Person and Organization under "agent").
Added "asGlobalRole" property to UserAccountDefinition.js
[`6d3466b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6d3466ba0b43fe2bf39ca4e83a08f583e98e7600) Improved GraphQL schema misformed error on startup.
[`f55d49a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f55d49a41ddb3f18bf12df770db89ebaabb8505d) Isolate getter/setter on data caching in session, for allowing overriding
[`40d5b5d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/40d5b5d4b93980125daf5191ffd7d17dabe51836) Added unit test for removing big list of objects.
[`50c3c71`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/50c3c7178f8d10de03da4b3a0545ccf754d5997b) Fixed unit test
[`374b48e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/374b48e775c868ddb47b28eba79c204b755531e4) Change session fromCache/toCache as async methods
[`5184415`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5184415904360a2ea6c51d78fc69673b3811dac2) Fixed test.
[`fea8b33`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fea8b337c6f8777641de91c7b1eca34c49d72c46) Added terms aggregation default size to 1000 (instead of 10)
[`5fcbe45`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5fcbe457acb69af005b688b7893db0712b339cec) Fix putting of user and person, into cache
[`85b1a1f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/85b1a1f1ca7f54bd617d2b9ba37d23bdd9277909) Disable import of checkDatamodel script (compatibility error with rdflib)
[`ac6e4ae`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ac6e4ae0104afd711e7f4e1a7adcfb758f4124fd) Added delay after a batch removal to optimize synchronization.
[`f3c9b93`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f3c9b93365f14b91acaaf8227bff55636c8e2d63) Reverted .gitlab-ci.yml
[`1d8eafd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1d8eafddc158bbdf9ec3bc2e5e24212848701c89) Merge origin/road-to-4.9 into road-to-4.9

## [4.9.0-beta.15](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.14...4.9.0-beta.15) - 2022-01-07

### Commits

[`db8f09c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/db8f09cf381283659dc13d4f677cf3d859eb33e8) Update mnx-project model.

## [4.9.0-beta.14](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.13...4.9.0-beta.14) - 2022-01-07

### Commits

[`84a8c25`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/84a8c2593be53dc56328a5b6448cfab7333817f5) **Breaking change:** BREAKING CHANGE : Updated GraphQL to 15.6.1 (required as peer dependencies)
BREAKING CHANGE : Removed FormInputDirective.js so @formInput(type:...) in GraphQL scheme is no longer supported.
Removed graphql-tools in favour of @graphql-tools/*
[`500e645`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/500e64500e3e9413fbbb63a6120ad93e10cf4333) **Breaking change:** Potential BREAKING CHANGE : Updated Apollo Server to version 3. See https://www.apollographql.com/docs/apollo-server/migration
[`538075f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/538075fa312c4b1e4396a73814c9b57217bce543) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`cb96ef8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cb96ef850b1c6ffa753fcb86fdc5835158ccdee8) **Breaking change:** Potential BREAKING CHANGE. Changed ES text nested mapping "keyword_case_sentitive" by "keyword_not_normalized".
[`8c4471b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8c4471b81db4e66497e76fe31abd58273e6da38e) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`4b71952`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4b71952a500fdd9b899e42411b25a3c55da9afd4) Added some shields rules related to access targets
Added securization middleware related to access targets.
Added "Permissions" GraphQL type and "permissions" property to EntityInterface GraphQL interface.
[`ab426e9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ab426e92663ca85347861da06a670246a8b993b9) Added new LinkPath step called ConcatStep.js that defines a computed property resulting in the concatenation of a list of propertyStep.
Added runtime fields feature for Literal defined by that ConcatStep (Used in PersonDefinition::fullName and EntityDefinition::createdBy properties)
[`45b2974`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/45b29746a506c4cf53e8524a80e64ec2f0525358) Removed useless ACL classes
Added new ACL classes
[`541fb6b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/541fb6b583a470bee4738c0bb5156b3007ac490f) Added totalCount property to GraphQL connection in order to avoid useless additional ES request (this piece of information is always sent by ES). Working too with GraphStore (without any optimization while no similar feature is available in SPARQL).
[`6cf5911`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6cf5911b727d07ee70e7fde8f2722760ded3e8e3) Restored Apollo server cache while bug has been fixed.
Restored GraphQL playground in production environment.
[`d9814aa`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d9814aadc009af3aa0f5baa26d4e10d28e936d47) Fixed bug on runtime fields (that broke fulltext search)
Fixed bug on SynaptixDatastoreSession::parseRawFilters method when modelDefinition is extensible (inherited model definitions properties were not recognized)
Added aggregations fields on ResourceDefinition.js and FileDefinition.js
[`8a2d575`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8a2d575e6c12427cdaa8ec39b9491090f2a9230d) Fixed tests.
[`4f893d5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4f893d52701374a3e8aab13b4d54886151472d48) Added "hasReadAccessGranted" filter on EntityDefinition.js.
[`e7bbcd9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e7bbcd91a1712c7dad2bdf2fd5d16352b1856aa6) Used SYNAPTIX_USER_SESSION_COOKIE_NAME env variable to define session cookie name.
Make UserAccountDefinition.js and UserGroupDefinition.js inherit from AccessTargetDefinition.js and gather them inside a same index.
[`8ed6239`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8ed6239c4faae0b11d698f3ff7c207ef7ac0987a) Fixed bug fragmentHelpers.js when using alias
[`796d1ac`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/796d1acdd6a1b85dc86390452eb0cf34ab768ab1) Added "langFlag" parameter on GraphQL multilang properties. Returns "maison@fr" instead of "maison" if set to true.
[`66885cd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/66885cd67c756bc89642a8d4f5a4af840e44c710) Gathered UserAccount and UserGroup under same index "access-target" (like Person and Organization under "agent").
Added "asGlobalRole" property to UserAccountDefinition.js
[`c6400ec`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c6400ec65e983f4f627d8c0293de7b307de2cae7) Improved GraphQL schema misformed error on startup.
[`eadc519`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/eadc51981d137e76e7af1e253855bb4b5f660ab3) Isolate getter/setter on data caching in session, for allowing overriding
[`2c6c37a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2c6c37aad28289ed8674f76fb1dede77066afd39) Fixed unit test
[`20498f8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/20498f85e81303a6e960ced489678802f6a230c8) Change session fromCache/toCache as async methods
[`ec31140`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ec31140b9beb52d5ee8d44e0a597b5079c29db07) Removed Apollo sandbox in dev env (reverted playground).
This is caused by recent Cookie "SameSite=None; Secure" addition and impossibility to forward session Cookie in localhost env.
[`96c23c8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/96c23c8142f3d856e90995931f9ef0f970aecc31) Fixed test.
[`3813167`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3813167ff5b8e4164a06d005d686cf1dda6aa598) Added terms aggregation default size to 1000 (instead of 10)
[`27269c9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/27269c98e5031ba08cb8d1800c98c9bda8219562) Fix putting of user and person, into cache
[`5c17b48`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5c17b482911b0b6a75c000edaf390b5550b6d310) Disable import of checkDatamodel script (compatibility error with rdflib)

## [4.9.0-beta.13](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.12...4.9.0-beta.13) - 2021-12-03

### Commits

[`5fcbe45`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5fcbe457acb69af005b688b7893db0712b339cec) Fix putting of user and person, into cache

## [4.9.0-beta.12](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.11...4.9.0-beta.12) - 2021-12-02

### Commits

[`f55d49a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f55d49a41ddb3f18bf12df770db89ebaabb8505d) Isolate getter/setter on data caching in session, for allowing overriding
[`374b48e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/374b48e775c868ddb47b28eba79c204b755531e4) Change session fromCache/toCache as async methods
[`85b1a1f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/85b1a1f1ca7f54bd617d2b9ba37d23bdd9277909) Disable import of checkDatamodel script (compatibility error with rdflib)

## [4.9.0-beta.11](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.10...4.9.0-beta.11) - 2021-11-30

### Commits

[`72117f9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/72117f91c35d6067fb2d9d404e3125bd80a388b2) **Breaking change:** BREAKING CHANGE : Updated GraphQL to 15.6.1 (required as peer dependencies)
BREAKING CHANGE : Removed FormInputDirective.js so @formInput(type:...) in GraphQL scheme is no longer supported.
Removed graphql-tools in favour of @graphql-tools/*
[`607da7b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/607da7bcca90afb181460d0f26b452d2ae0b5d8b) **Breaking change:** Potential BREAKING CHANGE : Updated Apollo Server to version 3. See https://www.apollographql.com/docs/apollo-server/migration
[`d6e3516`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d6e35168142f45bf845dc6e4c7f332cdc042e6e0) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`d1fdd73`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d1fdd73c0d14c9e9e7056645c8df0d3a11d00b7a) **Breaking change:** Potential BREAKING CHANGE. Changed ES text nested mapping "keyword_case_sentitive" by "keyword_not_normalized".
[`a677f46`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a677f46f3ec36e425d1877db2b74580f82adc8c2) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`bd5ba00`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/bd5ba009658e564ac14028340992ed69d371e841) Added some shields rules related to access targets
Added securization middleware related to access targets.
Added "Permissions" GraphQL type and "permissions" property to EntityInterface GraphQL interface.
[`b5273e5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b5273e527940822e34daccf0bd4a4787983f0d6f) Added new LinkPath step called ConcatStep.js that defines a computed property resulting in the concatenation of a list of propertyStep.
Added runtime fields feature for Literal defined by that ConcatStep (Used in PersonDefinition::fullName and EntityDefinition::createdBy properties)
[`fe050d1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fe050d1ad6a89ab0c4d4a1650c73f25294144a00) Removed useless ACL classes
Added new ACL classes
[`cb27b36`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cb27b368ee1215955ca8f0fdc24ca30affa68ab0) Added totalCount property to GraphQL connection in order to avoid useless additional ES request (this piece of information is always sent by ES). Working too with GraphStore (without any optimization while no similar feature is available in SPARQL).
[`cd0a47f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cd0a47f37236cc590ce6e0be12aef773a8a9e153) Fixed bug on runtime fields (that broke fulltext search)
Fixed bug on SynaptixDatastoreSession::parseRawFilters method when modelDefinition is extensible (inherited model definitions properties were not recognized)
Added aggregations fields on ResourceDefinition.js and FileDefinition.js
[`e564565`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e5645659d142fcc5171fe607de033aee601894cb) Fixed tests.
[`4a7f6ce`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4a7f6ce44008b069b727f428208d5b66a586377a) Added "hasReadAccessGranted" filter on EntityDefinition.js.
[`5491a15`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5491a15ada8fad31c8a63386f98adaaccb99c0ec) Used SYNAPTIX_USER_SESSION_COOKIE_NAME env variable to define session cookie name.
Make UserAccountDefinition.js and UserGroupDefinition.js inherit from AccessTargetDefinition.js and gather them inside a same index.
[`e9fc0f0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e9fc0f00dac6664a60661911640bee554eaec25b) Added "langFlag" parameter on GraphQL multilang properties. Returns "maison@fr" instead of "maison" if set to true.
[`9e2cf53`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9e2cf53b73b8966ec7fef6ac263efc016840808f) Gathered UserAccount and UserGroup under same index "access-target" (like Person and Organization under "agent").
Added "asGlobalRole" property to UserAccountDefinition.js
[`6d3466b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6d3466ba0b43fe2bf39ca4e83a08f583e98e7600) Improved GraphQL schema misformed error on startup.
[`50c3c71`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/50c3c7178f8d10de03da4b3a0545ccf754d5997b) Fixed unit test
[`42a27b2`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/42a27b214e0316de4947dfbf7c995ada394efc75) Fixed bug on RemoveEntityLinkGraphQLMutation.js
[`5184415`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5184415904360a2ea6c51d78fc69673b3811dac2) Fixed test.
[`fea8b33`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fea8b337c6f8777641de91c7b1eca34c49d72c46) Added terms aggregation default size to 1000 (instead of 10)

## [4.9.0-beta.10](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.9...4.9.0-beta.10) - 2021-11-26

### Commits

[`1dacb52`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1dacb52562cd36dc378ce0629f5039af685deaf9) Added "langFlag" parameter on GraphQL multilang properties. Returns "maison@fr" instead of "maison" if set to true.

## [4.9.0-beta.9](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.8...4.9.0-beta.9) - 2021-11-26

### Commits

[`13f8b19`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/13f8b190323bfc9ea06c7d8bdbeb60870bf65d4b) Added terms aggregation default size to 1000 (instead of 10)

## [4.9.0-beta.8](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.7...4.9.0-beta.8) - 2021-11-25

### Commits

[`c055fe8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c055fe825708236f91854f4ae5697bcb3766ebe6) Added some shields rules related to access targets
Added securization middleware related to access targets.
Added "Permissions" GraphQL type and "permissions" property to EntityInterface GraphQL interface.
[`24ce9e1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/24ce9e147f3904cea7213ba0e492105ce0f84e8d) Used SYNAPTIX_USER_SESSION_COOKIE_NAME env variable to define session cookie name.
Make UserAccountDefinition.js and UserGroupDefinition.js inherit from AccessTargetDefinition.js and gather them inside a same index.
[`b5981d8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b5981d8cf30074da435108f22b5d36c820836430) Fixed unit test

## [4.9.0-beta.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.6...4.9.0-beta.7) - 2021-11-19

### Commits

[`7b44131`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7b44131829755920e082cec8017c83e12a4de085) Improved GraphQL schema misformed error on startup.

## [4.9.0-beta.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.5...4.9.0-beta.6) - 2021-11-18

### Commits

[`92a99a9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/92a99a9889e9254241a0f6be762acebe3be75998) Gathered UserAccount and UserGroup under same index "access-target" (like Person and Organization under "agent").
Added "asGlobalRole" property to UserAccountDefinition.js

## [4.9.0-beta.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.4...4.9.0-beta.5) - 2021-11-16

### Commits

[`c800a0e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c800a0e7acc6f6542af1c3f46b61531b9eed210b) **Breaking change:** BREAKING CHANGE : Updated GraphQL to 15.6.1 (required as peer dependencies)
BREAKING CHANGE : Removed FormInputDirective.js so @formInput(type:...) in GraphQL scheme is no longer supported.
Removed graphql-tools in favour of @graphql-tools/*
[`2c0eadf`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2c0eadfefb244650d10b793b249b5247f978dd11) **Breaking change:** Potential BREAKING CHANGE : Updated Apollo Server to version 3. See https://www.apollographql.com/docs/apollo-server/migration
[`7693e37`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7693e3736d9efbb629bf1796ce247415b7ee9959) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`a7b6cb0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a7b6cb05fc7ff640540afbe149c94a1e3ca52114) **Breaking change:** Potential BREAKING CHANGE. Changed ES text nested mapping "keyword_case_sentitive" by "keyword_not_normalized".
[`463565e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/463565e4730217533bdf9565ea7993e65c84f795) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`4b4019b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4b4019beabb9de3d83b599a52432c8beb350d196) Added new LinkPath step called ConcatStep.js that defines a computed property resulting in the concatenation of a list of propertyStep.
Added runtime fields feature for Literal defined by that ConcatStep (Used in PersonDefinition::fullName and EntityDefinition::createdBy properties)
[`e7597d7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e7597d77c4b03f72023b0869f242b46e9a79b137) Removed useless ACL classes
Added new ACL classes
[`47671b7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/47671b701253638b9914f84e3bb980dd088a81f1) Added totalCount property to GraphQL connection in order to avoid useless additional ES request (this piece of information is always sent by ES). Working too with GraphStore (without any optimization while no similar feature is available in SPARQL).
[`4ea3622`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4ea3622bbd32e94ac7128ef704148be1550bda26) Change the key pattern for fragments register (some were squeezed with old one) + add fragment parsing for returned object at root of mutations
[`5e39b10`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5e39b103cb19ece1e273bf56482d5da8e53ad078) Fixed bug on runtime fields (that broke fulltext search)
Fixed bug on SynaptixDatastoreSession::parseRawFilters method when modelDefinition is extensible (inherited model definitions properties were not recognized)
Added aggregations fields on ResourceDefinition.js and FileDefinition.js
[`816a05b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/816a05b0cc5a633d91a380f77ba8d0f852d86f60) Fixed tests.
[`dc9a4c7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/dc9a4c72fe0728f449a94d9fe0aa792146c2f794) Added "hasReadAccessGranted" filter on EntityDefinition.js.
[`7075ed7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7075ed7b324f3090aad11c573beb46aec4ebe02c) Extends fragment parsing to mutations
[`fa9f4f8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fa9f4f89f30f059ef89afcd67e2eecbe360be2ce) Added a forceFallbackIfEmpty option to SynaptixDatastoreSession::getObjects
[`eaeff93`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/eaeff934f426a133850ebb879480ba1176647744) Fixed test.
[`60f1233`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/60f1233334e46c66dcf7e6154fe84acb1fbdf3c8) Make tests work...
[`f1df716`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f1df716eb48bc1b9c74277851be04ef8fc72d9d0) Fixed QS forwarding in SynaptixDatastoreSession::getLinkedObjectsCountFor()

## [4.9.0-beta.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.3...4.9.0-beta.4) - 2021-10-25

### Commits

[`8b91a86`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8b91a86ad678f6450a85c5f88fa5ffd98a457010) **Breaking change:** BREAKING CHANGE : Updated GraphQL to 15.6.1 (required as peer dependencies)
BREAKING CHANGE : Removed FormInputDirective.js so @formInput(type:...) in GraphQL scheme is no longer supported.
Removed graphql-tools in favour of @graphql-tools/*
[`16f1df5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/16f1df5dd0c0e1b7e442f41e26e416649318bac6) **Breaking change:** Potential BREAKING CHANGE : Updated Apollo Server to version 3. See https://www.apollographql.com/docs/apollo-server/migration
[`3641376`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3641376872909d743165920eac71f824c74f8017) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`2b7ff97`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2b7ff97e69d3f70d19d2b2b514b68cd1bc9edde1) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`36d961d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/36d961d00bfd559d23c26df6149dc61fb0ce51ec) Removed useless ACL classes
Added new ACL classes
[`4584a21`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4584a2166adedba356f9f770960b58354ac13b48) Fixed tests.
[`3485d10`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3485d1075d766f4f244e4a40db2979dfc5f4a746) Added "hasReadAccessGranted" filter on EntityDefinition.js.
[`369e1e3`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/369e1e3081081f88df66c8fe2b27ad9dbce45cf0) Fixed bug in fragmentHelpers.js

## [4.9.0-beta.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.2...4.9.0-beta.3) - 2021-10-25

### Commits

[`e6422c4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e6422c43f26dc7f46fb057b09b3d499fe5fb540c) **Breaking change:** BREAKING CHANGE : Updated GraphQL to 15.6.1 (required as peer dependencies)
BREAKING CHANGE : Removed FormInputDirective.js so @formInput(type:...) in GraphQL scheme is no longer supported.
Removed graphql-tools in favour of @graphql-tools/*
[`dac3438`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/dac343877c51aa7b022526848493eb752fd9ce08) **Breaking change:** Potential BREAKING CHANGE : Updated Apollo Server to version 3. See https://www.apollographql.com/docs/apollo-server/migration
[`0d5b55c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0d5b55cc69ffee4fbf461e77741525ca6612d6bc) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`937e667`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/937e667008d9acef4b5a72027594a73aae63651f) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`daca9cf`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/daca9cfcc4af594e4ccaacea82f6f6a0c86160d3) Removed useless ACL classes
Added new ACL classes
[`adc3ab1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/adc3ab11a89fc11650e242cdcfb089c02aab6c4f) Fixed bugs in FragmentDefinition parsing in cases of :
- a FragmentSpread that includes an other FragmentSpread.
- a FragmentSpread situated deeper than first type (or connection) resolution.
[`952ff02`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/952ff02a34febeb60722aff00dbdd053043e88de) Fixed tests.
[`0b29cf5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0b29cf5110b32784cc18ec383794e8f300ccd399) Added "hasReadAccessGranted" filter on EntityDefinition.js.

## [4.9.0-beta.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.9.0-beta.1...4.9.0-beta.2) - 2021-10-19

### Commits

[`4f7bc30`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4f7bc30dcdc3510fa16d5eaa4646c14806543f0c) **Breaking change:** Potential BREAKING CHANGE : Updated Apollo Server to version 3. See https://www.apollographql.com/docs/apollo-server/migration

## [4.9.0-beta.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.10...4.9.0-beta.1) - 2021-10-12

### Commits

[`a87dadf`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a87dadf2964a819162c77bafc5fea4048c9faaf4) **Breaking change:** BREAKING CHANGE : Updated GraphQL to 15.6.1 (required as peer dependencies)
BREAKING CHANGE : Removed FormInputDirective.js so @formInput(type:...) in GraphQL scheme is no longer supported.
Removed graphql-tools in favour of @graphql-tools/*
[`f70b474`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f70b4742565d5c26cfba056944964a886a5e3494) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`394f634`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/394f6347973d2018acf893fa5c4dda0ddca9f71c) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`e893914`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e893914b03c743aee035e2c52c7f7b8aca4a4cda) Removed useless ACL classes
Added new ACL classes
[`6cf2790`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6cf279090954164450c790da1ba162a6f06f06c2) Fixed tests.
[`1b4a074`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1b4a0744462c29726a5aebd37cf21f09a7280f52) Added "hasReadAccessGranted" filter on EntityDefinition.js.

## [4.8.10](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.9...4.8.10) - 2022-03-18

### Commits

[`a8389b3`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a8389b391807c1bbedc730a12a39d94d4da623c3) Improved logging options.
Moved `RABBITMQ_LOG_LEVEL` env var to `LOG_LEVEL`
Added `LOG_LEVEL`, `LOG_FILTER` and `LOG_FORMAT` env var.
[`a6ca9ba`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a6ca9ba6badacdcb8f9101bb7436c8fd9429db6a) Fixed bug on single link deletion. The bug appeared when a link is registered with a rdfObjectProperty in the LinkDefinition but is saved in the graph with the owl:inverseOf notation.

## [4.8.9](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.8...4.8.9) - 2021-12-15

## [4.8.8](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.7...4.8.8) - 2021-12-13

## [4.8.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.6...4.8.7) - 2021-12-13

### Commits

[`8ed6239`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8ed6239c4faae0b11d698f3ff7c207ef7ac0987a) Fixed bug fragmentHelpers.js when using alias

## [4.8.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.5...4.8.6) - 2021-11-29

### Commits

[`42a27b2`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/42a27b214e0316de4947dfbf7c995ada394efc75) Fixed bug on RemoveEntityLinkGraphQLMutation.js

## [4.8.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.4...4.8.5) - 2021-10-30

### Commits

[`4ea3622`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4ea3622bbd32e94ac7128ef704148be1550bda26) Change the key pattern for fragments register (some were squeezed with old one) + add fragment parsing for returned object at root of mutations

## [4.8.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.3...4.8.4) - 2021-10-28

### Commits

[`7075ed7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7075ed7b324f3090aad11c573beb46aec4ebe02c) Extends fragment parsing to mutations
[`fa9f4f8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fa9f4f89f30f059ef89afcd67e2eecbe360be2ce) Added a forceFallbackIfEmpty option to SynaptixDatastoreSession::getObjects
[`60f1233`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/60f1233334e46c66dcf7e6154fe84acb1fbdf3c8) Make tests work...
[`f1df716`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f1df716eb48bc1b9c74277851be04ef8fc72d9d0) Fixed QS forwarding in SynaptixDatastoreSession::getLinkedObjectsCountFor()

## [4.8.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.2...4.8.3) - 2021-10-25

### Commits

[`369e1e3`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/369e1e3081081f88df66c8fe2b27ad9dbce45cf0) Fixed bug in fragmentHelpers.js

## [4.8.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.1...4.8.2) - 2021-10-25

### Commits

[`adc3ab1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/adc3ab11a89fc11650e242cdcfb089c02aab6c4f) Fixed bugs in FragmentDefinition parsing in cases of :
- a FragmentSpread that includes an other FragmentSpread.
- a FragmentSpread situated deeper than first type (or connection) resolution.

## [4.8.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.1-beta.0...4.8.1) - 2021-10-12

### Merged

[`#78`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/78) Introspect all fragments only once, at top of graphql query

## [4.8.1-beta.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.0...4.8.1-beta.0) - 2021-10-08

### Commits

[`1bd9d80`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1bd9d80b206b64a315577725037742bea93e6039) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`cfed4a2`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cfed4a272d57c43407e0b15b298fabdd0fef020d) Removed Socket.io
Removed OwncloudClient.js and dependencies
Added LinkDefinition::excludeFromIndex option
Make ProjectDefinition.js inherited from DurationDefinition
[`30fd43b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/30fd43b5d52993e563524e99bd30b1af753621b2) Removed useless ACL classes
Added new ACL classes
[`4169201`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/41692018faf3b0f1b66df751c1a3913b1f1a1be4) Fixed tests.
[`282d209`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/282d20950b483770a7f90b85b096b1d55987e1da) Added "hasReadAccessGranted" filter on EntityDefinition.js.

## [4.8.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.0-beta.5...4.8.0) - 2021-10-08

### Merged

[`#77`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/77) ES requests optimizations to lighten index search load.

## [4.8.0-beta.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.0-beta.4.1-acl...4.8.0-beta.5) - 2021-10-07

## [4.8.0-beta.4.1-acl](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.0-beta.4...4.8.0-beta.4.1-acl) - 2021-10-07

### Commits

[`02b7660`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/02b7660e8fc129ba5de8bc08c30632c708f85bd0) **Breaking change:** Fixed unit tests
Added LinkDefinition::nestedIndexedFields and LinkDefinition::defaultValue fields
POTENTIAL BREAKING : Change session-wide cache to application-wide cache to share between SynaptixSession to store UserAccounts/Persons/Group memberships.
[`1e6f73b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1e6f73b484d3d9f7974f2205036d6cf4389e3996) Removed useless ACL classes
Added new ACL classes
[`00b036a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/00b036a62d5d76d9f4ecf82c9536fc8517a4fcab) Fixed tests.
[`2edcb18`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2edcb18826404a69ca887045e3d4c9d97117713b) Added "hasReadAccessGranted" filter on EntityDefinition.js.
[`7f31ffc`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7f31ffc2ce72647d3bb7cb5a71cc39a65bd9951d) Handle case of reversed link in getLinkedNodes with linkPath + add label ..._locales in fragment indexed keys
[`8faaded`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8faaded329c372cbdb7bd9943339cfdf4cbabba2) Improved LinkPath.js. Now possible to clone it, and handle property() and step() methods with existing linkPaths.
[`dcd27c5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/dcd27c53af636169bf2233287b905498404de904) Improved GraphQLQuery::generateFieldName() method to handle "y" an "us" field terminations.

## [4.8.0-beta.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.0-beta.3...4.8.0-beta.4) - 2021-09-17

### Commits

[`e962aa6`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/e962aa62eeffa83950c47c60af792fd8297cd7fc) Fixed bugs in ES request optimizations

## [4.8.0-beta.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.0-beta.2...4.8.0-beta.3) - 2021-09-17

### Commits

[`9f7b933`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9f7b933133b55cc0c69cf3c4d88be1b4fbb2cc61) Fixed bugs in ES request optimizations. FragmentDefinitions where wrongly used.

## [4.8.0-beta.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.8.0-beta.1...4.8.0-beta.2) - 2021-09-16

### Commits

[`0ceccb9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0ceccb9893928635fd4b87cf1b4fc8cd446f229c) Fixed bugs in ES request optimizations dealing with linkPath filtering.
[`8c83b53`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8c83b53fdf2fc8ff165c748ce12ed537182e9b08) Fixed bugs in ES request optimizations dealing with linkPath filtering.

## [4.8.0-beta.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.43...4.8.0-beta.1) - 2021-09-14

### Commits

[`3abb10e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3abb10ee08cbd638379c41e38e6229cc5394547d) - Improved ES requests during step nodes, fetch only documents ids instead of reconstructing the whole object.
- Avoid extra ES requests when only "id" or "uri" GraphQL nested object property is asked (for a connection too)
[`75a52da`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/75a52dacb02b3439a333047ed4d9898e9eceefda) Improved FragmentDefinition.js during ES request by using "includes" clause on selected GraphQL properties.

## [4.7.43](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.42...4.7.43) - 2021-09-13

### Commits

[`10f69a6`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/10f69a629e4e37862a1c9d4e4a56fd1a77735621) Added some colors on AMQP debugging

## [4.7.42](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.41...4.7.42) - 2021-09-12

### Merged

[`#76`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/76) Fix cors + fix must exists filter

## [4.7.41](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.40...4.7.41) - 2021-09-09

### Commits

[`6fcc6d0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6fcc6d0f7e465d2c7938ee482d8c3c52826ac021) Fixed bug in linkPath resolution when dealing with PropertyFilterStep.js. Session used to treat that as fulltext search that returns unaccurated results.
[`5490f92`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5490f92318a75ee80f76239192569ddd0830be26) Reverted generator.js hack and added "types" as EntityDefinition.js literal to enable filtering on.

## [4.7.40](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.39...4.7.40) - 2021-09-02

### Merged

[`#75`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/75) ES search improvement and bugfix.

### Commits

[`17aebca`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/17aebca0915af3fc03c1ca40a96f46903aa2fd6c) Added an ES multi-index wildcard search on EntityDefinition.
[`4cdea76`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4cdea769f6f915307e0960f16a42d44938a04ed2) Fixed bug when GraphQL nested link filter value is an array of ids. Eg : `filter:["hasProject.hasProjectContribution.id": ["id1", "id2"]]`

## [4.7.39](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.38...4.7.39) - 2021-08-31

### Merged

[`#74`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/74) Consider case when origin not set in request header

## [4.7.38](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.37...4.7.38) - 2021-08-31

### Merged

[`#73`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/73) Add authorization header on apollo response, for remote clients
[`#72`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/72) Fix parsing of ES hits for nested plural links

## [4.7.37](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.36...4.7.37) - 2021-08-27

### Commits

[`2e8d816`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2e8d816c413ec77f5b248879cbd7d2ae1e4b94e2) Changed relation between mnx:ProjectContribution =&gt; mnx:Project to plural.

## [4.7.36](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.36-patched.3...4.7.36) - 2021-08-24

## [4.7.36-patched.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.36-patched.2+0ddcdab5...4.7.36-patched.3) - 2022-10-07

### Commits

[`f5e2b32`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f5e2b323f6994019dfedf3012811aab726061a33) Fixed nested filtering
[`f3ccc50`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f3ccc50e654e60795eea84a0f02d837dfb8a57d0) Fixed missing imports

## [4.7.36-patched.2+0ddcdab5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.36-patched+0ddcdab5...4.7.36-patched.2+0ddcdab5) - 2021-10-27

## [4.7.36-patched+0ddcdab5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.36-patched+51d0a59e...4.7.36-patched+0ddcdab5) - 2021-10-27

### Commits

[`8b5bf5c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8b5bf5c7ea9f59e671b9eaab92c9344a33df75e5) Fixed QS forwarding in SynaptixDatastoreSession::getLinkedObjectsCountFor()

## [4.7.36-patched+51d0a59e](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.35...4.7.36-patched+51d0a59e) - 2021-10-27

### Commits

[`51d0a59`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/51d0a59e4f0f249dbe1514855ab18b0f5cefa7f7) Added a forceFallbackIfEmpty option to SynaptixDatastoreSession::getObjects

## [4.7.35](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.34...4.7.35) - 2021-08-17

### Merged

[`#71`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/71) Delete multiple edges

### Commits

[`4abccb6`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4abccb633c843080719b55d410d41073168411af) Proposal for removing some edges at once
[`54819f0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/54819f0b8321e0d4d240be0c3be95b3ab9417cf4) Add declaration of exist/not exists filter step
[`c7676cc`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c7676ccbc7bfdbd2378d93034ea59d2a1b604594) Fix deletion of obsolete nested edges on node update (concerns not plural link)
[`9c44605`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9c44605032457f5197b7614bdd56e500e720d08f) Set yarn version to berry, and downgrade, in gitlab-ci

## [4.7.34](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.33...4.7.34) - 2021-08-16

### Commits

[`4f44656`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4f44656ed5dd192a283b80c5a3a5cc01cd5d5948) Fixed and tested stringifyUpdateFromSparqlPattern.js method to included named graph only around INSERT part of SPARQL query.
[`9589d33`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9589d33b207310e29c528e5c2114d1e5d1429c01) Added creatorFullName field to EntityDefinition.js
[`b9a0e8b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b9a0e8b78a730c7cab8ff5060e832afd798d70cd) Revert some changespushed by mistake
[`925ce34`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/925ce347653d2ac16ad0d5e04715ef483cab7ec8) Force Yarn downgrade on CI

## [4.7.33](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.32...4.7.33) - 2021-07-01

### Merged

[`#70`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/70) Some fixes and proposals

### Commits

[`b1eae12`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b1eae1243465941080b1672839b8b752a3cdc6d3) Allow to customize props of amqp queues on NetworkLayerAMQP.listen
[`af7576e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/af7576ee5558aada646d741a0ff63f73922bd270) Merge from master
[`6fd1cf1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6fd1cf1a1b639579870f0d26b59f45b7a2b45400) Some proposals (see below);
Consider plural option on gql schema generation for literals;
Add nestedIndexedFields option to LinkDefinition for nesting some properties on indexation;
Fix deletion of old triple on update (for no plural link, and that does not clean potential zombies)
[`9d7072d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9d7072d4a8aaa6d3bfd5c32275acfac0ccf5a277) Fix test

## [4.7.32](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.31...4.7.32) - 2021-07-01

### Commits

[`d2ac0d4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d2ac0d4a51c0f8dcd2a3364d48135f2ff5519912) Add percolation on IndexControllerV7Native.js

## [4.7.31](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.30...4.7.31) - 2021-06-29

### Commits

[`9f2fa15`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9f2fa15decdf36c9276e09850f30aeba9d4edb4d) Fixed GraphQL substituted modelDefinition GraphQL input. Inherited properties were not passed.

## [4.7.30](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.29...4.7.30) - 2021-06-24

### Commits

[`4776ec8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4776ec8e89c65f2a9e827b5447fc585e6b98f8c1) Fixed bug in nested singular link deletion.

## [4.7.29](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.28...4.7.29) - 2021-06-18

### Commits

[`57f8d62`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/57f8d6296ab7481be727a771c3a973c9d55df8c4) Updated ConceptDefinition.js
Added plural scalar types in GraphQL generator

## [4.7.28](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.27...4.7.28) - 2021-06-08

### Commits

[`0bbcb75`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0bbcb75c69b6e53e06b3bfc41f12f7b0bf90c154) Fixed missing parsing for '*' value in GraphQL literal/label filters.

## [4.7.27](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.26...4.7.27) - 2021-06-04

### Commits

[`3c2845f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3c2845facd6c920c7d6cd4c33ce90cd587e83a32) Add a standard method SynaptixDatastoreSession.getDataPublisher() to get an direct access to AMQP bus.

## [4.7.26](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.25...4.7.26) - 2021-05-10

### Commits

[`7ff30d5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7ff30d5a11c1c9c5c8ccf7715e10b1f9112dfd00) Fixed bug on ES search boost attributions.

## [4.7.25](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.24...4.7.25) - 2021-05-10

### Commits

[`f6dfa96`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f6dfa9604e311c8132bf0f4d3296aa7c76639c55) Fixed regression on link filters in case of array of ids

## [4.7.24](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.23...4.7.24) - 2021-04-26

### Commits

[`fe9be4d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fe9be4d638dc31f3a634d0dae508e67ed67e3cec) Enhanced model definitions.

## [4.7.23](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.23-1...4.7.23) - 2021-04-15

## [4.7.23-1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.22...4.7.23-1) - 2021-04-26

### Commits

[`499ee34`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/499ee3442e6c9da78db1a9b7b7830c556d6be11c) Added new ShieldError.js to use un GraphQL shields blocking. This type of Error will be skipped from logs.
[`6fd1cf1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6fd1cf1a1b639579870f0d26b59f45b7a2b45400) Some proposals (see below);
Consider plural option on gql schema generation for literals;
Add nestedIndexedFields option to LinkDefinition for nesting some properties on indexation;
Fix deletion of old triple on update (for no plural link, and that does not clean potential zombies)
[`9d7072d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9d7072d4a8aaa6d3bfd5c32275acfac0ccf5a277) Fix test
[`fb52023`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fb52023115cb3b66f89796ea8e62197f9aee6c23) Enhanced ConceptDefinition.js

## [4.7.22](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.21...4.7.22) - 2021-04-13

### Commits

[`efeb6a5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/efeb6a52c776a2317c069fa16a4d82b35555e182) Fixed ConceptDefinition.js property

## [4.7.21](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.21-1...4.7.21) - 2021-04-06

### Merged

[`#69`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/69) Fix link paths navigation on getObjects in index controller + fix closing of ES client connections

### Commits

[`f8b0ad7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f8b0ad76d8bd1213ee50350bf854d740f2ca08bb) Close ES client connection at the end of session + rewrite index controller fix on nodes linked with LinkPath
[`1f2b5ba`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1f2b5ba8c4379b7fd3fc318d1160e2d9e20289ac) Fix link paths navigation on getObjects in index controller;
(or perhaps change indexing process at elzeard -_-')
[`9f6dd92`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/9f6dd92464553e77156e9be29bff9793f8f283ba) Fixed tests
[`d4d9706`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d4d970614fd2542f12b8dfb9a628f8922a950cd6) Add some error handling on ES client connection closing issue.

## [4.7.21-1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.21-0...4.7.21-1) - 2021-04-06

### Commits

[`7bf8534`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7bf85348c64a0fd1ae850d9dcb8d371eb27c47e0) Fix unit tests on graph publisher
[`0e39fc6`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0e39fc67ee37b69451547c89d96c835cccc4852a) Merge index-controller-patch
[`3b1acc4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3b1acc406d00fd4c44c3e04308f68e717e61da90) Updated Skos datamodel.
[`169a48d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/169a48d967912076904ea7f52ad8c087b885e5fc) Close ES client connection at the end of session + rewrite index controller fix on nodes linked with LinkPath
[`fe4b79a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fe4b79acfd23740dbe20f5468e8ecb735e740722) Updated IndexControllerV7Publisher.js and GraphControllerPublisher.js to comply with last Synaptix Protocol.
[`fb9455a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/fb9455a3b72020c91a40253183cdb234ff69b5d3) Fixed ES count query (limited to 10000 if track_total_hits is not provided)
Reusing ES client in case of INDEX_VERSION=NATIVE.
[`f5b9c3c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f5b9c3c3335feee28427c1fb01274eca50672ae0) Fix link paths navigation on getObjects in index controller;
(or perhaps change indexing process at elzeard -_-')

## [4.7.21-0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.20...4.7.21-0) - 2021-03-23

### Commits

[`1f25729`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1f25729ad24ef236c6fdab6f0fe4724c315cc142) Fix link paths navigation on getObjects in index controller;
(or perhaps change indexing process at elzeard -_-')

## [4.7.20](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.19...4.7.20) - 2021-03-19

### Commits

[`62155d1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/62155d1dacc5b39269651b3e01a8ad682644a1e0) Upgraded reified model definitions to mark them as deleted if one of reified object is deleted.

## [4.7.19](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.18...4.7.19) - 2021-03-15

### Commits

[`a9d89d9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a9d89d94123f3bfa45f4170eef7ac89ea9f10a94) Fixed bug on GraphQL cache that can leak information between users.

## [4.7.18](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.17...4.7.18) - 2021-03-10

### Merged

[`#67`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/67) cannot read getRdfAliasDataProperty of undefined

## [4.7.17](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.16...4.7.17) - 2021-03-09

### Merged

[`#66`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/66) Native ES driver added to bypass AMQP bus.

### Commits

[`5d59386`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5d59386204c050939b08386e60ea97d5130197e9) [Experimental] Added a native IndexController driver to bypass AMQP.
[`05efbe7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/05efbe7c02f41c54a721ea329790388735e5d175) Added SynaptixDatastoreSession::grantUserInGroup method.
[`8d9259e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8d9259e2c274832c88f5683439c3bdb2a480fa58) Added SSOApiClient::getUsers() method to get all registered users on SSO.
Added SSOControllerService::synchronizeUserAccount() method to apply a SSOUser synchronization between SSO store and Graph store.

## [4.7.16](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.15...4.7.16) - 2021-03-05

### Merged

[`#65`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/65) Fix session updateObjects method
[`#64`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/64) Fix graphql access rules for model links

## [4.7.15](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.14...4.7.15) - 2021-03-01

## [4.7.14](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.13...4.7.14) - 2021-02-25

### Commits

[`4b9f3a7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4b9f3a72e5931812119040e47cb6c2a1c1e2adb4) Added isReadOnlyModeDisabledRule.js to block every mutation based on READ_ONLY_MODE_ENABLED env.
Fixed GraphControllerServer optional LinkPath properties.

## [4.7.13](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.11...4.7.13) - 2021-02-22

### Commits

[`0910e62`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0910e621a95027b16c8234c18c3019aec00706dc) - Added LabelDefinition.js isPlural capabilities to returns an array of strings for localized scalar properties in GraphQL.
- Added concepts indexation fields for all instances of EntityDefinition.js
[`a1b4296`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a1b4296fe6b8344bb5dfda21b946a788da56d8e2) Added methods SynaptixDatastoreSession::isLoggedUserInGroup() and SynaptixDatastoreSession::isLoggedUserInAdminGroup()
[`7679123`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/767912324f23ea02366ac8cce55a2638fcaefbaa) Removed @babel/polyfill
Fixed bug on IndexControllerService.js for count nodes queries of inheritable ModelDefinition. See comment in source code.

## [4.7.11](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.11-2...4.7.11) - 2021-02-16

### Merged

[`#63`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/63) Adding mutation subgraph in mnx:Action.

### Commits

[`2323eff`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2323effcf2d11505366dbad5f8c86c48ec7a9b02) - Removed Update actions when no snapshot is created to preserve space.
- Added @type in parseObjectInputToJsonLdNode.js JSONLD links
[`f1bdcea`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f1bdcead64fab3c7cb309368bd315c1a6731dc6d) Fixed typo in parseObjectInputToJsonLdNode.js. URI properties are in @id properties and not @value.
[`3b0a188`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3b0a18808181d58d525aa6ff3cec8fe3ccffb6e6) Fixed missing @reversed property in parseObjectInputToJsonLdNode.js for reversed link or linkDefinition with getRdfReversedObjectProperty property.

## [4.7.11-2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.11-1...4.7.11-2) - 2021-02-09

## [4.7.11-1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.11-0...4.7.11-1) - 2021-02-09

## [4.7.11-0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.10...4.7.11-0) - 2021-02-08

### Commits

[`382182b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/382182b099688ed9396142af6f154722f153eced) **Breaking change:** Fixed multiple bugs in actions handlers.
Added snapshots (missing portion for now).
BREAKING CHANGE: GraphMiddleware.js methods has been renamed.
Added DEFAULT_LOCALE env to set it globally.
[`da8dcae`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/da8dcaeda67cbeed2e7909f6618f0d5bce7c6ec9) Optimized parseObjectFromEsHit.js to increase resolution speed. (Might be more optimized with fragments)
[`481307b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/481307b179d73b12ae0a7bd4abfb77ad346f3f75) Optimized SPARQL update queries by gathering subjects.
[`3fda657`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3fda657c401e3334b4058819369c6f8e07edb3cc) Fixed mnx:Action hasEntity link.
Fixed snapshot resolver.

## [4.7.10](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.9...4.7.10) - 2021-02-05

### Commits

[`b9b9e25`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b9b9e25ff121223ad08527c5d29674d962c91115) Added `filteringArgs` parameter to autogenerated objectCount GraphQL types.

## [4.7.9](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.8...4.7.9) - 2021-02-02

### Merged

[`#62`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/62) Use the same type literal mapper for types and inputs in graphql schema generator

## [4.7.8](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.7...4.7.8) - 2021-01-15

### Fixed

## [4.7.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.5...4.7.7) - 2020-12-19

### Commits

[`1bfa465`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1bfa46503213394e4d1fb064dd212726b12ecd22) Fixed missing client secret in OAuth2RefreshTokenStrategy.js

## [4.7.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.4...4.7.5) - 2020-12-15

### Commits

[`3385188`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/33851885a359f2905a4d729ef0f8ef186d4453c3) Moved GraphQL Input generation into generator.js.
That enables to gather narrower/broader properties of a model definition, ensure that there is no duplicate and add some extra description.

## [4.7.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.3...4.7.4) - 2020-12-14

## [4.7.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.2...4.7.3) - 2020-12-14

### Commits

[`4733ce2`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4733ce2cc360d68e6d14ca91f0be5f6404236106) Removed `passport-oauth2-middleware` deprecated dependency to integrate code inside Synaptix.js

## [4.7.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.1...4.7.2) - 2020-12-11

### Commits

[`c2798a8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c2798a8e77043bc66e3757f604d21d84ebad42ca) Fixed GraphQL upload by circumventing Apollo Server bug with NodeJS &gt; 13
@see https://github.com/apollographql/apollo-server/issues/3508#issuecomment-662371289

## [4.7.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.7.0...4.7.1) - 2020-12-08

### Commits

[`da0935f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/da0935f944a409d9488a8d7f8087c1647d1b702a) AMQP callback/listening queue are now unique, exclusive and autoDelete. This allows to spawn multiple nodeJS instances (in K8s for instance) to increase performance.
[`2d3219e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2d3219efce04da8e321a6873118b35a4237e8158) Releasing 4.7.1
[`3b2cd6c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3b2cd6c8c0ed84b0a02d3604ea262425e24b8841) Releasing 4.7.1

## [4.7.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.5...4.7.0) - 2020-12-03

### Commits

[`cc2eb0b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cc2eb0bc298cf28abca6fc7f91651582b350aa82) Switched to Apollo Server (instead of combination of GraphQLExpress, GraphQLPlaygroundMiddleware...)
Updated env-var version.
[`f902f9d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f902f9d39c5e703400246d86f5ed415d673bea30) Fixed gitlab-ci to work with yarn berry.

## [4.6.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.4...4.6.5) - 2020-12-01

### Commits

[`91dfccc`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/91dfccc88e2089490b90a5188a31285e41d26e95) Added extraInputArgs to GraphQLUpdateMutation (like GraphQLCreateMutation)
[`d73becd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d73becd30fe8bcfad9c900d0f6c992d7fc96c594) Fixed tests

## [4.6.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.3...4.6.4) - 2020-12-01

### Commits

[`b03c68c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b03c68c8db33de9a80a8a013df33347d2a0e05e4) Fixed missing await clause in mutation resolvers.

## [4.6.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.2...4.6.3) - 2020-11-30

### Merged

[`#61`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/61) Add typescript support for datamodel files generator

### Commits

[`02af066`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/02af0662583c160545aa429b607abf0126d70e62) Add typescript renderer for datamodel code generator
[`447add1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/447add18241b55b5844184ace3803c73a764260d) Add option for generating typescript model files
[`97e8f17`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/97e8f17438a5206b75e8dbd107644bab4820c9fe) Make an substitute ModelDefinition have the same isExtensible() property by default than the substituted one.
Changed generator.js. When an interface/extensible ModelDefinition is substituted, the related GraphQL Interface is generated only during generation of substitute model definition. (That's explain point 1)
Changed SynaptixDatastoreRdfSession::getGraphQLTypeForId(). Fast way based on ModelDefinitionAbstract::getGraphQLTypeForURI() is prepended before asking the graph.
[`6d60022`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6d60022a5b52757842692fbdf2d73bfdfa9fcbb9) Fix location of typescript definitions generator script

## [4.6.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.1...4.6.2) - 2020-11-29

### Commits

[`f5f6455`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f5f6455441a6750cf56ff87df5e1510c003c22be) Added defaultValue to PropertyDefinitionAbstract.js
Fixed bug in `ModelDefinitionAbstract::generateGraphQLInputProperties()` when a ModelDefinition extends several model definitions with same parents. (GraphQL fields defined once issue)
Fixed bug in generator.js when a ModelDefinition extends several model definitions with same parents. (GraphQL fields defined once)
Added LinkPath::append() method to extend a LinkPath with another.

## [4.6.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0...4.6.1) - 2020-11-19

## [4.6.0](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.5-wip-20200925...4.6.0) - 2020-11-16

### Merged

[`#60`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/60) Merge 4.6 developments
[`#59`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/59) MNX generic datamodel is now fully autogenerated.
[`#58`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/58) Fix hasAffiliation link + check tooling for data model
[`#57`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/57) Fix node deletion on graphController.removeNode
[`#56`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/56) Clear dataloader cache when run an update query in graph publisher
[`#55`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/55) Fix resource input
[`#54`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/54) Upgrading tagging
[`#53`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/53) Fixed AgentInput adding missing and concatenated PersonInput (Try to automate this later)

### Fixed

### Commits

[`7f372e9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7f372e902316d6f1382d339ecaec2ed77f848399) mnx-project, mnx-agent, mnx-contribution have now an auto-generated GraphQL schema.
[`0cefda6`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0cefda674a159f26838a8ed52aa6530b6eb9a6b5) mnx-common mnx-skos have now an auto-generated GraphQL schema.
[`eb65cb2`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/eb65cb2e49f35aa17a513cf836ee8cad4320d280) Removed useless Model classes and deprecated ModelIndexMatchers
[`5a5583b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5a5583b0c25721ba0dc0be51d0b7b87783f36548) mnx-time and mnx-geo have now an auto-generated GraphQL schema.
[`eff0572`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/eff05728b38930c523a18a78dd213459133b33a5) Add script for checking diffs between ontology and model definitions
[`6eec83e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6eec83ef3d51eb4af3c658270d0a009a4ebc7965) Add ModelDefinitionAbstract::isExtensible() and ModelDefinitionAbstract::substituteModelDefinition() to resolve the problems of :
- Extending a GraphQL type using GraphQL interfaces
- Substituting a GraphQL type by another one that extends it.
Solution 1 experimented to ConceptDefinition.js
Solution 2 experimented to PersonDefinition.js
[`f3db1c8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f3db1c8516326b0de355fcb86b96f8a171c4afb9) Fixed misconfigs in MNX datamodels.
[`69f26a9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/69f26a93bccc1bf0b3e661e6772a133c096bddba) Upgraded TaggingDefinition.js with a GraphQLTypeDefinition.
[`bde599f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/bde599f9dbe63dd486dffe846523f467434b8e5e) Fixed missing links/typos in datamodel.
[`3aaf87f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3aaf87f2a12e8f3f9572ab3c6615863ea6d686e7) Added missing `graphQLPropertyName` properties in datamodels.
[`1d315ce`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1d315ce4c7a70de52d8bada61db83a1a9c9e552c) Added ModelDefinitionAbstract::isSubstituted() and ModelDefinitionAbstract::getSubstitutionModelDefinition() methods.
[`1ce9106`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1ce910664c3ab0dbd412f1723ed4c86bc021f72f) Fixed Resource input.
[`8638885`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/863888526dce19161328515771e39071906a9625) Remove non used dependencies using depcheck.
[`c596931`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c596931b675d7e61a4ebc1f8da972637981fda25) Removed some searching fields in ConceptDefinition.js
[`94b7651`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/94b7651ab73b2d8fc91c78fac9566b180caa9da2) Updated UserAccountDefinition.js to make it searchable.
[`d39036e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d39036e34f03d029c3dd7d0bf32b3889e073c4d6) Fixed missing filter transmission in SynaptixDatastoreSession::getLinkedObjectsCountFor()
[`01d63f4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/01d63f41c436a72dc34fa733b44aedc73a1489cf) Fixed broken unit tests
[`4436170`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/44361702e023efcdb626ce6d2ca140cf28da3d30) Fixed typos in graphQLPropertyName|graphQLInputName properties un datamodel link definitions.
[`7e6611c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7e6611c0cefb5f6a16c667347ed3fe07fb25e087) Removed hasInvolvement link in AgentDefinition.js
[`ca6178b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ca6178b623c87d66afb142397cf0faa7ec971e7b) Add displayName property to AgentDefinition.js
[`d50e1cd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d50e1cdee0a78ab015cf1d129e1ff592f80766b2) Set PersonInterface as return type on me resolver;
Set OrganizationDefinition as extensible
[`8fce7f4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8fce7f4922071aa47e6a61bfbd553eed70f2689b) Separated RABBITMQ_LOG_LEVEL variable into two debug modes :
- DEBUG : To log AMQP requests payloads.
- VERBOSE : To log AMQP requests AND responses payloads.
[`b901141`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b9011412c6c5720391f977d87e84f194a7adae05) Upgraded ConceptDefinition.js
[`cef4fc7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cef4fc7414a5a75593eacf8be1140e2a9335901a) Fixed GraphQL schema generator to enable resolver overriding in the case of herited link (see AffilationDefinition)
[`44da874`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/44da874bdcebf368499ca0321702dd7853434229) Fix getLinks in Person and Organization definitions
[`b1b71fd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b1b71fda8fbbb74f3ac2ef2b72efab3da8a7f54c) Added label in AddressInput GraphQL type
[`7a2c04c`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7a2c04c2ab34813483c8ca1140e53bbe31dc4270) Fix a miss from previous fix
[`88889bb`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/88889bbc15b6526012e9eff9ea10e0eac4550001) Fixed broken normalization in SynaptixDatastoreRdfSession:normalizeAbsoluteUri() in edge cases.
[`2fa4953`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2fa49537cddd2bb49e8f6c882f064ecd6738ab41) Remove console.log [ignore]
[`2f401dd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2f401dd3965fb23c4b6ac645effe3b26871c0033) Fixed circular dependency import

## [4.6.0-rc.5-wip-20200925](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.5-wip-20200924...4.6.0-rc.5-wip-20200925) - 2020-09-25

### Commits

[`7940632`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7940632c43e7663a0898af0339dcb0a2e8fa8b9c) Add ModelDefinitionAbstract::isExtensible() and ModelDefinitionAbstract::substituteModelDefinition() to resolve the problems of :
- Extending a GraphQL type using GraphQL interfaces
- Substituting a GraphQL type by another one that extends it.
Solution 1 experimented to ConceptDefinition.js
Solution 2 experimented to PersonDefinition.js
[`5392066`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/53920668341317394ab4f4e406e42366135a147b) Set PersonInterface as return type on me resolver;
Set OrganizationDefinition as extensible
[`6459276`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/64592766d4c698d798a00e0f3438ed5a8838f8ad) Upgraded ConceptDefinition.js
[`b8e3567`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b8e3567adbb07bb84c2985a4211cd9349c2771c5) Fixed auth mechanism using JWT token in query param
[`cadfbdf`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cadfbdfa8ac74a08edcdc207eacc46986d509e54) Fixed circular dependency import

## [4.6.0-rc.5-wip-20200924](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.5-patched...4.6.0-rc.5-wip-20200924) - 2020-09-24

### Commits

[`53f31e8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/53f31e8ab4625d31c4cc7bc44cb9dd551f591461) Add ModelDefinitionAbstract::isExtensible() and ModelDefinitionAbstract::substituteModelDefinition() to resolve the problems of :
- Extending a GraphQL type using GraphQL interfaces
- Substituting a GraphQL type by another one that extends it.
Solution 1 experimented to ConceptDefinition.js
Solution 2 experimented to PersonDefinition.js
[`6059c37`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6059c3747f0026c13af10229c2ccb3fca8dab61b) Set PersonInterface as return type on me resolver;
Set OrganizationDefinition as extensible
[`115fa53`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/115fa5308226bdcb574c052bc5d626444c0d8180) Upgraded ConceptDefinition.js
[`43c54d4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/43c54d455d3d5707fab469634eb1b2c4a22ea74f) Fixed circular dependency import

## [4.6.0-rc.5-patched](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.11...4.6.0-rc.5-patched) - 2020-09-15

### Merged

[`#52`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/52) Fix me resolver orga extensible

### Commits

[`c348be1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c348be1188cff1edffc1881d24e73d71210e0113) Add ModelDefinitionAbstract::isExtensible() and ModelDefinitionAbstract::substituteModelDefinition() to resolve the problems of :
- Extending a GraphQL type using GraphQL interfaces
- Substituting a GraphQL type by another one that extends it.
Solution 1 experimented to ConceptDefinition.js
Solution 2 experimented to PersonDefinition.js
[`cb8419f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cb8419faa1b97827f9da4642614f9594ec931120) Set PersonInterface as return type on me resolver;
Set OrganizationDefinition as extensible
[`5002d1d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5002d1d08e2fcb4b5643ece473126a0af4b99b89) Upgraded ConceptDefinition.js
[`a007b9b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a007b9be9dc41c53e36c40467f82543b39debc30) Fixed circular dependency import

## [4.6.0-rc.11.patched.11](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.10...4.6.0-rc.11.patched.11) - 2020-11-12

### Commits

[`8638885`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/863888526dce19161328515771e39071906a9625) Remove non used dependencies using depcheck.
[`ca6178b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ca6178b623c87d66afb142397cf0faa7ec971e7b) Add displayName property to AgentDefinition.js

## [4.6.0-rc.11.patched.10](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.9...4.6.0-rc.11.patched.10) - 2020-11-10

### Commits

[`f3db1c8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f3db1c8516326b0de355fcb86b96f8a171c4afb9) Fixed misconfigs in MNX datamodels.
[`cef4fc7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cef4fc7414a5a75593eacf8be1140e2a9335901a) Fixed GraphQL schema generator to enable resolver overriding in the case of herited link (see AffilationDefinition)

## [4.6.0-rc.11.patched.9](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.8...4.6.0-rc.11.patched.9) - 2020-11-10

### Commits

[`bde599f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/bde599f9dbe63dd486dffe846523f467434b8e5e) Fixed missing links/typos in datamodel.
[`3aaf87f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/3aaf87f2a12e8f3f9572ab3c6615863ea6d686e7) Added missing `graphQLPropertyName` properties in datamodels.

## [4.6.0-rc.11.patched.8](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.7...4.6.0-rc.11.patched.8) - 2020-11-03

### Merged

[`#57`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/57) Fix node deletion on graphController.removeNode
[`#56`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/56) Clear dataloader cache when run an update query in graph publisher

## [4.6.0-rc.11.patched.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.6...4.6.0-rc.11.patched.7) - 2020-10-23

### Commits

[`88889bb`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/88889bbc15b6526012e9eff9ea10e0eac4550001) Fixed broken normalization in SynaptixDatastoreRdfSession:normalizeAbsoluteUri() in edge cases.

## [4.6.0-rc.11.patched.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.5...4.6.0-rc.11.patched.6) - 2020-10-23

### Merged

[`#55`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/55) Fix resource input

### Commits

[`7f372e9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7f372e902316d6f1382d339ecaec2ed77f848399) mnx-project, mnx-agent, mnx-contribution have now an auto-generated GraphQL schema.
[`0cefda6`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/0cefda674a159f26838a8ed52aa6530b6eb9a6b5) mnx-common mnx-skos have now an auto-generated GraphQL schema.
[`5a5583b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5a5583b0c25721ba0dc0be51d0b7b87783f36548) mnx-time and mnx-geo have now an auto-generated GraphQL schema.
[`c596931`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c596931b675d7e61a4ebc1f8da972637981fda25) Removed some searching fields in ConceptDefinition.js
[`01d63f4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/01d63f41c436a72dc34fa733b44aedc73a1489cf) Fixed broken unit tests

## [4.6.0-rc.11.patched.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.4...4.6.0-rc.11.patched.5) - 2020-10-20

### Merged

[`#54`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/54) Upgrading tagging

### Commits

[`1ce9106`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1ce910664c3ab0dbd412f1723ed4c86bc021f72f) Fixed Resource input.

## [4.6.0-rc.11.patched.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.3...4.6.0-rc.11.patched.4) - 2020-10-16

### Merged

[`#53`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/53) Fixed AgentInput adding missing and concatenated PersonInput (Try to automate this later)

### Commits

[`69f26a9`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/69f26a93bccc1bf0b3e661e6772a133c096bddba) Upgraded TaggingDefinition.js with a GraphQLTypeDefinition.

## [4.6.0-rc.11.patched.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched.2...4.6.0-rc.11.patched.3) - 2020-10-16

### Commits

[`f8fc33d`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/f8fc33d8bf2c583b693e9a83892c9c45f041798f) Fixed AgentInput adding missing and concatenated PersonInput (Try to automate this later)

## [4.6.0-rc.11.patched.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11.patched...4.6.0-rc.11.patched.2) - 2020-10-15

### Commits

[`eb65cb2`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/eb65cb2e49f35aa17a513cf836ee8cad4320d280) Removed useless Model classes and deprecated ModelIndexMatchers
[`6eec83e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6eec83ef3d51eb4af3c658270d0a009a4ebc7965) Add ModelDefinitionAbstract::isExtensible() and ModelDefinitionAbstract::substituteModelDefinition() to resolve the problems of :
- Extending a GraphQL type using GraphQL interfaces
- Substituting a GraphQL type by another one that extends it.
Solution 1 experimented to ConceptDefinition.js
Solution 2 experimented to PersonDefinition.js
[`1d315ce`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1d315ce4c7a70de52d8bada61db83a1a9c9e552c) Added ModelDefinitionAbstract::isSubstituted() and ModelDefinitionAbstract::getSubstitutionModelDefinition() methods.
[`d50e1cd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d50e1cdee0a78ab015cf1d129e1ff592f80766b2) Set PersonInterface as return type on me resolver;
Set OrganizationDefinition as extensible
[`b901141`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b9011412c6c5720391f977d87e84f194a7adae05) Upgraded ConceptDefinition.js
[`2fa4953`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2fa49537cddd2bb49e8f6c882f064ecd6738ab41) Remove console.log [ignore]
[`2f401dd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2f401dd3965fb23c4b6ac645effe3b26871c0033) Fixed circular dependency import

## [4.6.0-rc.11.patched](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.11...4.6.0-rc.11.patched) - 2020-10-09

### Commits

[`a72bd79`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a72bd7926debf7ff17d9e8e0d6f51c5a09b11ef4) Add ModelDefinitionAbstract::isExtensible() and ModelDefinitionAbstract::substituteModelDefinition() to resolve the problems of :
- Extending a GraphQL type using GraphQL interfaces
- Substituting a GraphQL type by another one that extends it.
Solution 1 experimented to ConceptDefinition.js
Solution 2 experimented to PersonDefinition.js
[`2a85172`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2a851728975785ead9bda99b7ea5da6b8f367d54) Set PersonInterface as return type on me resolver;
Set OrganizationDefinition as extensible
[`24c4164`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/24c4164d4e0b4c8df2aff09a9734ae9525c3e40c) Upgraded ConceptDefinition.js
[`ba4cea5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/ba4cea5fd05a3a913d614f8843266b635cc4b54e) Remove console.log [ignore]
[`7ae957e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/7ae957e1b31af5a0a2cf2dba38b91ffc32142fd1) Fixed circular dependency import

## [4.6.0-rc.11](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.10...4.6.0-rc.11) - 2020-10-08

### Commits

[`94b7651`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/94b7651ab73b2d8fc91c78fac9566b180caa9da2) Updated UserAccountDefinition.js to make it searchable.

## [4.6.0-rc.10](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.9...4.6.0-rc.10) - 2020-10-08

### Commits

[`b1b71fd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b1b71fda8fbbb74f3ac2ef2b72efab3da8a7f54c) Added label in AddressInput GraphQL type

## [4.6.0-rc.9](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.8...4.6.0-rc.9) - 2020-10-06

### Commits

[`8fce7f4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/8fce7f4922071aa47e6a61bfbd553eed70f2689b) Separated RABBITMQ_LOG_LEVEL variable into two debug modes :
- DEBUG : To log AMQP requests payloads.
- VERBOSE : To log AMQP requests AND responses payloads.

## [4.6.0-rc.8](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.7...4.6.0-rc.8) - 2020-10-06

### Commits

[`d39036e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d39036e34f03d029c3dd7d0bf32b3889e073c4d6) Fixed missing filter transmission in SynaptixDatastoreSession::getLinkedObjectsCountFor()

## [4.6.0-rc.7](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.6...4.6.0-rc.7) - 2020-10-06

### Commits

[`b8e3567`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/b8e3567adbb07bb84c2985a4211cd9349c2771c5) Fixed auth mechanism using JWT token in query param

## [4.6.0-rc.6](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.5...4.6.0-rc.6) - 2020-09-21

### Commits

[`86e0b68`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/86e0b6822688d018295cc095414f947af79f73b2) Fixed graphInspector query with some OPTIONAL clauses.
Renamed generateJSDefinitionsFromOntology.js to generateDataModel.js
Renamed LinkDefinition.js property `relatedInputName` to `graphQLInputName` (`relatedInputName` still working but deprecated)
Removed unused stuff in utilities and add some organization in directories.

## [4.6.0-rc.5](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.4...4.6.0-rc.5) - 2020-08-25

### Commits

[`d64cb8f`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d64cb8fc2c5c79e5d7f5d50cefd47a096815b7a2) Added `ids` parameter in GraphQL connection search parameters that are converted into `idsFilters` and returns desired objects in desired order.
Updated TeamDefinition.js

## [4.6.0-rc.4](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.3...4.6.0-rc.4) - 2020-08-24

### Commits

[`2e636c0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2e636c00c724b591e5758f8c7d88cf56decba4d3) Fixed broken ES sorting when query string exists.
Fixed broken GraphStore updating.

## [4.6.0-rc.3](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.2...4.6.0-rc.3) - 2020-08-21

### Commits

[`5fb1c03`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5fb1c0369c3a84f6eb4d1a746e41da39fce0f49f) Added objects batch removal feature with new GraphQL mutation : RemoveEntities.graphql.js

## [4.6.0-rc.2](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-rc.1...4.6.0-rc.2) - 2020-08-17

### Commits

[`200f224`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/200f2247f957e19eb2c6318dabd2233e3b2e611d) **Breaking change:** POTENTIAL BREAKING : Renamed object property between UserAccountDefinition.js and PersonDefinition.js.

To be sure your data is compatible with this fix :
1. Import last version of MNX ontology.
2. See CHANGELOG.md =&gt; 4.6.0-beta.16.

## [4.6.0-rc.1](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.59...4.6.0-rc.1) - 2020-08-14

### Commits

[`866e9cb`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/866e9cbe271e8dd86fb41a2321f6532fbd53dd88) **Breaking change:** - Big refactoring to only use mocked model definitions in unit tests
- Fixed fragmentHelpers.js for interfaces
- BREAKING : Update EntityDefinition.js actions links to comply with mnx-model v1.0.5.
To be compatible, apply these patches in your RDF store :
`
PREFIX mnx: &lt;http://ns.mnemotix.com/ontologies/2019/8/generic-model/&gt;
PREFIX prov: &lt;http://www.w3.org/ns/prov#&gt;
INSERT {
?p mnx:hasCreation ?a.
} WHERE {
?p prov:wasGeneratedBy ?a.
?a a mnx:Creation
}
`

`
PREFIX mnx: &lt;http://ns.mnemotix.com/ontologies/2019/8/generic-model/&gt;
PREFIX prov: &lt;http://www.w3.org/ns/prov#&gt;
INSERT {
?p mnx:hasUpdate ?a.
} WHERE {
?p prov:wasGeneratedBy ?a.
?a a mnx:Update
}
`

`
PREFIX mnx: &lt;http://ns.mnemotix.com/ontologies/2019/8/generic-model/&gt;
PREFIX prov: &lt;http://www.w3.org/ns/prov#&gt;
INSERT {
?p mnx:hasDeletion ?a.
} WHERE {
?p prov:wasGeneratedBy ?a.
?a a mnx:Deletion
}
`
[`42818a8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/42818a8941721fce71fb47f6e9731ab2db5f63ca) Fixed fragmentHelpers.js when GraphQL type is a legacy named GraphQL Interface.

## [4.6.0-beta.59](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.58...4.6.0-beta.59) - 2020-08-11

### Merged

[`#51`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/merge_requests/51) Add qsFuzziness + fix filtering arg parsers

### Commits

[`6f28111`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6f2811101cb9ee25f5c1712e0e40d30f484bd3b2) WIP Added some documentation on indexation.
Added gte/gt/lte/lt PropertyFilter capabilities on IndexControllerService (missing on GraphControllerService)
[`c53ee8b`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/c53ee8bbf43eb0ea92ef88b2afcf22fce98d2133) Fixed bug in fragmentHelpers.js to retrieve correct ModelDefinition based on GraphQL type.
[`aaade00`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/aaade00f4750ea6ffdb9cfec77fcb9a98714b46a) Add qsFuzziness argument on connection resolvers (for fuzzy ES setting);
Fix filtering args parsing

## [4.6.0-beta.58](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.57...4.6.0-beta.58) - 2020-07-03

### Commits

[`cac3f5e`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/cac3f5e17dfe047053aa5cfbaa2286d7fd537a36) Fixed typo in GraphQL schema generator.js
Added "any" property on PropertyFilter.js to return document where property exists (or not with "isNeq=true")

## [4.6.0-beta.57](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.56...4.6.0-beta.57) - 2020-07-02

### Commits

[`662d4e8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/662d4e84e21b273198c187ff4d9769a2c11b5778) Updated mnxProject datamodel.
Added LinkDefinition.js `graphQLPropertyName` property used in GraphQL generator to override `linkName`.
Updated GraphQL generator.js and GraphQLQuery.js to add related "***Count" connection properties.

## [4.6.0-beta.56](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.55...4.6.0-beta.56) - 2020-06-29

### Commits

[`4e2fed2`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4e2fed28659358bab5d8e0f04b72fc9d89bf1ab3) Removed console.log [ignore in changelog]

## [4.6.0-beta.55](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.54...4.6.0-beta.55) - 2020-06-26

### Commits

[`2a4afbd`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/2a4afbd755b0702fedca197f2c22b660570c788f) **Breaking change:** - Added an "args" parameter to SynaptixDatastoreSession::getObject method. Mostly required to add filters.
- Added ES scriptsFields notion in IndexControllerService and merge results with Model props.
- Added description parametes in PropertyDefinitionAbstract to add documentation directly in the ModelDefinition. Usefull to generate better GraphQL documentation.
- Potential BREAKING. Fixed model definitions generator. GraphQL EntityInterfaceProperties is prepended to a GraphQLType only if its related modelDefinition is a descendant of EntityDefinition.

## [4.6.0-beta.54](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.53...4.6.0-beta.54) - 2020-06-26

### Commits

[`a662711`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/a6627111cbcc48a20315eb413e6f781984f600a2) Add generation scripts to bin.

## [4.6.0-beta.53](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.52...4.6.0-beta.53) - 2020-06-25

## [4.6.0-beta.52](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.51...4.6.0-beta.52) - 2020-06-22

### Commits

[`5292f69`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5292f69af73b23c403c1727e714294d567971800) Cleaning Mnx datamodels. Removing useless `pathInIndex` to have same props in DB and Index.

## [4.6.0-beta.51](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.50...4.6.0-beta.51) - 2020-06-22

## [4.6.0-beta.50](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.49...4.6.0-beta.50) - 2020-06-22

### Commits

[`5aa89c7`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/5aa89c7cae332d7f829cf9ff155aae17b74f2882) Fixed test [ignore in changelog]

## [4.6.0-beta.49](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.48...4.6.0-beta.49) - 2020-06-19

### Commits

[`6d7c2d1`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/6d7c2d12109ac223a995266bf6f67daa47fafcda) Fixed bug in GraphQLSchema generator.js when parentHierarchy is empty

## [4.6.0-beta.48](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.47...4.6.0-beta.48) - 2020-06-18

### Commits

[`d0782c0`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/d0782c0ed420cdbc294b35ac45d02be866f3620d) **Breaking change:** Added `ModelDefinitionAbstract::substituteModelDefinition()` to enabled model definition substitution. See TeamDefinition.js.
BREAKING changed `mnxCoreDataModel` now includes `mnxAgentDataModel`. Possible breaking in application codes.
[`620527a`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/620527a05f7efc6b8b4056a283151d1c18f64f2d) Updated dependencies.

## [4.6.0-beta.47](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.46...4.6.0-beta.47) - 2020-06-16

### Commits

[`1e5d916`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/1e5d9167f9ec4d1acf0877b22f340c34183065ef) Fixed autochangelog script [ignore in changelog]

## [4.6.0-beta.46](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/compare/4.6.0-beta.45...4.6.0-beta.46) - 2020-06-15

### Commits

[`76598a8`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/76598a8102b9524e234b3ed3fc0c9d95cb09b6bd) Added "isNeq" attributes on PropertyFilter.js and LinkFilter.js.
Fixed MnxAcl and MnxProject dataModels
[`4b90ed4`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/4b90ed446edd79c01fd54ec48be2b82a44d29c67) Installed auto-changelog to generated CHANGELOG.md at version time.
[`482c405`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/482c405aea7c770f9b92823750add406ffb79379) Fixed test [ignore in changelog]
[`63f49b5`](https://gitlab.com/mnemotix/synaptix-js/synaptix.js/commit/63f49b5eb1143bde24c10dc40bf16faef7bac7dc) Fixed autochangelog script [ignore in changelog]
