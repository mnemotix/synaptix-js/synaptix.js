Synaptix.js accepts several environment variables

#### Synaptix Legacy
- `USE_GRAPHQL_RELAY` (**0**|1) : Force GraphQL endpoint to fully comply to [Relay]()
- `USE_LEGACY_SYNAPTIX_FORMAT` (**0**|1) : Force AMQP publishers/consumers to comply with legacy Synaptix (v1) format.

#### Network layer

- `RABBITMQ_RPC_TIMEOUT` (5000) : AMQP RPC timeout.

#### RDF Graph Store (namely GraphDB)

- `RDFSTORE_USER` : RDF Graph Store user 
- `RDFSTORE_PWD`  : RDF Graph Store password
- `RDFSTORE_ROOT_URI` : RDF Graph root URI
- `RDFSTORE_REPOSITORY_NAME` : RDF Graph Store default repository name

#### Index Store (namely ElasticSearch)

- `INDEX_DISABLED` (**0**|1): Disable index. Force data to be fetched in graphstores (Triplestore or Property graph)
- `INDEX_FLATTENED` (**0**|1): This option means that no nested field exists in index. The flattened "_" notation is used instead. This is needed to work with GraphDB ElasticSearch connector.

!!! warning
    Breaking change in version `4.4.0-beta.2`. This variable was called `DISABLE_INDEX` in previous versions.
    
- `INDEX_VERSION` (**NATIVE**|PUBLISHER): Choose index AMQP publisher version to comply with index used in Synaptix (LEGACY is V2).
- `INDEX_PREFIX_TYPES_WITH` : Prepend types defined in ModelDefinitions with a prefix and use the result as an index name.
- `INDEX_MASTER_URI` : Index master URI (ES endpoint)
- `INDEX_CLUSTER_USER` : Index cluster user
- `INDEX_CLUSTER_PWD` : Index cluster password

#### SSO

- `OAUTH_REGISTRATION_DISABLED` (**0**|1) : Is SSO account registration disabled ? 
- `OAUTH_REALM` : SSO Realmn  name
- `OAUTH_AUTH_URL` : SSO authentication endpoint.
- `OAUTH_TOKEN_URL` :  SSO token validation endpoint.
- `OAUTH_CERTS_URL` :  SSO certificates endpoint.
- `OAUTH_LOGOUT_URL` : SSO logout endpoint.
- `OAUTH_REALM_CLIENT_ID` : SSO client ID.
- `OAUTH_REALM_CLIENT_SECRET` : SSO client secret.
- `OAUTH_ADMIN_USERNAME` : SSO admin username
- `OAUTH_ADMIN_PASSWORD` : SSO admin password
- `OAUTH_ADMIN_TOKEN_URL` : SSO admin Token endpoint
- `OAUTH_ADMIN_API_URL` :  SSO admin API endpoint

### SECURITY

- `CORS_ORIGIN` (**"*"**) : CORS Origin option
- `CORS_METHODS` (**"GET,HEAD,PUT,PATCH,POST,DELETE"**) : CORS authorized HTTP methods
- `CORS_HEADERS`  : Cors authorized HTTP Headers
- `CORS_CREDENTIALS`  (0|**1**): Cors authorized cookies or other credentials methods

- `CSRF_DSC_PROTECTION_ENABLED` (**0**|1) : Enable CSRF Double Submission Cookie protection.

### Access Control Layer

- `ADMIN_USER_GROUP_ID` : mnx:UserGroup URI that corresponds to Administrators group.

## Locales

- `DEFAULT_LOCALE` (**fr**): Default locale to use with labels.

## Mutations

- `READ_ONLY_MODE_ENABLED` (**0**|1) : Block all mutations and send a I18nError 'READ_ONLY_MODE_ENABLED'.

## Logging.

- `LOG_LEVEL` (**ERROR**|DEBUG|TRACE) : Set the log level. `DEBUG` enables the data fetching requests. `TRACE` adds the data fetching responses.
- `LOG_FORMAT` (**default**|json|logstash) : Set the logger format output.
- `LOG_FILTER` : Only when `LOG_LEVEL=(DEBUG|TRACE)`. Filters the requests/responses trace given a regexp string.
