## Overview

All the previous explained concepts are gathered into a [DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) instance.

The DataModel is the entrypoint for the application to the model description by concatening :

- GraphQL types/mutations/subscriptions
- GraphQL types/mutations/subscriptions resolvers
- Model definitions

## Implementing DataModel

This class can be used like so :

```javascript
import {
  DataModel, mergeResolvers, MnxDatamodels
} from "@mnemotix/synaptix.js"; 
import {Types, TypesResolvers} from "./types";                         // (1)                            
import {Mutations, MutationsResolvers} from "./mutations";             // (2)
import {Subscriptions, SubscriptionsResolvers} from "./subscriptions"; // (3)
import ModelDefinitions  from "../modelDefinitions";                   // (4)

export let dataModel = (new DataModel({
  typeDefs: [].concat(Types, Mutations, Subscriptions),                // (5)
  resolvers: mergeResolvers(
    TypesResolvers, MutationsResolvers, SubscriptionsResolvers
  ),                                                                    // (6)
  modelDefinitions: ModelDefinitions                                    // (7)
}))
  .mergeWithDataModels(
    [MnxDatamodels.mnxCoreDataModel, MnxDatamodels.mnxAgentDataModel]  // (8)
  )
  .addDefaultSchemaTypeDefs();
```

- (1) Project specific GraphQL types and respective resolvers
- (2) Project specific GraphQL mutations and respective resolvers
- (3) Project specific GraphQL subscriptions and respective resolvers
- (4) Project specific model definitions
- (5) List here all the typeDefs (aka: Types/Mutations/Subscriptions in GraphQL format)
- (6) List here all the resolvers linked to typeDefs.
- (7) List here the model definitions used for organizing and helping data resolution.
- (8) Optionnaly merge with other DataModels

UPDATE (2020-03-30) : when using dynamic generation, `typeDefs` and `resolvers` must not be specified anymore on dataModel instantiation.

## Using DataModel in application

[DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) instance exposes two convenient methods :

```javascript
import {dataModel} from './datamodel.js'

let modelDefinitionRegister = dataModel.generateModelDefinitionsRegister();  // (1)

let schema = dataModel.generateExecutableSchema({                            // (2)
 printGraphQLSchemaPath: path.join(__dirname, '../schema.graphql'),          // (3)
});
```

- (1) Generate a model definitions register required by a [DatastoreAdapter](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/adapters/datastore/SynaptixDatastoreAdapter.js)
- (2) Generate an executable schema requited by a GraphQL endpoint. 
- (3) Optionnaly print schema in a file (usefull to debug) 

## Shipped MNX Datamodels

Synaptix.js exposes DataModels describing MNX generic model ontology.

```js
import { MnxDatamodels } from "@mnemotix/synaptix.js"; 

let {
  mnxCommonDataModel,
  mnxGeoDataModel,
  mnxTimeDataModel,
  mnxContributionDataModel,
  mnxAgentDataModel,
  mnxProjectDataModel,
  mnxSkosDataModel,
  mnxAclDataModel,  
} = MnxDatamodels;
```

And because these sub-models are strongly linked together you can simple use a bundled version :

```js
import { MnxDatamodels } from "@mnemotix/synaptix.js"; 

let {
  mnxCoreDataModel  
} = MnxDatamodels;
```

where `#!js mnxCoreDataModel` is `!js (new DataModel()).mergeWithDataModels([mnxCommonDataModel, mnxGeoDataModel, mnxTimeDataModel, mnxContributionDataModel,  mnxSkosDataModel, mnxAclDataModel])`


## Datamode extra features

### GraphQL middlewares

[DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) instance ships the excellent [graphql-middleware](https://github.com/prisma-labs/graphql-middleware) mechanisms.

To add middlewares to the GraphQL schema, [DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) provides a method :

```js
import {dataModel} from './datamodel.js'
import {middlewares} from './middlewares.js'

dataModel.addMiddlewares(middlewares);

let schema = dataModel.generateExecutableSchema({                            
 printGraphQLSchemaPath: path.join(__dirname, '../schema.graphql'),       
});
```

In Synaptix.js several builtin middlewares are used to :

- Perform form input validation (see [Manipulation data](../manipulating_data.md#form-input-validation))
- Securize data (see [Securizing data](../securizing_data.md#graphql-shield-middlewares))

### Specializing mnx:PersonDefinition

By default, using Synaptix.js defines the logged user related person with [PersonDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-agent/definitions/PersonDefinition.js)

That means that the following GraphQL query will use [PersonDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-agent/definitions/PersonDefinition.js) as definition.


```graphql
query{
  me{
    id
    firstName
    lastName
    #...
  } 
}
```

But what if [PersonDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-agent/definitions/PersonDefinition.js) is extends with extra fields in a project or just not used in favour of another definition. Let's say `MySpecializedPersonDefinition`. The preceding GraphQL `me` query won't work anymore.

To make it work, you need to add the following line :

```js
import {dataModel} from './datamodel.js'
import MySpecializedPersonDefinition from './MySpecializedPersonDefinition.js'

dataModel.setModelDefinitionForLoggedUserPerson(MySpecializedPersonDefinition);
```

## Relay or not Relay ?

Synaptix.js can instantiate a GraphQL server compatible with Modern Relay clients. 

That means :

 - The implementation of Relay definition of [global id](https://facebook.github.io/relay/docs/en/graphql-server-specification.html#object-identification)
 - The implementation of Relay definition of [Pagination](https://facebook.github.io/relay/docs/en/graphql-server-specification.html#connections). Actually set be default.
 
To enable that features you just need to set an environment variable to `USE_GRAPHQL_RELAY=1`