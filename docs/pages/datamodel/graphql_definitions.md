
## Generating GraphQL types at runtime

### Overview

Historically, GraphQL schema must be declared explicitly by specifying schema JS files (see previous section). It's now possible to let Synaptix generate type definitions and resolvers for a RDF type, at runtime, and to reduce GraphQL specification to some details.

### Dynamic generation

#### How to enable it

Dynamic generation on a RDF type is enabled if corresponding [ModelDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) implements following method : 

- `ModelDefinitionAbstract::getGraphQLDefinition()` that returns a [GraphQLDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLDefinition.js), that is a class that specifies some parameters for a schema generation (see details below).

By default, ModelDefinitionAbstract does not implement this method to remain retro-compatible with existing code.

So to use dynamic generation for a RDF type, a ModelDefinition must implement following method like so :

```js
  static getGraphQLDefinition(){
    return GraphQLTypeDefinition;
  }
```

Synaptix come with three implementations of GraphQLDefinition : [GraphQLTypeDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLTypeDefinition.js) for an instantiable type, [GraphQLInterfaceDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLInterfaceDefinition.js) for a not instantiable type (and so giving a GraphQL interface), and [GraphQLExtensionDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLExtensionDefinition.js) for extending an existing type. See further for details on extension case.

It's possible to override [GraphQLDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLDefinition.js), or one of the three implementations above, and to return an inherited version if needed.

```js
  static getGraphQLDefinition(){
    return BookGraphQLDefinition;
  }
```

#### What is generated

When a [GraphQLDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLDefinition.js) is defined for a RDF type, its type definitions and resolvers are generated dynamically, following specification described in "graphql schemas" section.

Let's take the example of a Book RDF type, and let's say it's instantiable. If we use dynamic generation with the standard [GraphQLTypeDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLTypeDefinition.js), it will generate following items.

First the GraphQL types and inputs :

- `type Book` : book type, extending parent type if any, and serving literals, labels and links properties ;
- `ìnput BookInput` : book input type, containing literals, labels and links properties (using `<linkName>Input` for the latest) ;
- `input CreateBookInput` : input for create mutation, containing a key `objectInput` of type `BookInput`
- `type CreateBookPayload` : payload returned on a create mutation
- `input UpdateBookInput` : input for update mutation, containing a key `objectInput` of type `BookInput`, and a `objectId` key containing id of the updated item
- `type UpdateBookPayload` : payload returned on a update mutation

!!! info 
    For some details, see previous section or in Synaptix package.

It also generates following properties under GraphQL Query root :

- `books` : access to a collection of items, and can take several arguments
- `book` : access to a particular item, taking an id in argument

And following properties under GraphQL Mutation  root : 

- `createBook` : this mutation takes a `CreateBookInput` input in argument, creates an item in datastore (a book in this example), and returns created object
- `updateBook` : this mutation takes a `UpdateBookInput` input in argument, updates item in datastore (a book in this example), and returns updated object

Corresponding resolvers are also generated, following what it's described in previous section.

!!! info
    If you want to define your Book type as a GraphQL interface, by using the standard [GraphQLInterfaceDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLInterfaceDefinition.js), the main difference with GraphQLTypeDefinition is that create and update mutations won't be generated.

!!! info
    If you want to define an extension of an existing type, for example let's say `Book` is an extension of `Product`, and use [GraphQLExtensionDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLExtensionDefinition.js), your `Product` type will be enhanced with properties defined for `Book` specification, but no queries or mutations will be generated for `Book`, except if you use methods of GraphQLDefinition (see below).

### GraphQLDefinition in details

[GraphQLDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLDefinition.js) is a not instantiable class as you have to implement at least `isTypeGenerated` method. You can use or override as well one of the three implementations introduced above.

Back to our book... It might be necessary to customize GraphQL dynamic generation for adding extra properties, customize mutations, etc... So you can implement GraphQLDefinition, for example by specifying a `BookGraphQLDefinition` and override following methods :

- `#!js GraphQLDefinition::isTypeGenerated()` returns if GraphQL type must be generated

```js
static isTypeGenerated() {
    return true;
}
```

- `#!js GraphQLDefinition::getTypeQuery()` returns specification of the query for getting a single item for type using this definition. See details about [GraphQLTypeQuery](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLTypeQuery.js#L72) below 

```js
static getTypeQuery() {
    return new GraphQLTypeQuery();
}
```

- `#!js GraphQLDefinition::getTypeConnectionQuery()` returns specification of the query for getting a connection of items for type using this definition. See details about [GraphQLTypeConnectionQuery](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLTypeConnectionQuery.js#L110) below 

```js
static getTypeConnectionQuery() {
    return new GraphQLTypeConnectionQuery();
}
```

- `#!js GraphQLDefinition::getCreateMutation()` returns a create mutation for type using this definition. See details about [GraphQLCreateMutation](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLMutation.js#L71) below 

```js
static getCreateMutation() {
    return new GraphQLCreateMutation();
}
```

- `#!js GraphQLDefinition::getUpdateMutation()` returns a update mutation for type using this definition. See details about [GraphQLUpdateMutation](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLMutation.js#L129) below 

```js
static getUpdateMutation() {
    return new GraphQLUpdateMutation();
}
```

- `#!js GraphQLDefinition::getRemoveMutation()` returns a remove mutation. By default, you can also use Synaptix `removeObject` mutation, which takes id of the object as argument, and is agnostic about type of the object to remove.

```js
static getRemoveMutation() {
    return null;
}
```

- `#!js GraphQLDefinition::getExtraQueries()` returns a list of GraphQLQuery objects, corresponding to queries to add to GraphQL schema.

```js
static getExtraQueries() {
    return [];
}
```

- `#!js GraphQLDefinition::getExtraMutations()` returns a list of GraphQLMutation objects, corresponding to mutations to add to GraphQL schema.

```js
static getExtraMutations() {
    return [];
}
```

- `#!js GraphQLDefinition::getOverridenProperties()` returns a list of properties (GraphQLProperty) to add to GraphQL type. This method is here to allow to override GraphQL specification for some properties existing in related type. An error is thrown if property name defined, does not match property of the related type (and so better use `getExtraProperties`). See details about [GraphQLProperty](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLProperty.js) below

```js
static getOverridenProperties() {
    return [];
}
```

- `#!js GraphQLDefinition::getExtraProperties()` returns a list of properties (GraphQLProperty) to add to GraphQL type. An error is thrown if you define a property with a name matching property of a model which uses this definition (and so better use `getOverridenProperties`). See details about [GraphQLProperty](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLProperty.js) below

```js
static getExtraProperties() {
    return [];
}
```

- `#!js GraphQLDefinition::getExtraGraphQLCode()` returns some GraphQL code to add to GraphQL schema. It can be used, for example, for defining a GraphQL enum or extending a type with specific property, etc...

```js
static getExtraGraphQLCode() {
    return ``;
}
```

- `#!js GraphQLDefinition::getExtraResolvers()` returns an object implementing resolvers for extra GraphQL pieces defined in `GraphQLDefinition:getExtraGraphQLCode`

```js
static getExtraResolvers() {
    return null;
}
```

- `#!js GraphQLDefinition::getRules()` returns an object specifying access rules for a type and its properties. Rules uses behavior defined in [graphql-shield](https://github.com/maticzav/graphql-shield). Object returned must match GraphQL schema, so for our `Book` type :
  - Query: must contain rules corresponding to queries defined in same graphql definition
  - Mutation: must contain rules corresponding to mutations defined in same graphql definition
  - Book: must contain rules for properties of the `Book` type
  
  An error is thrown if a key in rules does not match names of queries, mutations and properties defined for the related type.

```js
static getRules() {
    return {};
}
```

### GraphQL properties

GraphQL properties returned in `GraphQLDefinition::getOverridenProperties` or `GraphQLDefinition::getExtraProperties` must instanciate [GraphQLProperty](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLProperty.js) class.

A GraphQLProperty must be instanciated with following arguments:

- `name` : name of the property
- `description` : description of property (optional)
- `isConnection` : is property a connection or not (false by default)
- `type` : type of property (or null if not a readable property)
- `inputType` : type of input property (or null if read-only property)
- `typeResolver` : resolver function for this property

About `typeResolver`, it takes the following specification :

```js
    //...
    typeResolver: (object, args, synaptixSession) => { /* ... */ }
    //...
```

### GraphQL queries

Synaptix implements some query objects, which all extend a [GraphQLQuery](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLQuery.js). If implementation of type or connection query does not cover your need, it's possible to extend GraphQLQuery object.

#### Type query

A standard type query is implemented by [GraphQLTypeQuery](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLTypeQuery.js#L72)

It takes following arguments :

- `description` : description about type query

#### Type connection query

A standard connection query is implemented by [GraphQLTypeConnectionQuery](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLTypeConnectionQuery.js#L110)

It takes following arguments :

- `description` : description about type query
- `extraArgs` : extra arguments for graphQL field, which will be added to filtering, sorting and connection default arguments

### GraphQL mutations

Synaptix implements some mutation objects, which all extend a [GraphQLMutation](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLMutation.js). If implementation of create, update or remove mutations does not cover your need, it's possible to extend GraphQLMutation object.

#### Create mutation

A standard create mutation is implemented by [GraphQLCreateMutation](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLMutation.js#L71)

It takes following arguments :

- `description` : description about create mutation
- `extraInputArgs` : arguments to add to existing ones (by default: `objectInput`), as a GraphQL string format
- `edgeType` : type of the edge returned
- `resolver` : define a resolver for this create mutation, using extra arguments and returning created object

For example, if we want to create a book and specify id of the author in input, we could implement mutation like this:

```js
//...
static getCreateMutation() {
    return new GraphQLCreateMutation({
        description: 'Some description about this mutation',
        extraInputArgs: `
            authorId: ID!
        `,
        resolver: async ({ objectInput, authorId}, synaptixSession) => { /* ...*/ }
    })
}
//...
```

#### Update mutation

A standard update mutation is also implemented by [GraphQLUpdateMutation](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLMutation.js#L119)

It takes following arguments :

- `description` : description about update mutation
- `resolver` : define a resolver for this update mutation

For example, if we want to customize update of a book, we could implement mutation like this:

```js
//...
static getUpdateMutation() {
    return new GraphQLUpdateMutation({
        description: 'Some description about this update mutation',
        resolver: async ({ objectId, objectInput}, synaptixSession) => { /* ...*/ }
    })
}
//...
```

#### Remove mutation

A standard remove mutation is also implemented by [GraphQLRemoveMutation](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLMutation.js#L167)

It takes following arguments :

- `resolver` : define a resolver for this remove mutation

For example, if we want to customize removal of a book, we could implement mutation like this:

```js
//...
static getRemoveMutation() {
    return new GraphQLRemoveMutation({
        resolver: async ({ objectId, permanent}, synaptixSession) => { /* ...*/ }
    })
}
//...
```