## Installation

Install NPM dependencies running:

```
yarn install
```

## Dev linking

To develop an application alongside Synaptix.js as dependency. Just link it with yarn :

Synaptix.js side :

```bash
yarn link
yarn build-dev
```

`build-dev` command will lauch the CJS transpilation and watch for file changes.

Application side :

```bash
yarn link @mnemotix/synaptix.js
```


## Test

### Unit tests

- `yarn run test` will run the unit test suite.
- `yarn run test:debug` will run the unit test suite in debugger mode and open chrome developper tools to inspect it. You can 
  write `debugger;` statements into your code to set breakpoints.


### Integration tests

- `yarn run test:integration` will runs the integration test suites.

## Publishing

Publish on NPM just running :

```
yarn version
```

Yarn will ask you the version you want to assign, then change it in `package.json`, create a related tag version on git. That will trigger a new Gitlab-CI pipeline that finally will release on NPM a new package **if tests passed**.